<nav class="navbar navbar-expand navbar-dark fixed-top flex-nowrap p-0 shadow navbarPadding">
    <?php
	    if(!empty($_SESSION))
	    {      
    ?>
            <a href="indexDashboard.php" class="mr-auto">
    <?php
        }
    ?>
        <img src="img/logo.png" height="64px" width="225px"/>
        <?php
	    if(!empty($_SESSION))
	    {      
    ?>
            </a>
    <?php
        }
    ?>
    <?php
	if(!empty($_SESSION))
	{
    ?>

        
        <div class="navbarButtons">
            <button class="btn btn-danger mr-2">
                <a class="support" href="#">Support</a>
            </button>
            <button class="btn btn-light mr-5">
                <a class=" logout" href="indexLogOut.php">Log Out</a>
            </button>
        </div>
    <?php
    }
    ?>
</nav>
