<?php
require 'generalFunction.php';
$conn = connDB();

$fromPage = $_POST['fromPage'];
$condition = $_POST['condition'];
$pageNo = $_POST['pageNo'];
$filter = $_POST['filter'];
$searchWord = $_POST['searchWord'];

if($filter == null)
{
    $filter= 1;
}
if($pageNo == null)
{
    $filter= 1;
}
if($searchWord == null)
{
    $searchWord = "";
}
$sqlPageNo = 0;
$sqlPageNo = ($pageNo - 1) * 10;

if($filter == 1)
{
    if($fromPage == 9)
    {
        $orderBy = "dtmDateCreated";
    }
    if($fromPage == 10)
    {
        $orderBy = "dsfDateCreated";
    }
}

$sql = "";
$sql2 = "";

if($fromPage == 9)
{
    $sql .= " SELECT * FROM (((((((dtmlist
    INNER JOIN company ON dtmlist.companyID_FK = company.companyID_PK)
    INNER JOIN driver ON dtmlist.driverID_FK = driver.driverID_PK)
    INNER JOIN pointzone ON dtmlist.dtmDestinationPointID_FK = pointzone.pointzoneID_PK)
    INNER JOIN trucks ON dtmlist.truckID_FK = trucks.truckID_PK)
    INNER JOIN user ON dtmlist.dtmPlannerID_FK = user.userID_PK)
    INNER JOIN zones ON dtmlist.dtmDestinationZoneID_FK = zones.zonesID_PK)
    INNER JOIN costcenter ON dtmlist.costCenterID_FK = costcenter.costCenterID_PK) 
    WHERE dtmlist.dtmIsPaidDriverServiceFee = 0 ";

    $sql2 .= " SELECT COUNT(*) AS total2 FROM (((((((dtmlist
    INNER JOIN company ON dtmlist.companyID_FK = company.companyID_PK)
    INNER JOIN driver ON dtmlist.driverID_FK = driver.driverID_PK)
    INNER JOIN pointzone ON dtmlist.dtmDestinationPointID_FK = pointzone.pointzoneID_PK)
    INNER JOIN trucks ON dtmlist.truckID_FK = trucks.truckID_PK)
    INNER JOIN user ON dtmlist.dtmPlannerID_FK = user.userID_PK)
    INNER JOIN zones ON dtmlist.dtmDestinationZoneID_FK = zones.zonesID_PK)
    INNER JOIN costcenter ON dtmlist.costCenterID_FK = costcenter.costCenterID_PK) 
    WHERE dtmlist.dtmIsPaidDriverServiceFee = 0 ";
}
if($fromPage == 10)
{
    $sql .= " SELECT * FROM driverservicefee ";
    $sql2 .= " SELECT COUNT(*) as total2 FROM driverservicefee ";
}


// if($searchWord != null && $searchWord != "")
// {
//     if($fromPage == 9)
//     {
//         $sql .= " AND driverName LIKE '%".$searchWord."%' OR driverNickName LIKE '%".$searchWord."%' OR driverICno LIKE '%".$searchWord."%' OR driverPhoneNo LIKE '%".$searchWord."%' ";
//         $sql2 .= " AND driverName LIKE '%".$searchWord."%' OR driverNickName LIKE '%".$searchWord."%' OR driverICno LIKE '%".$searchWord."%' OR driverPhoneNo LIKE '%".$searchWord."%'";
//     }
//     if($fromPage == 10)
//     {
//         $sql .= " AND driverName LIKE '%".$searchWord."%' OR driverNickName LIKE '%".$searchWord."%' OR driverICno LIKE '%".$searchWord."%' OR driverPhoneNo LIKE '%".$searchWord."%' ";
//         $sql2 .= " AND driverName LIKE '%".$searchWord."%' OR driverNickName LIKE '%".$searchWord."%' OR driverICno LIKE '%".$searchWord."%' OR driverPhoneNo LIKE '%".$searchWord."%'";
//     }
// }


if ($orderBy != "") 
{
    if($filter == 1)
    {
        $sql .= " ORDER BY ".$orderBy." DESC ";
        $sql2 .= " ORDER BY ".$orderBy." DESC ";
    }
    // else
    // {
    //     $sql .= " ORDER BY ".$orderBy." ASC ";
    //     $sql2 .= " ORDER BY ".$orderBy." ASC ";
    // }
}

$sql .=" LIMIT ".$sqlPageNo.",10 ";

if($condition == 1)
{
    if($fromPage == 9)
    {
        $initialSql = " SELECT COUNT(*) as total FROM (((((((dtmlist
        INNER JOIN company ON dtmlist.companyID_FK = company.companyID_PK)
        INNER JOIN driver ON dtmlist.driverID_FK = driver.driverID_PK)
        INNER JOIN pointzone ON dtmlist.dtmDestinationPointID_FK = pointzone.pointzoneID_PK)
        INNER JOIN trucks ON dtmlist.truckID_FK = trucks.truckID_PK)
        INNER JOIN user ON dtmlist.dtmPlannerID_FK = user.userID_PK)
        INNER JOIN zones ON dtmlist.dtmDestinationZoneID_FK = zones.zonesID_PK)
        INNER JOIN costcenter ON dtmlist.costCenterID_FK = costcenter.costCenterID_PK) 
        WHERE dtmlist.dtmIsPaidDriverServiceFee = 0 ";
    }
    if($fromPage == 10)
    {
        $initialSql = "SELECT COUNT(*) as total from driverservicefee ";
    }

    $result = mysqli_query($conn,$initialSql);
    $data = mysqli_fetch_assoc($result);
    $no_of_pages = 0;
    $no_of_pages = ceil($data['total'] / 10);
}
else
{
    $result2 = mysqli_query($conn,$sql2);
    $dataCount = mysqli_fetch_assoc($result2);
    $no_of_pages = 0;
    $no_of_pages = ceil($dataCount['total2'] / 10);
    
}

$querylisting = mysqli_query($conn,$sql);

generateDeleteModal($fromPage);
generateConfirmationDeleteModal($fromPage);
?>
<script>$("#pagination"+<?php echo $fromPage;?>+" option").remove();</script>
<table class="table table-sm table-hovered  dtmTableNoWrap table-bordered table-striped table-responsive-xl removebottommargin"style="text-align:center;">
    <thead>
        <?php 
            if($fromPage == 9)
            {
                ?>
                <tr>
                    <th class="adminTableWidthTD" rowspan="2">Selection</th>
                    <th rowspan="2">Driver Name</th>
                    <th rowspan="2">Company</th>
                    <th colspan="2">Origin/From</th>
                    <th colspan="2">Destination/To</th>
                    <th colspan="3">Quantity</th>
                    
                </tr>
                <?php
            }
        ?>
        <tr>
        <?php 
            if($fromPage == 9)
            {
                ?>
                    <th>Origin Point</th>
                    <th>Origin Zone</th>
                    <th>Destination Point</th>
                    <th>Destination Zone</th>
                    <th>Cartons</th>
                    <th>Pallets</th>
                    <th>Cages</th>
                <?php
            }
            if($fromPage == 10)
            {
                ?>
                    <th class="adminTableWidthTD">Selection</th>
                    <th>dsfDateCreated</th>
                    <th>dsfDateUpdated</th>
                    <th>dsfDONumber</th>
                    <th>DTMID_FK</th>
                <?php
            }
        ?>
        </tr>
  </thead>
  <tbody>
    <?php 
        if (mysqli_num_rows($querylisting) > 0) 
        {
            while($row = mysqli_fetch_array($querylisting))
            {
    ?>
        <tr>
            <?php 
                if($fromPage == 9)
                {  
            ?>
                <td class="adminTableWidthTD">
                    <div class="adminAlignOptionInline">
                        <form action="dsfAdd.php" method="POST" class="adminformEdit">
                            <input type="hidden" name="tableType" value="<?php echo $fromPage;?>">
                            <input type="hidden" name="driverName" value="<?php echo $row['driverName'];?>">
                            <input type="hidden" name="driverIC" value="<?php echo $row['driverICno'];?>">
                            <input type="hidden" name="driverNickname" value="<?php echo $row['driverNickName'];?>">
                            <button class="btn btn-success edtOpt" name="add" value="<?php echo $row['dtmID_PK'];?>">Add Service Fee</button>
                        </form>
                    </div>
                </td>
                <td>
                    <?php 
                        echo $row['driverName'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['companyName'];
                    ?>
                </td>
                <td>
                <?php 
                        $costCenterDisplay = "SELECT * FROM pointzone WHERE pointzoneID_PK = ".$row['dtmOriginPointID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['pointzonePlaceName'];
                            }
                        }
                    ?>
                </td>
                <td>
                     <?php 
                        $costCenterDisplay = "SELECT * FROM zones WHERE zonesID_PK = ".$row['dtmOriginZoneID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['zonesName'];
                            }
                        }
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['pointzonePlaceName'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['zonesName'];
                    ?>
                </td>
               
                <td>
                    <?php 
                        echo $row['dtmQuantityCarton'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['dtmQuantityPalllets'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['dtmQuantityCages'];
                    ?>
                </td>
            <?php
                }
            ?>
            <?php 
                if($fromPage == 10)
                {  
            ?>
                <td class="adminTableWidthTD">
                    <div class="adminAlignOptionInline">
                        <form action="dsfAdd.php" method="POST" class="adminformEdit">
                            <input type="hidden" name="tableType" value="<?php echo $fromPage;?>">
                            <button class="btn btn-warning edtOpt" name="edit" value="<?php echo $row['dtmID_PK'];?>">Edit Service Fee</button>
                        </form>
                    </div>
                </td>
                <td>
                    <?php 
                        echo $row['dsfDateCreated'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['dsfDateUpdated'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['dsfDONumber'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['dtmID_FK'];
                    ?>
                </td>
            <?php
                }
            ?>
        </tr>
    <?php 
            }
        }
        else
        {
            ?>
            <tr>
                <td colspan="6" style="text-align:center;">No Records Found</td>
            </tr>
            <?php
        }
    ?>
  </tbody>
</table>
<?php
    if($condition == 1)
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$data['total']);
    }
    else
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$dataCount['total2']);
    }
?>
 