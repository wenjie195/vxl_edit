<?php
$fromDate = $_POST['fromDate'];
$toDate = $_POST['toDate'];

date_default_timezone_set("Asia/Kuala_Lumpur");
$todayDate = date("Ymd_his");
$filename = 'filename="DTM_'.$fromDate.'_'.$toDate.'_List.xlsx"';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; '.$filename);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;

define('NUMBER_OF_COLUMNS', 37); 
require 'generalFunction.php';
require 'phpexcel/vendor/autoload.php';
ob_end_clean();


$conn = connDB();
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

function putWidth($alphabet,$widthsize,$sheet)
{
     for($x='A'; $x != $alphabet; $x++)
     {
          $sheet->getColumnDimension($x)->setWidth($widthsize);
     }
}
function putNewLine($before,$after,$line,$sheet)
{
     for($x=$before; $x != $after; $x++)
     {
          $sheet->getStyle($x.$line)->getAlignment()->setWrapText(true);
     }
}
function putCenter($alphabet,$line,$position,$sheet)
{
     for($x='A'; $x != $alphabet; $x++)
     {
          $sheet->getStyle($x.$line)->getAlignment()->setHorizontal($position);
     }
}
function getDatePHPWithTime($dateVar,$type) 
{
    date_default_timezone_set('Asia/Kuala_Lumpur');
    $date = date("Y-m-d", strtotime($dateVar));
    $time = date("H-i-s", strtotime("23:59:59"));

    return $date." ".$time;
}
function getPlaceZone($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT zonesName FROM zones WHERE zonesID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['zonesName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
     
function getPlace($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['pointzonePlaceName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getDriver($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT driverName FROM driver WHERE driverID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['driverName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}

$fromDateFormatted = getDatePHP($fromDate);
$toDateFormatted = getDatePHPWithTime($toDate,0);

$sheet->setCellValue('A1', 'Bil No(DTM NO)');
$sheet->setCellValue('B1', 'DGF REF');
$sheet->setCellValue('C1', 'Agent');
$sheet->setCellValue('D1', 'Cost Centre');
$sheet->setCellValue('E1', 'Booking Date');
$sheet->setCellValue('F1', 'Booking Time');
$sheet->setCellValue('G1', 'Pickup Date');
$sheet->setCellValue('H1', 'Pickup Time');
$sheet->setCellValue('I1', 'Pickup Point');
$sheet->setCellValue('J1', 'Pickup Zone');

$sheet->setCellValue('K1', 'Delivery Point');
$sheet->setCellValue('L1', 'Delivery Zone');
$sheet->setCellValue('M1', 'Truck Plate No');
$sheet->setCellValue('N1', 'Load Cap');
$sheet->setCellValue('O1', 'Consol Status');
$sheet->setCellValue('P1', 'Driver 1 Name');
$sheet->setCellValue('Q1', 'Driver 2 Name');
$sheet->setCellValue('R1', 'Urgent Memo');
$sheet->setCellValue('S1', 'F');
$sheet->setCellValue('T1', 'Dock Time');

$sheet->setCellValue('U1', 'Complete Loading Time');
$sheet->setCellValue('V1', 'Departure Time From Post');
$sheet->setCellValue('W1', 'Arrival Time At Post');
$sheet->setCellValue('X1', 'Dock Time');
$sheet->setCellValue('Y1', 'Complete Unload Time');
$sheet->setCellValue('Z1', 'Depart Time From Post');
$sheet->setCellValue('AA1', 'Pick-up vs Arrival');
$sheet->setCellValue('AB1', 'Arrival vs Dock');
$sheet->setCellValue('AC1', 'Dock vs Complete Loading');
$sheet->setCellValue('AD1', 'Arrival vs Complete Loading');

$sheet->setCellValue('AE1', 'Complete Loading vs Departure');
$sheet->setCellValue('AF1', 'Departure vs Arrival');
$sheet->setCellValue('AG1', 'Arrival vs Dock');
$sheet->setCellValue('AH1', 'Dock vs Complete Unloading');
$sheet->setCellValue('AI1', 'Arrival vs Complete Unloading');
$sheet->setCellValue('AJ1', 'Complete Unloading vs Departure');
$sheet->setCellValue('AK1', 'TPT Complete Trip');
$sheet->setCellValue('AL1', 'TPT for All');
$sheet->setCellValue('AM1', 'Cartons');
$sheet->setCellValue('AN1', 'Pallets');

$sheet->setCellValue('AO1', 'Cages/Crates');
$sheet->setCellValue('AP1', 'Trip On Time');
$sheet->setCellValue('AQ1', 'Delay Reasons');
$sheet->setCellValue('AR1', 'Planner');
$sheet->setCellValue('AS1', 'Request By');
$sheet->setCellValue('AT1', 'Remarks');

putWidth('AT',15,$sheet);
for($x='S'; $x != 'AA'; $x++)
{
     $sheet->getColumnDimension($x)->setWidth(25);
}
$sheet->getColumnDimension('P')->setWidth(45);
$sheet->getColumnDimension('Q')->setWidth(45);
$sheet->getColumnDimension('AT')->setWidth(35);

putCenter('AT',1,"center",$sheet);

$sqlo = " SELECT * FROM (((((((dtmlist
INNER JOIN company ON dtmlist.companyID_FK = company.companyID_PK)
INNER JOIN driver ON dtmlist.driverID_FK = driver.driverID_PK)
INNER JOIN pointzone ON dtmlist.dtmDestinationPointID_FK = pointzone.pointzoneID_PK)
INNER JOIN trucks ON dtmlist.truckID_FK = trucks.truckID_PK)
INNER JOIN user ON dtmlist.dtmPlannerID_FK = user.userID_PK)
INNER JOIN zones ON dtmlist.dtmDestinationZoneID_FK = zones.zonesID_PK)
INNER JOIN costcenter ON dtmlist.costCenterID_FK = costcenter.costCenterID_PK)
WHERE dtmlist.dtmPickupDate BETWEEN '$fromDateFormatted' AND '$toDateFormatted'
AND dtmIsFinished = 1 ORDER BY dtmlist.dtmPickupDate DESC ";
     
$result = mysqli_query($conn,$sqlo);
$bilangan = 1;

if (mysqli_num_rows($result) > 0) 
{
     $line = 2;
     while($row = mysqli_fetch_array($result))
     {
          $isConsolWording = "";
          $secondDriver = "";
          $urgentMemo = "no";
          $dtmTripOnTime = "no";
          $dtmDelayReasons = "-";
          $remarksDTM = "->".strtoupper($row['dtmRemarks']);

          if($row['isConsol'] == 1)
          {
               $isConsolWording = "CONSOL";
          }
          if($row['driver2ID_FK'])
          {
               $secondDriver = getDriver($row['driver2ID_FK']);
          }
          if($row['dtmDelayReasons'])
          {
               $dtmDelayReasons = $row['dtmDelayReasons'];
          }
          if($row['dtmUrgentMemo'] == 1)
          {
               $urgentMemo = "yes";
          }
          if ($row['dtmTripOnTime'] == 1)
          {
               $dtmTripOnTime = "yes";
          }

          $originF = "".date("G:i",strtotime($row['dtmOriginF']));
          $originDockTime = "".date("G:i",strtotime($row['dtmOriginDockTime']));
          $originCompleteLoadingTime = "".date("G:i",strtotime($row['dtmOriginCompleteLoadingTime']));
          $originDepartureTimeFromPost = "".date("G:i",strtotime($row['dtmOriginDepartureTimeFromPost']));

          $destinationArrivalTimeAtPost = "".date("G:i",strtotime($row['dtmDestinationArrivalTimeAtPost']));
          $destinationDockTime = "".date("G:i",strtotime($row['dtmDestinationDockTime']));
          $destinationCompleteUnloadTime = "".date("G:i",strtotime($row['dtmDestinationCompleteUnloadTime']));
          $destinationDepartTimeFromPost = "".date("G:i",strtotime($row['dtmDestinationDepartTimeFromPost']));


          if($row['lockupf'])
          {
              $originF.="\nNext Day : ".date("d M Y",strtotime($row['lockupf']));
          }
          if($row['lockupdockTime'])
          {
              $originDockTime.="\nNext Day : ".date("d M Y",strtotime($row['lockupdockTime']));
          }
          if($row['lockupcompleteLoadingTime'])
          {
               $originCompleteLoadingTime.="\nNext Day : ".date("d M Y",strtotime($row['lockupcompleteLoadingTime']));
          }
          if($row['lockupdepartureTimeFromPost'])
          {
               $originDepartureTimeFromPost.="\nNext Day : ".date("d M Y",strtotime($row['lockupdepartureTimeFromPost']));
          }
          if($row['lockuparrivalTimeAtPost'])
          {
               $destinationArrivalTimeAtPost.="\nNext Day : ".date("d M Y",strtotime($row['lockuparrivalTimeAtPost']));
          }
          if($row['lockupdestinationDockTime'])
          {
               $destinationDockTime.="\nNext Day : ".date("d M Y",strtotime($row['lockupdestinationDockTime']));
          }
          if($row['lockupcompleteUnloadTime'])
          {
               $destinationCompleteUnloadTime.="\nNext Day : ".date("d M Y",strtotime($row['lockupcompleteUnloadTime']));
          }
          if($row['lockupdepartTimeFromPost'])
          {
               $destinationDepartTimeFromPost.="\nNext Day : ".date("d M Y",strtotime($row['lockupdepartTimeFromPost']));
          }

          if( $row['lockupf'] || $row['lockupdockTime'] || $row['lockupcompleteLoadingTime'] || $row['lockupdepartureTimeFromPost'] ||
          $row['lockuparrivalTimeAtPost'] || $row['lockupdestinationDockTime'] || $row['lockupcompleteUnloadTime'] || $row['lockupdepartTimeFromPost'])
          {
               $remarksDTM .="\n->Lockup/Detention";
          }

          $sheet->setCellValue('A'.$line, $bilangan.'('.$row['dtmID_PK'].')');
          $sheet->setCellValue('B'.$line, $row['dtmDGF']);
          $sheet->setCellValue('C'.$line, $row['companyName']);
          $sheet->setCellValue('D'.$line, $row['costCenterName']);
          $sheet->setCellValue('E'.$line, date("d M Y",strtotime($row['dtmBookingDate'])));
          $sheet->setCellValue('F'.$line, date("G:i",strtotime($row['dtmBookingTime'])));
          $sheet->setCellValue('G'.$line, date("d M Y",strtotime($row['dtmPickupDate'])));
          $sheet->setCellValue('H'.$line, date("G:i",strtotime($row['dtmPickupTime'])));
          $sheet->setCellValue('I'.$line, getPlace($row['dtmOriginPointID_FK']));
          $sheet->setCellValue('J'.$line, getPlaceZone($row['dtmOriginZoneID_FK']));

          $sheet->setCellValue('K'.$line, $row['pointzonePlaceName']);
          $sheet->setCellValue('L'.$line, $row['zonesName']);
          $sheet->setCellValue('M'.$line, $row['truckPlateNo']);
          $sheet->setCellValue('N'.$line, $row['loadCap']);
          $sheet->setCellValue('O'.$line, $isConsolWording);
          $sheet->setCellValue('P'.$line, $row['driverName']);
          $sheet->setCellValue('Q'.$line, $secondDriver);
          $sheet->setCellValue('R'.$line, $urgentMemo);
          $sheet->setCellValue('S'.$line, $originF);
          $sheet->setCellValue('T'.$line, $originDockTime);

          $sheet->setCellValue('U'.$line, $originCompleteLoadingTime);
          $sheet->setCellValue('V'.$line, $originDepartureTimeFromPost);
          $sheet->setCellValue('W'.$line, $destinationArrivalTimeAtPost);
          $sheet->setCellValue('X'.$line, $destinationDockTime);
          $sheet->setCellValue('Y'.$line, $destinationCompleteUnloadTime);
          $sheet->setCellValue('Z'.$line, $destinationDepartTimeFromPost);
          $sheet->setCellValue('AA'.$line, "0:00");
          $sheet->setCellValue('AB'.$line, "0:00");
          $sheet->setCellValue('AC'.$line, compareValue($row['dtmOriginCompleteLoadingTime'],$row['dtmOriginDockTime'],$row['lockupcompleteLoadingTime'],$row['lockupdockTime'],$row['dtmPickupDate']));
          $sheet->setCellValue('AD'.$line, compareValue($row['dtmOriginCompleteLoadingTime'],$row['dtmOriginF'],$row['lockupcompleteLoadingTime'],$row['lockupf'],$row['dtmPickupDate']));

          $sheet->setCellValue('AE'.$line, compareValue($row['dtmOriginDepartureTimeFromPost'],$row['dtmOriginCompleteLoadingTime'],$row['lockupdepartureTimeFromPost'],$row['lockupcompleteLoadingTime'],$row['dtmPickupDate']));
          $sheet->setCellValue('AF'.$line, compareValue($row['dtmDestinationArrivalTimeAtPost'],$row['dtmOriginDepartureTimeFromPost'],$row['lockuparrivalTimeAtPost'],$row['lockupdepartureTimeFromPost'],$row['dtmPickupDate']));
          $sheet->setCellValue('AG'.$line, compareValue($row['dtmDestinationDockTime'],$row['dtmDestinationArrivalTimeAtPost'],$row['lockupdestinationDockTime'],$row['lockuparrivalTimeAtPost'],$row['dtmPickupDate']));
          $sheet->setCellValue('AH'.$line, compareValue($row['dtmDestinationCompleteUnloadTime'],$row['dtmDestinationDockTime'],$row['lockupcompleteUnloadTime'],$row['lockupdestinationDockTime'],$row['dtmPickupDate']));
          $sheet->setCellValue('AI'.$line, compareValue($row['dtmDestinationCompleteUnloadTime'],$row['dtmDestinationArrivalTimeAtPost'],$row['lockupcompleteUnloadTime'],$row['lockuparrivalTimeAtPost'],$row['dtmPickupDate']));
          $sheet->setCellValue('AJ'.$line, compareValue($row['dtmDestinationDepartTimeFromPost'],$row['dtmDestinationCompleteUnloadTime'],$row['lockupdepartTimeFromPost'],$row['lockupcompleteUnloadTime'],$row['dtmPickupDate']));
          $sheet->setCellValue('AK'.$line, compareValue($row['dtmDestinationDepartTimeFromPost'],$row['dtmOriginF'],$row['lockupdepartTimeFromPost'],$row['lockupf'],$row['dtmPickupDate']));
          $sheet->setCellValue('AL'.$line, compareValue($row['dtmDestinationDepartTimeFromPost'],$row['dtmOriginF'],$row['lockupdepartTimeFromPost'],$row['lockupf'],$row['dtmPickupDate']));
          $sheet->setCellValue('AM'.$line, $row['dtmQuantityCarton']);
          $sheet->setCellValue('AN'.$line, $row['dtmQuantityPalllets']);

          $sheet->setCellValue('AO'.$line, $row['dtmQuantityCages']);
          $sheet->setCellValue('AP'.$line, $dtmTripOnTime);
          $sheet->setCellValue('AQ'.$line, $dtmDelayReasons);
          $sheet->setCellValue('AR'.$line, $row['userName']);
          $sheet->setCellValue('AS'.$line, $row['dtmRequestBy']);
          $sheet->setCellValue('AT'.$line, $remarksDTM);

          putCenter('AT',$line,"center",$sheet);
          putNewLine('S','AA',$line,$sheet);
          putNewLine('AT','AU',$line,$sheet);

          $bilangan++;
          $line++;
     }
}

$writer = new Xlsx($spreadsheet);
$writer->save('php://output');

?>