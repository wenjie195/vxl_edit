<?php
require 'generalFunction.php';
$conn = connDB();

$fromPage = $_POST['fromPage'];
$condition = $_POST['condition'];
$pageNo = $_POST['pageNo'];
$filter = $_POST['filter'];
$searchWord = $_POST['searchWord'];

// echo $fromPage;
// echo $condition;
// echo $pageNo;
// echo $filter;
// echo " =".$searchWord."=";

if($filter == null)
{
    $filter= 1;
}
if($pageNo == null)
{
    $filter= 1;
}
if($searchWord == null)
{
    $searchWord = "";
}

$sqlPageNo = 0;
$sqlPageNo = ($pageNo - 1) * 10;

if($filter == 1)
{
    if($fromPage == 31)
    {
        $orderBy = "origin";
    }
}
if($filter == 2)
{
    if($fromPage == 31)
    {
        $orderBy = "destination";
    }
}


$sql = "";
$sql2 = "";

if($fromPage == 31)
{
    $sql .= " SELECT * FROM servicefeeratesplace WHERE showThis = 1 ";
    $sql2 .= " SELECT COUNT(*) as total2 FROM servicefeeratesplace WHERE showThis = 1 ";
}


if($searchWord != null && $searchWord != "")
{
    if($fromPage == 31)
    {
        $sql .= " AND companyName LIKE '%".$searchWord."%' ";
        $sql2 .= " AND companyName LIKE '%".$searchWord."%' ";
    }
}

if ($orderBy != "") 
{
    if($filter == 1)
    {
        $sql .= " ORDER BY ".$orderBy." DESC ";
        $sql2 .= " ORDER BY ".$orderBy." DESC ";
    }
    else if($filter == 2)
    {
        $sql .= " ORDER BY ".$orderBy." DESC ";
        $sql2 .= " ORDER BY ".$orderBy." DESC ";
    }
}

$sql .=" LIMIT ".$sqlPageNo.",10 ";

if($condition == 1)
{
    $initialSql = "SELECT COUNT(*) as total from servicefeeratesplace  WHERE showThis = 1 ";


    $result = mysqli_query($conn,$initialSql);
    $data = mysqli_fetch_assoc($result);
    $no_of_pages = 0;
    $no_of_pages = ceil($data['total'] / 10);
}
else
{
    $result2 = mysqli_query($conn,$sql2);
    $dataCount = mysqli_fetch_assoc($result2);
    $no_of_pages = 0;
    $no_of_pages = ceil($dataCount['total2'] / 10);
    
}

$querylisting = mysqli_query($conn,$sql);

generateDeleteModal($fromPage);
generateConfirmationDeleteModal($fromPage);
?>
<script>$("#pagination"+<?php echo $fromPage;?>+" option").remove();</script>
<table class="table table-sm dtmTableNoWrap table-hovered table-striped table-responsive-xl removebottommargin">
    <thead>
        <tr>
        <?php 
            if($fromPage == 31)
            {
                ?>
                    <th class="adminTableWidthTD">Selection</th>
                    <th >From Zones</th>
                    <th >To Zones</th>
                    <th >Load Cap</th>
                    <th >No of driver</th>
                    <th >Service Fee</th>
                <?php
            }
        ?>
        </tr>
  </thead>
  <tbody>
    <?php 
        if (mysqli_num_rows($querylisting) > 0) 
        {
            while($row = mysqli_fetch_array($querylisting))
            {
    ?>
    <tr>
        <?php 

            
            if($fromPage == 31)
            {
                
                ?>
                    <td class="adminTableWidthTD">
                        <div class="adminAlignOptionInline">
                            <!-- <form action="CDEdit.php" method="POST" class="adminformEdit">
                                <!-- <input type="hidden" name="tableType" value="<?php// echo $fromPage;?>"> -->
                                <!-- <button class="btn btn-warning edtOpt" value="<?php// echo $row['id'];?>" name="edit">Update</button> -->
                            <!-- </form> --> 
                            <button class="btn btn-danger edtOpt" value="<?php echo $row['id'];?>" onclick="deleteAdmin(<?php echo $fromPage;?>,this.value)">Delete</button>
                        </div>
                    </td>
                    <td >
                        <?php
                        $conn = connDB();
                        $sql_select_costCenter = "SELECT zonesName FROM zones WHERE zonesID_PK = ".$row['origin'];
                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                        if (mysqli_num_rows($result_select_costCenter) > 0)
                        {
                            // output data of each row
                            while($row2 = mysqli_fetch_assoc($result_select_costCenter))
                            {
                                echo $row2["zonesName"];
                            }
                        }?>
                    </td>
                    <td >
                        <?php
                        $sql_select_costCenter = "SELECT zonesName FROM zones WHERE zonesID_PK = ".$row['destination'];
                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                        if (mysqli_num_rows($result_select_costCenter) > 0)
                        {
                            // output data of each row
                            while($row3 = mysqli_fetch_assoc($result_select_costCenter))
                            {
                                echo $row3["zonesName"];
                            }
                        }?>
                    </td>
                    <td >
                        <?php echo $row["loadTransport"];?>
                    </td>
                    <td >
                        <?php echo $row["noOfDrivers"];?>
                    </td>
                    <td >
                        <?php echo $row["rates"];?>
                    </td>
                <?php
            }
                ?>
        </tr>
    <?php 
            }
        }
        else
        {
            echo  $conn->error;
            if($fromPage == 31)
            {
                ?>
                    <tr>
                        <td colspan="6" style="text-align:center;">No Records Found</td>
                    </tr>
                <?php
            }
        }
    ?>
  </tbody>
</table>
<?php
    if($condition == 1)
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$data['total']);
    }
    else
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$dataCount['total2']);
    }
?>
 