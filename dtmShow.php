<?php

require 'generalFunction.php';
session_start();
if(isset($_POST['fromPage']))
{    
?>
<div class="container-fluid">
    <div class="row mb-3">
        <div class="col-12 optional-display-inline">
            <div class="optional-buttons">
                <?php 
                // if($_SESSION['userLevel'] == 2)
                // {
                ?>
                <form action="dtmAdd.php" method="POST" class="optional-dtm-add">
                    <button class="btn btn-success mr-2" value="<?php echo $_POST['fromPage'];?>" name="add">
                        Add DTM
                    </button>
                </form>
                <?php 
                // }
                ?>
                <form action="dtmPrint.php" method="POST" class="">
                    <button class="btn btn-success">
                        Print DTM
                    </button>
                </form>
            </div>
            <div class="optional-display-inline optional-pagination-total ml-5">
                <p class="optional-pagination">
                    Page : 
                    <select class="optional-pagination-padding" onchange="checkCondition(this.value,null,null,0,<?php echo $_POST['fromPage'];?>);" id="pagination<?php echo $_POST['fromPage']; ?>"></select> 
                    of 
                </p>
                <p class="ml-2 optional-pagination optional-pagination-total-dtm" id="totalpages<?php echo $_POST['fromPage']; ?>"></p>
            </div>
            <div class="optional-display-inline optional-display-vertical optional-display-none">
                <div class="optional-display-inline ml-5">
                    <p class="optional-filter">Filter By: 
                        <select class="optional-filter-padding" id="filter<?php echo $_POST['fromPage']; ?>" onchange="checkCondition(null,this.value,null,0,<?php echo $_POST['fromPage'];?>);">
                            <option selected disabled>Filter By</option>
                            <option value="1">Unfinished</option>
                            <option value="2">DTM No(Newest)</option>
                            <option value="3">Agent</option>
                            <option value="4">Truck No</option>
                            <option value="5">Pickup Date(Newest)</option>
                            <option value="6">Booking Date(Newest)</option>
                            <option value="7">T`king Coded</option>
                            <option value="8">Finished</option>
                            <option value="9">VXL D/O Number</option>
                            <option value="100">Show All</option>
                        </select>
                    </p>
                </div>
                <div class="ml-5 mt-1 optional-search-div">
                    <input type="text" id="search<?php echo $_POST['fromPage']; ?>" class="optional-search" placeholder="Search Keyword">
                    <button class="btn btn-primary optional-search-button" onclick="checkCondition(null,null,this.value,0,<?php echo $_POST['fromPage'];?>);">Search</button>
                </div>
            </div>
        </div>
        <div class="col-12 optional-display-inline optional-display-block">
            <div class="optional-display-inline optional-display-vertical ">
                <div class="optional-display-inline">
                    <p class="optional-filter">Filter By: 
                        <select class="optional-filter-padding" id="filter<?php echo $_POST['fromPage']; ?>" onchange="checkCondition(null,this.value,null,0,<?php echo $_POST['fromPage'];?>);">
                            <option selected disabled>Filter By</option>
                            <option value="1">Unfinished</option>
                            <option value="2">DTM No(Newest)</option>
                            <option value="3">Agent</option>
                            <option value="4">Truck No</option>
                            <option value="5">Pickup Date(Newest)</option>
                            <option value="6">Booking Date(Newest)</option>
                            <option value="7">T`king Coded</option>
                            <option value="8">Finished</option>
                            <option value="9">VXL D/O Number</option>
                            <option value="100">Show All</option>
                        </select>
                    </p>
                </div>
                <div class="ml-5 mt-1 optional-search-div">
                    <input type="text" id="search<?php echo $_POST['fromPage']; ?>" class="optional-search" placeholder="Search Keyword">
                    <button class="btn btn-primary optional-search-button" onclick="checkCondition(null,null,this.value,0,<?php echo $_POST['fromPage'];?>);">Search</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="getTableOverflow" id="getTable<?php echo $_POST['fromPage'];?>"></div>
<?php
}
?>