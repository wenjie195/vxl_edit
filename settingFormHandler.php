<?php
function checkPass()
{
	if ($_SERVER["REQUEST_METHOD"] == "POST") 
	{
        if(isset($_POST['changeOldPassword']) && isset($_POST['changeNewPassword']) && isset($_POST['checkNewPassword']))
        {
            $changeOldPassword = $_POST['changeOldPassword'];
            $changeNewPassword = $_POST['changeNewPassword'];
            $checkNewPassword = $_POST['checkNewPassword'];
            $uid = $_POST['button'];

            if($changeNewPassword == $checkNewPassword)
            {
                if(strlen($changeNewPassword) >= 6)
                {
                    $message = checkPassInDB($changeOldPassword,$changeNewPassword,$uid);
                    return $message;
                }
                else
                {
                    return "Password must be longer than 5!";
                }
            }
            else
            {
                return "New password doesnt match!";
            }
        }
        else
        {
            return "Please fill up all the fields!";
        }
    }
}
function checkPassInDB($oldPass,$newPass,$uid)
{
    $conn = connDB();
    $sql = "SELECT userPassword FROM user WHERE userID_PK = ? AND showThis = 1 ";
    if ($stmt = $conn->prepare($sql)) 
    {
        $stmt->bind_param('i',$uid);

        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($currPasswordInDB);

        while($stmt->fetch())
        {
            
        }

        $stmt->free_result();
        $stmt->close();

        if ($num_of_rows > 0) 
        {
            if($currPasswordInDB == $oldPass)
            {
                $sql = "UPDATE user SET userPassword = $newPass WHERE userID_PK = $uid ";

                if ($conn->query($sql) === TRUE) 
                {
                    return "User password is updated";
                } else {
                    return "Error updating record: " . $conn->error;
                }
            }
            else
            {
                return "the current password does not match!";
            }
        } 
        else
        {
            return "There is no userID with this";
        }
    }
    else
    {
        return $conn->error;
    }
}