<?php
ini_set('max_execution_time', 300);
set_time_limit(300);
echo $monthThis = $_POST['monthThis'];
echo $yearThis = $_POST['yearThis'];


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;

define('NUMBER_OF_COLUMNS', 37); 
require 'generalFunction.php';
require 'phpexcel/vendor/autoload.php';
ob_end_clean();
$date = strtotime(sprintf('%s-%s-01',$_POST['yearThis'],$_POST['monthThis']));
renderCalenderMonth($date);



function putBorderStyleRow($sheet,$cellAlphabet,$type = null,$colour = null)
{
     if($type && $type == 1)
     {
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
     }
     if($type && $type == 2)
     {
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          if($colour)
          {
               $sheet->getStyle($cellAlphabet)
               ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
               $sheet->getStyle($cellAlphabet)
               ->getFill()->getStartColor()->setARGB($colour);
          }

     }
     else
     {
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
     }
     
}    
function putBorderStyle($sheet,$cellAlphabet,$cellNo)
{
     $sheet->getStyle($cellAlphabet.$cellNo)
     ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
     $sheet->getStyle($cellAlphabet.$cellNo)
     ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
     $sheet->getStyle($cellAlphabet.$cellNo)
     ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
     $sheet->getStyle($cellAlphabet.$cellNo)
     ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
}
function renderCalenderMonth($date)
{
$conn = connDB();
$day = date('d', $date);
$month = date('m', $date);
$year = date('Y', $date);
$daysInMonth = cal_days_in_month(0, $month, $year);

$TransportChargeArray = array();

$sqlo = " SELECT transportcharge.transportcharge,transportcharge.faf,dtmlist.dtmPickupDate,company.companyName,dtmlist.isConsol,dtmlist.costCenterID_FK FROM ((transportcharge 
     INNER JOIN dtmlist ON transportcharge.dtmID_FK = dtmlist.dtmID_PK)
     INNER JOIN company ON transportcharge.companyID_FK = company.companyID_PK)
     WHERE transportcharge.isInvoiceNoAdded LIKE 1 
     AND dtmlist.dtmPickupDate >= '$year-$month-01 00:00:00' 
     AND dtmlist.dtmPickupDate <= '$year-$month-$daysInMonth 23:59:59' 
     ORDER BY dtmlist.dtmPickupDate ASC ";
     
$result = mysqli_query($conn,$sqlo);

if (mysqli_num_rows($result) > 0) 
{
     while($row = mysqli_fetch_array($result))
     { 
          $thisTrans = new TransportCharge;
          $thisTrans->getTransportChargeAll($row['transportcharge'],date("d",strtotime($row['dtmPickupDate'])),$row['companyName'],$row['faf'],$row['isConsol'],$row['costCenterID_FK']);
          array_push($TransportChargeArray,$thisTrans);
     }
}
else
{
     
}



$firstDay = mktime(0,0,0,$month, 1, $year);
$title = strftime('%B', $firstDay);
$dayOfWeek = date('D', $firstDay);
$grandTotal = 0; 
/* Get the name of the week days */
$timestamp = strtotime('next Sunday');
$weekDays = array();

for ($i = 0; $i < NUMBER_OF_COLUMNS; $i++) {
    $weekDays[] = strftime('%a', $timestamp);
    $timestamp = strtotime('+1 day', $timestamp);
}
$blank = date('w', strtotime("{$year}-{$month}-01"));
$blank2 = date('w', strtotime("{$year}-{$month}-01"));



$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->getColumnDimension('A')->setWidth(30);
$sheet->getColumnDimension('AM')->setWidth(20);
$sheet->getColumnDimension('AN')->setWidth(20);

$sheet->setCellValue('A1', 'Agent Name');
$sheet->mergeCells('A1:A3');
putBorderStyleRow($sheet,'A1:A3',2,'00FF7F');
$sheet->setCellValue('B1', $title." ".$year);
$sheet->mergeCells('B1:AN1');
putBorderStyleRow($sheet,'B1:AN1',2,'FFD700');
putBorderStyleRow($sheet,'B2:AL3',null,null);
$sheet->getStyle('B1:AN1')->getAlignment()->setHorizontal('center');
$sheet->getStyle('B2:AN2')->getAlignment()->setHorizontal('center');
$sheet->getStyle('B3:AN3')->getAlignment()->setHorizontal('center');
$spreadsheet->getActiveSheet()->fromArray( $weekDays, NULL, 'B2' );
$sheet->setCellValue('AM2', 'Total Sales');
$sheet->setCellValue('AN2', 'Total Trip');
$sheet->mergeCells('AN2:AN3');
putBorderStyleRow($sheet,'AN2:AN3',2,'FAD7A0');

$counterBlank = 0;
$counterDays = 0;


for($x='A'; $x != 'AM'; $x++)
{
     putBorderStyleRow($sheet,$x.'2:'.$x.'3',2,'FA8072');
     if($counterBlank <= $blank)
     {
          $sheet->setCellValue($x . '3', '');
         
     }
     else
     {
          if($counterDays+1 <=$daysInMonth)
          {
               $sheet->setCellValue($x . '3',$counterDays+1);
               $counterDays++;
          }
     }
     $counterBlank++;
}

$costCenterDisplay = "SELECT * FROM costcenter ORDER BY companyID_FK ASC , costCenterName ASC";
$costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);

$noOfRows = mysqli_num_rows($costCenterDisplayQuery);

$companyStartCounter = 4;
$tripArray = array();
$chargesArray = array();
$tripX = array();

if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
{
     while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
     {

          $companyID_FK_FK = $urow1['companyID_FK'];
          $costcenterID_FK_FK = $urow1['costCenterID_PK'];

     $xx = "SELECT * FROM company WHERE companyID_PK = ".$companyID_FK_FK;
     $xxs = mysqli_query($conn,$xx);
     if (mysqli_num_rows($xxs) > 0) 
     {
          while($xxsa = mysqli_fetch_array($xxs))
          {

          $xxsa['companyName'];

          $counterBlankField = 0;
          $counterDaysField = 0;
          $sheet->setCellValue('A'.$companyStartCounter ,$xxsa['companyName']."(".$urow1['costCenterName'].")");
          putBorderStyle($sheet,'A',$companyStartCounter);
          $total = 0;
          $totalTrip = 0;
          for($x='B'; $x != 'AM'; $x++)
          {
               putBorderStyleRow($sheet,$x.$companyStartCounter,null,null);
               $sheet->getColumnDimension($x)->setWidth(10);
               $sheet->getStyle('B'. $companyStartCounter.':'.'AM'. $companyStartCounter)->getAlignment()->setHorizontal('center');
               if($counterBlankField < $blank)
               {
                    $sheet->setCellValue($x . $companyStartCounter,'');

               }
               else
               {
                    if($counterDaysField+1 <=$daysInMonth)
                    {
                         $value = 0;
                         $tripNo = 0;

                         for($cnt = 0;$cnt < count($TransportChargeArray);$cnt++)
                         {
                              if($TransportChargeArray[$cnt]->getDtmBookingDate() == ($counterDaysField+1) && $TransportChargeArray[$cnt]->getCompanyName() == $xxsa['companyName'] && $TransportChargeArray[$cnt]->getCostCenterID_FK() == $costcenterID_FK_FK)
                              {
                                   $idfk = $TransportChargeArray[$cnt]->getCostCenterID_FK();
                                   $fafPercentageAfter = ($TransportChargeArray[$cnt]->getFaf() / 100) * $TransportChargeArray[$cnt]->getTransportCharge();
                                   $value += ($TransportChargeArray[$cnt]->getTransportCharge() + $fafPercentageAfter);
                                   $total += ($TransportChargeArray[$cnt]->getTransportCharge() + $fafPercentageAfter);
                                   if($TransportChargeArray[$cnt]->getIsConsol() != 1)
                                   {
                                        $tripNo++;
                                        $totalTrip++;
                                   }
                                   $grandTotal += ($TransportChargeArray[$cnt]->getTransportCharge() +  $fafPercentageAfter);
                              }
                         }
                         if($tripNo != 0 && $idfk == $costcenterID_FK_FK)
                         {
                              array_push($tripArray,$tripNo);
                              array_push($chargesArray,$value);
                              array_push($tripX,$x);
                              // $sheet->setCellValue($x . $companyStartCounter,$tripNo." trip\n(RM ".sprintf('%0.2f',$value).")");
                              $sheet->setCellValue($x . $companyStartCounter,$value);
                              $sheet->getStyle($x . $companyStartCounter)->getNumberFormat()->setFormatCode('0.00'); 
                              $sheet->getStyle($x . $companyStartCounter)->getAlignment()->setWrapText(true);
                         }
                         else
                         {
                              $sheet->setCellValue($x . $companyStartCounter,'0');
                              $sheet->getStyle($x . $companyStartCounter)->getNumberFormat()->setFormatCode('0.00'); 
                         }
                         // $sheet->setCellValue('AM'. $companyStartCounter,sprintf("%0.2f",$total));
                         $sheet->setCellValue('AM'.$companyStartCounter,'=SUM(B'.$companyStartCounter.':AL'.($companyStartCounter).')');
                         $sheet->getStyle('AN'.$companyStartCounter)->getAlignment()->setHorizontal('center');
                         $sheet->getStyle('AM'. $companyStartCounter)->getNumberFormat()->setFormatCode('0.00'); 
                         $sheet->setCellValue('AN'.$companyStartCounter,$totalTrip);
                         $counterDaysField++;
                    }
               }
               $counterBlankField++;
          }
          $companyStartCounter++;
               }
          }
     }
}
$sheet->setCellValue('A'. $companyStartCounter,'GRAND TOTAL');
$sheet->setCellValue('A'. ($companyStartCounter+1),'DAILY TRIP TOTAL');
$sheet->setCellValue('A'. ($companyStartCounter+2),'DAILY CHARGES TOTAL');
$sheet->setCellValue('AM'. ($companyStartCounter+2),'=SUM(B'.($companyStartCounter+2).':AL'.($companyStartCounter+2).')');
$sheet->getStyle('AM'.($companyStartCounter+2))->getAlignment()->setHorizontal('center');
$sheet->getStyle('AM'. ($companyStartCounter+2))->getNumberFormat()->setFormatCode('0.00'); 
putBorderStyleRow($sheet,'A'. $companyStartCounter,2,'ADFF2F');
// $sheet->setCellValue('AM'. $companyStartCounter,'RM '.sprintf('%0.2f',$grandTotal));
$sheet->setCellValue('AM'. $companyStartCounter,'=SUM(AM4:AM'.($companyStartCounter-1).')');
$sheet->getStyle('AM'. $companyStartCounter)->getNumberFormat()->setFormatCode('0.00'); 
putBorderStyleRow($sheet,'AM'. $companyStartCounter,2,'FFF8DC');
$sheet->setCellValue('AN'. ($companyStartCounter+1),'=SUM(AN4:AN'.($companyStartCounter-1).')');
$sheet->getStyle('AN'.$companyStartCounter)->getAlignment()->setHorizontal('center');
$sheet->getStyle('AN'.($companyStartCounter+1))->getAlignment()->setHorizontal('center');

$sheet->getStyle('AM'.$companyStartCounter)->getAlignment()->setHorizontal('center');
$sheet->mergeCells('A'.$companyStartCounter.':AL'.$companyStartCounter);
putBorderStyleRow($sheet,'A1:AM'.$companyStartCounter,null,null);
putBorderStyleRow($sheet,'A1:A3',null,null);
putBorderStyleRow($sheet,'A'.$companyStartCounter.':AL'.$companyStartCounter,null,null);
putBorderStyleRow($sheet,'AM2:AM'.($companyStartCounter-1),2,'99FFFF');
putBorderStyleRow($sheet,'AM'.$companyStartCounter,1,null);
putBorderStyleRow($sheet,'AN2:AN'.$companyStartCounter,2,'99AFFF');
putBorderStyleRow($sheet,'AN'. $companyStartCounter,2,'FFF8DC');
putBorderStyleRow($sheet,'AN'. ($companyStartCounter+1),2,'FFF8DC');
putBorderStyleRow($sheet,'AN4:AN'.$companyStartCounter,null,null);
putBorderStyleRow($sheet,'A'.($companyStartCounter+1).':AN'.($companyStartCounter+1),2,'FAB701');

for($newCounter = 4;$newCounter < $companyStartCounter;$newCounter++)
{
     putBorderStyleRow($sheet,'AM'.$newCounter,2,'99FFFF');
     putBorderStyleRow($sheet,'AN'.$newCounter,2,'99AFFF');
}
//  array_push($tripArray,$tripNo);
// array_push($tripX,$x);
for($c='B'; $c != 'AM'; $c++)
{
     $countDailyTrips = 0;
     $countDailyCharges = 0;
     for($cs = 0; $cs < count($tripX);$cs++)
     {
          if($tripX[$cs] == $c)
          {
               $countDailyTrips += $tripArray[$cs];
               $countDailyCharges += $chargesArray[$cs];
          }
     }

     $sheet->setCellValue($c. ($companyStartCounter+1),$countDailyTrips);
     $sheet->setCellValue($c. ($companyStartCounter+2),$countDailyCharges);
     $sheet->getStyle($c.($companyStartCounter+2))->getNumberFormat()->setFormatCode('0.00'); 
     $sheet->getStyle($c.($companyStartCounter+2))->getAlignment()->setHorizontal('center');
     $sheet->getStyle($c.($companyStartCounter+1))->getAlignment()->setHorizontal('center');
     
     putBorderStyleRow($sheet,$c. ($companyStartCounter+1),null,null);
     putBorderStyleRow($sheet,$c. ($companyStartCounter+2),null,null);
}
putBorderStyleRow($sheet,'A'. ($companyStartCounter+2),null,null);
putBorderStyleRow($sheet,'AM'. ($companyStartCounter+2),null,null);
putBorderStyleRow($sheet,'AN'. ($companyStartCounter+2),null,null);

$filename = 'filename="salesReportByCompany'.$month.'_'.$year.'.xlsx"';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; '.$filename);

$writer = new Xlsx($spreadsheet);
$writer->save('php://output');
}
?>