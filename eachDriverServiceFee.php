<?php
//Start the session
session_start();
if(isset($_POST['edit']))
{
     $driverID = $_POST['edit'] ;
}
else
{
     header('Location:driverHome.php');
}

//Check f the session is empty/exist or not
if(!empty($_SESSION))
{
    require 'generalFunction.php';
    $conn = connDB();

    ?>
    <!doctype html>
    <html lang="en">
    <head>
        <title>Driver Service Fee</title>
        <?php require 'indexHeader.php';?>
        <style>
        .dsfPagination {
            margin-left: 25px;
        }
        .dsfFilterPara {
            margin-left: 350px;
        }
    </style>
    </head>
    <body>
    <?php require 'indexNavbar.php';?>
    <div class="container-fluid">
        <div class="row">
            <?php require 'indexSidebar.php';
            generateSimpleModal();
            ?>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h3><?php
                    $_SESSION['thisDriverID_PK'] = $driverID;
                    $sql_select_costCenter = "SELECT * FROM driver WHERE driverID_PK = '$driverID'";
                    $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                    if (mysqli_num_rows($result_select_costCenter) > 0)
                    {
                        // output data of each row
                        while($row = mysqli_fetch_assoc($result_select_costCenter))
                        {
                            $name = $row['driverName'];
                            $driverNickName = $row['driverNickName'];
                            $driverICno = $row['driverICno'];
                            $driverPhoneNo = $row['driverPhoneNo'];
                            echo "Driver Service Fee Details";
                        }
                    }
                    ?></h3>
                </div>
                <div class="row">
                    <div class="col-xl-6 row" >
                         <div class="col-xl-12 row" >
                              <div class="col-xl-4" ><p>Full Name</p></div>
                              <div class="col-xl-8" ><p>: <?php  echo $name ; ?></p></div>
                         </div>
                         <div class="col-xl-12 row" >
                              <div class="col-xl-4" ><p>Nick Name</p></div>
                              <div class="col-xl-8" ><p>: <?php  echo $driverNickName ; ?></p></div>
                         </div>
                         <div class="col-xl-12 row" >
                              <div class="col-xl-4" ><p>IC Number</p></div>
                              <div class="col-xl-8" ><p>: <?php  echo $driverICno ; ?></p></div>
                         </div>
                         <div class="col-xl-12 row" >
                              <div class="col-xl-4" ><p>Phone No</p></div>
                              <div class="col-xl-8" ><p>: <?php  echo $driverPhoneNo ; ?></p></div>
                         </div>
                    </div>
                    <form class="col-xl-6 row" method="POST" action="printDSF.php">
                        <div class="col-xl-1" ></div>
                        <div class="form-group col-xl-5">
                            <label for="fromDate" >From(Date)</label>
                            <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="fromDate" name="fromDate">
                        </div>
                        <div class="form-group col-xl-5">
                            <label for="toDate" >To(Date)</label>
                            <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="toDate" name="toDate">
                        </div>
                        <div class="col-xl-1" ></div>

                        <div class="col-xl-3"></div>
                        <div class="col-xl-6 text-center">
                            <input type="hidden" name="driverID" value="<?php echo $driverID;?>">
                            <button class="btn formButtonPrimary " onclick="checkDSFDate();">Print DSF</button>
                        </div>
                        <div class="col-xl-3"></div>
                    </form>
                </div>
                <div class="row" style="border-top: 1px solid #dee2e6!important;">
                    <div class="col-xl-12 mt-4" id="showEachServiceFee"></div>
                </div>
            </main>
        </div>
    </div>
    <?php require 'indexFooter.php';?>
    <script>
        $(document).ready(function()
        {
            let from,to;
          ajaxEachServiceFee('<?php echo $driverID;?>');


          from = initializeDate(document.getElementById('fromDate'),from);
          to = initializeDate(document.getElementById('toDate'),to);
        });

        function initializeDate(thisId,setVariable)
        {
            var setVariable = new Pikaday({
                field: thisId,
                format: 'YYYYMMDD',
                onSelect: function() {
                    // console.log(this.getMoment().format('Do MMMM YYYY'));
                }
            });
            return setVariable;
        }
    </script>
    </body>
    </html>
    <?php
}
else
{
    // Go back to index page
    // NOTE : MUST PROMPT ERROR
    header('Location:index.php');
}
?>