<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        if(isset($_POST['edit']))
        {   
            $add = $_POST['edit'];
            require 'generalFunction.php';

?>
<!doctype html>
<html lang="en">
    <head>
        <title>Edit Transport Charges</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>
                              Edit Transport Charges
                        </h3>
                    </div>
                    <?php   
                        generateConfirmationModal();
                        generateSimpleModal();
                    ?>
                    <?php
                    $conn = connDB(); 

                    $costCenterDisplay1 = "SELECT * FROM transportcharge WHERE id = ".$add;
                    $costCenterDisplayQuery1 = mysqli_query($conn,$costCenterDisplay1);
                    if (mysqli_num_rows($costCenterDisplayQuery1) > 0) 
                    {    
                         while($grow = mysqli_fetch_array($costCenterDisplayQuery1))
                         {
                         $costCenterDisplay = "SELECT * FROM dtmlist WHERE dtmID_PK = ".$grow['dtmID_FK'];
                         $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                         if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                         {    
                              while($row = mysqli_fetch_array($costCenterDisplayQuery))
                              {
                                   ?>
                                   <table class="table dtmTableNoWrap table-sm table-hovered table-bordered table-striped table-responsive-xl  dtmTableNoWrap"style="text-align:center;">
                                        <thead>
                                             <tr>
                                                  <th>Planner</th>
                                                  <th>DTM NO</th>
                                                  <th>VXL D/O No</th>
                                                  <th>Pickup Date</th>
                                                  <th>Request By</th>
                                                  <th>Pickup Time</th>
                                                  <th>From</th>
                                                  <th>To</th>
                                             </tr>
                                        </thead>
                                        <tbody>
                                             <tr>
                                                  <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT userNickName FROM user WHERE userID_PK = ".$row['dtmPlannerID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['userNickName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php echo $row['dtmID_PK'];?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                        $vxlDO = "";
                                                        if($row['dtmAdhoc'])
                                                        {
                                                            $vxlDO = $row['dtmAdhoc'];
                                                        }
                                                            echo $vxlDO;
                                                       ?>
                                                  </td>
                                                  <!-- <td>
                                                       <?php 
                                                       // $shipmentDate = date("d M Y",strtotime($row['dtmBookingDate']));
                                                       // echo $shipmentDate;
                                                       ?>
                                                  </td> -->
                                                  <td>
                                                       <?php 
                                                            $pickupDate = date("d M Y",strtotime($row['dtmPickupDate']));
                                                            echo $pickupDate;
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php echo $row['dtmRequestBy'];?>
                                                  </td>
                                                  <!-- <td>
                                                       <?php 
                                                       // $shipmentTime = date("G:i",strtotime($row['dtmBookingTime']));
                                                       // echo $shipmentTime;
                                                       ?>
                                                  </td> -->
                                                  <td>
                                                       <?php 
                                                            $pickupTime = date("G:i",strtotime($row['dtmPickupTime']));
                                                            echo $pickupTime;
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$row['dtmOriginPointID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['pointzonePlaceName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$row['dtmDestinationPointID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['pointzonePlaceName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                             </tr>
                                        </tbody>
                                   </table>
                                   <table class="table table-sm table-hovered table-bordered table-striped table-responsive-xl  dtmTableNoWrap"style="text-align:center;">
                                        <thead>
                                             <tr>
                                                  <th>Agent</th>
                                                  <th>Cost Centre</th>
                                                  <th>Truck No</th>
                                                  <th>CAP</th>
                                                  <th>Consol</th>
                                                  <th>Driver Name</th>
                                                  <th>Remark</th>
                                             </tr>
                                        </thead>
                                        <tbody>
                                             <tr>

                                             <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT companyName FROM company WHERE companyID_PK = ".$row['companyID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['companyName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td><?php 
                                                            
                                                   $ee = "SELECT costCenterName FROM costcenter WHERE costCenterID_PK = ".$row['costCenterID_FK'];
                                                   $e = mysqli_query($conn,$ee);
                                                   if (mysqli_num_rows($e) > 0) 
                                                   {
                                                        while($urow1 = mysqli_fetch_array($e))
                                                        {
                                                             echo $adhoc  = $urow1['costCenterName'];
                                                        }
                                                   }
                                                  ?></td>
                                                  <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT truckPlateNo FROM trucks WHERE truckID_PK = ".$row['truckID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['truckPlateNo'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  
                                                  <td>
                                                       <?php 
                                                       echo $loadCap = $row['loadCap'];
                                                       ?>
                                                  </td>
                                                  <td>
                                                  <?php
                                                  $isConsol = $row['isConsol'];
                                                  if($isConsol == 1)
                                                  {
                                                       echo "CONSOL";
                                                  }
                                                  else
                                                  {
                                                       echo "-";
                                                  }
                                                  ?>
                                                  </td>
                                                  <td class="text-left">
                                                       <?php 
                                                       $costCenterDisplay = "SELECT driverName FROM driver WHERE driverID_PK = ".$row['driverID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo "1.".$urow1['driverName'];
                                                            }
                                                       }

                                                       if($row['driver2ID_FK'])
                                                       {
                                                                 $costCenterDisplay = "SELECT driverName FROM driver WHERE driverID_PK = ".$row['driver2ID_FK'];
                                                                 $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                                 if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                                 {
                                                                      while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                                      {
                                                                           echo "<br>2.".$urow1['driverName'];
                                                                      }
                                                                 }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td style="text-align:left;"><?php 
                                                  if( $row['lockupf'] || $row['lockupdockTime'] || $row['lockupcompleteLoadingTime'] || $row['lockupdepartureTimeFromPost'] ||
                                                  $row['lockuparrivalTimeAtPost'] || $row['lockupdestinationDockTime'] || $row['lockupcompleteUnloadTime'] || $row['lockupdepartTimeFromPost'])
                                                  {
                                                  echo "->Lockup/Detention<br>->".strtoupper($row['dtmRemarks']);
                                                  }
                                                  else
                                                  {
                                                  echo "->".strtoupper($row['dtmRemarks']);
                                                  }?></td>
                                                  
                                             </tr>
                                        </tbody>
                                   </table>
                                   <?php
                              }
                         }
                    ?>
                    <div class="row adminAddMarginTop">
                        <div class="col-xl-12 row">
                         <div class="col-xl-1"></div>
                         <div class="form-group col-xl-5">
                              <label for="field_1" >Remarks For Transport Charges</label>
                              <input  type="text" class="form-control adminAddSetPadding" id="field_1" value="<?php echo $grow['remarks'];?>">
                         </div>
                         <div class="form-group col-xl-5">
                              <label for="field_2" >Operating Hours</label>
                              <input  type="number" class="form-control adminAddSetPadding" id="field_2" min="1" step="1" value="<?php echo $grow['operatingHour'];?>">
                         </div>
                         <div class="col-xl-1"></div>
                         <div class="col-xl-1"></div>
                         <div class="form-group col-xl-5">
                         <?php 
                              if($isConsol == 1)
                              {
                                   ?><label for="field_3" >Consol Charges</label><?php 
                              }
                              else
                              {
                                   ?><label for="field_3" >Transport Charges</label><?php 
                              }
                         ?>
                              <input  type="number" class="form-control adminAddSetPadding" id="field_3" min="0"  step="0.01" value="<?php echo sprintf('%0.2f',$grow['transportcharge']);?>" >
                         </div>
                         <div class="form-group col-xl-5">
                              <label for="field_4" >FAF (%)</label>
                              <input  type="number" class="form-control adminAddSetPadding" id="field_4" step="0.01" value="<?php echo sprintf('%0.2f',$grow['faf']);?>">
                         </div>
                         <div class="col-xl-1"></div>
                         <div class="col-xl-3"></div>
                         <div class="col-xl-6 adminAddUserButton">
                              <input  type="hidden" name="dtmidfk" id="dtmidfk" value="<?php $grow['dtmID_FK'];?>">
                              <button class="btn formButtonPrimary indexSubmitButton" onclick="addTransportCharge(<?php echo $add;?>,2);">Edit Transport Charge</button>
                         </div>
                         <div class="col-xl-3"></div>
                             
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
    </body>
</html>
<?php
                    }
               }

        }
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>