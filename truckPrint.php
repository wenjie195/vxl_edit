<?php
date_default_timezone_set("Asia/Kuala_Lumpur");
$todayDate = date("Ymd_his");
$filename = 'filename="TruckList.xlsx"';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; '.$filename);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;

define('NUMBER_OF_COLUMNS', 37); 
require 'generalFunction.php';
require 'phpexcel/vendor/autoload.php';
ob_end_clean();


$conn = connDB();
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$sheet->setCellValue('A1', 'No');
$sheet->getColumnDimension('A')->setWidth(15);
$sheet->setCellValue('B1', 'Truck Plate No');
$sheet->getColumnDimension('B')->setWidth(20);
$sheet->setCellValue('C1', 'Truck Capacity');
$sheet->getColumnDimension('C')->setWidth(20);
$sheet->setCellValue('D1', 'Truck Model');
$sheet->getColumnDimension('D')->setWidth(20);
$sheet->setCellValue('E1', 'Truck Made');
$sheet->getColumnDimension('E')->setWidth(20);
$sheet->setCellValue('F1', 'Truck Custom Bond');
$sheet->getColumnDimension('F')->setWidth(30);
$sheet->setCellValue('G1', 'Expired Custom Bond Date');
$sheet->getColumnDimension('G')->setWidth(30);

$sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
$sheet->getStyle('B1')->getAlignment()->setHorizontal('center');
$sheet->getStyle('C1')->getAlignment()->setHorizontal('center');
$sheet->getStyle('D1')->getAlignment()->setHorizontal('center');
$sheet->getStyle('E1')->getAlignment()->setHorizontal('center');
$sheet->getStyle('F1')->getAlignment()->setHorizontal('center');
$sheet->getStyle('G1')->getAlignment()->setHorizontal('center');


$sqlo = " SELECT * FROM trucks WHERE showThis = 1 ";
     
$result = mysqli_query($conn,$sqlo);
$bilangan = 1;

if (mysqli_num_rows($result) > 0) 
{
     $line = 2;
     while($row = mysqli_fetch_array($result))
     {
          $sheet->setCellValue('A'.$line, $bilangan);
          $sheet->setCellValue('B'.$line, $row['truckPlateNo']);
          $sheet->setCellValue('C'.$line, $row['truckCapacity']);
          $sheet->setCellValue('D'.$line, $row['truckModel']);
          $sheet->setCellValue('E'.$line, $row['truckMade']);
          $sheet->setCellValue('F'.$line, $row['truckCustomBond']);
          $sheet->setCellValue('G'.$line, date("d M Y",strtotime($row['truckCustomExpired'])));

          $sheet->getStyle('A'.$line)->getAlignment()->setHorizontal('center');
          $sheet->getStyle('B'.$line)->getAlignment()->setHorizontal('left');
          $sheet->getStyle('C'.$line)->getAlignment()->setHorizontal('center');
          $sheet->getStyle('D'.$line)->getAlignment()->setHorizontal('center');
          $sheet->getStyle('E'.$line)->getAlignment()->setHorizontal('center');
          $sheet->getStyle('F'.$line)->getAlignment()->setHorizontal('center');
          $sheet->getStyle('G'.$line)->getAlignment()->setHorizontal('center');

          $bilangan++;
          $line++;
     }
}

$writer = new Xlsx($spreadsheet);
$writer->save('php://output');

?>