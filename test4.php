<?php
date_default_timezone_set("Asia/Kuala_Lumpur");
$todayDate = date("Ymd_his");

require_once 'generalFunction.php';
require_once 'templateFunctions.php';
$conn = connDB();

$companyName = getCompanyName($_POST['company1'],$conn);
$fromDate = $_POST['fromDate1'];
$toDate = $_POST['toDate1'];

$filename = 'filename="Invoice_'.$companyName.'__'.$fromDate.'_'.$toDate.'.xlsx"';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; '.$filename);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;


require_once 'phpexcel/vendor/autoload.php';

// echo $fromDate."<br>";
// echo $toDate."<br>";
// echo $companyName."<br>";

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$fromDateFormatted = getDatePHP($fromDate);
$toDateFormatted = getDatePHPWithTime($toDate,0);

$companyID = $_POST['company1'];

$costCenterDisplay = " SELECT * FROM ((invoice 
INNER JOIN transportcharge ON invoice.transportID_FK = transportcharge.id)
INNER JOIN dtmlist ON transportcharge.dtmID_FK = dtmlist.dtmID_PK)
WHERE (invoice.isSelected = 0 OR invoice.isSelected IS NULL)
AND (dtmlist.dtmPickupDate BETWEEN '$fromDateFormatted' AND '$toDateFormatted')
AND (dtmlist.companyID_FK = '$companyID')
ORDER BY dtmlist.truckID_FK ASC
";

$costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
$counterNo = mysqli_num_rows($costCenterDisplayQuery);
$line = 2;

if($counterNo > 0)
{
     $sheet->setCellValue('A1', 'Invoice No');
     $sheet->getColumnDimension('A')->setWidth(15);
     $sheet->setCellValue('B1', 'D/O No');
     $sheet->getColumnDimension('B')->setWidth(15);
     $sheet->setCellValue('C1', 'DTM No');
     $sheet->getColumnDimension('C')->setWidth(15);
     $sheet->setCellValue('D1', 'Planner');
     $sheet->getColumnDimension('D')->setWidth(15);
     $sheet->setCellValue('E1', 'Booking Date');
     $sheet->getColumnDimension('E')->setWidth(18);
     $sheet->setCellValue('F1', 'Pickup Date');
     $sheet->getColumnDimension('F')->setWidth(18);
     $sheet->setCellValue('G1', 'Request By');
     $sheet->getColumnDimension('G')->setWidth(25);
     $sheet->setCellValue('H1', 'Booking Time');
     $sheet->getColumnDimension('H')->setWidth(15);
     $sheet->setCellValue('I1', 'Pickup Time');
     $sheet->getColumnDimension('I')->setWidth(15);
     $sheet->setCellValue('J1', 'From');
     $sheet->getColumnDimension('J')->setWidth(25);
     $sheet->setCellValue('K1', 'To');
     $sheet->getColumnDimension('K')->setWidth(25);
     $sheet->setCellValue('L1', 'Remark');
     $sheet->getColumnDimension('L')->setWidth(25);
     $sheet->setCellValue('M1', 'Agent');
     $sheet->getColumnDimension('M')->setWidth(25);
     $sheet->setCellValue('N1', 'Cost Center');
     $sheet->getColumnDimension('N')->setWidth(20);
     $sheet->setCellValue('O1', 'Truck No');
     $sheet->getColumnDimension('O')->setWidth(25);
     $sheet->setCellValue('P1', 'CAP');
     $sheet->getColumnDimension('P')->setWidth(20);
     $sheet->setCellValue('Q1', 'Consol');
     $sheet->getColumnDimension('Q')->setWidth(15);
     $sheet->setCellValue('R1', 'Driver Name');
     $sheet->getColumnDimension('R')->setWidth(50);
     $sheet->setCellValue('S1', 'Remark 2');
     $sheet->getColumnDimension('S')->setWidth(15);
     $sheet->setCellValue('T1', 'Operating Hour');
     $sheet->getColumnDimension('T')->setWidth(20);
     $sheet->setCellValue('U1', 'Transport Charge');
     $sheet->getColumnDimension('U')->setWidth(20);
     $sheet->setCellValue('V1', 'FAF %');
     $sheet->getColumnDimension('V')->setWidth(15);
     $sheet->setCellValue('W1', 'Total');
     $sheet->getColumnDimension('W')->setWidth(15);
    
     $sheet->getStyle('A1:X1')->getAlignment()->setHorizontal('center');
     while($row = mysqli_fetch_array($costCenterDisplayQuery))
     {
          // Invoice Table
          $invoiceNo = $row['invoiceNo'];

          // Transport Charges Table
          $transportchargeRemarks = strtoupper($row['remarks']);
          $transportchargeOperatingHour = $row['operatingHour'];
          $transportchargeTransportCharge = $row['transportcharge'];
          $transportchargeFaf = ($row['faf'] / 100) * $row['transportcharge'];
          $transportchargeTotalCharges = $transportchargeTransportCharge + $transportchargeFaf;

          // DTM Table
          $dtmNoID_PK = $row['dtmID_PK'];
          $dtmDGF = $row['dtmDGF'];
          // $companyName = getCompanyName($_POST['company'],$conn);
          $costCenterName = getCostCenterName($row['costCenterID_FK'],$conn);

               // Booking & Pickup Time
          $dtmBookingTime = getTimeFormats($row['dtmBookingTime']);
          $dtmPickupTime = getTimeFormats($row['dtmPickupTime']);
          $dtmPickupTime1 = getTimeFormats2($row['dtmPickupTime']);
          $dtmBookingDate = getDateFormats($row['dtmBookingDate']);
          $dtmPickupDate = getDateFormats($row['dtmPickupDate']);

               // Origin & Destination Time
          $dtmOriginPointID_FK = getPlaceName($row['dtmOriginPointID_FK'],$conn);
          $dtmDestinationPointID_FK = getPlaceName($row['dtmDestinationPointID_FK'],$conn);
          $dtmOriginZone = getZoneName($row['dtmOriginZoneID_FK'],$conn);
          $dtmDestinationZone = getZoneName($row['dtmDestinationZoneID_FK'],$conn);
          
               // Trucks & Drivers & Actual Loads
          $truckArray = getTruckDetails($row['truckID_FK'],$conn);
          $truckPlateNo = $truckArray[0];
          $truckCap = $truckArray[1];
          $loadCap = $row['loadCap'];
          $dtmUrgentMemo = "No";

          $driversName = "(1) ".getDriverName($row['driverID_FK'],$conn);
          if(isset($row['driver2ID_FK']))
          {
               $driversName .= "\n(2) ".getDriverName($row['driver2ID_FK'],$conn);
          }

          if($row['dtmUrgentMemo'] == 1)
          {
               $dtmUrgentMemo = "Yes";
          }
          
               // Trucks Loading Times
          $startIn = getTimeFormats($row['dtmOriginF']);
               $timedockTime = getTimeFormats($row['dtmOriginDockTime']);
               $timecompleteLoadingTime = getTimeFormats($row['dtmOriginCompleteLoadingTime']);
          $startOut = getTimeFormats($row['dtmOriginDepartureTimeFromPost']);

          $endIn = getTimeFormats($row['dtmDestinationArrivalTimeAtPost']);
               $timedestinationDockTime = getTimeFormats($row['dtmDestinationDockTime']);
               $timecompleteUnloadTime = getTimeFormats($row['dtmDestinationCompleteUnloadTime']);
          $endOut = getTimeFormats($row['dtmDestinationDepartTimeFromPost']);

          $startIn1 = getTimeFormats2($row['dtmOriginF']);
               $timedockTime1 = getTimeFormats2($row['dtmOriginDockTime']);
               $timecompleteLoadingTime1 = getTimeFormats2($row['dtmOriginCompleteLoadingTime']);
          $startOut1 = getTimeFormats2($row['dtmOriginDepartureTimeFromPost']);

          $endIn1 = getTimeFormats2($row['dtmDestinationArrivalTimeAtPost']);
               $timedestinationDockTime1 = getTimeFormats2($row['dtmDestinationDockTime']);
               $timecompleteUnloadTime1 = getTimeFormats2($row['dtmDestinationCompleteUnloadTime']);
          $endOut1 = getTimeFormats2($row['dtmDestinationDepartTimeFromPost']);

          $tpt = compareValue($row['dtmDestinationDepartTimeFromPost'],$row['dtmOriginF'],$row['lockupdepartTimeFromPost'],$row['lockupf'],$row['dtmPickupDate']);
          
          $lockupf = checkLockupDates($row['lockupf']);
          $lockupdockTime = checkLockupDates($row['lockupdockTime']);
          $lockupcompleteLoadingTime = checkLockupDates($row['lockupcompleteLoadingTime']);
          $lockupdepartureTimeFromPost = checkLockupDates($row['lockupdepartureTimeFromPost']);

          $lockuparrivalTimeAtPost = checkLockupDates($row['lockuparrivalTimeAtPost']);
          $lockupdestinationDockTime = checkLockupDates($row['lockupdestinationDockTime']);
          $lockupcompleteUnloadTime = checkLockupDates($row['lockupcompleteUnloadTime']);
          $lockupdepartTimeFromPost = checkLockupDates($row['lockupdepartTimeFromPost']);

               // Quantities
          $quantityLoad = "";

          $cartonQuantity = 0;
          $palletsQuantity = 0;
          $cagesQuantity = 0;

          if($row['dtmIsQuantity'] == 1)
          {
               $quantityLoad .= "Carton = ".$row['dtmQuantityCarton'].",";
               $cartonQuantity = $row['dtmQuantityCarton'];
          }
          if($row['dtmIsPallets']== 1)
          {
               $quantityLoad .= "Pallets = ".$row['dtmQuantityPalllets'].",";
               $palletsQuantity = $row['dtmQuantityPalllets'];
          }
          if($row['dtmIsCages']== 1)
          {
               $quantityLoad .= "Cages = ".$row['dtmQuantityCages'];
               $cagesQuantity = $row['dtmQuantityCages'];
          }

               //DTM additional info
          $dtmTripOnTime = $row['dtmTripOnTime'];
          $dtmDelayReasons = "";
          if($dtmTripOnTime == 0 && isset($row['dtmDelayReasons']))
          {
               $dtmDelayReasons = $row['dtmDelayReasons'];
          }
          $planner = getPlannerName($row['dtmPlannerID_FK'],$conn);
          $dtmRequestBy = $row['dtmRequestBy'];
          $dtmRemarks = getDTM_Remark("",$row); 

          $consolShow = "";
          $isConsol = $row['isConsol'];

          if($isConsol == 1)
          {
               $consolShow="CONSOL"; 
          }

          $vxlDO = "";
          if($row['dtmAdhoc'])
          {
               $vxlDO = $row['dtmAdhoc'];
          }

          $sheet->setCellValue('A'.$line, $invoiceNo);
          $sheet->setCellValue('B'.$line, $vxlDO);
          $sheet->setCellValue('C'.$line, $dtmNoID_PK);
          $sheet->setCellValue('D'.$line, $planner);
          $sheet->setCellValue('E'.$line, $dtmBookingDate);
          $sheet->setCellValue('F'.$line, $dtmPickupDate);
          $sheet->setCellValue('G'.$line, $dtmRequestBy);
          $sheet->setCellValue('H'.$line, $dtmBookingTime);
          $sheet->setCellValue('I'.$line, $dtmPickupTime);
          $sheet->setCellValue('J'.$line, $dtmOriginPointID_FK);
          $sheet->setCellValue('K'.$line, $dtmDestinationPointID_FK);
          $sheet->setCellValue('L'.$line, $dtmRemarks);
          $sheet->setCellValue('M'.$line, $companyName);
          $sheet->setCellValue('N'.$line, $costCenterName);
          $sheet->setCellValue('O'.$line, $truckPlateNo);
          
          $sheet->setCellValue('P'.$line, $loadCap);
          $sheet->setCellValue('Q'.$line, $consolShow);
          $sheet->setCellValue('R'.$line, $driversName);
          $sheet->getStyle('R'.$line)->getAlignment()->setWrapText(true);
          $sheet->setCellValue('S'.$line, $transportchargeRemarks);
          $sheet->setCellValue('T'.$line, $transportchargeOperatingHour." Hour");
          $sheet->setCellValue('U'.$line, sprintf('%0.2f',$transportchargeTransportCharge));
          $sheet->setCellValue('V'.$line, sprintf('%0.2f',$transportchargeFaf));
          $sheet->setCellValue('W'.$line, sprintf('%0.2f',$transportchargeTotalCharges));
          
          $sheet->getStyle('U'.$line)->getNumberFormat()->setFormatCode('0.00'); 
          $sheet->getStyle('V'.$line)->getNumberFormat()->setFormatCode('0.00'); 
          $sheet->getStyle('W'.$line)->getNumberFormat()->setFormatCode('0.00'); 
          $sheet->getStyle('A'.$line.':X'.$line)->getAlignment()->setHorizontal('center');
          $line++;
     }

}
else 
{
     $sheet->setCellValue('A1','No data matches');
     $sheet->setCellValue('A2','Please Try Again');
     $sheet->setCellValue('A3','Using Correct Info');
}


$writer = new Xlsx($spreadsheet);
$writer->save('php://output');
?>