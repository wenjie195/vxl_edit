<?php
require 'generalFunction.php';
$conn = connDB();
session_start();
$fromPage = $_POST['fromPage'];
$condition = $_POST['condition'];
$pageNo = $_POST['pageNo'];
$filter = $_POST['filter'];
$searchWord = $_POST['searchWord'];

// echo $fromPage;
// echo $condition;
// echo $pageNo;
// echo $filter;
// echo " =".$searchWord."=";

if($filter == null)
{
    $filter= 1;
}
if($pageNo == null)
{
    $filter= 1;
}
if($searchWord == null)
{
    $searchWord = "";
}

$sqlPageNo = 0;
$sqlPageNo = ($pageNo - 1) * 10;

if($filter == 100)
{
    if($fromPage == 8)
    {
        $orderBy = "dtmlist.dtmID_PK";
    }
}
if($filter == 1)
{
    if($fromPage == 8)
    {
        $orderBy = "dtmlist.dtmIsFinished";
    }
}
if($filter == 2)
{
    if($fromPage == 8)
    {
        $orderBy = "dtmlist.dtmID_PK";
    }
}
if($filter == 3)
{
    if($fromPage == 8)
    {
        $orderBy = "company.companyName";
    }
}
if($filter == 4)
{
    if($fromPage == 8)
    {
        $orderBy = "trucks.truckPlateNo";
    }
}
if($filter == 5)
{
    if($fromPage == 8)
    {
        $orderBy = "dtmlist.dtmPickupDate";
    }
}
if($filter == 6)
{
    if($fromPage == 8)
    {
        $orderBy = "dtmlist.dtmBookingDate";
    }
}
if($filter == 7)
{
    if($fromPage == 8)
    {
        $orderBy = "costcenter.costCenterName";
    }
}
if($filter == 8)
{
    if($fromPage == 8)
    {
        $orderBy = "dtmlist.dtmIsFinished";
    }
}
if($filter == 9)
{
    if($fromPage == 8)
    {
        $orderBy = "dtmlist.dtmAdhoc";
    }
}

$sql = "";
$sql2 = "";

if($fromPage == 8)
{
    $sql .= " SELECT * FROM (((((((dtmlist
    INNER JOIN company ON dtmlist.companyID_FK = company.companyID_PK)
    INNER JOIN driver ON dtmlist.driverID_FK = driver.driverID_PK)
    INNER JOIN pointzone ON dtmlist.dtmDestinationPointID_FK = pointzone.pointzoneID_PK)
    INNER JOIN trucks ON dtmlist.truckID_FK = trucks.truckID_PK)
    INNER JOIN user ON dtmlist.dtmPlannerID_FK = user.userID_PK)
    INNER JOIN zones ON dtmlist.dtmDestinationZoneID_FK = zones.zonesID_PK)
    INNER JOIN costcenter ON dtmlist.costCenterID_FK = costcenter.costCenterID_PK) ";

    $sql2 .= " SELECT COUNT(*) AS total2 FROM (((((((dtmlist
    INNER JOIN company ON dtmlist.companyID_FK = company.companyID_PK)
    INNER JOIN driver ON dtmlist.driverID_FK = driver.driverID_PK)
    INNER JOIN pointzone ON dtmlist.dtmDestinationPointID_FK = pointzone.pointzoneID_PK)
    INNER JOIN trucks ON dtmlist.truckID_FK = trucks.truckID_PK)
    INNER JOIN user ON dtmlist.dtmPlannerID_FK = user.userID_PK)
    INNER JOIN zones ON dtmlist.dtmDestinationZoneID_FK = zones.zonesID_PK)
    INNER JOIN costcenter ON dtmlist.costCenterID_FK = costcenter.costCenterID_PK) ";
}


if($searchWord != null && $searchWord != "")
{
    if($fromPage == 8)
    {
        if($filter == 1)
        {
            $sql .= " WHERE dtmlist.dtmIsFinished LIKE 0 ";
            $sql2 .= " WHERE dtmlist.dtmIsFinished LIKE 0 ";
        }
        if($filter == 2)
        {
            $sql .= " WHERE dtmlist.dtmID_PK LIKE '%".$searchWord."%' ";
            $sql2 .= " WHERE dtmlist.dtmID_PK LIKE '%".$searchWord."%' ";
        }
        if($filter == 3)
        {
            $sql .= " WHERE company.companyName LIKE '%".$searchWord."%' ";
            $sql2 .= " WHERE company.companyName LIKE '%".$searchWord."%' ";
        }
        if($filter == 4)
        {
            $sql .= " WHERE trucks.truckPlateNo LIKE '%".$searchWord."%' ";
            $sql2 .= " WHERE trucks.truckPlateNo LIKE '%".$searchWord."%' ";
        }
        if($filter == 5)
        {
            $sql .= " WHERE dtmlist.dtmPickupDate LIKE '%".date("d M Y",strtotime($searchWord))."%' ";
            $sql2 .= " WHERE dtmlist.dtmPickupDate LIKE '%".date("d M Y",strtotime($searchWord))."%' ";
        }
        if($filter == 6)
        {
            $sql .= " WHERE dtmlist.dtmBookingDate LIKE '%".date("d M Y",strtotime($searchWord))."%' ";
            $sql2 .= " WHERE dtmlist.dtmBookingDate LIKE '%".date("d M Y",strtotime($searchWord))."%' ";
        }
        if($filter == 7)
        {
            $sql .= " WHERE costcenter.costCenterName LIKE '%".$searchWord."%' ";
            $sql2 .= " WHERE costcenter.costCenterName LIKE '%".$searchWord."%' ";
        }
        if($filter == 8)
        {
            $sql .= " WHERE dtmlist.dtmIsFinished LIKE 1 ";
            $sql2 .= " WHERE dtmlist.dtmIsFinished LIKE 1 ";
        }

        if($filter == 9)
        {
            $sql .= " WHERE dtmlist.dtmAdhoc LIKE '%".$searchWord."%' ";
            $sql2 .= " WHERE dtmlist.dtmAdhoc LIKE '%".$searchWord."%' ";
        }
    }
}

if ($orderBy != "") 
{
    
    if($filter == 1)
    {
        $sql .= " ORDER BY ".$orderBy." ASC , dtmPickupDate DESC , dtmDateCreated DESC ";
        $sql2 .= " ORDER BY ".$orderBy." ASC , dtmPickupDate DESC , dtmDateCreated DESC";
    }
    else if($filter == 100)
    {
        $sql .= " ORDER BY ".$orderBy." ASC , dtmPickupDate DESC ";
        $sql2 .= " ORDER BY ".$orderBy." ASC , dtmPickupDate DESC ";
    }
    else if($filter == 3)
    {
        $sql .= " ORDER BY ".$orderBy." ASC , costCenterName ASC ";
        $sql2 .= " ORDER BY ".$orderBy." ASC , costCenterName ASC ";
    }
    else
    {
        $sql .= " ORDER BY ".$orderBy." DESC ";
        $sql2 .= " ORDER BY ".$orderBy." DESC ";
    }
}

$sql .=" LIMIT ".$sqlPageNo.",10 ";

if($condition == 1)
{
    if($fromPage == 8)
    {
        $initialSql = " SELECT COUNT(*) as total FROM (((((((dtmlist
        INNER JOIN company ON dtmlist.companyID_FK = company.companyID_PK)
        INNER JOIN driver ON dtmlist.driverID_FK = driver.driverID_PK)
        INNER JOIN pointzone ON dtmlist.dtmDestinationPointID_FK = pointzone.pointzoneID_PK)
        INNER JOIN trucks ON dtmlist.truckID_FK = trucks.truckID_PK)
        INNER JOIN user ON dtmlist.dtmPlannerID_FK = user.userID_PK)
        INNER JOIN zones ON dtmlist.dtmDestinationZoneID_FK = zones.zonesID_PK)
        INNER JOIN costcenter ON dtmlist.costCenterID_FK = costcenter.costCenterID_PK) ";
    }

    $result = mysqli_query($conn,$initialSql);
    $data = mysqli_fetch_assoc($result);
    $no_of_pages = 0;
    $no_of_pages = ceil($data['total'] / 10);
}
else
{
    $result2 = mysqli_query($conn,$sql2);
    $dataCount = mysqli_fetch_assoc($result2);
    $no_of_pages = 0;
    $no_of_pages = ceil($dataCount['total2'] / 10);
    
}

$querylisting = mysqli_query($conn,$sql);
generateConfirmationModal();
?>
<script>$("#pagination"+<?php echo $fromPage;?>+" option").remove();</script>
<style> th,td{text-align: center;}</style>
<table class="table table-sm table-hovered table-sm table-striped dtmTableNoWrap">
    <thead>
    <tr>
        <?php 
            if($fromPage == 8)
            {
                if($_SESSION['userLevel'] == 2)
                {
                ?>
                    <th colspan="17">DTM Info</th>
                <?php 
                }
                else
                {
                    ?>
                    <th colspan="16">DTM Info</th>
                    <?php
                } 
                ?>
                    <th colspan="4">Pickup</th>
                    <th colspan="4">Delivery</th>
                    <th colspan="12">TPT for Each Operations Activity</th>
                    <th colspan="3">Quantity</th>
                    <th colspan="7">Additional Info</th>
                <?php
            }
        ?>
        </tr>
        <tr>
        <?php 
            if($fromPage == 8)
            {
                if($_SESSION['userLevel'] == 2)
                {
                ?>
                    <th colspan="2" class="adminTableWidthTD">Selection</th>
                <?php 
                }
                else
                {
                    ?>
                    <th class="adminTableWidthTD">Selection</th>
                    <?php
                }
                ?>
                    <th>DTM No</th>
                    <th>VXL D/O No</th>
                    <th>DGF REF</th>
                    
                    <th>Agent</th>
                    <th>Cost Centre</th>
                    <th>Booking Date</th>
                    <th>Booking Time</th>
                    <th>Pickup Date</th>
                    <th>Pickup Time</th>
                    <th>Pickup Point</th>
                    <th>Pickup Zone</th>
                    <th>Delivery Point</th>

                    <th>Delivery Zone</th>
                    <th>Truck Plate No</th>
                    <th>Cap</th>
                    <th>Consol</th>
                    <th>Driver 1 Name</th>
                    <th>Driver 2 Name</th>
                    <th>Urgent Memo</th>
                    <th>F</th>
                    <th>Dock Time</th>
                    <th>Complete Loading Time</th>
                    <th>Departure Time From Post</th>
                    <th>Arrival Time At Post</th>
                    <th>Dock Time</th>

                    <th>Complete Unload Time</th>
                    <th>Depart Time From Post</th>
                    <th>Pick-up <br>vs<br> Arrival</th>
                    <th>Arrival <br>vs<br> Dock</th>
                    <th>Dock <br>vs<br> Complete Loading</th>
                    <th>Arrival <br>vs<br> Complete Loading</th>
                    <th>Complete Loading<br> vs<br> Departure</th>
                    <th>Departure <br>vs<br> Arrival</th>
                    <th>Arrival <br>vs<br> Dock</th>
                    <th>Dock <br>vs<br> Complete Unloading</th>

                    <th>Arrival <br>vs<br> Complete Unloading</th>
                    <th>Complete Unloading <br>vs<br> Departure</th>
                    <th>TPT Complete Trip</th>
                    <th>TPT for All</th>
                    <th>Cartons</th>
                    <th>Pallets</th>
                    <th>Cages/Crates</th>
                    <th>Trip On Time</th>
                    <th>Delay Reasons</th>
                    
                    <th>Planner</th>
                    <th>Request By</th>
                    <th>Remarks</th>
                <?php
            }
        ?>
        </tr>
  </thead>
  <tbody>
    <?php 
        if (mysqli_num_rows($querylisting) > 0) 
        {
            while($row = mysqli_fetch_array($querylisting))
            {
    ?>
    <tr>
            <?php 
                if($fromPage == 8)
                {  
                    if($_SESSION['userLevel'] == 2)
                    {
            ?>
                <td>
                    <div class="adminAlignOptionInline">
                        <form method ="POST">
                            <?php 
                            if($row['dtmIsFinished'] == 1)
                            {
                                ?> <input type="checkbox" value="<?php echo $row['dtmID_PK'];?>"  onchange="handleChangeDTM(this);" name="iscompletedtmID" checked disabled><?php
                            }
                            else
                            {
                                ?> <input type="checkbox" value="<?php echo $row['dtmID_PK'];?>"  onchange="handleChangeDTM(this);" name="iscompletedtmID"><?php
                            }
                            ?>
                        </form>
                    </div>
                </td>
                <td class="adminTableWidthTD">
                    <div class="adminAlignOptionInline">
                        <?php 
                        if($row['dtmIsFinished'] == 0)
                        {
                            ?>
                            <form action="dtmEdit.php" method="POST" class="adminformEdit">
                                <input type="hidden" name="tableType" value="<?php echo $fromPage;?>">
                                <button class="btn btn-warning edtOpt" value="<?php echo $row['dtmID_PK'];?>" name="edit">Update</button>
                            </form>
                            <?php 
                        }
                        ?>
                            
                        <!-- <button class="btn btn-danger edtOpt" value="<?php //echo $row['truckID_PK'];?>" onclick="deleteHR(<?php //echo $fromPage;?>,this.value)">Delete</button> -->
                    </div>
                </td>
                    <?php 
                    }
                    else
                    {
                        ?>
                        <td>
                            <div class="adminAlignOptionInline">
                                <form method ="POST">
                                    <?php 
                                    if($row['dtmIsFinished'] == 1)
                                    {
                                        ?> <input type="checkbox" value="<?php echo $row['dtmID_PK'];?>"  onchange="handleChangeDTM(this);" name="iscompletedtmID" checked><?php
                                    }
                                    else
                                    {
                                        ?> <input type="checkbox" value="<?php echo $row['dtmID_PK'];?>"  onchange="handleChangeDTM(this);" name="iscompletedtmID" disabled><?php
                                    }
                                    ?>
                                </form>
                            </div>
                        </td>
                        <?php
                    }
                ?>
                 <td>
                    <?php 
                        echo $row['dtmID_PK'];
                    ?>
                </td>
                <td>
                    <?php 
                    $vxlDO = "";
                    if($row['dtmAdhoc'])
                    {
                        $vxlDO = $row['dtmAdhoc'];
                    }
                        echo $vxlDO;
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['dtmDGF'];
                    ?>
                </td>
                <td>
                    <?php
                        echo $row['companyName'];
                    ?>
                </td>
                
                <td>
                    <?php 
                        echo $row['costCenterName'];
                    ?>
                </td>
                <td>
                    <?php 
                        $shipmentDate = date("d M Y",strtotime($row['dtmBookingDate']));
                        echo $shipmentDate;
                    ?>
                </td>
                <td>
                    <?php 
                        $shipmentTime = date("G:i",strtotime($row['dtmBookingTime']));
                        echo $shipmentTime;
                    ?>
                </td>
                <td>
                    <?php 
                         $pickupDate = date("d M Y",strtotime($row['dtmPickupDate']));
                         echo $pickupDate;
                    ?>
                </td>
                <td>
                    <?php 
                     
                        $pickupTime = date("G:i",strtotime($row['dtmPickupTime']));
                        echo $pickupTime;
                    ?>
                </td>
                <td>
                    <?php 
                        $costCenterDisplay = "SELECT * FROM pointzone WHERE pointzoneID_PK = ".$row['dtmOriginPointID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['pointzonePlaceName'];
                            }
                        }
                    ?>
                </td>
                <td>
                    <?php 
                        $costCenterDisplay = "SELECT * FROM zones WHERE zonesID_PK = ".$row['dtmOriginZoneID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['zonesName'];
                            }
                        }
                    ?>
                </td>
                <td>
                    <?php
                        echo $row['pointzonePlaceName'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['zonesName'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['truckPlateNo'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['loadCap'];
                    ?>
                </td>
                <td>
                    <?php 
                        if($row['isConsol'] == 1)
                        {
                            echo "CONSOL";
                        }
                        else
                        {
                            echo "-";
                        }
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['driverName'];
                    ?>
                </td>
                <td>
                    <?php 
                    if($row['driver2ID_FK'])
                    {
                        $costCenterDisplay = "SELECT driverName FROM driver WHERE driverID_PK = ".$row['driver2ID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['driverName'];
                            }
                        }
                    }
                    else
                    {
                        echo "-";
                    }
                    ?>
                </td>
                <td>
                    <?php 
                        if($row['dtmUrgentMemo'] == 1)
                        {
                            echo "yes";
                        }
                        else
                        {
                            echo "no";
                        } 
                    ?>
                </td>
                <td>
                    <?php 

                        if($row['dtmOriginF'])
                        {
                            $shipmentTime = date("G:i",strtotime($row['dtmOriginF']));
                            if($row['lockupf'])
                            {
                                echo $shipmentTime."<br> Next Day : ".date("d M Y",strtotime($row['lockupf']));
                            }
                            else
                            {
                                echo $shipmentTime;
                            }
                        }                        
                    ?>
                </td>
                <td>
                    <?php 
                    if($row['dtmOriginDockTime'])
                    {
                        $shipmentTime = date("G:i",strtotime($row['dtmOriginDockTime']));
                        if($row['lockupdockTime'])
                        {
                            echo $shipmentTime."<br> Next Day : ".date("d M Y",strtotime($row['lockupdockTime']));
                        }
                        else
                        {
                            echo $shipmentTime;
                        }
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if($row['dtmOriginCompleteLoadingTime'])
                    { 
                        $shipmentTime = date("G:i",strtotime($row['dtmOriginCompleteLoadingTime']));
                        if($row['lockupcompleteLoadingTime'])
                        {
                            echo $shipmentTime."<br> Next Day : ".date("d M Y",strtotime($row['lockupcompleteLoadingTime']));
                        }
                        else
                        {
                            echo $shipmentTime;
                        }
                    }
                    ?>
                </td>
                <td>
                    <?php 
                    if($row['dtmOriginDepartureTimeFromPost'])
                    { 
                        $shipmentTime = date("G:i",strtotime($row['dtmOriginDepartureTimeFromPost']));
                        if($row['lockupdepartureTimeFromPost'])
                        {
                            echo $shipmentTime."<br> Next Day : ".date("d M Y",strtotime($row['lockupdepartureTimeFromPost']));
                        }
                        else
                        {
                            echo $shipmentTime;
                        }
                    }
                    ?>
                </td>
                <td>
                    <?php 
                    if($row['dtmDestinationArrivalTimeAtPost'])
                    { 
                       $shipmentTime = date("G:i",strtotime($row['dtmDestinationArrivalTimeAtPost']));
                       if($row['lockuparrivalTimeAtPost'])
                        {
                            echo $shipmentTime."<br> Next Day : ".date("d M Y",strtotime($row['lockuparrivalTimeAtPost']));
                        }
                        else
                        {
                            echo $shipmentTime;
                        }
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if($row['dtmDestinationDockTime'])
                    {  
                        $shipmentTime = date("G:i",strtotime($row['dtmDestinationDockTime']));
                        if($row['lockupdestinationDockTime'])
                        {
                            echo $shipmentTime."<br> Next Day : ".date("d M Y",strtotime($row['lockupdestinationDockTime']));
                        }
                        else
                        {
                            echo $shipmentTime;
                        }
                    }
                    ?>
                </td>
                <td>
                    <?php 
                    if($row['dtmDestinationCompleteUnloadTime'])
                    { 
                        $shipmentTime = date("G:i",strtotime($row['dtmDestinationCompleteUnloadTime']));
                        if($row['lockupcompleteUnloadTime'])
                        {
                            echo $shipmentTime."<br> Next Day : ".date("d M Y",strtotime($row['lockupcompleteUnloadTime']));
                        }
                        else
                        {
                            echo $shipmentTime;
                        }
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if($row['dtmDestinationDepartTimeFromPost'])
                    { 
                        $shipmentTime = date("G:i",strtotime($row['dtmDestinationDepartTimeFromPost']));
                        if($row['lockupdepartTimeFromPost'])
                        {
                            echo $shipmentTime."<br> Next Day : ".date("d M Y",strtotime($row['lockupdepartTimeFromPost']));
                        }
                        else
                        {
                            echo $shipmentTime;
                        }
                    }
                    ?>
                </td>
                <td>
                    <?php 
                        echo "0:00";//Pick-up vs Arrival
                    ?>
                </td>
                <td>
                    <?php 
                        echo "0:00";//Arrival vs Dock
                    ?>
                </td>
                <td>
                    <?php 
                        echo compareValue($row['dtmOriginCompleteLoadingTime'],$row['dtmOriginDockTime'],$row['lockupcompleteLoadingTime'],$row['lockupdockTime'],$row['dtmPickupDate']);
                    ?>
                </td>
                <td>
                    <?php 
                        echo compareValue($row['dtmOriginCompleteLoadingTime'],$row['dtmOriginF'],$row['lockupcompleteLoadingTime'],$row['lockupf'],$row['dtmPickupDate']);
                    ?>
                </td>
                <td>
                    <?php 
                        echo compareValue($row['dtmOriginDepartureTimeFromPost'],$row['dtmOriginCompleteLoadingTime'],$row['lockupdepartureTimeFromPost'],$row['lockupcompleteLoadingTime'],$row['dtmPickupDate']);
                    ?>
                </td>
                <td>
                    <?php 
                        echo compareValue($row['dtmDestinationArrivalTimeAtPost'],$row['dtmOriginDepartureTimeFromPost'],$row['lockuparrivalTimeAtPost'],$row['lockupdepartureTimeFromPost'],$row['dtmPickupDate']);
                    ?>
                </td>
                <td>
                    <?php 
                        echo compareValue($row['dtmDestinationDockTime'],$row['dtmDestinationArrivalTimeAtPost'],$row['lockupdestinationDockTime'],$row['lockuparrivalTimeAtPost'],$row['dtmPickupDate']);
                    ?>
                </td>
                <td>
                    <?php 
                        echo compareValue($row['dtmDestinationCompleteUnloadTime'],$row['dtmDestinationDockTime'],$row['lockupcompleteUnloadTime'],$row['lockupdestinationDockTime'],$row['dtmPickupDate']);
                    ?>
                </td>
                <td>
                    <?php 
                        echo compareValue($row['dtmDestinationCompleteUnloadTime'],$row['dtmDestinationArrivalTimeAtPost'],$row['lockupcompleteUnloadTime'],$row['lockuparrivalTimeAtPost'],$row['dtmPickupDate']);
                    ?>
                </td>
                <td>
                    <?php 
                        echo compareValue($row['dtmDestinationDepartTimeFromPost'],$row['dtmDestinationCompleteUnloadTime'],$row['lockupdepartTimeFromPost'],$row['lockupcompleteUnloadTime'],$row['dtmPickupDate']);
                    ?>
                </td>
                <td>
                    <?php 
                        echo compareValue($row['dtmDestinationDepartTimeFromPost'],$row['dtmOriginF'],$row['lockupdepartTimeFromPost'],$row['lockupf'],$row['dtmPickupDate']);
                    ?>
                </td>
                <td>
                    <?php 
                        echo compareValue($row['dtmDestinationDepartTimeFromPost'],$row['dtmOriginF'],$row['lockupdepartTimeFromPost'],$row['lockupf'],$row['dtmPickupDate']);
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['dtmQuantityCarton'];
                    ?>
                </td>
                <td>
                    <?php
                        echo $row['dtmQuantityPalllets']; 
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['dtmQuantityCages'];
                    ?>
                </td>
                <td>
                    <?php 
                        if ($row['dtmTripOnTime'] == 1)
                        {
                            echo "yes";
                        }
                        else
                        {
                            echo "no";
                        }
                    ?>
                </td>
                <td>
                    <?php 
                        if( $row['dtmDelayReasons'])
                        {
                            echo $row['dtmDelayReasons'];
                        }
                        else
                        {
                            echo "-";
                        }
                        
                    ?>
                </td>
                <td>
                    <?php
                        echo $row['userName'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['dtmRequestBy'];
                    ?>
                </td>
                <td style="text-align:left;">
                    <?php 
                        if( $row['lockupf'] || $row['lockupdockTime'] || $row['lockupcompleteLoadingTime'] || $row['lockupdepartureTimeFromPost'] ||
                            $row['lockuparrivalTimeAtPost'] || $row['lockupdestinationDockTime'] || $row['lockupcompleteUnloadTime'] || $row['lockupdepartTimeFromPost'])
                        {
                            echo "->Lockup/Detention<br>->".strtoupper($row['dtmRemarks']);
                        }
                        else
                        {
                            echo "->".strtoupper($row['dtmRemarks']);
                        }
                    ?>
                </td>
            <?php
                }
            ?>
        </tr>
    <?php 
            }
        }
        else
        {
            ?>
            <tr>
                <td colspan="46" style="text-align:center;">No Records Found</td>
            </tr>
            <?php
        }
    ?>
  </tbody>
</table>
<?php
    if($condition == 1)
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$data['total']);
    }
    else
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$dataCount['total2']);
    }
?>
 