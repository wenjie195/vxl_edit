<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        if(isset($_POST['edit']))
        {   
            $edit = $_POST['edit'];
            $tableType = $_POST['tableType'];

            require 'generalFunction.php';
            $conn = connDB();
?>
<!doctype html>
<html lang="en">
    <head>
        <title>HR Edit Driver</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>
                            <?php 
                            if($tableType == 6)
                            {
                                echo "HR Edit Drivers";
                            }
                            ?>
                        </h3>
                    </div>
                    <?php   
                        generateConfirmationModal();
                        generateSimpleModal();
                    ?>
                    <div class="row adminAddMarginTop">
                        <div class="col-xl-12 row">
                        <?php 
                            if($tableType == 6)
                            {
                                $sql = "SELECT * FROM driver WHERE driverID_PK = ".$edit;
                                $query = mysqli_query($conn,$sql);
                                if (mysqli_num_rows($query) > 0) 
                                {
                                    while($urow = mysqli_fetch_array($query))
                                    {
                                ?>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                        <label for="field_1" >Driver Name</label>
                                        <input  type="text" class="form-control adminAddFormControl" id="field_1" value="<?php echo $urow['driverName'];?>">
                                </div>
                                <div class="form-group col-xl-5">
                                        <label for="field_2" >Driver Nickname</label>
                                        <input  type="text" class="form-control adminAddFormControl" id="field_2" value="<?php echo $urow['driverNickName'];?>">
                                    
                                </div>
                                <div class="col-xl-1"></div>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                        <label for="field_3" >Driver IC No</label>
                                        <input  type="text" class="form-control adminAddFormControl"  id="field_3" value="<?php echo $urow['driverICno'];?>">
                                
                                </div>
                                <div class="form-group col-xl-5">
                                        <label for="field_4" >Driver Phone No</label>
                                        <input  type="text" class="form-control adminAddFormControl"  id="field_4" value="<?php echo $urow['driverPhoneNo'];?>">
                               
                                </div>
                                <div class="col-xl-1"></div>
                                <div class="col-xl-3"></div>
                                <div class="col-xl-6 adminAddUserButton">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addHRData(<?php echo $tableType;?>,2);" value="<?php echo $edit;?>" id="idpk">Edit Driver</button>
                                </div>
                                <div class="col-xl-3"></div>
                                <?php
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
    </body>
</html>
<?php
        }
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>