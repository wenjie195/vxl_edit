<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
?>
        <html lang="en">
            <head>
                <?php require 'indexHeader.php';?>
                <title>Dashboard</title>
            </head>
            <body>
                <?php require 'indexNavbar.php';?>
                <div class="container-fluid row">
                    <div class="col"></div>
                    
                        <?php
                        if($_SESSION['userLevel'] == 1)//Admin
                        {
                            ?>
                        <div class="row col-xl-10 indexDashboardAlignCenter">
                            <div class="col-xl-4 col-md-4 col-sm-6 col-12">
                                <a href="dtmHome.php" a href="#" class="indexDashboardLinks">
                                    <img class="dashboardImages" src="./img/menuSSIP.png" >
                                    <p class="indexDashboardItem">DTM</p>
                                </a>
                            </div>
                            <div class="col-xl-4 col-md-4 col-sm-6 col-12">
                                <a href="usersHome.php" a href="#" class="indexDashboardLinks">
                                    <img class="dashboardImages" src="./img/menuAdmin.png" >
                                    <p class="indexDashboardItem">System Users</p>
                                </a>
                            </div>
                            <div class="col-xl-4 col-md-4 col-sm-6 col-12">
                                <a href="financeHome.php" a href="#" class="indexDashboardLinks">
                                    <img class="dashboardImages" src="./img/menuFinance.png" >
                                    <p class="indexDashboardItem">Finance</p>
                                </a>
                            </div>
                            <div class="col-xl-4 col-md-4 col-sm-6 col-12">
                                <a href="HRHome.php" a href="#" class="indexDashboardLinks">
                                    <img class="dashboardImages" src="./img/menuHR.png" >
                                    <p class="indexDashboardItem">Drivers & Trucks</p>
                                </a>
                            </div>
                            <div class="col-xl-4 col-md-4 col-sm-6 col-12">
                                <a href="CDHome.php" a href="#" class="indexDashboardLinks">
                                    <img class="dashboardImages" src="./img/menuReport.png" >
                                    <p class="indexDashboardItem">Coordinator Data</p>
                                </a>
                            </div>
                            <div class="col-xl-4 col-md-4 col-sm-6 col-12">
                                <a href="settingsHome.php" a href="#" class="indexDashboardLinks">
                                    <img class="dashboardImages" src="./img/menuSetting.png" >
                                    <p class="indexDashboardItem">Settings</p>
                                </a>
                            </div>
                            <?php
                        }
                        if($_SESSION['userLevel'] == 2)//Coordinator
                        {
                            ?>
                        <div class="row col-xl-10 indexDashboardAlignCenter2">
                            <div class="col-xl-2 col-md-2 col-sm-12 col-12"></div>
                            <div class="col-xl-4 col-md-4 col-sm-6 col-12">
                                <a href="dtmHome.php" a href="#" class="indexDashboardLinks">
                                    <img class="dashboardImages" src="./img/menuSSIP.png" >
                                    <p class="indexDashboardItem">DTM</p>
                                </a>
                            </div>
                            <div class="col-xl-4 col-md-4 col-sm-6 col-12">
                                <a href="settingsHome.php" a href="#" class="indexDashboardLinks">
                                    <img class="dashboardImages" src="./img/menuSetting.png" >
                                    <p class="indexDashboardItem">Settings</p>
                                </a>
                            </div>
                            <div class="col-xl-2 col-md-2 col-sm-12 col-12"></div>
                            <?php
                        }
                        
                            ?>
                    </div>
                    <div class="col"></div>
                </div>
                <?php require 'indexFooter.php';?>
            </body>
        </html>
<?php
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>