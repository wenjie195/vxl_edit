<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        if(isset($_POST['add']))
        {   
            echo $add = $_POST['add'];// --> dtmID_FK
            require 'generalFunction.php';

?>
<!doctype html>
<html lang="en">
    <head>
        <title>Add Service Fee</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>
                              Add Service Fee
                        </h3>
                    </div>
                    <?php   
                        generateConfirmationModal();
                        generateSimpleModal();
                    ?>
                    <?php
                         $conn = connDB(); 
                         $costCenterDisplay = "SELECT * FROM dtmlist WHERE dtmID_PK = ".$add;
                         $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                         if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                         {    
                              while($row = mysqli_fetch_array($costCenterDisplayQuery))
                              {
                                   ?>
                                   <table class="table dtmTableNoWrap table-sm table-hovered table-bordered table-striped table-responsive-xl  dtmTableNoWrap"style="text-align:center;">
                                        <thead>
                                             <tr>
                                                <th >Pickup Date</th>
                                                <th >Pickup Time</th>
                                                <th >From</th>
                                                <th >To</th>
                                                <th >Agent</th>
                                                <th >Truck No</th>
                                                <th >Load Capacity</th>
                                                <th >Consol Status</th>
                                             </tr>
                                        </thead>
                                        <tbody>
                                             <tr>
                                                  <td>
                                                       <?php 
                                                            $pickupDate = date("d M Y",strtotime($row['dtmPickupDate']));
                                                            echo $pickupDate;
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                            $pickupTime = date("G:i",strtotime($row['dtmPickupTime']));
                                                            echo $pickupTime;
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$row['dtmOriginPointID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['pointzonePlaceName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$row['dtmDestinationPointID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['pointzonePlaceName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php
                                                       $companyID_FK =  $row['companyID_FK']; 
                                                       $costCenterDisplay = "SELECT companyName FROM company WHERE companyID_PK = ".$row['companyID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['companyName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php
                                                       $truckID_FK =  $row['truckID_FK'];
                                                       $costCenterDisplay = "SELECT truckPlateNo FROM trucks WHERE truckID_PK = ".$row['truckID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['truckPlateNo'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                       echo $loadCap = $row['loadCap'];
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                       $isConsol = $row['isConsol'];
                                                         if($row['isConsol'] == 1)
                                                         {
                                                             echo "CONSOL";
                                                         }
                                                         else
                                                         {
                                                             echo "-";
                                                         }
                                                       ?>
                                                  </td>
                                             </tr>
                                        </tbody>
                                   </table>
                                   <table class="table dtmTableNoWrap table-sm table-hovered table-bordered table-striped table-responsive-xl  dtmTableNoWrap"style="text-align:center;">
                                        <thead>
                                             <tr>
                                                <th >Driver 1</th>
                                                <th >Driver 2</th>
                                                <th >From Zones</th>
                                                <th >To Zones</th>
                                             </tr>
                                        </thead>
                                        <tbody>
                                             <tr>
                                                  
                                                  <td class="text-center">
                                                       <?php 
                                                       $costCenterDisplay = "SELECT driverName FROM driver WHERE driverID_PK = ".$row['driverID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo  $urow1['driverName'];
                                                                 $d1= $row['driverID_FK'];
                                                            }
                                                       }
                                                    ?>
                                                    </td>
                                                    <td class="text-center">
                                                    <?php
                                                       if($row['driver2ID_FK'])
                                                       {
                                                        $secDriver = $row['driver2ID_FK'];
                                                            $costCenterDisplay = "SELECT driverName FROM driver WHERE driverID_PK = ".$row['driver2ID_FK'];
                                                            $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                            if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                            {
                                                                while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                                {
                                                                    echo $urow1['driverName'];
                                                                    $d2= $row['driver2ID_FK'];
                                                                }
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                    <?php 
                                                    $or = $row['dtmOriginZoneID_FK'];
                                                        $costCenterDisplay = "SELECT * FROM zones WHERE zonesID_PK = ".$row['dtmOriginZoneID_FK'];
                                                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                        {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                echo $urow1['zonesName'];
                                                            }
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $dest = $row['dtmDestinationZoneID_FK'];
                                                        $costCenterDisplay = "SELECT * FROM zones WHERE zonesID_PK = ".$row['dtmDestinationZoneID_FK'];
                                                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                        {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                echo $urow1['zonesName'];
                                                            }
                                                        }
                                                    ?>
                                                </td>
                                             </tr>
                                        </tbody>
                                   </table>
                                   <?php
                              }
                         }
                         if(isset($secDriver))
                         {
                             $driverN =2;
                            $costCenterDisplay = "SELECT * FROM servicefeeratesplace WHERE origin = '".$or."' AND destination = '".$dest."' AND loadTransport = '".$loadCap."' AND noOfDrivers = '2' ORDER BY dateCreated DESC LIMIT 1";
                         }
                         else
                         {
                            $driverN =1;
                            $costCenterDisplay = "SELECT * FROM servicefeeratesplace WHERE origin = '".$or."' AND destination = '".$dest."' AND loadTransport = '".$loadCap."' AND noOfDrivers = '1' ORDER BY dateCreated DESC LIMIT 1";
                         }
                       
                        
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {    
                              while($row2 = mysqli_fetch_array($costCenterDisplayQuery))
                              {
                                 $rateOfservice = $row2['rates'];
                              }
                        }
                        else
                        {
                            $rateOfservice = 0;
                        }
                    ?>
                    <div class="row adminAddMarginTop">
                        <div class="col-xl-12 row">
                         <!-- <div class="col-xl-1"></div>
                         <div class="form-group col-xl-5"> 
                              <label for="field_1" >Gross DSF</label>
                              <input  type="text" class="form-control adminAddSetPadding" id="field_1" disabled value = "<?php //echo $rateOfservice;?>">
                         </div>
                         <div class="form-group col-xl-5">
                            <label for="field_2" >Night </label>
                            <select class="form-control adminAddSetPadding" id="field_2">
                                <option disabled selected hidden>-- Pick one of the following --</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                         </div>
                         <div class="col-xl-1"></div>
                         
                         <div class="col-xl-1"></div>
                         <div class="form-group col-xl-5">
                            <label for="field_3" >Detention </label>
                            <input type="number" class="form-control adminAddSetPadding" id="field_3" min="0">
                        </div>
                         <div class="form-group col-xl-5">
                         <label for="field_4" >Sunday </label>
                            <select class="form-control adminAddSetPadding" id="field_4">
                                <option disabled selected hidden>-- Pick one of the following --</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                         </div>
                         <div class="col-xl-1"></div>

                         <div class="col-xl-1"></div>
                         <div class="form-group col-xl-5">
                         <label for="field_5" >Public Holiday </label>
                            <select class="form-control adminAddSetPadding" id="field_5">
                                <option disabled selected hidden>-- Pick one of the following --</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                         </div>
                         <div class="col-xl-6"></div>

                         <div class="col-xl-3"></div>
                         <div class="col-xl-6 adminAddUserButton">
                         <input type="hidden" id="dtmID" value="<?php //echo $add;?>">
                         <input type="hidden" id="driver1" value="<?php //echo $d1;?>">
                         <input type="hidden" id="driver2" value="<?php  //if(isset($d2)){echo $d2;}?>">
                         <input type="hidden" id="driverNo" value="<?php //echo $driverN;?>">
                              <button class="btn formButtonPrimary indexSubmitButton" onclick="addSerFee(<?php //echo $add;?>,1);">Add Service Fee</button>
                         </div>
                         <div class="col-xl-3"></div> -->

                         <?php 
                         $costCenterDisplay = "SELECT * FROM additionalservicefee WHERE loadTrans = '".$loadCap."'";
                         $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                         if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                         {    
                              while($row2 = mysqli_fetch_array($costCenterDisplayQuery))
                              {
                                   $overnight = $row2['overnight'];
                                   $nightDeliveries = $row2['nightDeliveries'];
                                   $sunday = $row2['sunday'];
                                   $public = $row2['public'];
                                   $cancel = $row2['cancel'];
                                   $lockupDay = $row2['lockupDay'];
                                   $lockupNight = $row2['lockupNight'];
                                   $consolVal = $row2['consol'];
                              }
                         }
                         else
                         {
                                   $overnight = 0;
                                   $nightDeliveries = 0;
                                   $sunday = 0;
                                   $public = 0;
                                   $cancel = 0;
                                   $lockupDay = 0;
                                   $lockupNight = 0;
                                   $consolVal = 0;
                                   echo $conn->error;
                         }
                         
                         ?>
                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <?php 
                              if($isConsol == 1)
                              {
                                   ?> 
                                        <p>Consol Rates DSF:</p>  
                                   <?php 
                              }
                              else
                              {
                                   ?> 
                                        <p>Gross Rates DSF :</p>
                                   <?php 
                              }
                              ?>

                         </div>
                         <div class="col-xl-2">
                              <?php 
                              if($isConsol == 1)
                              {
                                   $rateOfservice = $consolVal;
                                   ?>
                                        <input type="number" class="form-control adminAddSetPadding" id="dsfGross" min="0" value ="<?php echo sprintf('%0.2f',$rateOfservice);?>" onchange="detentionCharges(this.value,document.getElementById('detentioncharge'))">
                                   <?php
                              }
                              else
                              {
                                   ?>
                                        <input type="number" class="form-control adminAddSetPadding" id="dsfGross" min="0" value ="<?php echo sprintf('%0.2f',$rateOfservice);?>" onchange="detentionCharges(this.value,document.getElementById('detentioncharge'))">
                                   <?php
                                   
                              }
                              ?>
                         </div>     
                         <div class="col-xl-4"></div>

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <p>Overnight Charges :</p>
                         </div>
                         <div class="col-xl-2">
                              <input type="number" class="form-control adminAddSetPadding" id="dsfOvernight" min="0" value="0" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" disabled>
                         </div>     
                         <div class="col-xl-4">
                              <label class="cb-container cb-padding">
                                   <input type="checkbox" name="overnight" id="overnight" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $overnight;?>',this,document.getElementById('dsfOvernight'));" value="<?php echo $overnight;?>">
                                   <span class="checkmark"></span>
                              </label>
                         </div>

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <p>Night Deliveries Charges :</p>
                         </div>
                         <div class="col-xl-2">
                              <input type="number" class="form-control adminAddSetPadding" id="dsfNightDelivery" min="0" value="0" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" disabled>
                         </div>     
                         <div class="col-xl-4">
                              <label class="cb-container cb-padding">
                                   <input type="checkbox" name="nightDeliveries" id="nightDeliveries" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $nightDeliveries;?>',this,document.getElementById('dsfNightDelivery'));" value="<?php echo $nightDeliveries;?>">
                                   <span class="checkmark"></span>
                              </label>
                         </div>  

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <p>Sunday/Rest Day Charges :</p>
                         </div>
                         <div class="col-xl-2">
                             <input type= "number" class="form-control adminAddSetPadding" id="dsfSunday" min="0" value="0" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" disabled>
                         </div>     
                         <div class="col-xl-4">
                              <label class="cb-container cb-padding">
                                   <input type="checkbox" name="sunday" id="sunday" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $sunday;?>',this,document.getElementById('dsfSunday'));" value="<?php echo $sunday;?>">
                                   <span class="checkmark"></span>
                              </label>
                         </div> 

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <p>Public Day Charges :</p>
                         </div>
                         <div class="col-xl-2">
                              <input type="number" class="form-control adminAddSetPadding" id="dsfPublic" min="0" value="0" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" disabled>
                         </div>     
                         <div class="col-xl-4">
                              <label class="cb-container cb-padding">
                                   <input type="checkbox" name="public" id="public" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $public;?>',this,document.getElementById('dsfPublic'));" value="<?php echo $public;?>">
                                   <span class="checkmark"></span>
                              </label>
                         </div> 

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <p>Cancellation Charges :</p>
                         </div>
                         <div class="col-xl-2">
                              <input type="number" class="form-control adminAddSetPadding" id="dsfCancellation" min="0" value="0" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" disabled>
                         </div>     
                         <div class="col-xl-4">
                              <label class="cb-container cb-padding">
                                   <input type="checkbox" name="cancelllation" id="cancelllation" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $cancel;?>',this,document.getElementById('dsfCancellation'));" value="<?php echo $cancel;?>">
                                   <span class="checkmark"></span>
                              </label>
                         </div> 

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <p>Lockup <br>(Day) :</p>
                         </div>
                         <div class="col-xl-2">
                              <input type="number" class="form-control adminAddSetPadding" id="dsfLockupDay" min="0" value="0" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" disabled>
                         </div>     
                         <div class="col-xl-4">
                              <label class="cb-container cb-padding">
                                   <input type="checkbox" name="lockupDay" id="lockupDay" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $lockupDay;?>',this,document.getElementById('dsfLockupDay'));" value="<?php echo $lockupDay;?>">
                                   <span class="checkmark"></span>
                              </label>
                         </div> 

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <p>Lockup <br>(Night):</p>
                         </div>
                         <div class="col-xl-2">
                              <input type="number" class="form-control adminAddSetPadding" id="dsfLockupNight" min="0" value="0" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" disabled>
                         </div>     
                         <div class="col-xl-4">
                              <label class="cb-container cb-padding">
                                   <input type="checkbox" name="lockupNight" id="lockupNight" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $lockupNight;?>',this,document.getElementById('dsfLockupNight'));" value="<?php echo $lockupNight;?>">
                                   <span class="checkmark"></span>
                              </label>
                         </div> 

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2"><label for="detentioncharge" >Detention </label></div>
                         <div class="form-group col-xl-2">
                            <input type="number" class="form-control adminAddSetPadding" id="detentioncharge" min="0" onchange="detentionCharges(document.getElementById('dsfGross').value,this)" value="0">
                         </div>
                         <div class="col-xl-4"></div>
                         
                          

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <p>TOTAL:</p>
                         </div>
                         <div class="col-xl-2">
                              <p id="option8" >$ <?php echo sprintf('%0.2f',$rateOfservice);?></p>
                         </div>     
                         <div class="col-xl-4"></div>

                         <div class="col-xl-3"></div>
                         <div class="form-group col-xl-6">
                              <label for="remarkDsf" >Remark</label>
                              <input type="text" class="form-control adminAddSetPadding" id="remarkDsf">
                         </div>
                         <div class="col-xl-3"></div>
                          
                         <div class="col-xl-3"></div>
                         <div class="col-xl-6 text-center mt-3 mb-5">
                              <input type="hidden" id="loadCap" value="<?php echo $loadCap;?>">
                              <input type="hidden" id="grossDsf" value="<?php echo $rateOfservice;?>">
                              <input type="hidden" id="dtmID" value="<?php echo $add;?>">
                              <input type="hidden" id="driver1" value="<?php echo $d1;?>">
                              <input type="hidden" id="driver2" value="<?php  if(isset($d2)){echo $d2;}?>">
                              <input type="hidden" id="driverNo" value="<?php echo $driverN;?>">
                              <button class="btn formButtonPrimary indexSubmitButton" onclick="addSerFee(<?php echo $add;?>,1);">Add Service Fee</button>
                         </div>     
                         <div class="col-xl-3"></div> 
                         
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
        <script>
            $(document).ready(function()
            {
                
            });
            
        </script>
    </body>
</html>
<?php
        }
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>