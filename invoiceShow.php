<?php

require 'generalFunction.php';
session_start();
if(isset($_POST['fromPage']))
{    
    //CONTINUE HERE And change file format xls,
?>

<div class="container-fluid">
    
    <div class="row">
        <div class="col-xl-12 adminAlignThings">
                <button class="btn btn-warning"><a href="invoicePrint.php">Print Summary</a></button>
           
            <p class="paginationClass">
                Page : 
                <select onchange="checkCondition(this.value,null,null,0,<?php echo $_POST['fromPage'];?>);" id="pagination<?php echo $_POST['fromPage']; ?>"></select> 
                of 
            </p>
            <p class="paginationClass paginationClassTotal " id="totalpages<?php echo $_POST['fromPage']; ?>"></p>
            <p class="filterPara dtmFilterPara">Filter By: </p>
            <div class="adminAlignRight">  
                <select class="filterClass" id="filter<?php echo $_POST['fromPage']; ?>" onchange="checkCondition(null,this.value,null,0,<?php echo $_POST['fromPage'];?>);">
                    <option selected disabled>Filter By</option>
                    <option value="1">Date Created</option>
                    <option value="2">Invoice No</option>
                    <option value="3">Non Selected Invoice</option>
                    <option value="4">Selected Invoice</option>
                    <option value="5">Cost Center</option>
                    <option value="6">Trucks</option>
                    <option value="7">Agents</option>
                    <option value="8">DTM No</option>
                    <option value="9">VXL D/O Number</option>
                </select>
                <input type="text" id="search<?php echo $_POST['fromPage']; ?>" class="searchClass" placeholder="Search">
                <button class="searchClass searchClassButton btn btn-primary " onclick="checkCondition(null,null,this.value,0,<?php echo $_POST['fromPage'];?>);">Search</button>
            </div>
        </div>
    </div>
</div>
<div style="overflow-x:auto;margin-top:20px;" id="getTable<?php echo $_POST['fromPage'];?>"></div>
<?php
}
?>