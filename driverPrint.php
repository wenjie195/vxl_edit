<?php
date_default_timezone_set("Asia/Kuala_Lumpur");
$todayDate = date("Ymd_his");
$filename = 'filename="DriverList.xlsx"';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; '.$filename);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;

define('NUMBER_OF_COLUMNS', 37); 
require 'generalFunction.php';
require 'phpexcel/vendor/autoload.php';
ob_end_clean();


$conn = connDB();
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$sheet->setCellValue('A1', 'No');
$sheet->getColumnDimension('A')->setWidth(15);
$sheet->setCellValue('B1', 'Driver Name');
$sheet->getColumnDimension('B')->setWidth(50);
$sheet->setCellValue('C1', 'Driver IC NO');
$sheet->getColumnDimension('C')->setWidth(20);
$sheet->setCellValue('D1', 'Driver Phone No');
$sheet->getColumnDimension('D')->setWidth(20);

$sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
$sheet->getStyle('B1')->getAlignment()->setHorizontal('left');
$sheet->getStyle('C1')->getAlignment()->setHorizontal('center');
$sheet->getStyle('D1')->getAlignment()->setHorizontal('center');


$sqlo = " SELECT * FROM driver WHERE showThis = 1 ";
     
$result = mysqli_query($conn,$sqlo);
$bilangan = 1;

if (mysqli_num_rows($result) > 0) 
{
     $line = 2;
     while($row = mysqli_fetch_array($result))
     {
          $sheet->setCellValue('A'.$line, $bilangan);
          $sheet->setCellValue('B'.$line, $row['driverName']);
          $sheet->setCellValueExplicit('C'.$line,$row['driverICno'],\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
          $sheet->setCellValue('D'.$line, $row['driverPhoneNo']);

          $sheet->getStyle('A'.$line)->getAlignment()->setHorizontal('center');
          $sheet->getStyle('B'.$line)->getAlignment()->setHorizontal('left');
          $sheet->getStyle('C'.$line)->getAlignment()->setHorizontal('center');
          $sheet->getStyle('D'.$line)->getAlignment()->setHorizontal('center');

          $bilangan++;
          $line++;
     }
}

$writer = new Xlsx($spreadsheet);
$writer->save('php://output');

?>