<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        if(isset($_POST['add']))
        {   
            $add = $_POST['add'];
            require 'generalFunction.php';

?>
<!doctype html>
<html lang="en">
    <head>
        <title>HR Add Truck</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>
                            <?php 
                            if($add == 7)
                            {
                                echo "HR Add Trucks";
                            }
                            ?>
                        </h3>
                    </div>
                    <?php   
                        generateConfirmationModal();
                        generateSimpleModal();
                    ?>
                    <div class="row adminAddMarginTop">
                        <div class="col-xl-12 row">
                        <?php 
                            if($add == 7)
                            {
                                ?>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                        <label for="field_1" >Plate No</label>
                                        <input  type="text" class="form-control adminAddSetPadding" id="field_1" >
                                </div>
                                <div class="form-group col-xl-5">
                                        <label for="field_2" >Capacity</label>
                                        <select class="form-control adminAddSetPadding" id="field_2">
                                            <option disabled selected hidden>-- Pick one capacity --</option>
                                            <option value="20 Ft / 10 Ton">20 Ft / 10 Ton</option>
                                            <option value="1 Tonner">1 Tonner</option>
                                            <option value="3 Ton"> 3 Ton</option>
                                            <option value="40 Ft / Q6"> 40 Ft / Q6</option>
                                            <option value="40 Ft / Q6 Air-Ride">40 Ft / Q6 Air-Ride</option>
                                            <option value="45 Ft / Q7">45 Ft / Q7</option>
                                            <option value="OTHERS">OTHERS</option>
                                        </select>
                                </div>
                                <div class="col-xl-1"></div>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                        <label for="field_3" >Made</label>
                                        <input  type="text" class="form-control adminAddSetPadding"  id="field_3" >
                                </div>
                                <div class="form-group col-xl-5">
                                        <label for="field_4" >Model</label>
                                        <input  type="text" class="form-control adminAddSetPadding"  id="field_4" >
                               
                                </div>
                                <div class="col-xl-1"></div>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                        <label for="field_5" >Custom Bond</label>
                                        <input  type="text" class="form-control adminAddSetPadding"  id="field_5" >
                                
                                </div>
                                <div class="form-group col-xl-5">
                                        <label for="field_6" >Expired Custom Bond Date</label>
                                        <input  type="text" class="form-control adminAddSetPadding"  id="field_6" >
                                </div>
                                <div class="col-xl-1"></div>
                                <div class="col-xl-3"></div>
                                <div class="col-xl-6 adminAddUserButton">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addHRData(<?php echo $add;?>,1);">Create New Truck Record</button>
                                </div>
                                <div class="col-xl-3"></div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
        <script>
        $(document).ready(function()
        {
            var picker = new Pikaday({
                    field: document.getElementById('field_6'),
                    format: 'YYYYMMDD',
                    onSelect: function() {
                        console.log(this.getMoment().format('Do MMMM YYYY'));
                    }
                });
        });
        </script>
    </body>
</html>
<?php
        }
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>