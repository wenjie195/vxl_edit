<?php
//Start the session
session_start();

//Check f the session is empty/exist or not
if(!empty($_SESSION))
{
     require 'generalFunction.php';
     $conn = connDB();

    ?>
    <!doctype html>
    <html lang="en">
    <head>
        <title>Invoice Print</title>
        <?php require 'indexHeader.php';?>
        <style>
        .dsfPagination {
            margin-left: 25px;
        }
        .dsfFilterPara {
            margin-left: 350px;
        }
    </style>
    </head>
    <body>
    <?php require 'indexNavbar.php';?>
    <div class="container-fluid">
        <div class="row">
            <?php require 'indexSidebar.php';
            generateSimpleModal();
            if($_GET['message'])
            {
                 if($_GET['message']== 1) 
                 {
                    putNotice("Notice","There are no Invoice No  with this Number!");
                 }
            }
            ?>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h3>Print Invoice</h3>
                </div>
                <div class="row">
                    <form class="col-xl-12 row" method="POST" action="printInvoiceByCostCenter.php">
                         <div class="col-xl-1" ></div>
                         <div class="form-group col-xl-5">
                              <label for="fromDate" >Pickup Date (FROM)</label>
                              <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="fromDate" name="fromDate">
                         </div>
                         <div class="form-group col-xl-5">
                              <label for="toDate" >Pickup Date (TO)</label>
                              <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="toDate" name="toDate">
                         </div>
                         <div class="col-xl-1" ></div>

                         <div class="col-xl-1" ></div>
                         <div class="form-group col-xl-5">
                              <label for="company">Select Agent</label>
                              <select class="form-control adminAddSetPadding" id="company" name="company" onchange="getThisCompanyToChangeCostCenter(this,1,null,1);">
                                   <option disabled selected hidden>-- Pick one company --</option>
                                   <?php

                                   $sql_select_costCenter = "SELECT * FROM company WHERE showThis = 1";
                                   $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                   if (mysqli_num_rows($result_select_costCenter) > 0)
                                   {
                                        // output data of each row
                                        while($row = mysqli_fetch_assoc($result_select_costCenter))
                                        {
                                             echo '<option value="'.$row["companyID_PK"].'">'.$row["companyName"].' </option>';
                                        }
                                   }
                                   ?>
                              </select>
                         </div>
                         <div class="form-group col-xl-5" id="showCostCenterByCompany"></div>
                         <div class="col-xl-1" ></div>

                         <div class="col-xl-1" ></div>
                         <div class="form-group col-xl-5">
                              <label for="invoiceNo" >Invoice No</label>
                              <input type="text" class="form-control adminAddSetPadding " placeholder="" id="invoiceNo" name="invoiceNo">
                         </div>
                         <div class="col-xl-6" ></div>
                         

                         <div class="col-xl-4"></div>
                         <div class="col-xl-4 text-center mt-5">
                              <button class="btn formButtonPrimary " onclick="checkInvoicePrint();" style="width: -webkit-fill-available;">Print Invoice</button>
                         </div>
                         <div class="col-xl-4"></div>
                    </form>
               </div>
               <div class=" mt-5 d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h3>Print All Unselected</h3>
                </div>
                <div class="row">
                    <form class="col-xl-12 row" action="printInv.php" method="POST">
                         <div class="col-xl-1" ></div>
                         <div class="form-group col-xl-5">
                              <label for="fromDate1" >Pickup Date (FROM)</label>
                              <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="fromDate1" name="fromDate1">
                         </div>
                         <div class="form-group col-xl-5">
                              <label for="toDate1" >Pickup Date (TO)</label>
                              <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="toDate1" name="toDate1">
                         </div>
                         <div class="col-xl-1" ></div>

                         <div class="col-xl-1" ></div>
                         <div class="form-group col-xl-5">
                              <label class="cb-container cb-padding">
                                   <input type="checkbox" name="isCompany" id="isCompany" >
                                   <span class="checkmark"></span>
                              </label>
                              <label for="company1">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSelect Agent</label>
                              <select class="form-control adminAddSetPadding" id="company1" name="company1" onchange="getThisCompanyToChangeCostCenter(this,1,null,2);">
                                   <option disabled selected hidden>-- Pick one company --</option>
                                   <?php

                                   $sql_select_costCenter = "SELECT * FROM company WHERE showThis = 1";
                                   $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                   if (mysqli_num_rows($result_select_costCenter) > 0)
                                   {
                                        // output data of each row
                                        while($row2 = mysqli_fetch_assoc($result_select_costCenter))
                                        {
                                             echo '<option value="'.$row2["companyID_PK"].'">'.$row2["companyName"].' </option>';
                                        }
                                   }
                                   ?>
                              </select>
                         </div>
                         <div class="form-group col-xl-5" id="showCostCenterByCompany1"></div>
                         <div class="col-xl-1" ></div>

                         <div class="col-xl-4"></div>
                         <div class="col-xl-4 text-center mt-5">
                              <button class="btn btn-warning mt-5 mb-5" value="21" name="add" style="width: -webkit-fill-available;">
                                   Print Unselected Invoice
                              </button>
                            </div>
                        <div class="col-xl-4"></div>
                    </form>
                </div>
            </main>
        </div>
    </div>
    <?php require 'indexFooter.php';?>
    <script>
        $(document).ready(function()
        {
             let from,to;
             let from1,to1;
             from = initializeDate(document.getElementById('fromDate'),from);
             to = initializeDate(document.getElementById('toDate'),to); 

             from1 = initializeDate(document.getElementById('fromDate1'),from1);
             to1 = initializeDate(document.getElementById('toDate1'),to1); 

             
        });
        function initializeDate(thisId,setVariable)
        {
            var setVariable = new Pikaday({
                field: thisId,
                format: 'YYYYMMDD',
                onSelect: function() {
                    // console.log(this.getMoment().format('Do MMMM YYYY'));
                }
            });
            return setVariable;
        }
    </script>
    </body>
    </html>
    <?php
}
else
{
    // Go back to index page
    // NOTE : MUST PROMPT ERROR
    header('Location:index.php');
}
?>