<?php
require 'generalFunction.php';
$conn = connDB();

$fromPage = $_POST['fromPage'];
$condition = $_POST['condition'];
$pageNo = $_POST['pageNo'];
$filter = $_POST['filter'];
$searchWord = $_POST['searchWord'];

// echo $fromPage;
// echo $condition;
// echo $pageNo;
// echo $filter;
// echo " =".$searchWord."=";

if($filter == null)
{
    $filter= 1;
}
if($pageNo == null)
{
    $filter= 1;
}
if($searchWord == null)
{
    $searchWord = "";
}

$sqlPageNo = 0;
$sqlPageNo = ($pageNo - 1) * 10;

if($filter == 1)
{
    if($fromPage == 7)
    {
        $orderBy = "truckDateCreated";
    }
}
if($filter == 2)
{
    if($fromPage == 7)
    {
        $orderBy = "truckPlateNo";
    }
}
if($filter == 3)
{
    if($fromPage == 7)
    {
        $orderBy = "truckMade";
    }
}
if($filter == 4)
{
    if($fromPage == 7)
    {
        $orderBy = "truckCapacity";
    }
}
if($filter == 5)
{
    if($fromPage == 7)
    {
        $orderBy = "truckCustomExpired";
    }
}

$sql = "";
$sql2 = "";

if($fromPage == 7)
{
    $sql .= " SELECT * FROM trucks WHERE showThis = 1 ";
    $sql2 .= " SELECT COUNT(*) as total2 FROM trucks WHERE showThis = 1 ";
}


if($searchWord != null && $searchWord != "")
{
    if($fromPage == 7)
    {
        $sql .= " AND truckPlateNo LIKE '%".$searchWord."%' OR truckMade LIKE '%".$searchWord."%' OR truckCapacity LIKE '%".$searchWord."%' ";
        $sql2 .= " AND truckPlateNo LIKE '%".$searchWord."%' OR truckMade LIKE '%".$searchWord."%' OR truckCapacity LIKE '%".$searchWord."%' ";
    }
}

if ($orderBy != "") 
{
    if($filter == 1 )
    {
        $sql .= " ORDER BY ".$orderBy." DESC ";
        $sql2 .= " ORDER BY ".$orderBy." DESC ";
    }
    else
    {
        $sql .= " ORDER BY ".$orderBy." ASC ";
        $sql2 .= " ORDER BY ".$orderBy." ASC ";
    }
}

$sql .=" LIMIT ".$sqlPageNo.",10 ";
// echo "<br>'".$sql. "'<br>'".$sql2."'";

if($condition == 1)
{
    if($fromPage == 7)
    {
        $initialSql = " SELECT COUNT(*) as total from trucks WHERE showThis = 1 ";
    }

    $result = mysqli_query($conn,$initialSql);
    $data = mysqli_fetch_assoc($result);
    $no_of_pages = 0;
    $no_of_pages = ceil($data['total'] / 10);
}
else
{
    $result2 = mysqli_query($conn,$sql2);
    $dataCount = mysqli_fetch_assoc($result2);
    $no_of_pages = 0;
    $no_of_pages = ceil($dataCount['total2'] / 10);
    
}

$querylisting = mysqli_query($conn,$sql);

generateDeleteModal($fromPage);
generateConfirmationDeleteModal($fromPage);
?>
<script>$("#pagination"+<?php echo $fromPage;?>+" option").remove();</script>
<table class="table table-sm table-hovered table-striped table-responsive-xl  dtmTableNoWrap removebottommargin">
    <thead>
        <tr>
        <?php 
            if($fromPage == 7)
            {
                ?>
                    <th class="adminTableWidthTD">Selection</th>
                    <th>Truck Plate No</th>
                    <th>Truck Capacity</th>
                    <th>Truck Made</th>
                    <th>Truck Model</th>
                    <th>Truck Custom Bond</th>
                    <th>Expired Custom Bond Date</th>
                <?php
            }
        ?>
        </tr>
  </thead>
  <tbody>
    <?php 
        if (mysqli_num_rows($querylisting) > 0) 
        {
            while($row = mysqli_fetch_array($querylisting))
            {
    ?>
    <tr>
            <?php 
                if($fromPage == 7)
                {  
            ?>
                <td class="adminTableWidthTD">
                    <div class="adminAlignOptionInline">
                        <form action="truckEdit.php" method="POST" class="adminformEdit">
                            <input type="hidden" name="tableType" value="<?php echo $fromPage;?>">
                            <button class="btn btn-warning edtOpt" value="<?php echo $row['truckID_PK'];?>" name="edit">Update</button>
                        </form>
                        <button class="btn btn-danger edtOpt" value="<?php echo $row['truckID_PK'];?>" onclick="deleteHR(<?php echo $fromPage;?>,this.value)">Delete</button>
                    </div>
                </td>
                <td>
                    <?php 
                        echo $row['truckPlateNo'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['truckCapacity'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['truckMade'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['truckModel'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['truckCustomBond'];
                    ?>
                </td>
                <td>
                    <?php 
                     
                        $customBondExpiredDate = date("d M Y",strtotime($row['truckCustomExpired']));
                        echo $customBondExpiredDate;
                    ?>
                </td>
            <?php
                }
            ?>
        </tr>
    <?php 
            }
        }
        else
        {
            ?>
            <tr>
                <td colspan="7" style="text-align:center;">No Records Found</td>
            </tr>
            <?php
        }
    ?>
  </tbody>
</table>
<?php
    if($condition == 1)
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$data['total']);
    }
    else
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$dataCount['total2']);
    }
?>
 