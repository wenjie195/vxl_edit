<script>

</script>
<?php
require 'generalFunction.php';
$conn = connDB();
session_start();
$fromPage = $_POST['fromPage'];
$condition = $_POST['condition'];
$pageNo = $_POST['pageNo'];
$filter = $_POST['filter'];
$searchWord = $_POST['searchWord'];

// echo $condition;
// echo $pageNo;
// echo $filter;
// echo " =".$searchWord."=";

if($filter == null)
{
    $filter= 1;
}
if($pageNo == null)
{
    $filter= 1;
}
if($searchWord == null)
{
    $searchWord = "";
}

$sqlPageNo = 0;
$sqlPageNo = ($pageNo - 1) * 10;

if($filter == 1)
{
     $orderBy = "adddriverservicefee.dateCreated";
}
if($filter == 2)
{
     $orderBy = "trucks.truckPlateNo";
}

if($filter == 3)
{
     $orderBy = "dtmlist.dtmPickupDate";
}

if($filter == 4)
{
     $orderBy = "company.companyName";
}


if(isset($_SESSION['thisDriverID_PK']))
{
     $thisDriver =  $_SESSION['thisDriverID_PK'];
}

$sql = "";
$sql2 = "";


$sql .= " SELECT * FROM ((((adddriverservicefee 
INNER JOIN driver ON adddriverservicefee.driverID_FK = driver.driverID_PK)
INNER JOIN dtmlist ON adddriverservicefee.dtmID_FK = dtmlist.dtmID_PK) 
INNER JOIN trucks ON dtmlist.truckID_FK = trucks.truckID_PK) 
INNER JOIN company ON dtmlist.companyID_FK = company.companyID_PK) 
WHERE adddriverservicefee.driverID_FK = '$thisDriver' AND adddriverservicefee.dsfComplete = 1
";

$sql2 .= " SELECT COUNT(*) as total2 FROM ((((adddriverservicefee 
INNER JOIN driver ON adddriverservicefee.driverID_FK = driver.driverID_PK)
INNER JOIN dtmlist ON adddriverservicefee.dtmID_FK = dtmlist.dtmID_PK) 
INNER JOIN trucks ON dtmlist.truckID_FK = trucks.truckID_PK) 
INNER JOIN company ON dtmlist.companyID_FK = company.companyID_PK) 
WHERE adddriverservicefee.driverID_FK = '$thisDriver'  AND adddriverservicefee.dsfComplete = 1
";

if($searchWord != null && $searchWord != "")
{


    if($filter == 2)
    {
        $sql .= " AND trucks.truckPlateNo LIKE '%".$searchWord."%'  ";
        $sql2 .= " AND trucks.truckPlateNo LIKE '%".$searchWord."%'  ";
    }

    if($filter == 4)
    {
        $sql .= " AND company.companyName LIKE '%".$searchWord."%'  ";
        $sql2 .= " AND company.companyName LIKE '%".$searchWord."%'  ";
    }
}

if ($orderBy != "") 
{
    if($filter == 1)
    {
        $sql .= " ORDER BY ".$orderBy." DESC ";
        $sql2 .= " ORDER BY ".$orderBy." DESC ";
    }
    else
    {
        $sql .= " ORDER BY ".$orderBy." ASC ";
        $sql2 .= " ORDER BY ".$orderBy." ASC ";
    }
}

$sql .=" LIMIT ".$sqlPageNo.",10 ";

if($condition == 1)
{

     $initialSql = "SELECT COUNT(*) as total from ((((adddriverservicefee 
     INNER JOIN driver ON adddriverservicefee.driverID_FK = driver.driverID_PK)
     INNER JOIN dtmlist ON adddriverservicefee.dtmID_FK = dtmlist.dtmID_PK) 
     INNER JOIN trucks ON dtmlist.truckID_FK = trucks.truckID_PK) 
     INNER JOIN company ON dtmlist.companyID_FK = company.companyID_PK) 
     WHERE adddriverservicefee.driverID_FK = '$thisDriver' AND dsfComplete = 1
     ";

    $result = mysqli_query($conn,$initialSql);
    $data = mysqli_fetch_assoc($result);
    $no_of_pages = 0;
    $no_of_pages = ceil($data['total'] / 10);
}
else
{
    $result2 = mysqli_query($conn,$sql2);
    $dataCount = mysqli_fetch_assoc($result2);
    $no_of_pages = 0;
    $no_of_pages = ceil($dataCount['total2'] / 10);
    
}

$querylisting = mysqli_query($conn,$sql);

?>
<script>$("#pagination"+<?php echo $fromPage;?>+" option").remove();</script>
<table class="table table-sm table-hovered table-striped table-responsive-xl dtmTableNoWrap removebottommargin">
    <thead>
        <tr>
        <?php 

                ?>
                    <th class="text-center">DTM NO</th>
                    <th class="text-center">DSF NO</th>
                    <th class="text-center">Pickup Date</th>
                    <th class="text-center">From</th>
                    <th class="text-center">To</th>
                    <th class="text-center">Agent</th>
                    <th class="text-center">Cost Center</th>
                    <th class="text-center">Consol Status</th>
                    <th class="text-center">Truck Plate No</th>
                    <th class="text-center">Truck Capacity</th>
                    <th class="text-center">Load Capacity</th>
                    <th class="text-center">Gross Service Fee<br>/Consol Charges</th>
                    <th class="text-center">Detention</th>
                    <th class="text-center">Overnight</th>
                    <th class="text-center">Night Deliveries</th>
                    <th class="text-center">Sunday</th>
                    <th class="text-center">Public</th>
                    <th class="text-center">Cancellation</th>
                    <th class="text-center">Lockup(Day)</th>
                    <th class="text-center">Lockup(Night)</th>
                    <th class="text-center">TOTAL DSF</th>
                    <th class="text-center">DSF Remarks</th>
                    <th class="text-center">From Zone</th>
                    <th class="text-center">To Zone</th>
                <?php
        ?>
        </tr>
  </thead>
  <tbody>
    <?php 
        if (mysqli_num_rows($querylisting) > 0) 
        {
            while($row = mysqli_fetch_array($querylisting))
            {


                $costCenterDisplay = "SELECT * FROM additionalservicefee WHERE loadTrans = '". $row['loadCap']."'";
                $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                {
                    while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                    {
                        $overnightRate = $urow1['overnight'];
                        $nightDeliveriesRate = $urow1['nightDeliveries'];
                        $sundayRate = $urow1['sunday'];
                        $publicRate = $urow1['public'];
                        $lockupNightRate = $urow1['lockupNight'];
                        $lockupDayRate = $urow1['lockupDay'];
                        $cancelRate = $urow1['cancel'];
                        
                    }
                }

                $remarkDsf = $row['remarkDsf'];
                $dsfTotal = 0;
    ?>
    <tr>
                <td class="text-center">
                    <?php 
                        echo $row['dtmID_FK'];
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                        echo $row['id'];
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                         $pickupDate = date("d M Y",strtotime($row['dtmPickupDate']));
                         echo $pickupDate;
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                        $costCenterDisplay = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$row['dtmOriginPointID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['pointzonePlaceName'];
                            }
                        }
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                        $costCenterDisplay = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$row['dtmDestinationPointID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['pointzonePlaceName'];
                            }
                        }
                    ?>
                </td>
                <td class="text-center">
                <?php 
                        $costCenterDisplay = "SELECT companyName FROM company WHERE companyID_PK = ".$row['companyID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['companyName'];
                            }
                        }
                    ?>
                </td>
                <td class="text-center">
                <?php 
                        $costCenterDisplay = "SELECT costcenterName FROM costcenter WHERE costcenterID_PK = ".$row['costCenterID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['costcenterName'];
                            }
                        }
                    ?>
                </td>
                <td class="text-center">
                <?php 
                       if($row['isConsol'] == 1)
                       {
                            echo "CONSOL";
                       }
                       else
                       {
                            echo "-";
                       }
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                        $costCenterDisplay = "SELECT truckPlateNo,truckCapacity FROM trucks WHERE truckID_PK = ".$row['truckID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['truckPlateNo'];
                           
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                                echo $urow1['truckCapacity'];
                            }
                        }
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                        echo $row['loadCap'];
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                        $dsfTotal+= $row['dsfGross'];
                        echo "RM ". $row['dsfGross'];
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                        $dsfTotal+= $row['detention'];
                        echo "RM ". $row['detention'];
                    ?>
                </td>
                <td class="text-center">
                    <?php
                        if($row['isovernight'] == 1)
                        {
                            $dsfTotal +=  $row['dsfOvernight'];
                            echo "RM ". $row['dsfOvernight'];
                        } 
                        else
                        {
                            echo "-";
                        }
                    ?>
                </td>
                <td class="text-center">
                <?php
                        if($row['isnightDeliveries'] == 1)
                        {
                            $dsfTotal +=  $row['dsfNightDelivery'];
                            echo "RM ". $row['dsfNightDelivery'];
                        } 
                        else
                        {
                            echo "-";
                        }
                    ?>
                </td>
                <td class="text-center">
                    <?php
                        if($row['issunday'] == 1)
                        {
                            $dsfTotal += $row['dsfSunday'];
                            echo "RM ".$row['dsfSunday'];
                        } 
                        else
                        {
                            echo "-";
                        }
                    ?>
                </td>
                <td class="text-center">
                    <?php
                        if($row['ispublic'] == 1)
                        {
                            $dsfTotal +=  $row['dsfPublic'];
                            echo "RM ". $row['dsfPublic'];
                        } 
                        else
                        {
                            echo "-";
                        }
                    ?>
                </td>
                <td class="text-center">
                <?php
                        if($row['iscancelllation'] == 1)
                        {
                            $dsfTotal += $row['dsfCancellation'];
                            echo "RM ".$row['dsfCancellation'];
                        } 
                        else
                        {
                            echo "-";
                        }
                    ?>
                </td>
                <td class="text-center">
                <?php
                        if($row['islockupDay'] == 1)
                        {
                            $dsfTotal +=  $row['dsfLockupDay'];
                            echo "RM ". $row['dsfLockupDay'];
                        } 
                        else
                        {
                            echo "-";
                        }
                    ?>
                </td>
                <td class="text-center">
                <?php
                        if($row['islockupNight'] == 1)
                        {
                            $dsfTotal +=  $row['dsfLockupNight'];
                            echo "RM ". $row['dsfLockupNight'];
                        } 
                        else
                        {
                            echo "-";
                        }
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                        echo "RM ".sprintf('%0.2f',$dsfTotal);
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                        echo $remarkDsf;
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                        $costCenterDisplay = "SELECT * FROM zones WHERE zonesID_PK = ".$row['dtmOriginZoneID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['zonesName'];
                            }
                        }
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                        $costCenterDisplay = "SELECT * FROM zones WHERE zonesID_PK = ".$row['dtmDestinationZoneID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['zonesName'];
                            }
                        }
                    ?>
                </td>
        </tr>
    <?php 
            }
        }
        else
        {
            ?>
            <tr>
                <td colspan="22" style="text-align:center;">No Records Found</td>
            </tr>
            <?php
        }
    ?>
  </tbody>
</table>
<?php
    if($condition == 1)
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$data['total']);
    }
    else
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$dataCount['total2']);
    }
?>
 