<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        require 'generalFunction.php';
        require 'settingFormHandler.php';
        $message = checkPass();
?>
<!doctype html>
<html lang="en">
    <head>
        <title>Settings</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>Settings</h3>
                    </div>
                    <div class="row">
                        <div class="col-xl-12" >
                        <?php generateSimpleModal();
                        if($message)
                        {
                            putNotice("Notice",$message);
                        }
                        ?>
                            <div class="accordion" id="aa">
                                <div class="card">
                                    <div class="card-header" id="h1">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed adminAccordionButton" type="button" data-toggle="collapse" data-target="#c1" aria-expanded="true" aria-controls="c1">
                                            Change Password
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="c1" class="collapse show" aria-labelledby="h1" data-parent="#aa">
                                        <form class="card-body row" method="POST">
                                            <div class="col-xl-3"></div>
                                            <div class="form-group col-xl-6">
                                                <label for="changeOldPassword">Old Password</label>
                                                <input required type="password" class="form-control adminAddSetPadding" id="changeOldPassword" name="changeOldPassword">
                                            </div>
                                            <div class="col-xl-3"></div>

                                            <div class="col-xl-3"></div>
                                            <div class="form-group col-xl-6">
                                                <label for="changeNewPassword">New Password</label>
                                                <input required type="password" class="form-control adminAddSetPadding" id="changeNewPassword" name="changeNewPassword">
                                            </div>
                                            <div class="col-xl-3"></div>
                                            <div class="col-xl-3"></div>
                                            <div class="form-group col-xl-6">
                                                <label for="checkNewPassword">Re-type New Password</label>
                                                <input required type="password" class="form-control adminAddSetPadding" id="checkNewPassword" name="checkNewPassword">
                                            </div>
                                            <div class="col-xl-3"></div>

                                            <div class="col-xl-3"></div>
                                            <div class="col-xl-6 adminAddUserButton">
                                                <button style="margin-bottom:20px;" class="btn formButtonPrimary indexSubmitButton dtmMarginSubmitButton" name="button" value="<?php echo $_SESSION['userID_PK'];?>">Change Password</button>
                                            </div>
                                            <div class="col-xl-3"></div>
                                        </form>
                                    </div>
                                </div>
                                <!-- <div class="card">
                                    <div class="card-header" id="h2">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed adminAccordionButton" type="button" data-toggle="collapse" data-target="#c2" aria-expanded="true" aria-controls="c2">
                                                --Currently On Work--
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="c2" class="collapse" aria-labelledby="h2" data-parent="#aa">
                                        <div class="card-body"></div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
        <script>

        </script>
    </body>
</html>
<?php
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>