<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        if(isset($_POST['edit']))
        {   
            $edit = $_POST['edit'];
            $tableType = $_POST['tableType'];

            require 'generalFunction.php';
            $conn = connDB();
?>
<!doctype html>
<html lang="en">
    <head>
        <title>Edit DTM</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>
                            <?php 
                            if($tableType == 8)
                            {
                                echo "Coordinator Edit DTM";
                            }
                            ?>
                        </h3>
                    </div>
                    <?php   
                        generateConfirmationModal();
                        generateSimpleModal();
                    ?>
                    <div class="row">
                        <div class="col-xl-12 row">
                        <?php 
                            if($tableType == 8)
                            {
                                $sql = "SELECT * FROM dtmlist WHERE dtmID_PK = ".$edit;
                                $query = mysqli_query($conn,$sql);
                                if (mysqli_num_rows($query) > 0) 
                                {
                                    while($urow = mysqli_fetch_array($query))
                                    {
                                        $cbDate1 = date("Ymd",strtotime($urow['dtmBookingDate']));
                                        $cbDate11 = date("Hi",strtotime($urow['dtmBookingTime']));
                                        $cbDate2 = date("Ymd",strtotime($urow['dtmPickupDate']));
                                        $cbDate22 = date("Hi",strtotime($urow['dtmPickupTime']));

                                        $costCenterID_FK = $urow['costCenterID_FK'];
                                ?>                            
                                <div class="dtmTrapezoid col-xl-4">
                                    <p class="dtmTrapezoidPara">DTM Info</p>
                                </div>
                                <div class="col-xl-8"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-dgfRef">DGF Reference</label>
                                    <input type="text" class="form-control adminAddSetPadding" id="input-ssip-dgfRef" value="<?php echo $urow['dtmDGF'];?>">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-company">Select Agent</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-company" onchange="getThisCompanyToChangeCostCenter(this,1,null,1);">
                                        <option disabled selected hidden>-- Pick one company --</option>
                                        <?php

                                        $sql_select_costCenter = "SELECT * FROM company WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                if($row["companyID_PK"] == $urow['companyID_FK'])
                                                {
                                                    echo '<option value="'.$row["companyID_PK"].'" selected>'.$row["companyName"].' </option>';
                                                }
                                                else
                                                {
                                                    echo '<option value="'.$row["companyID_PK"].'">'.$row["companyName"].' </option>';
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xl-1"></div>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-bookingDate" >Booking Date (YYYYMMDD)</label>
                                    <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="input-ssip-bookingDate" value="<?php echo $cbDate1;?>">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-bookingTime" >Booking Time</label>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding timepicker" id="input-ssip-bookingTime" placeholder="hhmm" value="<?php echo $cbDate11;?>" >
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-originPoint">Origin Point</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-originPoint">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php 

                                        $sql_select_costCenter = "SELECT * FROM pointzone WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0) 
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter)) 
                                            {
                                                if($row["pointzoneID_PK"] == $urow['dtmOriginPointID_FK'])
                                                {
                                                    echo '<option value="'.$row["pointzoneID_PK"].'" selected>'.$row["pointzonePlaceName"].'</option>';
                                                }
                                                else
                                                {
                                                    echo '<option value="'.$row["pointzoneID_PK"].'">'.$row["pointzonePlaceName"].'</option>';
                                                }
                                               
                                            }
                                        } 
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-destinationPoint" >Destination Point</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-destinationPoint">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php 

                                        $sql_select_costCenter = "SELECT * FROM pointzone WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0) 
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter)) 
                                            {
                                                if($row["pointzoneID_PK"] == $urow['dtmDestinationPointID_FK'])
                                                {
                                                    echo '<option value="'.$row["pointzoneID_PK"].'" selected>'.$row["pointzonePlaceName"].'</option>';
                                                }
                                                else
                                                {
                                                    echo '<option value="'.$row["pointzoneID_PK"].'">'.$row["pointzonePlaceName"].'</option>';
                                                }
                                            }
                                        }  
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-originZone" >Origin Zone</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-originZone">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php

                                        $sql_select_costCenter = "SELECT * FROM zones WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                if($row["zonesID_PK"] == $urow['dtmOriginZoneID_FK'])
                                                {
                                                    echo '<option value="'.$row["zonesID_PK"].'" selected>'.$row["zonesName"].','.$row["zonesState"].'</option>';
                                                }
                                                else
                                                {
                                                    echo '<option value="'.$row["zonesID_PK"].'">'.$row["zonesName"].','.$row["zonesState"].'</option>';
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-destinationZone" >Destination Zone</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-destinationZone">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php

                                        $sql_select_costCenter = "SELECT * FROM zones WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                if($row["zonesID_PK"] == $urow['dtmDestinationZoneID_FK'])
                                                {
                                                    echo '<option value="'.$row["zonesID_PK"].'" selected>'.$row["zonesName"].','.$row["zonesState"].'</option>';
                                                }
                                                else
                                                {
                                                    echo '<option value="'.$row["zonesID_PK"].'">'.$row["zonesName"].','.$row["zonesState"].'</option>';
                                                }
                                                
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5" id="showCostCenterByCompany"></div>
                                <div class="col-xl-6"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-pickupDate" >Pickup Date YYYYMMDD</label>
                                    <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="input-ssip-pickupDate" value="<?php echo $cbDate2;?>">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-pickupTime" >Pickup Time (hh:mm AM/PM)</label>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" placeholder="hhmm" id="input-ssip-pickupTime" value="<?php echo $cbDate22;?>">
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-truckPlateNo" >Truck Plate No</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-truckPlateNo">
                                        <option disabled selected hidden>-- Pick one Truck --</option>
                                        <?php

                                        $sql_select_costCenter = "SELECT * FROM trucks WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0) 
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter)) 
                                            {
                                                if($row["truckID_PK"] == $urow['truckID_FK'])
                                                {
                                                    echo '<option value="'.$row["truckID_PK"].'" selected>'.$row["truckPlateNo"].'</option>';
                                                }
                                                else
                                                {
                                                    echo '<option value="'.$row["truckID_PK"].'">'.$row["truckPlateNo"].'</option>';
                                                }
                                               
                                            }
                                        } 
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-urgentMemo" >Urgent Memo</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-urgentMemo">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php 
                                            if ($urow['dtmUrgentMemo'] == 1)
                                            {
                                                echo"
                                                <option value='yes' selected>Yes</option>
                                                <option value='no'>No</option>";
                                            }
                                            if ($urow['dtmUrgentMemo'] == 0)
                                            {
                                                echo"
                                                <option value='yes'>Yes</option>
                                                <option value='no'selected>No</option>";
                                            }
                                        ?>
                                    </select>
                                    
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-driverName" >Driver 1 Name</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-driverName">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php

                                        $sql_select_costCenter = "SELECT * FROM driver WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                if($row["driverID_PK"] == $urow['driverID_FK'])
                                                {
                                                    echo '<option value="'.$row["driverID_PK"].'"selected>'.$row["driverNickName"].'</option>';
                                                }
                                                else
                                                {
                                                    echo '<option value="'.$row["driverID_PK"].'">'.$row["driverNickName"].'</option>';
                                                }
                                                
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xl-6"></div>
                                
                                    <div class="col-xl-1"></div>
                                    <div class="form-group col-xl-5">
                                    <?php
                                if ($urow['driver2ID_FK'] != null )
                                {
                                    ?>
                                    <input type="checkbox" name="secDriver" id="secDriver" checked onchange="showsecDriver();">
                                    <label for="secDriver" id="secDriverLabel">Add Another Driver ?</label>
                                    <div id="showSecond" style="display:block;">
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <input type="checkbox" name="secDriver" id="secDriver" onchange="showsecDriver();">
                                    <label for="secDriver" id="secDriverLabel">Add Another Driver ?</label>
                                    <div id="showSecond" style="display:none;">
                                    <?php
                                }
                                        ?>
                                        <label for="input-ssip-driverName2" >Driver 2 Name</label>
                                        <select class="form-control adminAddSetPadding" id="input-ssip-driverName2">
                                            <option disabled selected hidden>-- Pick one of the following --</option>
                                            <?php

                                            $sql_select_costCenter = "SELECT * FROM driver WHERE showThis = 1";
                                            $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                            if (mysqli_num_rows($result_select_costCenter) > 0)
                                            {
                                                // output data of each row
                                                while($row = mysqli_fetch_assoc($result_select_costCenter))
                                                {
                                                    if($row["driverID_PK"] == $urow['driver2ID_FK'])
                                                    {
                                                        echo '<option value="'.$row["driverID_PK"].'"selected>'.$row["driverNickName"].'</option>';
                                                    }
                                                    else
                                                    {
                                                        echo '<option value="'.$row["driverID_PK"].'">'.$row["driverNickName"].'</option>';
                                                    }
                                                    
                                                }
                                            }
                                            ?>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-xl-6"></div>
                                
                                <?php 
                                    if(isset($urow['lockupf']))
                                    {
                                        $lockupf = date("Ymd",strtotime($urow['lockupf'])); 
                                    }
                                    if(isset($urow['lockupdockTime']))
                                    {
                                        $lockupdockTime = date("Ymd",strtotime($urow['lockupdockTime'])); 
                                    }
                                    if(isset($urow['lockupcompleteLoadingTime']))
                                    {
                                        $lockupcompleteLoadingTime = date("Ymd",strtotime($urow['lockupcompleteLoadingTime'])); 
                                    }
                                    if(isset($urow['lockupdepartureTimeFromPost']))
                                    {
                                        $lockupdepartureTimeFromPost = date("Ymd",strtotime($urow['lockupdepartureTimeFromPost'])); 
                                    }
                                ?>

                                <div class="dtmTrapezoid col-xl-4">
                                    <p class="dtmTrapezoidPara">Origin</p>
                                </div>
                                <div class="col-xl-8"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-3">
                                    <label for="input-ssip-f" >F</label>
                                    <?php
                                    if($urow['dtmOriginF'])
                                    {
                                        $aa = date("Hi",strtotime($urow['dtmOriginF']));
                                    } 
                                    else
                                    {
                                        $aa = "";
                                    }
                                    ?>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" id="input-ssip-f" placeholder="hhmm" value="<?php echo $aa;?>">
                                </div>
                                <div class="col-xl-2 form-group">
                                    <label class="cb-container cb-padding" style="margin-top:35px;">Next Day
                                    <?php 
                                    if(isset($lockupf))
                                    {
                                        ?> <input type="checkbox" id="date_F_checked" onchange="checklockup(this,document.getElementById('lockup-f'),document.getElementById('display-f'));" checked><?php 
                                    }
                                    else
                                    {
                                        ?> <input type="checkbox" id="date_F_checked" onchange="checklockup(this,document.getElementById('lockup-f'),document.getElementById('display-f'));"><?php 
                                    }
                                    ?>
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="form-group col-xl-3">
                                    <label for="input-ssip-originDockTime" >Dock Time</label>
                                    <?php 
                                    if($urow['dtmOriginDockTime'])
                                    {
                                        $bb = date("Hi",strtotime($urow['dtmOriginDockTime']));
                                    } 
                                    else
                                    {
                                        $bb = "";
                                    }
                                    ?>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" id="input-ssip-originDockTime" placeholder="hhmm" value="<?php echo $bb;?>">
                                </div>
                                <div class="col-xl-2 form-group">
                                    <label class="cb-container cb-padding" style="margin-top:35px;">Next Day
                                    <?php 
                                    if(isset($lockupdockTime))
                                    {
                                        ?> <input type="checkbox" id="date_dockTime_checked" onchange="checklockup(this,document.getElementById('lockup-dockTime'),document.getElementById('display-dockTime'));" checked><?php 
                                    }
                                    else
                                    {
                                        ?> <input type="checkbox" id="date_dockTime_checked" onchange="checklockup(this,document.getElementById('lockup-dockTime'),document.getElementById('display-dockTime'));"><?php 
                                    }
                                    ?>
                                        
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <?php if(isset($lockupf))
                                    {
                                        ?><div id="display-f" style="display:block;"><?php
                                    }
                                    else
                                    {
                                        ?><div id="display-f" style="display:none;"><?php
                                    }
                                    ?>
                                    
                                        <label for="lockup-f" >Next Date F</label>
                                        <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="lockup-f" value="<?php if(isset($lockupf)){echo $lockupf;}?>">
                                    </div>
                                </div>
                                <div class="form-group col-xl-5">
                                    <?php if(isset($lockupdockTime))
                                    {
                                        ?><div id="display-dockTime" style="display:block;"><?php
                                    }
                                    else
                                    {
                                        ?><div id="display-dockTime" style="display:none;"><?php
                                    }
                                    ?>
                                    
                                        <label for="lockup-dockTime" >Next Date Dock Time</label>
                                        <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="lockup-dockTime" value="<?php if(isset($lockupdockTime)){echo $lockupdockTime;}?>">
                                    </div>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-3">
                                    <label for="input-ssip-originCompleteLoadingTime" >Complete Loading Time</label>
                                    <?php 
                                    if($urow['dtmOriginCompleteLoadingTime'])
                                    {
                                        $cc = date("Hi",strtotime($urow['dtmOriginCompleteLoadingTime']));
                                    } 
                                    else
                                    {
                                        $cc = "";
                                    }?>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" id="input-ssip-originCompleteLoadingTime" placeholder="hhmm"  value="<?php echo $cc;?>">
                                </div>
                                <div class="col-xl-2 form-group">
                                    <label class="cb-container cb-padding" style="margin-top:35px;">Next Day
                                    <?php 
                                    if(isset($lockupcompleteLoadingTime))
                                    {
                                        ?> <input type="checkbox" id="date_completeLoadingTime_checked" onchange="checklockup(this,document.getElementById('lockup-completeLoadingTime'),document.getElementById('display-completeLoadingTime'));" checked><?php 
                                    }
                                    else
                                    {
                                        ?> <input type="checkbox" id="date_completeLoadingTime_checked" onchange="checklockup(this,document.getElementById('lockup-completeLoadingTime'),document.getElementById('display-completeLoadingTime'));"><?php 
                                    }
                                    ?>
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="form-group col-xl-3">
                                    <label for="input-ssip-originDepartureTimeFromPost" >Security Post Departure </label>
                                    <?php 
                                    
                                    if($urow['dtmOriginDepartureTimeFromPost'])
                                    {
                                        $dd = date("Hi",strtotime($urow['dtmOriginDepartureTimeFromPost']));
                                    } 
                                    else
                                    {
                                        $dd = "";
                                    }?>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" id="input-ssip-originDepartureTimeFromPost" placeholder="hhmm" value="<?php echo $dd;?>">
                                </div>
                                <div class="col-xl-2 form-group">
                                    <label class="cb-container cb-padding" style="margin-top:35px;">Next Day
                                    <?php 
                                    if(isset($lockupdepartureTimeFromPost))
                                    {
                                        ?> <input type="checkbox" id="date_departureTimeFromPost_checked" onchange="checklockup(this,document.getElementById('lockup-originDepartureTimeFromPost'),document.getElementById('display-departureTimeFromPost'));" checked><?php 
                                    }
                                    else
                                    {
                                        ?> <input type="checkbox" id="date_departureTimeFromPost_checked" onchange="checklockup(this,document.getElementById('lockup-originDepartureTimeFromPost'),document.getElementById('display-departureTimeFromPost'));"><?php 
                                    }
                                    ?>
                                        
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <?php if(isset($lockupcompleteLoadingTime))
                                    {
                                        ?><div id="display-completeLoadingTime" style="display:block;"><?php
                                    }
                                    else
                                    {
                                        ?><div id="display-completeLoadingTime" style="display:none;"><?php
                                    }
                                    ?>
                                    
                                        <label for="lockup-completeLoadingTime" >Next Date Complete Loading Time</label>
                                        <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="lockup-completeLoadingTime" value="<?php if(isset($lockupcompleteLoadingTime)){echo $lockupcompleteLoadingTime;}?>">
                                    </div>
                                </div>
                                <div class="form-group col-xl-5">
                                <?php if(isset($lockupdepartureTimeFromPost))
                                    {
                                        ?><div id="display-departureTimeFromPost" style="display:block;"><?php
                                    }
                                    else
                                    {
                                        ?><div id="display-departureTimeFromPost" style="display:none;"><?php
                                    }
                                    ?>
                                    
                                        <label for="lockup-originDepartureTimeFromPost" >Next Date Departure Time From Security Post</label>
                                        <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="lockup-originDepartureTimeFromPost" value="<?php if(isset($lockupdepartureTimeFromPost)){echo $lockupdepartureTimeFromPost;}?>">
                                    </div>
                                </div>
                                <div class="col-xl-1"></div>

                                <?php 
                                    if(isset($urow['lockuparrivalTimeAtPost']))
                                    {
                                        $lockuparrivalTimeAtPost = date("Ymd",strtotime($urow['lockuparrivalTimeAtPost'])); 
                                    }
                                    if(isset($urow['lockupdestinationDockTime']))
                                    {
                                        $lockupdestinationDockTime = date("Ymd",strtotime($urow['lockupdestinationDockTime'])); 
                                    }
                                    if(isset($urow['lockupcompleteUnloadTime']))
                                    {
                                        $lockupcompleteUnloadTime = date("Ymd",strtotime($urow['lockupcompleteUnloadTime'])); 
                                    }
                                    if(isset($urow['lockupdepartTimeFromPost']))
                                    {
                                        $lockupdepartTimeFromPost = date("Ymd",strtotime($urow['lockupdepartTimeFromPost'])); 
                                    }
                                ?>

                                <div class="dtmTrapezoid col-xl-4">
                                    <p class="dtmTrapezoidPara">Destination</p>
                                </div>
                                <div class="col-xl-8"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-3">
                                    <label for="input-ssip-destArrivalTimeAtPost" >Arrival Time</label>
                                    <?php 
                                    if($urow['dtmDestinationArrivalTimeAtPost'])
                                    {
                                        $ee = date("Hi",strtotime($urow['dtmDestinationArrivalTimeAtPost']));
                                    } 
                                    else
                                    {
                                        $ee = "";
                                    }
                                    ?>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" id="input-ssip-destArrivalTimeAtPost" placeholder="hhmm"  value="<?php echo $ee;?>">
                                </div>
                                <div class="col-xl-2 form-group">
                                    <label class="cb-container cb-padding" style="margin-top:35px;">Next Day
                                    <?php 
                                    if(isset($lockuparrivalTimeAtPost))
                                    {
                                        ?> <input type="checkbox" id="date_arrivalTimeAtPost_checked" onchange="checklockup(this,document.getElementById('lockup-arrivalTimeAtPost'),document.getElementById('display-arrivalTimeAtPost'));" checked><?php 
                                    }
                                    else
                                    {
                                        ?> <input type="checkbox" id="date_arrivalTimeAtPost_checked" onchange="checklockup(this,document.getElementById('lockup-arrivalTimeAtPost'),document.getElementById('display-arrivalTimeAtPost'));"><?php 
                                    }
                                    ?>
                                        
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="form-group col-xl-3">
                                    <label for="input-ssip-destDockTime" >Dock Time</label>
                                    <?php 
                                    
                                    if($urow['dtmDestinationDockTime'])
                                    {
                                        $ff = date("Hi",strtotime($urow['dtmDestinationDockTime']));
                                    } 
                                    else
                                    {
                                        $ff ="";
                                    }?>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" id="input-ssip-destDockTime" placeholder="hhmm"  value="<?php echo $ff;?>">
                                </div>
                                <div class="col-xl-2 form-group">
                                    <label class="cb-container cb-padding" style="margin-top:35px;">Next Day
                                    <?php 
                                    if(isset($lockupdestinationDockTime))
                                    {
                                        ?> <input type="checkbox" id="date_destinationDockTime_checked" onchange="checklockup(this,document.getElementById('lockup-destinationDockTime'),document.getElementById('display-destinationDockTime'));" checked><?php 
                                    }
                                    else
                                    {
                                        ?> <input type="checkbox" id="date_destinationDockTime_checked" onchange="checklockup(this,document.getElementById('lockup-destinationDockTime'),document.getElementById('display-destinationDockTime'));"><?php 
                                    }
                                    ?> 
                                        
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                <?php if(isset($lockuparrivalTimeAtPost))
                                    {
                                        ?><div id="display-arrivalTimeAtPost" style="display:block;"><?php
                                    }
                                    else
                                    {
                                        ?><div id="display-arrivalTimeAtPost" style="display:none;"><?php
                                    }
                                    ?>
                                    
                                        <label for="lockup-arrivalTimeAtPost" >Next Date Arrival Time</label>
                                        <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="lockup-arrivalTimeAtPost" value="<?php if(isset($lockuparrivalTimeAtPost)){echo $lockuparrivalTimeAtPost;}?>">
                                    </div>
                                </div>
                                <div class="form-group col-xl-5">
                                <?php if(isset($lockupdestinationDockTime))
                                    {
                                        ?><div id="display-destinationDockTime" style="display:block;"><?php
                                    }
                                    else
                                    {
                                        ?><div id="display-destinationDockTime" style="display:none;"><?php
                                    }
                                    ?>
                                    
                                        <label for="lockup-destinationDockTime" >Next Date Destination Dock Time</label>
                                        <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="lockup-destinationDockTime" value="<?php if(isset($lockupdestinationDockTime)){echo $lockupdestinationDockTime;}?>">
                                    </div>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-3">
                                    <label for="input-ssip-destCompleteUnloadTime" >Complete Unloading Time</label>
                                    <?php
                                     if($urow['dtmDestinationCompleteUnloadTime'])
                                    {
                                        $gg = date("Hi",strtotime($urow['dtmDestinationCompleteUnloadTime']));
                                    } 
                                    else
                                    {
                                        $gg = "";
                                    }?>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" id="input-ssip-destCompleteUnloadTime" placeholder="hhmm" value="<?php echo $gg;?>">
                                </div>
                                <div class="col-xl-2 form-group">
                                    <label class="cb-container cb-padding" style="margin-top:35px;">Next Day
                                    <?php 
                                    if(isset($lockupcompleteUnloadTime))
                                    {
                                        ?> <input type="checkbox" id="date_completeUnloadTime_checked" onchange="checklockup(this,document.getElementById('lockup-completeUnloadTime'),document.getElementById('display-completeUnloadTime'));" checked><?php 
                                    }
                                    else
                                    {
                                        ?> <input type="checkbox" id="date_completeUnloadTime_checked" onchange="checklockup(this,document.getElementById('lockup-completeUnloadTime'),document.getElementById('display-completeUnloadTime'));"><?php 
                                    }
                                    ?> 
                                        
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="form-group col-xl-3">
                                    <label for="input-ssip-destDepartTimeFromPost" >Security Post Departure </label>
                                    <?php
                                    if($urow['dtmDestinationDepartTimeFromPost'])
                                    {
                                        $hh = date("Hi",strtotime($urow['dtmDestinationDepartTimeFromPost']));
                                    } 
                                    else
                                    {
                                        $hh = "";
                                    }?>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" id="input-ssip-destDepartTimeFromPost" placeholder="hhmm"  value="<?php echo $hh;?>">
                                </div>
                                <div class="col-xl-2 form-group">
                                    <label class="cb-container cb-padding" style="margin-top:35px;">Next Day
                                    <?php 
                                    if(isset($lockupdepartTimeFromPost))
                                    {
                                        ?> <input type="checkbox" id="date_departTimeFromPost_checked" onchange="checklockup(this,document.getElementById('lockup-departTimeFromPost'),document.getElementById('display-departTimeFromPost'));" checked><?php 
                                    }
                                    else
                                    {
                                        ?> <input type="checkbox" id="date_departTimeFromPost_checked" onchange="checklockup(this,document.getElementById('lockup-departTimeFromPost'),document.getElementById('display-departTimeFromPost'));"><?php 
                                    }
                                    ?> 
                                        
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                <?php if(isset($lockupcompleteUnloadTime))
                                    {
                                        ?><div id="display-completeUnloadTime" style="display:block;"><?php
                                    }
                                    else
                                    {
                                        ?><div id="display-completeUnloadTime" style="display:none;"><?php
                                    }
                                    ?>
                                    
                                        <label for="lockup-completeUnloadTime" >Next Date Complete Unloading Time</label>
                                        <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="lockup-completeUnloadTime" value="<?php if(isset($lockupcompleteUnloadTime)){echo $lockupcompleteUnloadTime;}?>">
                                    </div>
                                </div>
                                <div class="form-group col-xl-5">
                                <?php if(isset($lockupdepartTimeFromPost))
                                    {
                                        ?><div id="display-departTimeFromPost" style="display:block;"><?php
                                    }
                                    else
                                    {
                                        ?><div id="display-departTimeFromPost" style="display:none;"><?php
                                    }
                                    ?>
                                        <label for="lockup-departTimeFromPost" >Next Date Departure Time From Security Post</label>
                                        <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="lockup-departTimeFromPost" value="<?php if(isset($lockupdepartTimeFromPost)){echo $lockupdepartTimeFromPost;}?>">
                                    </div>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="dtmTrapezoid col-xl-4">
                                    <p class="dtmTrapezoidPara">Quantity</p>
                                </div>
                                <div class="col-xl-8"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-10">
                                    <label for="input-ssip-quantityType" >Quantity Type</label>
                                    <div class="row">
                                        <div class="col">
                                            <div class="input-group displayBlockQuantity">
                                                <label class="cb-container cb-padding">Cartons
                                                    <?php 
                                                        if ($urow['dtmIsQuantity'] == 1 )
                                                        {
                                                            ?>
                                                            <input type="checkbox" id="input-ssip-quantityTypeCartons" checked>
                                                            <span class="checkmark"></span>
                                                        </label>
                                                        <input type="number" id="input-ssip-quantityCartons" class="form-control adminAddSetPadding" min="0" value="<?php echo $urow['dtmQuantityCarton'];?>" style="width: auto;">
                                                            <?php
                                                        }
                                                        else
                                                        {
                                                            ?>
                                                            <input type="checkbox" id="input-ssip-quantityTypeCartons">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                        <input type="number" id="input-ssip-quantityCartons" class="form-control adminAddSetPadding" min="0" value="0" style="width: auto;">
                                                            <?php
                                                        }
                                                    ?>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="input-group displayBlockQuantity">
                                                <label class="cb-container cb-padding">Pallets
                                                <?php 
                                                        if ($urow['dtmIsPallets'] == 1 )
                                                        {
                                                            ?>
                                                            <input type="checkbox" id="input-ssip-quantityTypePallets" checked>
                                                            <span class="checkmark"></span>
                                                        </label>
                                                        <input type="number" id="input-ssip-quantityPallets" class="form-control adminAddSetPadding" min="0" value="<?php echo $urow['dtmQuantityPalllets'];?>" style="width: auto;">
                                                            <?php
                                                        }
                                                        else
                                                        {
                                                            ?>
                                                            <input type="checkbox" id="input-ssip-quantityTypePallets">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                        <input type="number" id="input-ssip-quantityPallets" class="form-control adminAddSetPadding" min="0" value="0" style="width: auto;">
                                                            <?php
                                                        }
                                                    ?>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="input-group displayBlockQuantity">
                                                <label class="cb-container cb-padding">Cages/Crates
                                                    <?php 
                                                        if ($urow['dtmIsCages']==1 )
                                                        {
                                                            ?>
                                                            <input type="checkbox" id="input-ssip-quantityTypeCages" checked>
                                                            <span class="checkmark"></span>
                                                        </label>
                                                        <input type="number" id="input-ssip-quantityCages" class="form-control adminAddSetPadding" min="0" value="<?php echo $urow['dtmQuantityCages'];?>" style="width: auto;">
                                                            <?php
                                                        }
                                                        else
                                                        {
                                                            ?>
                                                            <input type="checkbox" id="input-ssip-quantityTypeCages">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                        <input type="number" id="input-ssip-quantityCages" class="form-control adminAddSetPadding" min="0" value="0" style="width: auto;">
                                                            <?php
                                                        }
                                                    ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-tripOnTime" >Trip On Time??</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-tripOnTime" onchange="reasonForLate();">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php 
                                            if ($urow['dtmTripOnTime'] == 1)
                                            {
                                                echo"
                                                <option value='yes' selected>Yes</option>
                                                <option value='no'>No</option>";
                                            }
                                            if ($urow['dtmTripOnTime'] == 0)
                                            {
                                                echo"
                                                <option value='yes'>Yes</option>
                                                <option value='no'selected>No</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-xl-5">
                                    <div class="form-group dtmListingAdd-padInsideFG" id="hideThis" style="display: none;">
                                        <label for="input-ssip-delayReason" >Delay Reason</label>
                                        <input type="text" class="form-control adminAddSetPadding" id="input-ssip-delayReason" value="<?php echo $urow['dtmDelayReasons'];?>">
                                    </div>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-tripOnTime" >Load Capacity</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-loadCap">
                                        <option disabled selected hidden>-- Pick one capacity --</option>
                                        <?php 
                                        if ($urow['loadCap'] == '20 Ft / 10 Ton')
                                        {
                                            echo"
                                            <option value='20 Ft / 10 Ton' selected>20 Ft / 10 Ton</option>
                                            <option value='1 Tonner'>1 Tonner</option>
                                            <option value='3 Ton'> 3 Ton</option>
                                            <option value='40 Ft / Q6'> 40 Ft / Q6</option>
                                            <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
                                            <option value='45 Ft / Q7'>45 Ft / Q7</option>";
                                        }
                                        if ($urow['loadCap'] == '1 Tonner')
                                        {
                                            echo"
                                            <option value='20 Ft / 10 Ton' >20 Ft / 10 Ton</option>
                                            <option value='1 Tonner' selected>1 Tonner</option>
                                            <option value='3 Ton'> 3 Ton</option>
                                            <option value='40 Ft / Q6'> 40 Ft / Q6</option>
                                            <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
                                            <option value='45 Ft / Q7'>45 Ft / Q7</option>";
                                        }
                                        if ($urow['loadCap'] == '3 Ton')
                                        {
                                            echo"
                                            <option value='20 Ft / 10 Ton' >20 Ft / 10 Ton</option>
                                            <option value='1 Tonner'>1 Tonner</option>
                                            <option value='3 Ton' selected> 3 Ton</option>
                                            <option value='40 Ft / Q6'> 40 Ft / Q6</option>
                                            <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
                                            <option value='45 Ft / Q7'>45 Ft / Q7</option>";
                                        }
                                        if ($urow['loadCap'] == '40 Ft / Q6')
                                        {
                                            echo"
                                            <option value='20 Ft / 10 Ton' >20 Ft / 10 Ton</option>
                                            <option value='1 Tonner'>1 Tonner</option>
                                            <option value='3 Ton'> 3 Ton</option>
                                            <option value='40 Ft / Q6' selected> 40 Ft / Q6</option>
                                            <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
                                            <option value='45 Ft / Q7'>45 Ft / Q7</option>";
                                        }
                                        if ($urow['loadCap'] == '40 Ft / Q6 Air-Ride')
                                        {
                                            echo"
                                            <option value='20 Ft / 10 Ton' >20 Ft / 10 Ton</option>
                                            <option value='1 Tonner'>1 Tonner</option>
                                            <option value='3 Ton'> 3 Ton</option>
                                            <option value='40 Ft / Q6'> 40 Ft / Q6</option>
                                            <option value='40 Ft / Q6 Air-Ride' selected>40 Ft / Q6 Air-Ride</option>
                                            <option value='45 Ft / Q7'>45 Ft / Q7</option>";
                                        }
                                        if ($urow['loadCap'] == '45 Ft / Q7')
                                        {
                                            echo"
                                            <option value='20 Ft / 10 Ton' >20 Ft / 10 Ton</option>
                                            <option value='1 Tonner'>1 Tonner</option>
                                            <option value='3 Ton'> 3 Ton</option>
                                            <option value='40 Ft / Q6'> 40 Ft / Q6</option>
                                            <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
                                            <option value='45 Ft / Q7' selected>45 Ft / Q7</option>";
                                        }
                                        ?>
                                        
                                    </select>
                                </div>
                                
                                <div class="form-group col-xl-5" >
                                    <label class="cb-container cb-padding " style="margin-top:2.5rem;">Is Consol ?
                                    <?php 
                                        if ($urow['isConsol'] == 1)
                                        {
                                            ?><input type="checkbox" id="input-ssip-isConsol" checked><?php 
                                        }
                                        else
                                        {
                                            ?><input type="checkbox" id="input-ssip-isConsol"><?php 
                                        }
                                    ?>
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="dtmTrapezoid col-xl-4">
                                    <p class="dtmTrapezoidPara">Additional DTM</p>
                                </div>
                                <div class="col-xl-8"></div>

                                <div class="col-xl-1"></div>
                               
                                <div class="form-group col-xl-5">
                                    <label for="input-dtm-requestBy" >Requested By</label>
                                    <input type="text" class="form-control adminAddSetPadding" id="input-dtm-requestBy" value="<?php echo $urow['dtmRequestBy'];?>">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-dtm-remark" >Remark</label>
                                    <input type="text" class="form-control adminAddSetPadding" id="input-dtm-remark" value="<?php echo $urow['dtmRemarks'];?>">
                                </div>
                                <div class="col-xl-1"></div>
                                
                                <?php 
                                $vxlDO = "";
                                if($urow['dtmAdhoc'])
                                {
                                    $vxlDO = $urow['dtmAdhoc'];
                                }
                                ?>
                                <div class="col-xl-1"></div> 
                                <div class="form-group col-xl-5">
                                    <label for="input-dtm-vxlDoNo" >VXL D/O Number</label>
                                    <input type="text" class="form-control adminAddSetPadding" id="input-dtm-vxlDoNo" value="<?php echo $vxlDO;?>">
                                </div>
                                <div class="col-xl-6"></div>

                                <div class="col-xl-3"></div>
                                <div class="col-xl-6 adminAddUserButton">
                                    <button class="btn formButtonPrimary indexSubmitButton dtmMarginSubmitButton" onclick="addDTMData(<?php echo $tableType;?>,2);" value="<?php echo $edit;?>" id="idpk">Update DTM</button>
                                </div>
                                <div class="col-xl-3"></div>
                                <?php
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
        <script>
        $(document).ready(function()
        {
            let pickup,booking;
            let lockupF,lockupDock,lockupCompleteLoadingTime,lockupDepartureTimeFromPost;
            let lockupArrivalTimeAtPost,lockupDestinationDock,lockupCompleteUnloadTime,lockupDepartTimeFromPost;

            getThisCompanyToChangeCostCenter((document.getElementById('input-ssip-company')),2,<?php echo $costCenterID_FK;?>,1);

            reasonForLate();
            
            // var bookingDatePicker = new Pikaday({
            //     field: document.getElementById('input-ssip-bookingDate'),
            //     format: 'YYYYMMDD',
            //     onSelect: function() {
            //         // console.log(this.getMoment().format('Do MMMM YYYY'));
            //     }
            // });

            // var pickupDatePicker = new Pikaday({
            //     field: document.getElementById('input-ssip-pickupDate'),
            //     format: 'YYYYMMDD',
            //     onSelect: function() {
            //         // console.log(this.getMoment().format('Do MMMM YYYY'));
            //     }
            // });
            
            booking = initializeDate(document.getElementById('input-ssip-bookingDate'),booking);
            pickup = initializeDate(document.getElementById('input-ssip-pickupDate'),pickup);

            lockupF = initializeDate(document.getElementById('lockup-f'),lockupF);
            lockupDock = initializeDate(document.getElementById('lockup-dockTime'),lockupDock);
            lockupCompleteLoadingTime = initializeDate(document.getElementById('lockup-completeLoadingTime'),lockupCompleteLoadingTime);
            lockupDepartureTimeFromPost = initializeDate(document.getElementById('lockup-originDepartureTimeFromPost'),lockupDepartureTimeFromPost);

            lockupArrivalTimeAtPost = initializeDate(document.getElementById('lockup-arrivalTimeAtPost'),lockupArrivalTimeAtPost);
            lockupDestinationDock = initializeDate(document.getElementById('lockup-destinationDockTime'),lockupDestinationDock);
            lockupCompleteUnloadTime = initializeDate(document.getElementById('lockup-completeUnloadTime'),lockupCompleteUnloadTime);
            lockupDepartTimeFromPost = initializeDate(document.getElementById('lockup-departTimeFromPost'),lockupDepartTimeFromPost);

            
            lockupF.getDate();
            
            // intializeTimePicker();
        });
        function initializeDate(thisId,setVariable)
        {
            var setVariable = new Pikaday({
                field: thisId,
                format: 'YYYYMMDD',
                onSelect: function() {
                    // console.log(this.getMoment().format('Do MMMM YYYY'));
                }
            });
            return setVariable;
        }

        function intializeTimePicker() {
            $('.timepicker').timepicki({
                show_meridian:false,
                min_hour_value:0,
                max_hour_value:23,
                step_size_minutes: 1,
                overflow_minutes: true,
                increase_direction: 'up',
                input_writable: false,
                disable_keyboard_mobile: true,
                start_time: ["12", "00", "PM"]
            });
        }

        function checklockup(thisTimeId,thisDateId,thisDisplayId)
        {
            if(thisTimeId.checked == true)
            {
                thisDisplayId.style.display = "block";
            }
            else
            {
                thisDisplayId.style.display = "none";
                thisDateId.value = "";
            }
        }
        function showsecDriver()
        {
            if($('#secDriver').prop('checked'))
            {
                $('#showSecond').css('display','block');
                $('#secDriverLabel').html("Uncheck to remove this driver");
            }
            else
            {
                $('#secDriverLabel').html("Add Another Driver ?");
                $('#showSecond').css('display','none');
            }
        }
        </script>
    </body>
</html>
<?php
        }
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>