<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        if(isset($_POST['edit']))
        {   
            $edit = $_POST['edit']; // idpk
            $tableType = $_POST['tableType'];
            
            require 'generalFunction.php';
            $conn = connDB();

?>
<!doctype html>
<html lang="en">
    <head>
        <title>Admin Edit Field</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>
                            <?php 
                            if($tableType == 1)
                            {
                                echo "Admin Edit Places";
                            }
                            if($tableType == 2)
                            {
                                echo "Admin Edit Zones";
                            }  
                            if($tableType == 3)
                            {
                                echo "Admin Edit Cost Centers";
                            }
                            if($tableType == 4)
                            {
                                echo "Admin Edit Agent";
                            }
                            if($tableType == 5)
                            {
                                echo "Admin Edit Users";
                            }
                            if($tableType == 30)
                            {
                                echo "Admin Edit Transport Rates";
                            }
                            ?>
                        </h3>
                    </div>
                    <?php   
                        generateConfirmationModal();
                        generateSimpleModal();
                    ?>
                    <div class="row adminAddMarginTop">
                        <div class="col-xl-12 row">
                        <?php 
                            if($tableType == 1)
                            {
                                $sql = "SELECT * FROM pointzone WHERE pointzoneID_PK = ".$edit;
                                $query = mysqli_query($conn,$sql);
                                if (mysqli_num_rows($query) > 0) 
                                {
                                    while($urow = mysqli_fetch_array($query))
                                    {
                                ?>
                                <div class="col"></div>
                                <div class="form-group col-xl-10">
                                    <label for="field_1">Place Name</label>
                                    <input type="text" class="form-control adminAddFormControl" id="field_1" value="<?php echo $urow['pointzonePlaceName'];?>">
                                </div>
                                <div class="col"></div>
                                <div class="indexSubmitButtonDiv col-xl-12">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addAdminData(<?php echo $tableType;?>,2);" value="<?php echo $edit;?>" id="idpk">Edit Place</button>
                                </div>
                                <?php
                                    }
                                }
                            }
                            if($tableType == 2)
                            {
                                $sql = "SELECT * FROM zones WHERE zonesID_PK = ".$edit;
                                $query = mysqli_query($conn,$sql);
                                if (mysqli_num_rows($query) > 0) 
                                {
                                    while($urow = mysqli_fetch_array($query))
                                    {
                                ?>
                                <div class="col"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_1">Zone Name</label>
                                    <input type="text" class="form-control adminAddFormControl" id="field_1"  value="<?php echo $urow['zonesName'];?>">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="field_2">Zone State</label>
                                    <input type="text" class="form-control adminAddFormControl" id="field_2"  value="<?php echo $urow['zonesState'];?>">
                                </div>
                                <div class="col"></div>
                                <div class="indexSubmitButtonDiv col-xl-12">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addAdminData(<?php echo $tableType;?>,2);" value="<?php echo $edit;?>" id="idpk">Edit Zone</button>
                                </div>
                                <?php
                                    }
                                }
                            }  
                            if($tableType == 3)
                            {
                                $sql = "SELECT * FROM costcenter WHERE costCenterID_PK = ".$edit;
                                $query = mysqli_query($conn,$sql);
                                if (mysqli_num_rows($query) > 0) 
                                {
                                    while($urow = mysqli_fetch_array($query))
                                    {
                                ?>
                                <div class="col"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_1">Cost Center Name</label>
                                    <input type="text" class="form-control adminAddSetPadding" id="field_1" value="<?php echo $urow['costCenterName'];?>">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="field_2">Select Agent</label>
                                    <select class="form-control adminAddSetPadding" id="field_2">
                                        <option disabled selected hidden>-- Pick one company --</option>
                                        <?php
                                        $conn = connDB();
                                        $sql_select_costCenter = "SELECT * FROM company WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row2 = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                if($row2["companyID_PK"] == $urow["companyID_FK"])
                                                {
                                                    echo '<option value="'.$row2["companyID_PK"].'" selected>'.$row2["companyName"].' </option>';
                                                }
                                                else
                                                {
                                                    echo '<option value="'.$row2["companyID_PK"].'">'.$row2["companyName"].' </option>';
                                                }
                                                
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col"></div>
                                <div class="indexSubmitButtonDiv col-xl-12">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addAdminData(<?php echo $tableType;?>,2);" value="<?php echo $edit;?>" id="idpk">Edit Cost Center</button>
                                </div>
                                <?php
                                    }
                                }
                            }
                            if($tableType == 4)
                            {
                                $sql = "SELECT * FROM company WHERE companyID_PK = ".$edit;
                                $query = mysqli_query($conn,$sql);
                                if (mysqli_num_rows($query) > 0) 
                                {
                                    while($urow = mysqli_fetch_array($query))
                                    {
                                ?>
                                <div class="col"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_1">Agent Name</label>
                                    <input type="text" class="form-control adminAddFormControl" id="field_1" value="<?php echo $urow['companyName'];?>">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="field_2">Agent Short Form</label>
                                    <input type="text" class="form-control adminAddFormControl" id="field_2" value="<?php echo $urow['companyShortForm'];?>">
                                </div>
                                <div class="col"></div>
                                <div class="indexSubmitButtonDiv col-xl-12">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addAdminData(<?php echo $tableType;?>,2);" value="<?php echo $edit;?>" id="idpk">Edit Agent</button>
                                </div>
                                <?php
                                    }
                                }
                            }
                            if($tableType == 5)
                            {
                                $sql = "SELECT * FROM user WHERE userID_PK = ".$edit;
                                $query = mysqli_query($conn,$sql);
                                if (mysqli_num_rows($query) > 0) 
                                {
                                    while($urow = mysqli_fetch_array($query))
                                    {
                                ?>
                                <div class="col-xl-1"></div>
                                <div class="col-xl-5 form-group">
                                    <label for="field_1">Name</label>
                                    <input type="text" class="form-control adminAddFormControl" id="field_1" value="<?php echo $urow["userName"];?>">
                                </div>
                                <div class="col-xl-5 form-group">
                                    <label for="field_2" >Nickname</label>
                                    <input type="text" class="form-control adminAddFormControl"  id="field_2" value="<?php echo $urow["userNickName"];?>">
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="col-xl-5">
                                    <div class="form-group">
                                        <label for="field_3" >Position</label>
                                        <select class="form-control adminAddSetPadding" id="field_3">
                                            <?php displayPosition($urow["userLevel"]);?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-5">
                                    <div class="form-group">
                                        <label for="field_4" >IC No</label>
                                        <input type="text" class="form-control adminAddSetPadding"  id="field_4" value="<?php echo $urow["userIC"];?>">
                                    </div>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="col-xl-5">
                                    <div class="form-group">
                                        <label for="field_5" >Telephone No</label>
                                        <input type="text" class="form-control adminAddSetPadding" id="field_5" value="<?php echo $urow["userTele"];?>">
                                    </div>
                                </div>
                                <div class="col-xl-5">
                                    <div class="form-group">
                                        <label for="field_6" >Email</label>
                                        <input type="email" class="form-control adminAddSetPadding" id="field_6" value="<?php echo $urow["userEmail"];?>">
                                    </div>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="col-xl-5">
                                    <div class="form-group">
                                        <label for="field_7" >Address</label>
                                        <input  type="text" class="form-control adminAddSetPadding" id="field_7"  value="<?php echo $urow["userAddress"];?>">
                                    </div>
                                </div>
                                <div class="col-xl-5">
                                    <div class="form-group">
                                        <label for="field_8" >State</label>
                                        <select class="form-control adminAddSetPadding" id="field_8">
                                            <?php displayState($urow["userState"]);?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-3"></div>
                                <div class="col-xl-6 adminAddUserButton">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addAdminData(<?php echo $tableType;?>,2);" value="<?php echo $edit;?>" id="idpk">Update User</button>
                                </div>
                                <div class="col-xl-3"></div>
                                <?php
                                    }
                                }
                            }
                            if($tableType == 30)
                            {
                                $sql = "SELECT * FROM transportrate WHERE id = ".$edit;
                                $query = mysqli_query($conn,$sql);
                                if (mysqli_num_rows($query) > 0) 
                                {
                                    while($urow = mysqli_fetch_array($query))
                                    {
                                ?>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_1">Select Agent</label>
                                    <select class="form-control adminAddSetPadding" id="field_1" onchange="getOtherCapacity(this.value);">
                                        <option disabled selected hidden>-- Pick one company --</option>
                                        <?php
                                        $conn = connDB();
                                        $sql_select_costCenter = "SELECT * FROM company WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row2 = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                if($row2["companyID_PK"] == $urow["companyID_FK"])
                                                {
                                                    echo '<option value="'.$row2["companyID_PK"].'" selected>'.$row2["companyName"].' </option>';
                                                }
                                                else
                                                {
                                                    echo '<option value="'.$row2["companyID_PK"].'">'.$row2["companyName"].' </option>';
                                                }
                                                
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-xl-5" >
                                <label for="field_2" >Load Capacity</label>
                                    <input type="text" class="form-control adminAddSetPadding" id="field_2" disabled value="<?php echo $urow["loadTransport"];?>">
                                </div>
                                <div class="col-xl-1"></div>
                                
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_3">Transport Rates</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_3" min="0" value="<?php echo $urow["rates"];?>">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="field_4">Consol Rates</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_4" min="0" value="<?php echo $urow["consoleRate"];?>">
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_5" >Origin Zone</label>
                                    <select class="form-control adminAddSetPadding" id="field_5">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php

                                        $sql_select_costCenter = "SELECT * FROM zones WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                if($row["zonesID_PK"] == $urow["origin"])
                                                {
                                                    echo '<option selected value="'.$row["zonesID_PK"].'">'.$row["zonesName"].','.$row["zonesState"].'</option>';
                                                }
                                                else
                                                {
                                                    echo '<option value="'.$row["zonesID_PK"].'">'.$row["zonesName"].','.$row["zonesState"].'</option>';
                                                }
                                               
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-xl-5" >
                                    <label for="field_6" >Destination Zone</label>
                                    <select class="form-control adminAddSetPadding" id="field_6" >
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php

                                        $sql_select_costCenter = "SELECT * FROM zones WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                if($row["zonesID_PK"] == $urow["destination"])
                                                {
                                                    echo '<option selected value="'.$row["zonesID_PK"].'">'.$row["zonesName"].','.$row["zonesState"].'</option>';
                                                }
                                                else
                                                {
                                                    echo '<option value="'.$row["zonesID_PK"].'">'.$row["zonesName"].','.$row["zonesState"].'</option>';
                                                }
                                                
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="indexSubmitButtonDiv col-xl-12">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addTransportRates(<?php echo $tableType;?>,2);" value="<?php echo $edit;?>" id="idpk">Edit Transport Rates</button>
                                </div>
                                <?php
                                    }
                                }
                            } 
                            if($tableType == 41)
                            {
                                ?>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                <?php
                                $conn = connDB();
                                echo"

                                <label for='field_1' >Load Capacity</label>
                                <select class='form-control adminAddSetPadding' id='field_1' disabled>
                                    <option disabled selected hidden>-- Pick one capacity --</option>";
                                   
                               

                                $sql_select_costCenter = "SELECT * FROM additionalservicefee WHERE id = ".$edit;
                                $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                if (mysqli_num_rows($result_select_costCenter) > 0)
                                {
                                    while($row = mysqli_fetch_assoc($result_select_costCenter))
                                    {
                                        $overnight = $row['overnight'];
                                        $nightDeliveries = $row['nightDeliveries'];
                                        $sunday = $row['sunday'];
                                        $public = $row['public'];
                                        $cancel = $row['cancel'];
                                        $consol = $row['consol'];
                                        $lockupDay = $row['lockupDay'];
                                        $lockupNight = $row['lockupNight'];

                                        if($row['loadTrans'] == '20 Ft / 10 Ton')
                                        {
                                            echo "<option value='20 Ft / 10 Ton' selected>20 Ft / 10 Ton</option>";
                                        }
                                        else
                                        {
                                            echo "<option value='20 Ft / 10 Ton'>20 Ft / 10 Ton</option>";
                                        }

                                        if($row['loadTrans'] == '1 Tonner')
                                        {
                                            echo "<option value='1 Tonner' selected>1 Tonner</option>";
                                        }
                                        else
                                        {
                                            echo "<option value='1 Tonner'>1 Tonner</option>";
                                        }

                                        if($row['loadTrans'] == '3 Ton')
                                        {
                                            echo "<option value='3 Ton' selected>3 Ton</option>";
                                        }
                                        else
                                        {
                                            echo "<option value='3 Ton'>3 Ton</option>";
                                        }

                                        if($row['loadTrans'] == '40 Ft / Q6')
                                        {
                                            echo "<option value='40 Ft / Q6' selected>40 Ft / Q6</option>";
                                        }
                                        else
                                        {
                                            echo "<option value='40 Ft / Q6'>40 Ft / Q6</option>";
                                        }

                                        if($row['loadTrans'] == '40 Ft / Q6 Air-Ride')
                                        {
                                            echo "<option value='40 Ft / Q6 Air-Ride' selected>40 Ft / Q6 Air-Ride</option>";
                                        }
                                        else
                                        {
                                            echo "<option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>";
                                        }

                                        if($row['loadTrans'] == '45 Ft / Q7')
                                        {
                                            echo "<option value='45 Ft / Q7' selected>45 Ft / Q7</option>";
                                        }
                                        else
                                        {
                                            echo "<option value='45 Ft / Q7'>45 Ft / Q7</option>";
                                        }
                                    }
                                }
                                echo " </select> ";
                                ?>
                                </div>
                                <div class="form-group col-xl-5" >
                                    <label for="field_2">Overnight Rates</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_2" min="0" value="<?php echo $overnight;?>">
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_3">Night Deliveries Rates</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_3" min="0" value="<?php echo $nightDeliveries;?>">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="field_4">Sunday Rates</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_4" min="0" value="<?php echo $sunday;?>"> 
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_5">Public Holiday Rates</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_5" min="0" value="<?php echo $public;?>">
                                </div>

                                <div class="form-group col-xl-5">
                                    <label for="field_6">Cancellation Charges</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_6" min="0" value="<?php echo $cancel;?>">
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_7">Lockup (Day)</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_7" min="0" value="<?php echo $lockupDay;?>">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="field_8">Lockup (Night)</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_8" min="0" value="<?php echo $lockupNight;?>">
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_9">Consol</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_9" min="0" value="<?php echo $consol;?>">
                                </div>
                                <div class="col-xl-6"></div>

                                <div class="indexSubmitButtonDiv col-xl-12">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addAdminData(<?php echo $tableType;?>,2);" value="<?php echo $edit;?>" id="idpk">Update Additional Service Fee</button>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
    </body>
</html>
<?php
        }
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>