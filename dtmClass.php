<?php
class Dtmlist 
{
     var $id;
     var $dtmDateCreated;
     var $dtmDGF;
     var $companyID_FK;
     var $dtmBookingDate;
     var $dtmBookingTime;
     var $dtmPickupDate;
     var $dtmPickupTime;
     var $dtmAdhoc;
     var $dtmOriginPointID_FK;
     var $dtmOriginZoneID_FK;
     var $dtmDestinationPointID_FK;
     var $dtmDestinationZoneID_FK;
     var $truckID_FK;
     var $driverID_FK;
     var $dtmUrgentMemo;
     var $dtmOriginF;
     var $dtmOriginDockTime;
     var $dtmOriginCompleteLoadingTime;
     var $dtmOriginDepartureTimeFromPost;
     var $dtmDestinationArrivalTimeAtPost;
     var $dtmDestinationDockTime;
     var $dtmDestinationCompleteUnloadTime;
     var $dtmDestinationDepartTimeFromPost;
     var $dtmQuantityCarton;
     var $dtmQuantityPalllets;
     var $dtmQuantityCages;
     var $dtmDelayReasons;
     var $costCenter;
     var $dtmPlannerID_FK;
     var $dtmRequestBy;
     var $dtmRemarks;
     var $dtmIsPaidDriverServiceFee;

     var $truckCap;
     var $loadCap;
     var $tpt;
     var $quantityLoad;

     var $invoiceNo;
     var $remarks;
     var $transportcharge;
     var $faf;
     var $operatingHour;
     var $totalCharges;

     /**
     * @return mixed
     */
     public function getoperatingHour()
     {
          return $this->operatingHour;
     }

     /**
     * @param mixed $operatingHour
     */
     public function setoperatingHour($operatingHour)
     {
          $this->operatingHour = $operatingHour;
     }
 /**
     * @return mixed
     */
     public function gettotalCharges()
     {
          return $this->totalCharges;
     }

     /**
     * @param mixed $totalCharges
     */
     public function settotalCharges($totalCharges)
     {
          $this->totalCharges = $totalCharges;
     }

      /**
     * @return mixed
     */
     public function getfaf()
     {
          return $this->faf;
     }

     /**
     * @param mixed $faf
     */
     public function setfaf($faf)
     {
          $this->faf = $faf;
     }
     /**
     * @return mixed
     */
     public function gettransportcharge()
     {
          return $this->transportcharge;
     }

     /**
     * @param mixed $transportcharge
     */
     public function settransportcharge($transportcharge)
     {
          $this->transportcharge = $transportcharge;
     }
/**
     * @return mixed
     */
     public function getremarks()
     {
          return $this->remarks;
     }

     /**
     * @param mixed $remarks
     */
     public function setremarks($remarks)
     {
          $this->remarks = $remarks;
     }

/**
     * @return mixed
     */
     public function getinvoiceNo()
     {
          return $this->invoiceNo;
     }

     /**
     * @param mixed $invoiceNo
     */
     public function setinvoiceNo($invoiceNo)
     {
          $this->invoiceNo = $invoiceNo;
     }

     /**
     * @return mixed
     */
     public function getId()
     {
          return $this->id;
     }

     /**
     * @param mixed $id
     */
     public function setId($id)
     {
          $this->id = $id;
     }

     /**
     * @return mixed
     */
     public function getDateCreated()
     {
          return $this->dtmDateCreated;
     }

     /**
     * @param mixed $dtmDateCreated
     */
     public function setDateCreated($dtmDateCreated)
     {
          $this->dtmDateCreated = $dtmDateCreated;
     }

     /**
     * @return mixed
     */
     public function getDGF()
     {
          return $this->dtmDGF;
     }

     /**
     * @param mixed $dtmDGF
     */
     public function setDGF($dtmDGF)
     {
          $this->dtmDGF = $dtmDGF;
     }

     /**
     * @return mixed
     */
     public function getCompanyId()
     {
          return $this->companyID_FK;
     }

     /**
     * @param mixed $companyID_FK
     */
     public function setCompanyId($companyID_FK)
     {
          $this->companyID_FK = $companyID_FK;
     }

     /**
     * @return mixed
     */
     public function getBookingDate()
     {
          return $this->dtmBookingDate;
     }

     /**
     * @param mixed $dtmBookingDate
     */
     public function setBookingDate($dtmBookingDate)
     {
          $this->dtmBookingDate = $dtmBookingDate;
     }

      /**
     * @return mixed
     */
     public function getBookingTime()
     {
          return $this->dtmBookingTime;
     }

     /**
     * @param mixed $dtmBookingTime
     */
     public function setBookingTime($dtmBookingTime)
     {
          $this->dtmBookingTime = $dtmBookingTime;
     }
      /**
     * @return mixed
     */
     public function getPickupDate()
     {
          return $this->dtmPickupDate;
     }

     /**
     * @param mixed $dtmPickupDate
     */
     public function setPickupDate($dtmPickupDate)
     {
          $this->dtmPickupDate = $dtmPickupDate;
     }

      /**
     * @return mixed
     */
     public function getPickupTime()
     {
          return $this->dtmPickupTime;
     }

     /**
     * @param mixed $dtmPickupTime
     */
     public function setPickupTime($dtmPickupTime)
     {
          $this->dtmPickupTime = $dtmPickupTime;
     }
      /**
     * @return mixed
     */
     public function getAdHoc()
     {
          return $this->dtmAdhoc;
     }

     /**
     * @param mixed $dtmAdhoc
     */
     public function setAdHoc($dtmAdhoc)
     {
          $this->dtmAdhoc = $dtmAdhoc;
     }

      /**
     * @return mixed
     */
     public function getDtmOriginPointID_FK()
     {
          return $this->dtmOriginPointID_FK;
     }

     /**
     * @param mixed $dtmOriginPointID_FK
     */
     public function setDtmOriginPointID_FK($dtmOriginPointID_FK)
     {
          $this->dtmOriginPointID_FK = $dtmOriginPointID_FK;
     }
     /**
     * @return mixed
     */
     public function getDtmOriginZoneID_FK()
     {
          return $this->dtmOriginZoneID_FK;
     }

     /**
     * @param mixed $dtmOriginZoneID_FK
     */
     public function setDtmOriginZoneID_FK($dtmOriginZoneID_FK)
     {
          $this->dtmOriginZoneID_FK = $dtmOriginZoneID_FK;
     }

     /**
     * @return mixed
     */
     public function getDtmDestinationPointID_FK()
     {
          return $this->dtmDestinationPointID_FK;
     }

     /**
     * @param mixed $dtmDestinationPointID_FK
     */
     public function setDtmDestinationPointID_FK($dtmDestinationPointID_FK)
     {
          $this->dtmDestinationPointID_FK = $dtmDestinationPointID_FK;
     }

     /**
     * @return mixed
     */
     public function getDtmDestinationZoneID_FK()
     {
          return $this->dtmDestinationZoneID_FK;
     }

     /**
     * @param mixed $dtmDestinationZoneID_FK
     */
     public function setDtmDestinationZoneID_FK($dtmDestinationZoneID_FK)
     {
          $this->dtmDestinationZoneID_FK = $dtmDestinationZoneID_FK;
     }

     /**
     * @return mixed
     */
     public function getTruckID_FK()
     {
          return $this->truckID_FK;
     }

     /**
     * @param mixed $truckID_FK
     */
     public function setTruckID_FK($truckID_FK)
     {
          $this->truckID_FK = $truckID_FK;
     }
     /**
     * @return mixed
     */
     public function getDriverID_FK()
     {
          return $this->driverID_FK;
     }

     /**
     * @param mixed $driverID_FK
     */
     public function setDriverID_FK($driverID_FK)
     {
          $this->driverID_FK = $driverID_FK;
     }
      /**
     * @return mixed
     */
     public function getDtmUrgentMemo()
     {
          return $this->dtmUrgentMemo;
     }

     /**
     * @param mixed $dtmUrgentMemo
     */
     public function setDtmUrgentMemo($dtmUrgentMemo)
     {
          $this->dtmUrgentMemo = $dtmUrgentMemo;
     }

      /**
     * @return mixed
     */
     public function getDtmOriginF()
     {
          return $this->dtmOriginF;
     }

     /**
     * @param mixed $dtmOriginF
     */
     public function setDtmOriginF($dtmOriginF)
     {
          $this->dtmOriginF = $dtmOriginF;
     }
      /**
     * @return mixed
     */
     public function getDtmOriginDockTime()
     {
          return $this->dtmOriginDockTime;
     }

     /**
     * @param mixed $dtmOriginDockTime
     */
     public function setDtmOriginDockTime($dtmOriginDockTime)
     {
          $this->dtmOriginDockTime = $dtmOriginDockTime;
     }

      /**
     * @return mixed
     */
     public function getDtmOriginCompleteLoadingTime()
     {
          return $this->dtmOriginCompleteLoadingTime;
     }

     /**
     * @param mixed $dtmOriginCompleteLoadingTime
     */
     public function setDtmOriginCompleteLoadingTime($dtmOriginCompleteLoadingTime)
     {
          $this->dtmOriginCompleteLoadingTime = $dtmOriginCompleteLoadingTime;
     }
      /**
     * @return mixed
     */
     public function getDtmOriginDepartureTimeFromPost()
     {
          return $this->dtmOriginDepartureTimeFromPost;
     }

     /**
     * @param mixed $dtmOriginDepartureTimeFromPost
     */
     public function setDtmOriginDepartureTimeFromPost($dtmOriginDepartureTimeFromPost)
     {
          $this->dtmOriginDepartureTimeFromPost = $dtmOriginDepartureTimeFromPost;
     }

      /**
     * @return mixed
     */
     public function getDtmDestinationArrivalTimeAtPost()
     {
          return $this->dtmDestinationArrivalTimeAtPost;
     }

     /**
     * @param mixed $dtmDestinationArrivalTimeAtPost
     */
     public function setDtmDestinationArrivalTimeAtPost($dtmDestinationArrivalTimeAtPost)
     {
          $this->dtmDestinationArrivalTimeAtPost = $dtmDestinationArrivalTimeAtPost;
     }

       /**
     * @return mixed
     */
     public function getDtmDestinationDockTime()
     {
          return $this->dtmDestinationDockTime;
     }

     /**
     * @param mixed $dtmDestinationDockTime
     */
     public function setDtmDestinationDockTime($dtmDestinationDockTime)
     {
          $this->dtmDestinationDockTime = $dtmDestinationDockTime;
     }
       /**
     * @return mixed
     */
     public function getDtmDestinationCompleteUnloadTime()
     {
          return $this->dtmDestinationCompleteUnloadTime;
     }

     /**
     * @param mixed $dtmDestinationCompleteUnloadTime
     */
     public function setDtmDestinationCompleteUnloadTime($dtmDestinationCompleteUnloadTime)
     {
          $this->dtmDestinationCompleteUnloadTime = $dtmDestinationCompleteUnloadTime;
     }

     /**
     * @return mixed
     */
     public function getDtmDestinationDepartTimeFromPost()
     {
          return $this->dtmDestinationDepartTimeFromPost;
     }

     /**
     * @param mixed $dtmDestinationDepartTimeFromPost
     */
     public function setDtmDestinationDepartTimeFromPost($dtmDestinationDepartTimeFromPost)
     {
          $this->dtmDestinationDepartTimeFromPost = $dtmDestinationDepartTimeFromPost;
     }
     /**
     * @return mixed
     */
     public function getDtmQuantityCarton()
     {
          return $this->dtmQuantityCarton;
     }

     /**
     * @param mixed $dtmQuantityCarton
     */
     public function setDtmQuantityCarton($dtmQuantityCarton)
     {
          $this->dtmQuantityCarton = $dtmQuantityCarton;
     }
     /**
     * @return mixed
     */
     public function getDtmQuantityPalllets()
     {
          return $this->dtmQuantityPalllets;
     }

     /**
     * @param mixed $dtmQuantityPalllets
     */
     public function setDtmQuantityPalllets($dtmQuantityPalllets)
     {
          $this->dtmQuantityPalllets = $dtmQuantityPalllets;
     }
     /**
     * @return mixed
     */
     public function getDtmQuantityCages()
     {
          return $this->dtmQuantityCages;
     }

     /**
     * @param mixed $dtmQuantityCages
     */
     public function setDtmQuantityCages($dtmQuantityCages)
     {
          $this->dtmQuantityCages = $dtmQuantityCages;
     }

     /**
     * @return mixed
     */
     public function getDtmDelayReasons()
     {
          return $this->dtmDelayReasons;
     }

     /**
     * @param mixed $dtmDelayReasons
     */
     public function setDtmDelayReasons($dtmDelayReasons)
     {
          $this->dtmDelayReasons = $dtmDelayReasons;
     }

     /**
     * @return mixed
     */
     public function getCostCenter()
     {
          return $this->costCenter;
     }

     /**
     * @param mixed $costCenter
     */
     public function setCostCenter($costCenter)
     {
          $this->costCenter = $costCenter;
     }

     /**
     * @return mixed
     */
     public function getDtmPlannerID_FK()
     {
          return $this->dtmPlannerID_FK;
     }

     /**
     * @param mixed $dtmPlannerID_FK
     */
     public function setDtmPlannerID_FK($dtmPlannerID_FK)
     {
          $this->dtmPlannerID_FK = $dtmPlannerID_FK;
     }
     /**
     * @return mixed
     */
     public function getDtmRequestBy()
     {
          return $this->dtmRequestBy;
     }

     /**
     * @param mixed $dtmRequestBy
     */
     public function setDtmRequestBy($dtmRequestBy)
     {
          $this->dtmRequestBy = $dtmRequestBy;
     }

     /**
     * @return mixed
     */
     public function getDtmRemarks()
     {
          return $this->dtmRemarks;
     }

     /**
     * @param mixed $dtmRemarks
     */
     public function setDtmRemarks($dtmRemarks)
     {
          $this->dtmRemarks = $dtmRemarks;
     }

     /**
     * @return mixed
     */
     public function getDtmIsPaidDriverServiceFee()
     {
          return $this->dtmIsPaidDriverServiceFee;
     }

     /**
     * @param mixed $dtmIsPaidDriverServiceFee
     */
     public function setDtmIsPaidDriverServiceFee($dtmIsPaidDriverServiceFee)
     {
          $this->dtmIsPaidDriverServiceFee = $dtmIsPaidDriverServiceFee;
     }
}

function putBorderStyleRow($sheet,$cellAlphabet,$type = null,$colour = null)
{
     if($type && $type == 1)
     {
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
     }
     if($type && $type == 2)
     {
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          if($colour)
          {
               $sheet->getStyle($cellAlphabet)
               ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
               $sheet->getStyle($cellAlphabet)
               ->getFill()->getStartColor()->setARGB($colour);
          }

     }
     else
     {
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
     }
     
}    
function getDatePHPWithTime($dateVar,$type) 
{
    date_default_timezone_set('Asia/Kuala_Lumpur');
    $date = date("Y-m-d", strtotime($dateVar));
    $time = date("H-i-s", strtotime("23:59:59"));

    return $date." ".$time;
}
function getUser($uid)
{
     $conn = connDB();
     $sqlA = " SELECT userName FROM user WHERE  userID_PK = ".$uid;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['userName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getPlaceZone($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT zonesName FROM zones WHERE zonesID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['zonesName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
     
function getPlace($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['pointzonePlaceName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getCompany($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT companyName FROM company WHERE companyID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['companyName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getDriver($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT driverName FROM driver WHERE driverID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['driverName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getTruckCap($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT truckCapacity FROM trucks WHERE truckID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['truckCapacity'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getTruckPlateNo($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT truckPlateNo FROM trucks WHERE truckID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['truckPlateNo'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getCostCenter($ccId)
{
     $conn = connDB();
     $sqlA = " SELECT costCenterName FROM costcenter WHERE costCenterID_PK = ".$ccId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['costCenterName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function putWidth($alphabet,$widthsize,$sheet)
{
     for($x='A'; $x != $alphabet; $x++)
     {
          $sheet->getColumnDimension($x)->setWidth($widthsize);

     }
}