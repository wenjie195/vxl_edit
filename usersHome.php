<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        require 'generalFunction.php';

?>
<!doctype html>
<html lang="en">
    <head>
        <title>System User Home</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>System Users Dashboard</h3>
                    </div>
                    <div class="row">
                        <div class="col-xl-12" id="showUser"></div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
        <script>
            $(document).ready(function()
            {
                ajaxUser();
            });
        </script>
    </body>
</html>
<?php
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>