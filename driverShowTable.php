<?php
require 'generalFunction.php';
$conn = connDB();

$fromPage = $_POST['fromPage'];
$condition = $_POST['condition'];
$pageNo = $_POST['pageNo'];
$filter = $_POST['filter'];
$searchWord = $_POST['searchWord'];

// echo $fromPage;
// echo $condition;
// echo $pageNo;
// echo $filter;
// echo " =".$searchWord."=";

if($filter == null)
{
    $filter= 1;
}
if($pageNo == null)
{
    $filter= 1;
}
if($searchWord == null)
{
    $searchWord = "";
}

$sqlPageNo = 0;
$sqlPageNo = ($pageNo - 1) * 10;

if($filter == 1)
{
    if($fromPage == 6)
    {
        $orderBy = "driverDateCreated";
    }
}
if($filter == 2)
{
    if($fromPage == 6)
    {
        $orderBy = "driverName";
    }
}
if($filter == 3)
{
    if($fromPage == 6)
    {
        $orderBy = "driverICno";
    }
}



$sql = "";
$sql2 = "";

if($fromPage == 6)
{
    $sql .= " SELECT * FROM driver WHERE showThis = 1";
    $sql2 .= " SELECT COUNT(*) as total2 FROM driver WHERE showThis = 1 ";
}


if($searchWord != null && $searchWord != "")
{
    if($fromPage == 6)
    {
        $sql .= " AND driverName LIKE '%".$searchWord."%' OR driverNickName LIKE '%".$searchWord."%' OR driverICno LIKE '%".$searchWord."%' OR driverPhoneNo LIKE '%".$searchWord."%' ";
        $sql2 .= " AND driverName LIKE '%".$searchWord."%' OR driverNickName LIKE '%".$searchWord."%' OR driverICno LIKE '%".$searchWord."%' OR driverPhoneNo LIKE '%".$searchWord."%'";
    }
}

if ($orderBy != "") 
{
    if($filter == 1 || $filter == 3)
    {
        $sql .= " ORDER BY ".$orderBy." DESC ,driverID_PK ASC ";
        $sql2 .= " ORDER BY ".$orderBy." DESC ,driverID_PK ASC ";
    }
    else
    {
        $sql .= " ORDER BY ".$orderBy." ASC ";
        $sql2 .= " ORDER BY ".$orderBy." ASC ";
    }
}

$sql .=" LIMIT ".$sqlPageNo.",10 ";

if($condition == 1)
{
    if($fromPage == 6)
    {
        $initialSql = "SELECT COUNT(*) as total from driver WHERE showThis = 1";
    }

    $result = mysqli_query($conn,$initialSql);
    $data = mysqli_fetch_assoc($result);
    $no_of_pages = 0;
    $no_of_pages = ceil($data['total'] / 10);
}
else
{
    $result2 = mysqli_query($conn,$sql2);
    $dataCount = mysqli_fetch_assoc($result2);
    $no_of_pages = 0;
    $no_of_pages = ceil($dataCount['total2'] / 10);
    
}

$querylisting = mysqli_query($conn,$sql);

generateDeleteModal($fromPage);
generateConfirmationDeleteModal($fromPage);
?>
<script>$("#pagination"+<?php echo $fromPage;?>+" option").remove();</script>
<table class="table table-sm table-hovered table-striped table-responsive-xl dtmTableNoWrap removebottommargin">
    <thead>
        <tr>
        <?php 
            if($fromPage == 6)
            {
                ?>
                    <th class="adminTableWidthTD text-center">Selection</th>
                    <th class="adminTableWidthTD text-center">Driver Service Fee</th>
                    <th class="text-center">Driver ID</th>
                    <th class="text-center">Driver IC No</th>
                    <th class="text-center">Driver Name</th>
                    <th class="text-center">Driver Nickname</th>
                    <th class="text-center">Driver Phone No</th>
                <?php
            }
        ?>
        </tr>
  </thead>
  <tbody>
    <?php 
        if (mysqli_num_rows($querylisting) > 0) 
        {
            while($row = mysqli_fetch_array($querylisting))
            {
    ?>
    <tr>
            <?php 
                if($fromPage == 6)
                {  
            ?>
                <td class="adminTableWidthTD ">
                    <div class="adminAlignOptionInline">
                        <form action="driverEdit.php" method="POST" class="adminformEdit">
                            <input type="hidden" name="tableType" value="<?php echo $fromPage;?>">
                            <button class="btn btn-warning edtOpt" value="<?php echo $row['driverID_PK'];?>" name="edit">Update</button>
                        </form>
                        <button class="btn btn-danger edtOpt" value="<?php echo $row['driverID_PK'];?>" onclick="deleteHR(<?php echo $fromPage;?>,this.value)">Delete</button>
                    </div>
                </td>
                <td class="adminTableWidthTD text-center">
                    <div class="adminAlignOptionInline">
                        <form action="eachDriverServiceFee.php" method="POST" class="adminformEdit">
                            <button class="btn btn-success edtOpt" value="<?php echo $row['driverID_PK'];?>" name="edit">Show</button>
                        </form>
                    </div>
                </td>
                <td class="text-center">
                    <?php 
                        echo $row['driverID_PK'];
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                        echo $row['driverICno'];
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                        echo $row['driverName'];
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                        echo $row['driverNickName'];
                    ?>
                </td>
                <td class="text-center">
                    <?php 
                        echo $row['driverPhoneNo'];
                    ?>
                </td>
            <?php
                }
            ?>
        </tr>
    <?php 
            }
        }
        else
        {
            ?>
            <tr>
                <td colspan="6" style="text-align:center;">No Records Found</td>
            </tr>
            <?php
        }
    ?>
  </tbody>
</table>
<?php
    if($condition == 1)
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$data['total']);
    }
    else
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$dataCount['total2']);
    }
?>
 