<?php
require 'generalFunction.php';
$conn = connDB();

$fromPage = $_POST['fromPage'];
$condition = $_POST['condition'];
$pageNo = $_POST['pageNo'];
$filter = $_POST['filter'];
$searchWord = $_POST['searchWord'];

// echo $fromPage;
// echo $condition;
// echo $pageNo;
// echo $filter;
// echo " =".$searchWord."=";

if($filter == null)
{
    $filter= 1;
}
if($pageNo == null)
{
    $filter= 1;
}
if($searchWord == null)
{
    $searchWord = "";
}

$sqlPageNo = 0;
$sqlPageNo = ($pageNo - 1) * 10;

if($filter == 1)
{
    if($fromPage == 1)
    {
        $orderBy = "pointzoneID_PK";
    }
    if($fromPage == 2)
    {
        $orderBy = "zonesID_PK";
    }
    if($fromPage == 3)
    {
        $orderBy = "costCenterDateCreated";
    }
    if($fromPage == 4)
    {
        $orderBy = "companyID_PK";
    }
    if($fromPage == 5)
    {
        $orderBy = "userDateCreated";
    }
}
if($filter == 2)
{
    if($fromPage == 1)
    {
        $orderBy = "pointzonePlaceName";
    }
    if($fromPage == 2)
    {
        $orderBy = "zonesName";
    }
    if($fromPage == 3)
    {
        $orderBy = "costCenterName";
    }
    if($fromPage == 4)
    {
        $orderBy = "companyName";
    }
    if($fromPage == 5)
    {
        $orderBy = "userName";
    }
}
if($filter == 3)
{
    if($fromPage == 2)
    {
        $orderBy = "zonesState";
    }
    if($fromPage == 5)
    {
        $orderBy = "userID_PK";
    }
}
if($filter == 4)
{
    if($fromPage == 5)
    {
        $orderBy = "userIC";
    }
}
if($filter == 5)
{
    if($fromPage == 5)
    {
        $orderBy = "userLevel";
    }
}


$sql = "";
$sql2 = "";

if($fromPage == 1)
{
    $sql .= " SELECT * FROM pointzone WHERE showThis = 1";
    $sql2 .= " SELECT COUNT(*) as total2 FROM pointzone WHERE showThis = 1";
}
if($fromPage == 2)
{
    $sql .= " SELECT * FROM zones WHERE showThis = 1";
    $sql2 .= " SELECT COUNT(*) as total2 FROM zones WHERE showThis = 1";
}
if($fromPage == 3)
{
    $sql .= " SELECT * FROM costcenter WHERE showThis = 1";
    $sql2 .= " SELECT COUNT(*) as total2 FROM costcenter WHERE showThis = 1";
}
if($fromPage == 4)
{
    $sql .= " SELECT * FROM company WHERE showThis = 1";
    $sql2 .= " SELECT COUNT(*) as total2 FROM company WHERE showThis = 1";
}
if($fromPage == 5)
{
    $sql .= " SELECT * FROM user WHERE showThis = 1";
    $sql2 .= " SELECT COUNT(*) as total2 FROM user WHERE showThis = 1";
}


if($searchWord != null && $searchWord != "")
{
    if($fromPage == 1)
    {
        $sql .= " AND pointzonePlaceName LIKE '%".$searchWord."%' ";
        $sql2 .= " AND pointzonePlaceName LIKE '%".$searchWord."%' ";
    }
    if($fromPage == 2)
    {
        $sql .= " AND zonesName LIKE '%".$searchWord."%' OR zonesState LIKE '%".$searchWord."%'";
        $sql2 .= " AND zonesName LIKE '%".$searchWord."%' OR zonesState LIKE '%".$searchWord."%'";
    }
    if($fromPage == 3)
    {
        $sql .= " AND costCenterName LIKE '%".$searchWord."%' ";
        $sql2 .= " AND costCenterName LIKE '%".$searchWord."%' ";
    }
    if($fromPage == 4)
    {
        $sql .= " AND companyName LIKE '%".$searchWord."%' ";
        $sql2 .= " AND companyName LIKE '%".$searchWord."%' ";
    }
    if($fromPage == 5)
    {
        $sql .= " AND userName LIKE '%".$searchWord."%' OR userIC LIKE '%".$searchWord."%' OR userState LIKE '%".$searchWord."%' ";
        $sql2 .= " AND userName LIKE '%".$searchWord."%' OR userIC LIKE '%".$searchWord."%' OR userState LIKE '%".$searchWord."%' ";
    }
}

if ($orderBy != "") 
{
    if($filter == 1)
    {
        if($fromPage == 1 || $fromPage == 2 || $fromPage == 4)
        {
            $sql .= " ORDER BY ".$orderBy." ASC ";
            $sql2 .= " ORDER BY ".$orderBy." ASC ";
        }
        else
        {
            $sql .= " ORDER BY ".$orderBy." DESC ";
            $sql2 .= " ORDER BY ".$orderBy." DESC ";
        }
    }
    else
    {
        $sql .= " ORDER BY ".$orderBy." ASC ";
        $sql2 .= " ORDER BY ".$orderBy." ASC ";
    }
}

$sql .=" LIMIT ".$sqlPageNo.",10 ";

if($condition == 1)
{
    if($fromPage == 1)
    {
        $initialSql = "SELECT COUNT(*) as total from pointzone WHERE showThis = 1";
    }
    if($fromPage == 2)
    {
        $initialSql = "SELECT COUNT(*) as total from zones WHERE showThis = 1";
    }
    if($fromPage == 3)
    {
        $initialSql = "SELECT COUNT(*) as total from costcenter WHERE showThis = 1";
    }
    if($fromPage == 4)
    {
        $initialSql = "SELECT COUNT(*) as total from company WHERE showThis = 1";
    }
    if($fromPage == 5)
    {
        $initialSql = "SELECT COUNT(*) as total from user WHERE showThis = 1";
    }

    $result = mysqli_query($conn,$initialSql);
    $data = mysqli_fetch_assoc($result);
    $no_of_pages = 0;
    $no_of_pages = ceil($data['total'] / 10);
}
else
{
    $result2 = mysqli_query($conn,$sql2);
    $dataCount = mysqli_fetch_assoc($result2);
    $no_of_pages = 0;
    $no_of_pages = ceil($dataCount['total2'] / 10);
    
}

$querylisting = mysqli_query($conn,$sql);

generateDeleteModal($fromPage);
generateConfirmationDeleteModal($fromPage);
?>
<script>$("#pagination"+<?php echo $fromPage;?>+" option").remove();</script>
<table class="table table-sm dtmTableNoWrap table-hovered table-striped table-responsive-xl removebottommargin">
    <thead>
        <tr>
        <?php 
            if($fromPage == 1)
            {
                ?>
                    <th class="adminTableWidthTD">Selection</th>
                    <th >Place Points</th>
                <?php
            }
            else if($fromPage == 2)
            {
                ?>
                    <th class="adminTableWidthTD">Selection</th>
                    <th class="adminTableWidthTD">Zone Name</th>
                    <th >Zone State</th>
                <?php
            }
            else if($fromPage == 3)
            {
                ?>
                    <th class="adminTableWidthTD">Selection</th>
                    <th>Cost Center</th>
                    <th>Agent</th>
                <?php
            }
            else if($fromPage == 4)
            {
                ?>
                    <th class="adminTableWidthTD">Selection</th>
                    <th>Agent Name</th>
                <?php
            }
            else if($fromPage == 5)
            {
                ?>
                    <th class="adminTableWidthTD">Selection</th>
                    <th>User ID</th>
                    <th>IC No</th>
                    <th>Position</th>
                    <th>Name</th>
                    <th>Nickname</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>State</th>
                    <th>Phone No</th>
                <?php
            }
        ?>
        </tr>
  </thead>
  <tbody>
    <?php 
        if (mysqli_num_rows($querylisting) > 0) 
        {
            while($row = mysqli_fetch_array($querylisting))
            {
    ?>
    <tr>
        <?php 

            
            if($fromPage == 1)
            {
                
                ?>
                    <td class="adminTableWidthTD">
                        <div class="adminAlignOptionInline">
                            <form action="CDEdit.php" method="POST" class="adminformEdit">
                                <input type="hidden" name="tableType" value="<?php echo $fromPage;?>">
                                <button class="btn btn-warning edtOpt" value="<?php echo $row['pointzoneID_PK'];?>" name="edit">Update</button>
                            </form>
                            <button class="btn btn-danger edtOpt" value="<?php echo $row['pointzoneID_PK'];?>" onclick="deleteAdmin(<?php echo $fromPage;?>,this.value)">Delete</button>
                        </div>
                    </td>
                    <td >
                        <?php echo $row["pointzonePlaceName"];?>
                    </td>
                <?php
            }
            else if($fromPage == 2)
            {
                ?>
                    <td class="adminTableWidthTD">
                        <div class="adminAlignOptionInline">
                            <form action="CDEdit.php" method="POST" class="adminformEdit">
                                <input type="hidden" name="tableType" value="<?php echo $fromPage;?>">
                                <button class="btn btn-warning edtOpt" value="<?php echo $row['zonesID_PK'];?>" name="edit">Update</button>
                            </form>
                            <button class="btn btn-danger edtOpt" value="<?php echo $row['zonesID_PK'];?>" onclick="deleteAdmin(<?php echo $fromPage;?>,this.value)">Delete</button>
                        </div>
                    </td>
                    <td class="adminTableWidthTD">
                        <?php echo $row["zonesName"];?>
                    </td>
                    <td >
                        <?php echo $row["zonesState"];?>
                    </td>
                <?php
            }
            else if($fromPage == 3)
            {
                ?>
                    <td class="adminTableWidthTD">
                        <div class="adminAlignOptionInline">
                            <form action="CDEdit.php" method="POST" class="adminformEdit">
                                <input type="hidden" name="tableType" value="<?php echo $fromPage;?>">
                                <button class="btn btn-warning edtOpt" value="<?php echo $row['costCenterID_PK'];?>" name="edit">Update</button>
                            </form>
                            <button class="btn btn-danger edtOpt" value="<?php echo $row['costCenterID_PK'];?>" onclick="deleteAdmin(<?php echo $fromPage;?>,this.value)">Delete</button>
                        </div>
                    </td>
                    <td >
                        <?php echo $row["costCenterName"];?>
                    </td>
                    <td >
                        <?php 
                        $costCenterDisplay = "SELECT companyName FROM company WHERE companyID_PK = ".$row['companyID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['companyName'];
                            }
                        }
                        ?>
                    </td>
                <?php
            }
            else if($fromPage == 4)
            {
                ?>
                    <td class="adminTableWidthTD">
                        <div class="adminAlignOptionInline">
                            <form action="CDEdit.php" method="POST" class="adminformEdit">
                                <input type="hidden" name="tableType" value="<?php echo $fromPage;?>">
                                <button class="btn btn-warning edtOpt" value="<?php echo $row['companyID_PK'];?>" name="edit">Update</button>
                            </form>
                            <button class="btn btn-danger edtOpt" value="<?php echo $row['companyID_PK'];?>" onclick="deleteAdmin(<?php echo $fromPage;?>,this.value)">Delete</button>
                        </div>
                    </td>
                    <td >
                        <?php echo $row["companyName"]." ";?>
                    </td>
                <?php
            }
            else if($fromPage == 5)
            {
                ?>
                    <td class="adminTableWidthTD">
                        <div class="adminAlignOptionInline">
                            <form action="CDEdit.php" method="POST" class="adminformEdit">
                                <input type="hidden" name="tableType" value="<?php echo $fromPage;?>">
                                <button class="btn btn-warning edtOpt" value="<?php echo $row['userID_PK'];?>" name="edit">Update</button>
                            </form>
                            <button class="btn btn-danger edtOpt" value="<?php echo $row['userID_PK'];?>" onclick="deleteAdmin(<?php echo $fromPage;?>,this.value)">Delete</button>
                        </div>
                    </td>
                    <td>
                        <?php 
                            echo $row['userID_PK'];
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo $row['userIC'];
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo position($row['userLevel']);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo $row['userName'];
                        ?>
                    </td>
                    
                    <td>
                        <?php 
                            echo $row['userNickName'];
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo $row['userEmail'];
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo $row['userAddress'];
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo $row['userState'];
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo $row['userTele'];
                        ?>
                    </td>
                <?php
                }
                ?>
        </tr>
    <?php 
            }
        }
        else
        {
            if($fromPage == 1)
            {
                ?>
                    <tr>
                        <td colspan="2" style="text-align:center;">No Records Found</td>
                    </tr>
                <?php
            }
            if($fromPage == 2)
            {
                ?>
                    <tr>
                        <td colspan="3" style="text-align:center;">No Records Found</td>
                    </tr>
                <?php
            }
            if($fromPage == 3)
            {
                ?>
                    <tr>
                        <td colspan="3" style="text-align:center;">No Records Found</td>
                    </tr>
                <?php
            }
            if($fromPage == 4)
            {
                ?>
                    <tr>
                        <td colspan="2" style="text-align:center;">No Records Found</td>
                    </tr>
                <?php
            }
            if($fromPage == 5)
            {
                ?>
                    <tr>
                        <td colspan="10" style="text-align:center;">No Records Found</td>
                    </tr>
                <?php
            }
        }
    ?>
  </tbody>
</table>
<?php
    if($condition == 1)
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$data['total']);
    }
    else
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$dataCount['total2']);
    }
?>
 