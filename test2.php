<?php

$fromDate = $_POST['fromDate'];
$toDate = $_POST['toDate'];
$invoiceNo = $_POST['invoiceNo'];
$companyID = $_POST['company'];


function normalTemplateTop($sheet,$fromDate,$toDate,$invoiceNo)
{
     $sheet->setCellValue('A1', 'Dates');
     $sheet->setCellValue('B1', $fromDate);
     $sheet->setCellValue('C1', 'To' );
     $sheet->setCellValue('D1', $toDate);

     $sheet->setCellValue('A2', 'VENDOR');
     $sheet->setCellValue('B2', 'VXL ALLIANCE');
     
     $sheet->setCellValue('A3', 'INVOICE NO');
     $sheet->setCellValue('B3', $invoiceNo);
     $sheet->setCellValue('A4', 'INVOICE DATE');

     $sheet->setCellValue('A6', 'Item No');
     $sheet->setCellValue('B6', 'Pickup Date');
     $sheet->setCellValue('C6', 'Truck No');
     $sheet->setCellValue('D6', 'Truck Size');
     $sheet->setCellValue('E6', 'D/O No');
     $sheet->setCellValue('F6', 'Quantity');
     $sheet->setCellValue('G6', 'Truck Charges');
     $sheet->setCellValue('H6', 'FAF (9%)');
     $sheet->setCellValue('I6', 'Overtime/Detention ');
     $sheet->setCellValue('J6', 'GPS Charge');
     $sheet->setCellValue('K6', 'Pickup/D`Point');
     $sheet->setCellValue('L6', 'Sunday/PH Pickup');
     $sheet->setCellValue('M6', 'Others');
     $sheet->setCellValue('N6', 'Total');
     $sheet->setCellValue('O6', 'Transport Remark');
     $sheet->setCellValue('P6', 'DTM No');
     $sheet->setCellValue('Q6', 'Consol Status');

     putBorderStyleRow($sheet,'A6:Q6',2,'99AFFF');
     putWidth('Q',20,$sheet);
     $sheet->getColumnDimension('F')->setWidth(30);
     $sheet->getStyle('A6:Q6')->getAlignment()->setHorizontal('center');
     $sheet->getStyle('A1:D4')->getAlignment()->setHorizontal('center');
}

function normalTemplateBottom($sheet,$line)
{
     $sheet->setCellValue('A'.$line, "GRAND TOTAL (RM)");
     $sheet->mergeCells('A'.$line.':F'.$line);
     $sheet->setCellValue('G'.$line,'=SUM(G7:G'.($line-1).')');
     $sheet->setCellValue('H'.$line,'=SUM(H7:H'.($line-1).')');
     $sheet->setCellValue('I'.$line,'=SUM(I7:I'.($line-1).')');
     $sheet->setCellValue('J'.$line,'=SUM(J7:J'.($line-1).')');
     $sheet->setCellValue('K'.$line,'=SUM(K7:K'.($line-1).')');
     $sheet->setCellValue('L'.$line,'=SUM(L7:L'.($line-1).')');
     $sheet->setCellValue('M'.$line,'=SUM(M7:M'.($line-1).')');
     $sheet->setCellValue('N'.$line,'=SUM(N7:N'.($line-1).')');
     $sheet->getStyle('G'.$line.':N'.$line)->getAlignment()->setHorizontal('center');
}
function putBorderStyleRow($sheet,$cellAlphabet,$type = null,$colour = null)
{
     if($type && $type == 1)
     {
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
     }
     else if($type && $type == 2)
     {
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          if($colour)
          {
               $sheet->getStyle($cellAlphabet)
               ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
               $sheet->getStyle($cellAlphabet)
               ->getFill()->getStartColor()->setARGB($colour);
          }
     }
     else
     {
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
     }
     
}    
function getDatePHPWithTime($dateVar,$type) 
{
    date_default_timezone_set('Asia/Kuala_Lumpur');
    $date = date("Y-m-d", strtotime($dateVar));
    $time = date("H-i-s", strtotime("23:59:59"));

    return $date." ".$time;
}
function getUser($uid)
{
     $conn = connDB();
     $sqlA = " SELECT userName FROM user WHERE  userID_PK = ".$uid;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['userName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getPlaceZone($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT zonesName FROM zones WHERE zonesID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['zonesName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
     
function getPlace($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['pointzonePlaceName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getCompany($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT companyName FROM company WHERE companyID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['companyName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getDriver($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT driverName FROM driver WHERE driverID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['driverName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getTruckCap($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT truckCapacity FROM trucks WHERE truckID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['truckCapacity'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getTruckPlateNo($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT truckPlateNo FROM trucks WHERE truckID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['truckPlateNo'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getCostCenter($ccId)
{
     $conn = connDB();
     $sqlA = " SELECT costCenterName FROM costcenter WHERE costCenterID_PK = ".$ccId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['costCenterName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function putWidth($alphabet,$widthsize,$sheet)
{
     for($x='A'; $x != $alphabet; $x++)
     {
          $sheet->getColumnDimension($x)->setWidth($widthsize);

     }
}


date_default_timezone_set("Asia/Kuala_Lumpur");
$todayDate = date("Ymd_his");

$filename = 'filename="Invoice_'.$company.'_'.$costcenter.'__'.$fromDate.'_'.$toDate.'.xls"';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; '.$filename);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;

require 'generalFunction.php';
require 'phpexcel/vendor/autoload.php';

ob_end_clean();

$fromDateFormatted = getDatePHP($fromDate);
$toDateFormatted = getDatePHPWithTime($toDate,0);

$conn = connDB();
$costcenterID = getCostCenter($_POST['costcenter']);
$selectedCompanyName = getCompany($companyID);

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$sqlS = " SELECT * FROM invoice WHERE invoiceNo = ".$invoiceNo;
$result1S = mysqli_query($conn,$sqlS);

if (mysqli_num_rows($result1S) > 0) 
{


if($companyID == 43)
{
     if($_POST['costcenter'] == 19 || $_POST['costcenter'] == 20 || $_POST['costcenter'] == 21 )
     {
          $sheet->setCellValue('A1', 'Attn');
          $sheet->setCellValue('A2', 'Mode');
          $sheet->setCellValue('A3', 'Dates');
          $sheet->setCellValue('A4', 'Invoice No');
          $sheet->setCellValue('B4', $invoiceNo);
     
          $sheet->setCellValue('B2', $costcenterID );
          $sheet->setCellValue('B3', $fromDate);
          $sheet->setCellValue('C3',' To ');
          $sheet->setCellValue('D3', $toDate);
          $sheet->getStyle('B3:D3')->getAlignment()->setHorizontal('center');
          $sheet->getStyle('B2')->getAlignment()->setHorizontal('center');
          $sheet->getStyle('B4')->getAlignment()->setHorizontal('center');

          $sheet->setCellValue('A6', 'No');
          $sheet->setCellValue('B6', 'Pickup Date');
          $sheet->setCellValue('C6', 'Truck No');
          $sheet->setCellValue('D6', 'Truck Capacity');
          $sheet->setCellValue('E6', 'D/O No');
          $sheet->setCellValue('F6', 'Job No');
          $sheet->setCellValue('G6', 'Pickup Location');
          $sheet->setCellValue('H6', 'Pickup From');
          $sheet->setCellValue('I6', 'Deliver To');
          $sheet->setCellValue('J6', 'Quantity');
          $sheet->setCellValue('K6', 'Time In (GBE)');
          $sheet->setCellValue('L6', 'Time Out (GBE)');
          $sheet->setCellValue('M6', 'Time In (PLAN)');
          $sheet->setCellValue('N6', 'Time Out (Plan)');
          $sheet->setCellValue('O6', 'TPT');
          $sheet->setCellValue('P6', 'Truck Charges');
          $sheet->setCellValue('Q6', 'FAF (9%)');
          $sheet->setCellValue('R6', 'Detention');
          $sheet->setCellValue('S6', 'D`Point');
          $sheet->setCellValue('T6', 'Lockup');
          $sheet->setCellValue('U6', 'Sunday/PH Pickup');
          $sheet->setCellValue('V6', 'Others');
          $sheet->setCellValue('W6', 'Total');
          $sheet->setCellValue('X6', 'Transport Remark');
          $sheet->setCellValue('Y6', 'DTM No');
          $sheet->setCellValue('Z6', 'Consol Status');

          putBorderStyleRow($sheet,'A6:Z6',2,'99AFFF');
          putWidth('Z',15,$sheet);
          $sheet->getColumnDimension('J')->setWidth(30);
          $sheet->getStyle('A6:Z6')->getAlignment()->setHorizontal('center');
          $sheet->getStyle('A1:D4')->getAlignment()->setHorizontal('center');
     }
     else if($_POST['costcenter'] == 28 || $_POST['costcenter'] ==  24)
     {
          $sheet->setCellValue('A1', 'Attn');
          $sheet->setCellValue('A2', 'Mode');
          $sheet->setCellValue('A3', 'Dates');
          $sheet->setCellValue('A4', 'Invoice No');
          $sheet->setCellValue('B4', $invoiceNo);
     
          $sheet->setCellValue('B2', $costcenterID );
          $sheet->setCellValue('B3', $fromDate);
          $sheet->setCellValue('C3',' To ');
          $sheet->setCellValue('D3', $toDate);
          $sheet->getStyle('B3:D3')->getAlignment()->setHorizontal('center');
          $sheet->getStyle('B2')->getAlignment()->setHorizontal('center');
          $sheet->getStyle('B4')->getAlignment()->setHorizontal('center');

          $sheet->setCellValue('A6', 'No');
          $sheet->setCellValue('B6', 'Pickup Date');
          $sheet->setCellValue('C6', 'Pickup Time');
          $sheet->setCellValue('D6', 'Delivery Time');
          $sheet->setCellValue('E6', 'TPT');
          $sheet->setCellValue('F6', 'Truck No');
          $sheet->setCellValue('G6', 'Truck Cap');
          $sheet->setCellValue('H6', 'D/O No');
          $sheet->setCellValue('I6', 'Job No');
          $sheet->setCellValue('J6', 'Booking Ref No');
          $sheet->setCellValue('K6', 'Pickup Location');
          $sheet->setCellValue('L6', 'C/Form Mode');
          $sheet->setCellValue('M6', 'Pickup From');
          $sheet->setCellValue('N6', 'Deliver To');
          $sheet->setCellValue('O6', 'Quantity');
          $sheet->setCellValue('P6', 'Truck Charges');
          $sheet->setCellValue('Q6', 'FAF (9%)');
          $sheet->setCellValue('R6', 'D`Point');
          $sheet->setCellValue('S6', 'Lockup');
          $sheet->setCellValue('T6', 'Sunday/PH Pickup');
          $sheet->setCellValue('U6', 'Others');
          $sheet->setCellValue('V6', 'GST');
          $sheet->setCellValue('W6', 'Total');
          $sheet->setCellValue('X6', 'Transport Remark');
          $sheet->setCellValue('Y6', 'DTM No');
          $sheet->setCellValue('Z6', 'Consol Status');

          putBorderStyleRow($sheet,'A6:Z6',2,'99AFFF');
          putWidth('Z',15,$sheet);
          $sheet->getColumnDimension('O')->setWidth(30);
          $sheet->getStyle('A6:Z6')->getAlignment()->setHorizontal('center');
          $sheet->getStyle('A1:D4')->getAlignment()->setHorizontal('center');
     }
     else if($_POST['costcenter'] == 38) //AIMP
     {
          $sheet->setCellValue('A1', 'Attn');
          $sheet->setCellValue('A2', 'Mode');
          $sheet->setCellValue('A3', 'Dates');
          $sheet->setCellValue('A4', 'Invoice No');
          $sheet->setCellValue('B4', $invoiceNo);
     
          $sheet->setCellValue('B2', $costcenterID );
          $sheet->setCellValue('B3', $fromDate);
          $sheet->setCellValue('C3',' To ');
          $sheet->setCellValue('D3', $toDate);
          $sheet->getStyle('B3:D3')->getAlignment()->setHorizontal('center');
          $sheet->getStyle('B2')->getAlignment()->setHorizontal('center');
          $sheet->getStyle('B4')->getAlignment()->setHorizontal('center');

          $sheet->setCellValue('A6', 'No');
          $sheet->setCellValue('B6', 'Pickup Date');
          $sheet->setCellValue('C6', 'Truck No');
          $sheet->setCellValue('D6', 'Truck Cap');
          $sheet->setCellValue('E6', 'D/O No');
          $sheet->setCellValue('F6', 'HAWB');
          $sheet->setCellValue('G6', 'Weight');
          $sheet->setCellValue('H6', 'Pickup Location');
          $sheet->setCellValue('I6', 'Pickup From');
          $sheet->setCellValue('J6', 'Deliver To');
          $sheet->setCellValue('K6', 'Quantity');
          $sheet->setCellValue('L6', 'Time In (GBE)');
          $sheet->setCellValue('M6', 'Time Out (GBE)');
          $sheet->setCellValue('N6', 'Time In (PLAN)');
          $sheet->setCellValue('O6', 'Time Out (Plan)');
          $sheet->setCellValue('P6', 'TPT');
          $sheet->setCellValue('Q6', 'Truck Charges');
          $sheet->setCellValue('R6', 'FAF (9%)');
          $sheet->setCellValue('S6', 'Detention');
          $sheet->setCellValue('T6', 'GPS Charge');
          $sheet->setCellValue('U6', 'D`Point');
          $sheet->setCellValue('V6', 'Lockup');
          $sheet->setCellValue('W6', 'Sunday/PH Pickup');
          $sheet->setCellValue('X6', 'Others');
          $sheet->setCellValue('Y6', 'Total');
          $sheet->setCellValue('Z6', 'Transport Remark');
          $sheet->setCellValue('AA6', 'DTM No');
          $sheet->setCellValue('AB6', 'Consol Status');

          putBorderStyleRow($sheet,'A6:AB6',2,'99AFFF');
          putWidth('Y',15,$sheet);
          $sheet->getColumnDimension('K')->setWidth(30);
          $sheet->getStyle('A6:Y6')->getAlignment()->setHorizontal('center');
          $sheet->getStyle('A1:D4')->getAlignment()->setHorizontal('center');
     }
     else if($_POST['costcenter'] == 39) //AEXP
     {
          $sheet->setCellValue('A1', 'Attn');
          $sheet->setCellValue('A2', 'Mode');
          $sheet->setCellValue('A3', 'Dates');
          $sheet->setCellValue('A4', 'Invoice No');
          $sheet->setCellValue('B4', $invoiceNo);
     
          $sheet->setCellValue('B2', $costcenterID );
          $sheet->setCellValue('B3', $fromDate);
          $sheet->setCellValue('C3',' To ');
          $sheet->setCellValue('D3', $toDate);
          $sheet->getStyle('B3:D3')->getAlignment()->setHorizontal('center');
          $sheet->getStyle('B2')->getAlignment()->setHorizontal('center');
          $sheet->getStyle('B4')->getAlignment()->setHorizontal('center');

          $sheet->setCellValue('A6', 'No');
          $sheet->setCellValue('B6', 'Pickup Date');
          $sheet->setCellValue('C6', 'Truck No');
          $sheet->setCellValue('D6', 'Truck Cap');
          $sheet->setCellValue('E6', 'D/O No');
          $sheet->setCellValue('F6', 'Pickup From');
          $sheet->setCellValue('G6', 'Deliver To');
          $sheet->setCellValue('H6', 'Quantity');
          $sheet->setCellValue('I6', 'Truck Charges');
          $sheet->setCellValue('J6', 'FAF (9%)');
          $sheet->setCellValue('K6', 'Detention');
          $sheet->setCellValue('L6', 'GPS Charge');
          $sheet->setCellValue('M6', 'D`Point');
          $sheet->setCellValue('N6', 'Sunday/PH Pickup');
          $sheet->setCellValue('O6', 'Others');
          $sheet->setCellValue('P6', 'Total');
          $sheet->setCellValue('Q6', 'Transport Remark');
          $sheet->setCellValue('R6', 'DTM No');
          $sheet->setCellValue('S6', 'Consol Status');

          putBorderStyleRow($sheet,'A6:S6',2,'99AFFF');
          putWidth('S',15,$sheet);
          $sheet->getColumnDimension('H')->setWidth(30);
          $sheet->getStyle('A6:S6')->getAlignment()->setHorizontal('center');
     }
     else
     {
          normalTemplateTop($sheet,$fromDate,$toDate,$invoiceNo);
     }
}
else if($companyID == 73)
{
     if($_POST['costcenter'] == 29 || $_POST['costcenter'] == 30 || $_POST['costcenter'] == 31) //BCOM,LOCAL,TRANSFER
     {
          $sheet->setCellValue('A1', 'Date');
          $sheet->setCellValue('A2', 'Project');
          $sheet->setCellValue('B2', $costcenterID );
          $sheet->setCellValue('D2', 'Invoice No');
          $sheet->setCellValue('E2', $invoiceNo);

          $sheet->setCellValue('A5', 'No');
          $sheet->mergeCells('A5:A6');
          $sheet->setCellValue('B5', 'Pickup Date');
          $sheet->mergeCells('B5:B6');
          $sheet->setCellValue('C5', 'Truck No');
          $sheet->mergeCells('C5:C6');
          $sheet->setCellValue('D5', 'Truck Cap');
          $sheet->mergeCells('D5:D6');
          $sheet->setCellValue('E5', 'Destination');
          $sheet->mergeCells('E5:G5');
          $sheet->setCellValue('E6', 'Pickup From');
          $sheet->setCellValue('F6', 'Deliver To');
          $sheet->setCellValue('G6', 'Agent');

          $sheet->setCellValue('H5', 'Destination');
          $sheet->mergeCells('H5:J5');
          $sheet->setCellValue('H6', 'Truck In');
          $sheet->setCellValue('I6', 'Truck Out');
          $sheet->setCellValue('J6', 'Total');

          $sheet->setCellValue('K5', 'Transport Cost');
          $sheet->mergeCells('K5:K6');
          $sheet->setCellValue('L5', 'Detention');
          $sheet->mergeCells('L5:M5');
          $sheet->setCellValue('L6', 'Hrs');
          $sheet->setCellValue('M6', 'Charges');

          $sheet->setCellValue('N5', 'Drop Point');
          $sheet->mergeCells('N5:O5');

          $sheet->setCellValue('N6', '#of Drop Point');
          $sheet->setCellValue('O6', 'Charges ');
          $sheet->setCellValue('P6', 'Delivery After');
          $sheet->setCellValue('Q6', 'Sunday/PH Pickup ');
          $sheet->setCellValue('R6', 'Others ');
          $sheet->setCellValue('S6', 'GST 0% ');
          $sheet->setCellValue('T6', 'Total');
          $sheet->setCellValue('U6', 'Transport Remark');
          $sheet->setCellValue('V6', 'DTM No');
          $sheet->setCellValue('W6', 'Consol Status');

          // putBorderStyleRow($sheet,'A5:P6',2,'99AFFF');
          putWidth('W',15,$sheet);
          $sheet->getColumnDimension('H')->setWidth(30);
          $sheet->getStyle('A5:W5')->getAlignment()->setHorizontal('center');
          $sheet->getStyle('A6:W6')->getAlignment()->setHorizontal('center');
          $sheet->getStyle('A1:D4')->getAlignment()->setHorizontal('center');
     }
     else
     {
          normalTemplateTop($sheet,$fromDate,$toDate,$invoiceNo);
     }
}
else if($companyID == 62)
{
     if($_POST['costcenter'] == 34 || $_POST['costcenter'] == 35)//Air Export And Air Import
     {
          $sheet->setCellValue('A1', 'Department');
          $sheet->setCellValue('B1', $costcenterID );
          $sheet->setCellValue('A2', 'Truckers');
          $sheet->setCellValue('B2', 'VXL Alliance SDN BHD' );
          
          $sheet->setCellValue('D1', 'Month');
          $sheet->setCellValue('E1', 'Serial No');
          $sheet->setCellValue('D1', 'Date');

          $sheet->setCellValue('A6', 'Item No');
          $sheet->setCellValue('B6', 'Shipment Date');
          $sheet->setCellValue('C6', 'Vendor Tax Invoice');
          $sheet->setCellValue('D6', 'Expeditors D/O /Pickup Order');
          $sheet->setCellValue('E6', 'Expeditors House Bills');
          $sheet->setCellValue('F6', 'Truck No');
          $sheet->setCellValue('G6', 'Tonner');
          $sheet->setCellValue('H6', 'Truck Charges ');
          $sheet->setCellValue('I6', 'Additional P/Up or DLV');
          $sheet->setCellValue('J6', 'Overtime/Detention ');
          $sheet->setCellValue('K6', 'Lockup Charges');
          $sheet->setCellValue('L6', 'Other Charges');
          $sheet->setCellValue('M6', 'FAF ');
          $sheet->setCellValue('N6', 'Total Cost');
          $sheet->setCellValue('O6', 'GST 0%');
          $sheet->setCellValue('P6', 'Grand Total Cost ');
          $sheet->setCellValue('Q6', 'Transport Remark');
          $sheet->setCellValue('R6', 'DTM No');
          $sheet->setCellValue('S6', 'Consol Status');

          putBorderStyleRow($sheet,'A6:P6',2,'99AFFF');
          putWidth('S',15,$sheet);
          $sheet->getColumnDimension('B')->setWidth(20);
          $sheet->getColumnDimension('C')->setWidth(20);
          $sheet->getColumnDimension('D')->setWidth(25);
          $sheet->getColumnDimension('E')->setWidth(20);
          $sheet->getColumnDimension('I')->setWidth(25);
          $sheet->getColumnDimension('J')->setWidth(20);
          $sheet->getColumnDimension('P')->setWidth(20);
          $sheet->getStyle('A6:S6')->getAlignment()->setHorizontal('center');
     }
     else
     {
          normalTemplateTop($sheet,$fromDate,$toDate,$invoiceNo);
     }
}
else if($companyID == 79)
{
     if($_POST['costcenter'] == 32 || $_POST['costcenter'] == 33)//Pickup And Delivery
     {
          $sheet->setCellValue('A1', 'Week');
          $sheet->setCellValue('B1', $fromDate);
          $sheet->setCellValue('C1',' To ');
          $sheet->setCellValue('D1', $toDate);

          $sheet->setCellValue('A2', 'Invoice No');
          $sheet->setCellValue('B2', $invoiceNo);
          $sheet->setCellValue('A3', 'Mode');
          $sheet->setCellValue('B3', $costcenterID);

          $sheet->setCellValue('A6', 'Item No');
          $sheet->setCellValue('B6', 'Pickup Date');
          $sheet->setCellValue('C6', 'Truck No');
          $sheet->setCellValue('D6', 'Truck Capacity');
          $sheet->setCellValue('E6', 'D/O No');
          $sheet->setCellValue('F6', 'Pickup From');
          $sheet->setCellValue('G6', 'Pickup Location');
          $sheet->setCellValue('H6', 'Deliver To');
          $sheet->setCellValue('I6', 'Deliver Location');
          $sheet->setCellValue('J6', 'Quantity');
          $sheet->setCellValue('K6', 'Truck Charges');
          $sheet->setCellValue('L6', 'FAF (9%)');
          $sheet->setCellValue('M6', 'Pickup/D`Point');
          $sheet->setCellValue('N6', 'Sunday/PH Pickup');
          $sheet->setCellValue('O6', 'Others');
          $sheet->setCellValue('P6', 'Total');
          $sheet->setCellValue('Q6', 'Transport Remark');
          $sheet->setCellValue('R6', 'DTM No');
          $sheet->setCellValue('S6', 'Consol Status');

          putBorderStyleRow($sheet,'A6:S6',2,'99AFFF');
          putWidth('S',15,$sheet);
          $sheet->getColumnDimension('J')->setWidth(30);
          $sheet->getStyle('A6:S6')->getAlignment()->setHorizontal('center');
          $sheet->getStyle('A1:D4')->getAlignment()->setHorizontal('center');
     }
     else
     {
          normalTemplateTop($sheet,$fromDate,$toDate,$invoiceNo);
     }
}
else if($companyID == 70)
{
     if($_POST['costcenter'] == 36 || $_POST['costcenter'] == 37)//Inbound and Outbound
     {
          $sheet->setCellValue('A1', $costcenterID);
          $sheet->setCellValue('A2', 'VXL Alliance SDN BHD' );
          $sheet->setCellValue('C1', $fromDate);
          $sheet->setCellValue('D1', 'To' );
          $sheet->setCellValue('E1', $toDate);

          $sheet->setCellValue('A6', 'Shipment Date');
          $sheet->setCellValue('C6', 'Quantity');
          $sheet->setCellValue('D6', 'Transport Size');
          $sheet->setCellValue('E6', 'Transport Cost');
          $sheet->setCellValue('F6', 'P`kup/Delivery Point');
          $sheet->setCellValue('G6', 'D/O No');
          $sheet->setCellValue('H6', 'From');
          $sheet->setCellValue('I6', 'To');
          $sheet->setCellValue('J6', 'Qty pits');
          $sheet->setCellValue('K6', 'Additional Charges');
          $sheet->setCellValue('L6', 'Cost');
          $sheet->setCellValue('M6', 'Remarks');
          $sheet->setCellValue('N6', 'Commodity');
          $sheet->setCellValue('O6', 'Transport Remark');
          $sheet->setCellValue('P6', 'DTM No');
          $sheet->setCellValue('Q6', 'Consol Status');

          putBorderStyleRow($sheet,'A6:Q6',2,'99AFFF');
          putWidth('Q',15,$sheet);
          $sheet->getColumnDimension('C')->setWidth(30);
          $sheet->getStyle('A6:Q6')->getAlignment()->setHorizontal('center');
          
     }
     else
     {
          normalTemplateTop($sheet,$fromDate,$toDate,$invoiceNo);
     }
}
else if($companyID == 55)
{
     $sheet->setCellValue('A1', 'Dates');
     $sheet->setCellValue('B1', $fromDate);
     $sheet->setCellValue('C1', 'To' );
     $sheet->setCellValue('D1', $toDate);

     $sheet->setCellValue('A2', 'VENDOR');
     $sheet->setCellValue('B2', 'VXL ALLIANCE');
     
     $sheet->setCellValue('A3', 'INVOICE NO');
     $sheet->setCellValue('B3', $invoiceNo);
     $sheet->setCellValue('A4', 'INVOICE DATE');

     $sheet->setCellValue('A6', 'Item No');
     $sheet->setCellValue('B6', 'Pickup Date');
     $sheet->setCellValue('C6', 'Pickup Time');
     $sheet->setCellValue('D6', 'DGF Reference');
     $sheet->setCellValue('E6', 'Urgent Memo');
     $sheet->setCellValue('F6', 'Requestor');
     $sheet->setCellValue('G6', 'Pickup Point');
     $sheet->setCellValue('H6', 'Pickup Zone');
     $sheet->setCellValue('I6', 'Delivery Point');
     $sheet->setCellValue('J6', 'Delivery Zone');
     $sheet->setCellValue('K6', 'Date Truck Reached');
     $sheet->setCellValue('L6', 'Time Truck Reached');
     $sheet->setCellValue('M6', 'Date Started to Load Cargo');
     $sheet->setCellValue('N6', 'Time Started to Load Cargo');
     $sheet->setCellValue('O6', 'Date Finished Loading Cargo');
     $sheet->setCellValue('P6', 'Time Finished Loading Cargo');
     $sheet->setCellValue('Q6', 'Date Received Custom Form');
     $sheet->setCellValue('R6', 'Time Received Custom Form');
     $sheet->setCellValue('S6', 'Date Truck Depart');
     $sheet->setCellValue('T6', 'Time Truck Depart');
     $sheet->setCellValue('U6', 'Date Truck Reached');
     $sheet->setCellValue('V6', 'Time Truck Reached');
     $sheet->setCellValue('W6', 'Date Received Custom Form');
     $sheet->setCellValue('X6', 'Time Received Custom Form');
     $sheet->setCellValue('Y6', 'Date Truck Reached');
     $sheet->setCellValue('Z6', 'Time Truck Reached');
     $sheet->setCellValue('AA6', 'Date Started to Unload Cargo');
     $sheet->setCellValue('AB6', 'Time Started to Unload Cargo');
     $sheet->setCellValue('AC6', 'Date Finished Unloading Cargo');
     $sheet->setCellValue('AD6', 'Time Finished Unloading Cargo');
     $sheet->setCellValue('AE6', 'Truck No');
     $sheet->setCellValue('AF6', 'Truck Size');
     $sheet->setCellValue('AG6', 'Truck Type');
     $sheet->setCellValue('AH6', 'Transportation Cost');
     $sheet->setCellValue('AI6', 'Fuel Adjustment Factor');
     $sheet->setCellValue('AJ6', 'GST');
     $sheet->setCellValue('AK6', 'Sub Total Amount (w`out GST)');
     $sheet->setCellValue('AL6', 'Net Total Amount');
     $sheet->setCellValue('AM6', 'Transport Remark');
     $sheet->setCellValue('AN6', 'DTM No');
     $sheet->setCellValue('AO6', 'Consol Status');

     putBorderStyleRow($sheet,'A6:AO6',2,'99AFFF');
     putWidth('AO',15,$sheet);
     $sheet->getStyle('A6:AO6')->getAlignment()->setHorizontal('center');
     $sheet->getStyle('A1:D4')->getAlignment()->setHorizontal('center');
}
else
{
     normalTemplateTop($sheet,$fromDate,$toDate,$invoiceNo);
}



$sqlo = " SELECT * FROM invoice WHERE 
invoiceNo = $invoiceNo AND
isSelected = 0 OR 
isSelected IS NULL 
ORDER BY invoiceNo DESC";
$result = mysqli_query($conn,$sqlo);

if (mysqli_num_rows($result) > 0) 
{
     
     $line = 7;
     while($row = mysqli_fetch_array($result))
     { 
          // if($_POST['costcenter'] == 19 || $_POST['costcenter'] == 20 || $_POST['costcenter'] == 21 ) 
          // {
          //      $invoiceNo = $row['invoiceNo'];
          //      $sheet->setCellValue('B4', $invoiceNo );
          // }

          $sql = " SELECT * FROM transportcharge WHERE id = ".$row['transportID_FK'];
          $result1 = mysqli_query($conn,$sql);
          if (mysqli_num_rows($result1) > 0) 
          {
               while($row2 = mysqli_fetch_array($result1))
               { 

                    $remarks = strtoupper($row2['remarks']);
                    $operatingHour = $row2['operatingHour'];
                    $transportcharge = $row2['transportcharge'];
                    // $faf = $row2['faf'];
                    $faf = ($row2['faf'] / 100) * $row2['transportcharge'];
                    $totalCharges = $transportcharge + $faf;


                    $sqlA = " SELECT * FROM dtmlist WHERE 
                    dtmID_PK = ".$row2['dtmID_FK']." 
                    AND dtmPickupDate BETWEEN '$fromDateFormatted' AND '$toDateFormatted' ";
                    $result1A = mysqli_query($conn,$sqlA);
                    
                    if (mysqli_num_rows($result1A) > 0) 
                    {
                         while($row3 = mysqli_fetch_array($result1A))
                         {
                              $planner = getUser($row3['dtmPlannerID_FK']);
                              $dtmRequestBy = $row3['dtmRequestBy'];
                              $dtmNoID_PK =  $row3['dtmID_PK'];

                              // $dtmBookingTime = $row3['dtmBookingTime'];
                              // $dtmPickupTime = $row3['dtmPickupTime'];

                              $dtmBookingTime = date("H i s",strtotime($row3['dtmBookingTime']));
                              $dtmPickupTime = date("H i s",strtotime($row3['dtmPickupTime']));

                              $dtmBookingDate = date("d M Y",strtotime($row3['dtmBookingDate']));
                              $dtmPickupDate = date("d M Y",strtotime($row3['dtmPickupDate']));

                              $dtmDGF = $row3['dtmDGF'];
                              if($row3['dtmUrgentMemo'] == 1)
                              {
                                   $dtmUrgentMemo = "Yes";
                              }
                              else
                              {
                                   $dtmUrgentMemo = "No";
                              }
                              $dtmRequestBy = $row3['dtmRequestBy'];
                              

                              $dtmOriginPointID_FK = getPlace($row3['dtmOriginPointID_FK']);
                              $dtmDestinationPointID_FK = getPlace($row3['dtmDestinationPointID_FK']);
                              
                              $dtmOriginZone = getPlaceZone($row3['dtmOriginZoneID_FK']);
                              $dtmDestinationZone = getPlaceZone($row3['dtmDestinationZoneID_FK']);
                              $companyID_FK = getCompany($row3['companyID_FK']);
                              $truckID_FK = getTruckPlateNo($row3['truckID_FK']);
                              $truckCap= getTruckCap($row3['truckID_FK']);
                              $loadCap = $row3['loadCap'];

                              $startIn = date("G:i",strtotime($row3['dtmOriginF']));
                              $startOut = date("G:i",strtotime($row3['dtmOriginDepartureTimeFromPost']));

                              $endIn = date("G:i",strtotime($row3['dtmDestinationArrivalTimeAtPost']));
                              $endOut = date("G:i",strtotime($row3['dtmDestinationDepartTimeFromPost']));

                              $tpt = compareValue($row3['dtmDestinationDepartTimeFromPost'],$row3['dtmOriginF'],$row3['lockupdepartTimeFromPost'],$row3['lockupf'],$row3['dtmPickupDate']);

                              $quantityLoad = "";
                              if($row3['dtmIsQuantity'] == 1)
                              {
                                   $quantityLoad .= "Carton = ".$row3['dtmQuantityCarton'].",";
                              }
                              if($row3['dtmIsPallets']== 1)
                              {
                                   $quantityLoad .= "Pallets = ".$row3['dtmQuantityPalllets'].",";
                              }
                              if($row3['dtmIsCages']== 1)
                              {
                                   $quantityLoad .= "Cages = ".$row3['dtmQuantityCages'];
                              }


                              $drivers = "";
                              $driverID_FK = getDriver($row3['driverID_FK']);
                              $drivers.= "(1) ".$driverID_FK;

                              if(isset($row3['driver2ID_FK']))
                              {$driver2ID_FK = getDriver($row3['driverID_FK']);
                              $drivers .= "\n(2) ".$driver2ID_FK;}

                              if( $row3['lockupf'] || $row3['lockupdockTime'] || $row3['lockupcompleteLoadingTime'] || $row3['lockupdepartureTimeFromPost'] ||
                              $row3['lockuparrivalTimeAtPost'] || $row3['lockupdestinationDockTime'] || $row3['lockupcompleteUnloadTime'] || $row3['lockupdepartTimeFromPost'])
                              { 
                                   $remarks1 = "LOCKUP,".strtoupper($row3['dtmRemarks']);
                              }
                              else
                              { 
                                   $remarks1 = $row3['dtmRemarks'];
                              }
                              $consolShow = "";
                              $isConsol = $row3['isConsol'];
                              if($isConsol == 1)
                              {
                                   $consolShow="CONSOL"; 
                              }
                              else {
                                   $consolShow = "";
                              }
                              echo $costcenterTemp = getCostCenter($row3['costCenterID_FK']);
                              
                              $lockupf = "";
                              $lockupdockTime = "";
                              $lockupcompleteLoadingTime = "";
                              $lockupdepartureTimeFromPost = "";

                              $lockuparrivalTimeAtPost = "";
                              $lockupdestinationDockTime = "";
                              $lockupcompleteUnloadTime = "";
                              $lockupdepartTimeFromPost = "";

                              $timeF = date("G:i",strtotime($row3['dtmOriginF']));;
                              $timedockTime = date("G:i",strtotime($row3['dtmOriginDockTime']));;
                              $timecompleteLoadingTime = date("G:i",strtotime($row3['dtmOriginCompleteLoadingTime']));;
                              $timedepartureTimeFromPost = date("G:i",strtotime($row3['dtmOriginDepartureTimeFromPost']));;

                              $timearrivalTimeAtPost = date("G:i",strtotime($row3['dtmDestinationArrivalTimeAtPost']));
                              $timedestinationDockTime = date("G:i",strtotime($row3['dtmDestinationDockTime']));
                              $timecompleteUnloadTime = date("G:i",strtotime($row3['dtmDestinationCompleteUnloadTime']));
                              $timedepartTimeFromPost = date("G:i",strtotime($row3['dtmDestinationDepartTimeFromPost']));

                              if($row3['lockupf'])
                              {
                                   $lockupf = date("G:i",strtotime($row3['lockupf']));
                              }
                              if($row3['lockupdockTime'])
                              {
                                   $lockupdockTime = date("G:i",strtotime($row3['lockupdockTime']));
                              }
                              if($row3['lockupcompleteLoadingTime'])
                              {
                                   $lockupcompleteLoadingTime = date("G:i",strtotime($row3['lockupcompleteLoadingTime']));
                              }
                              if($row3['lockupdepartureTimeFromPost'])
                              {
                                   $lockupdepartureTimeFromPost = date("G:i",strtotime($row3['lockupdepartureTimeFromPost']));
                              }

                              if($row3['lockuparrivalTimeAtPost'])
                              {
                                   $lockuparrivalTimeAtPost = date("G:i",strtotime($row3['lockuparrivalTimeAtPost']));
                              }
                              if($row3['lockupdestinationDockTime'])
                              {
                                   $lockupdestinationDockTime = date("G:i",strtotime($row3['lockupdestinationDockTime']));
                              }
                              if($row3['lockupcompleteUnloadTime'])
                              {
                                   $lockupcompleteUnloadTime = date("G:i",strtotime($row3['lockupcompleteUnloadTime']));
                              }
                              if($row3['lockupdepartTimeFromPost'])
                              {
                                   $lockupdepartTimeFromPost = date("G:i",strtotime($row3['lockupdepartTimeFromPost']));
                              }
                              
                              // $consol = $row3['dtmAdhoc'];
                              
                              // if($row3['dtmAdhoc'] == "consol")
                              // {
                              //      $consol = $row3['dtmAdhoc'];
                              // }
                              // else
                              // {
                              //      $consol = "";
                              // }
                              // if($costcenterTemp == "CONSOL")
                              // {
                              //      echo "CONSOL";
                              // }
                              // else
                              // {
                              //      echo "";
                              // }
                         }
                    }
                    else
                    {
                         $sheet->setCellValue('A'.$line, "error 3");
                    }
               }
          }
          else
          {
               $sheet->setCellValue('A'.$line, "error 2");
          }

          if($costcenterTemp == $costcenterID)
          {
               if($companyID == 43)
               {
                    if($_POST['costcenter'] == 19 || $_POST['costcenter'] == 20 || $_POST['costcenter'] == 21 ) 
                    {
                         // echo "SEarched = ".$costcenterID."<br>";
                         // echo $costcenterTemp."<br>";
                        
                              $sheet->setCellValue('A'.$line, ($line-6));
                              $sheet->setCellValue('B'.$line, $dtmPickupDate);
                              $sheet->setCellValue('C'.$line, $truckID_FK);
                              $sheet->setCellValue('D'.$line, $truckCap);
               
                              $sheet->setCellValue('G'.$line, $dtmOriginZone);
                              $sheet->setCellValue('H'.$line, $dtmOriginPointID_FK);
                              $sheet->setCellValue('I'.$line, $dtmDestinationPointID_FK);
                              $sheet->setCellValue('J'.$line, $quantityLoad);
     
                              $sheet->setCellValue('K'.$line, $startIn);
                              $sheet->setCellValue('L'.$line, $startOut);
                              $sheet->setCellValue('M'.$line, $endIn);
                              $sheet->setCellValue('N'.$line, $endOut);
                              $sheet->setCellValue('O'.$line, $tpt);
                              if($isConsol == 1)
                              {
                                   $sheet->setCellValue('P'.$line, $transportcharge);
                              }
                              else
                              {
                                   $sheet->setCellValue('S'.$line, $transportcharge);
                              }
                              $sheet->setCellValue('Q'.$line, $faf);
                              $sheet->setCellValue('W'.$line,'=SUM(P'.$line.':V'.$line.')');
                              $sheet->setCellValue('X'.$line, $remarks);
                              $sheet->setCellValue('Y'.$line, $dtmNoID_PK);
                              $sheet->setCellValue('Z'.$line, $consolShow);
                              $sheet->getStyle('A'.$line.':Z'.$line)->getAlignment()->setHorizontal('center');
                              $line++;
                        
                    }
                    else if($_POST['costcenter'] == 28 || $_POST['costcenter'] ==  24)
                    {
                         $sheet->setCellValue('A'.$line, ($line-6));
                         $sheet->setCellValue('B'.$line, $dtmPickupDate);
                         $sheet->setCellValue('C'.$line, $startIn);
                         $sheet->setCellValue('D'.$line, $endOut);
                         $sheet->setCellValue('E'.$line, $tpt);
                         $sheet->setCellValue('F'.$line, $truckID_FK);
                         $sheet->setCellValue('G'.$line, $truckCap);
     
                         $sheet->setCellValue('K'.$line, $dtmOriginZone);
     
                         $sheet->setCellValue('M'.$line, $dtmOriginPointID_FK);
                         $sheet->setCellValue('N'.$line, $dtmDestinationPointID_FK);
                         $sheet->setCellValue('O'.$line, $quantityLoad);
                         if($isConsol == 1)
                         {
                              $sheet->setCellValue('R'.$line, $transportcharge);
                         }
                         else
                         {
                              $sheet->setCellValue('P'.$line, $transportcharge);
                         }
                         $sheet->setCellValue('Q'.$line, $faf);
                         $sheet->setCellValue('W'.$line,'=SUM(P'.$line.':V'.$line.')');
                         $sheet->setCellValue('X'.$line, $remarks);
                         $sheet->setCellValue('Y'.$line, $dtmNoID_PK);
                         $sheet->setCellValue('Z'.$line, $consolShow);
                         $sheet->getStyle('A'.$line.':W'.$line)->getAlignment()->setHorizontal('center');
                         $line++;
                    }
                    else if($_POST['costcenter'] == 38) //AIMP
                    {
                         $sheet->setCellValue('A'.$line, ($line-6));
                         $sheet->setCellValue('B'.$line, $dtmPickupDate);
                         $sheet->setCellValue('C'.$line, $truckID_FK);
                         $sheet->setCellValue('D'.$line, $truckCap);
     
                         $sheet->setCellValue('H'.$line, $dtmOriginZone);
                         $sheet->setCellValue('I'.$line, $dtmOriginPointID_FK);
                         $sheet->setCellValue('J'.$line, $dtmDestinationPointID_FK);
                         $sheet->setCellValue('K'.$line, $quantityLoad);
     
                         $sheet->setCellValue('L'.$line, $startIn);
                         $sheet->setCellValue('M'.$line, $startOut);
                         $sheet->setCellValue('N'.$line, $endIn);
                         $sheet->setCellValue('O'.$line, $endOut);
                         $sheet->setCellValue('P'.$line, $tpt);
                         if($isConsol == 1)
                         {
                              $sheet->setCellValue('U'.$line, $transportcharge);
                         }
                         else
                         {
                              $sheet->setCellValue('Q'.$line, $transportcharge);
                         }
                         $sheet->setCellValue('R'.$line, $faf);

                         $sheet->setCellValue('Z'.$line, $remarks);
                         $sheet->setCellValue('AA'.$line, $dtmNoID_PK);
                         $sheet->setCellValue('AB'.$line, $consolShow);
     
                         $sheet->setCellValue('Y'.$line,'=SUM(Q'.$line.':X'.$line.')');
                         $sheet->getStyle('A'.$line.':AB'.$line)->getAlignment()->setHorizontal('center');
                         $line++;
                    }
                    else if($_POST['costcenter'] == 39) //AEXP
                    {
                         $sheet->setCellValue('A'.$line, ($line-6));
                         $sheet->setCellValue('B'.$line, $dtmPickupDate);
                         $sheet->setCellValue('C'.$line, $truckID_FK);
                         $sheet->setCellValue('D'.$line, $truckCap);
     
                         $sheet->setCellValue('F'.$line, $dtmOriginPointID_FK);
                         $sheet->setCellValue('G'.$line, $dtmDestinationPointID_FK);
                         $sheet->setCellValue('H'.$line, $quantityLoad);
                         if($isConsol == 1)
                         {
                              $sheet->setCellValue('M'.$line, $transportcharge);
                         }
                         else
                         {
                              $sheet->setCellValue('I'.$line, $transportcharge);
                         }
                         $sheet->setCellValue('J'.$line, $faf);

                         $sheet->setCellValue('Q'.$line, $remarks);
                         $sheet->setCellValue('R'.$line, $dtmNoID_PK);
                         $sheet->setCellValue('S'.$line, $consolShow);
     
                         $sheet->setCellValue('P'.$line,'=SUM(I'.$line.':O'.$line.')');
                         $sheet->getStyle('A'.$line.':S'.$line)->getAlignment()->setHorizontal('center');
                         $line++;
                    }
                    else
                    {
                         if($selectedCompanyName == $companyID_FK)
                         {
                              $sheet->setCellValue('A'.$line, ($line-6));
                              $sheet->setCellValue('B'.$line, $dtmPickupDate);
                              $sheet->setCellValue('C'.$line, $truckID_FK);
                              $sheet->setCellValue('D'.$line, $truckCap);
                              $sheet->setCellValue('F'.$line, $quantityLoad);
                              $sheet->setCellValue('G'.$line, $transportcharge);
                              if($isConsol == 1)
                              {
                                   $sheet->setCellValue('K'.$line, $transportcharge);
                              }
                              else
                              {
                                   $sheet->setCellValue('G'.$line, $transportcharge);
                              }
                              $sheet->setCellValue('H'.$line, $faf);

                              $sheet->setCellValue('O'.$line, $remarks);
                              $sheet->setCellValue('P'.$line, $dtmNoID_PK);
                              $sheet->setCellValue('Q'.$line, $consolShow);
                         
                              $sheet->setCellValue('N'.$line,'=SUM(G'.$line.':M'.$line.')');
                              $sheet->getStyle('A'.$line.':Q'.$line)->getAlignment()->setHorizontal('center');
                              $line++;
                         }
                    }
               }
               else if($companyID == 73)
               {
                    if($_POST['costcenter'] == 29 || $_POST['costcenter'] == 30 || $_POST['costcenter'] == 31) //BCOM,LOCAL,TRANSFER
                    {
                         $sheet->setCellValue('A'.$line, ($line-6));
                         $sheet->setCellValue('B'.$line, $dtmPickupDate);
                         $sheet->setCellValue('C'.$line, $truckID_FK);
                         $sheet->setCellValue('D'.$line, $truckCap);
                         $sheet->setCellValue('E'.$line, $dtmOriginPointID_FK);
                         $sheet->setCellValue('F'.$line, $dtmDestinationPointID_FK);
     
                         $sheet->setCellValue('H'.$line, $startIn);
                         $sheet->setCellValue('I'.$line, $startOut);
                         $sheet->setCellValue('J'.$line, $tpt);
                         if($isConsol == 1)
                         {
                              $sheet->setCellValue('O'.$line, $transportcharge);
                              $sheet->setCellValue('N'.$line, 1);
                         }
                         else
                         {
                              $sheet->setCellValue('K'.$line, $transportcharge);
                         }

                         
                         $sheet->setCellValue('U'.$line, $remarks);
                         $sheet->setCellValue('V'.$line, $dtmNoID_PK);
                         $sheet->setCellValue('W'.$line, $consolShow);
     
                         $sheet->setCellValue('T'.$line,'=SUM(K'.$line.'+M'.$line.'+O'.$line.'+P'.$line.'+Q'.$line.'+R'.$line.'+S'.$line.')');
                         $sheet->getStyle('A'.$line.':W'.$line)->getAlignment()->setHorizontal('center');
                         $line++;
                    }
                    else
                    {
                         if($selectedCompanyName == $companyID_FK)
                         {
                              $sheet->setCellValue('A'.$line, ($line-6));
                              $sheet->setCellValue('B'.$line, $dtmPickupDate);
                              $sheet->setCellValue('C'.$line, $truckID_FK);
                              $sheet->setCellValue('D'.$line, $truckCap);
                              $sheet->setCellValue('F'.$line, $quantityLoad);
                              if($isConsol == 1)
                              {
                                   $sheet->setCellValue('K'.$line, $transportcharge);
                              }
                              else
                              {
                                   $sheet->setCellValue('G'.$line, $transportcharge);
                              }
                              $sheet->setCellValue('H'.$line, $faf);

                              $sheet->setCellValue('O'.$line, $remarks);
                              $sheet->setCellValue('P'.$line, $dtmNoID_PK);
                              $sheet->setCellValue('Q'.$line, $consolShow);

                              $sheet->setCellValue('N'.$line,'=SUM(G'.$line.':M'.$line.')');
                              $sheet->getStyle('A'.$line.':N'.$line)->getAlignment()->setHorizontal('center');
                              $line++;
                         }
                    }
               }
               else if($companyID == 62)
               {
                    if($_POST['costcenter'] == 34 || $_POST['costcenter'] == 35)//Air Export And Air Import
                    {
                         $sheet->setCellValue('A'.$line, ($line-6));
                         $sheet->setCellValue('B'.$line, $dtmPickupDate);
     
                         $sheet->setCellValue('F'.$line, $truckID_FK);
                         $sheet->setCellValue('G'.$line, $truckCap);
                         if($isConsol == 1)
                         {
                              $sheet->setCellValue('I'.$line, $transportcharge);
                         }
                         else
                         {
                              $sheet->setCellValue('H'.$line, $transportcharge);
                         }
     
                         $sheet->setCellValue('M'.$line, $faf);

                         $sheet->setCellValue('Q'.$line, $remarks);
                         $sheet->setCellValue('R'.$line, $dtmNoID_PK);
                         $sheet->setCellValue('S'.$line, $consolShow);
     
                         $sheet->setCellValue('P'.$line,'=SUM(H'.$line.':O'.$line.')');
                         $sheet->getStyle('A'.$line.':S'.$line)->getAlignment()->setHorizontal('center');
                         $line++;
                    }
                    else
                    {
                         if($selectedCompanyName == $companyID_FK)
                         {
                              $sheet->setCellValue('A'.$line, ($line-6));
                              $sheet->setCellValue('B'.$line, $dtmPickupDate);
                              $sheet->setCellValue('C'.$line, $truckID_FK);
                              $sheet->setCellValue('D'.$line, $truckCap);
                              $sheet->setCellValue('F'.$line, $quantityLoad);
                              if($isConsol == 1)
                              {
                                   $sheet->setCellValue('K'.$line, $transportcharge);
                              }
                              else
                              {
                                   $sheet->setCellValue('G'.$line, $transportcharge);
                              }
                              $sheet->setCellValue('H'.$line, $faf);

                              $sheet->setCellValue('O'.$line, $remarks);
                              $sheet->setCellValue('P'.$line, $dtmNoID_PK);
                              $sheet->setCellValue('Q'.$line, $consolShow);

                              $sheet->setCellValue('N'.$line,'=SUM(G'.$line.':M'.$line.')');
                              $sheet->getStyle('A'.$line.':N'.$line)->getAlignment()->setHorizontal('center');
                              $line++;
                         }
                    }
               }
               else if($companyID == 79)
               {
                    if($_POST['costcenter'] == 32 || $_POST['costcenter'] == 33)//Pickup And Delivery
                    {
                         $sheet->setCellValue('A'.$line, ($line-6));
                         $sheet->setCellValue('B'.$line, $dtmPickupDate);
                         $sheet->setCellValue('C'.$line, $truckID_FK);
                         $sheet->setCellValue('D'.$line, $truckCap);
                         $sheet->setCellValue('F'.$line, $dtmOriginPointID_FK);
                         $sheet->setCellValue('G'.$line, $dtmOriginZone);
                         $sheet->setCellValue('H'.$line, $dtmDestinationPointID_FK);
                         $sheet->setCellValue('I'.$line, $dtmDestinationZone);
                         $sheet->setCellValue('J'.$line, $quantityLoad);
                         if($isConsol == 1)
                         {
                              $sheet->setCellValue('M'.$line, $transportcharge);
                         }
                         else
                         {
                              $sheet->setCellValue('K'.$line, $transportcharge);
                         }
                         $sheet->setCellValue('L'.$line, $faf);

                         $sheet->setCellValue('Q'.$line, $remarks);
                         $sheet->setCellValue('R'.$line, $dtmNoID_PK);
                         $sheet->setCellValue('S'.$line, $consolShow);

                         $sheet->setCellValue('P'.$line,'=SUM(K'.$line.':O'.$line.')');
                         $sheet->getStyle('A'.$line.':S'.$line)->getAlignment()->setHorizontal('center');
                         $line++;
                    }
                    else
                    {
                         if($selectedCompanyName == $companyID_FK)
                         {
                              $sheet->setCellValue('A'.$line, ($line-6));
                              $sheet->setCellValue('B'.$line, $dtmPickupDate);
                              $sheet->setCellValue('C'.$line, $truckID_FK);
                              $sheet->setCellValue('D'.$line, $truckCap);
                              $sheet->setCellValue('F'.$line, $quantityLoad);
                              if($isConsol == 1)
                              {
                                   $sheet->setCellValue('K'.$line, $transportcharge);
                              }
                              else
                              {
                                   $sheet->setCellValue('G'.$line, $transportcharge);
                              }
                              $sheet->setCellValue('H'.$line, $faf);

                              
                              $sheet->setCellValue('O'.$line, $remarks);
                              $sheet->setCellValue('P'.$line, $dtmNoID_PK);
                              $sheet->setCellValue('Q'.$line, $consolShow);

                              $sheet->setCellValue('N'.$line,'=SUM(G'.$line.':M'.$line.')');
                              $sheet->getStyle('A'.$line.':N'.$line)->getAlignment()->setHorizontal('center');
                              $line++;
                         }
                    }
               }
               else if($companyID == 70)
               {
                    if($_POST['costcenter'] == 36 || $_POST['costcenter'] == 37)//Inbound and Outbound
                    {
                         $sheet->setCellValue('A'.$line, $dtmPickupDate);
                         $sheet->setCellValue('C'.$line, $quantityLoad);
                         $sheet->setCellValue('D'.$line, $truckCap);
                         if($isConsol == 1)
                         {
                              $sheet->setCellValue('F'.$line, $transportcharge);
                         }
                         else
                         {
                              $sheet->setCellValue('E'.$line, $transportcharge);
                         }
                         $sheet->setCellValue('H'.$line, $dtmOriginPointID_FK);
                         $sheet->setCellValue('I'.$line, $dtmDestinationPointID_FK);
                         $sheet->setCellValue('L'.$line,'=SUM(E'.$line.'+F'.$line.'+K'.$line.')');

                         $sheet->setCellValue('O'.$line, $remarks);
                         $sheet->setCellValue('P'.$line, $dtmNoID_PK);
                         $sheet->setCellValue('Q'.$line, $consolShow);

                         $sheet->getStyle('A'.$line.':N'.$line)->getAlignment()->setHorizontal('center');
                         $line++;
                    }
                    else
                    {
                         if($selectedCompanyName == $companyID_FK)
                         {
                              $sheet->setCellValue('A'.$line, ($line-6));
                              $sheet->setCellValue('B'.$line, $dtmPickupDate);
                              $sheet->setCellValue('C'.$line, $truckID_FK);
                              $sheet->setCellValue('D'.$line, $truckCap);
                              $sheet->setCellValue('F'.$line, $quantityLoad);
                              if($isConsol == 1)
                              {
                                   $sheet->setCellValue('K'.$line, $transportcharge);
                              }
                              else
                              {
                                   $sheet->setCellValue('G'.$line, $transportcharge);
                              }
                              $sheet->setCellValue('H'.$line, $faf);

                              $sheet->setCellValue('O'.$line, $remarks);
                              $sheet->setCellValue('P'.$line, $dtmNoID_PK);
                              $sheet->setCellValue('Q'.$line, $consolShow);

                              $sheet->setCellValue('N'.$line,'=SUM(G'.$line.':M'.$line.')');
                              $sheet->getStyle('A'.$line.':N'.$line)->getAlignment()->setHorizontal('center');
                              $line++;
                         }
                    }
               }
               else if($companyID == 55)
               {
                    $sheet->setCellValue('A'.$line, ($line-6));
                    $sheet->setCellValue('B'.$line, $dtmPickupDate);
                    $sheet->setCellValue('C'.$line, $dtmPickupTime);
                    $sheet->setCellValue('D'.$line, $dtmDGF);
                    $sheet->setCellValue('E'.$line, $dtmUrgentMemo);
                    $sheet->setCellValue('F'.$line, $dtmRequestBy);
                    $sheet->setCellValue('G'.$line, $dtmOriginPointID_FK);
                    $sheet->setCellValue('H'.$line, $dtmOriginZone);
                    $sheet->setCellValue('I'.$line, $dtmDestinationPointID_FK);
                    $sheet->setCellValue('J'.$line, $dtmDestinationZone);

                    $sheet->setCellValue('K'.$line, $lockupf);
                    $sheet->setCellValue('L'.$line, $timeF);
                    $sheet->setCellValue('M'.$line, $lockupdockTime);
                    $sheet->setCellValue('N'.$line, $timedockTime);
                    $sheet->setCellValue('O'.$line, $lockupcompleteLoadingTime);
                    $sheet->setCellValue('P'.$line, $timecompleteLoadingTime);
                    $sheet->setCellValue('Q'.$line, "");
                    $sheet->setCellValue('R'.$line, "");
                    $sheet->setCellValue('S'.$line, $lockupdepartureTimeFromPost);
                    $sheet->setCellValue('T'.$line, $timedepartureTimeFromPost);
                    $sheet->setCellValue('U'.$line, $lockuparrivalTimeAtPost);
                    $sheet->setCellValue('V'.$line, $timearrivalTimeAtPost);
                    $sheet->setCellValue('W'.$line, "");
                    $sheet->setCellValue('X'.$line, "");
                    $sheet->setCellValue('Y'.$line, $lockupdestinationDockTime);
                    $sheet->setCellValue('Z'.$line, $timedestinationDockTime);
                    $sheet->setCellValue('AA'.$line, $lockupcompleteUnloadTime);
                    $sheet->setCellValue('AB'.$line, $timecompleteUnloadTime);
                    $sheet->setCellValue('AC'.$line, $lockupdepartTimeFromPost);
                    $sheet->setCellValue('AD'.$line, $timedepartTimeFromPost);

                    $sheet->setCellValue('AE'.$line, $truckID_FK);
                    $sheet->setCellValue('AF'.$line, $truckCap);
                    $sheet->setCellValue('AH'.$line, $transportcharge);
                    $sheet->setCellValue('AI'.$line, $faf);
                    $sheet->setCellValue('AJ'.$line, 0);

                    $sheet->setCellValue('AM'.$line, $remarks);
                    $sheet->setCellValue('AN'.$line, $dtmNoID_PK);
                    $sheet->setCellValue('AO'.$line, $consolShow);

                    for($x='K'; $x != 'AE'; $x++)
                    {
                         $sheet->getColumnDimension($x)->setWidth(15);
                    }
                    $sheet->getColumnDimension('AK')->setWidth(20);
                    $sheet->getColumnDimension('AL')->setWidth(20);
                    $sheet->getColumnDimension('AM')->setWidth(20);
                    $sheet->getColumnDimension('AN')->setWidth(20);
                    $sheet->getColumnDimension('AO')->setWidth(20);
                    $sheet->setCellValue('AK'.$line,'=SUM(AH'.$line.':AI'.$line.')');
                    $sheet->setCellValue('AL'.$line,'=SUM(AH'.$line.':AJ'.$line.')');
                    $sheet->getStyle('A'.$line.':AO'.$line)->getAlignment()->setHorizontal('center');
                    $line++;
               } 
               else
               {
                    if($selectedCompanyName == $companyID_FK)
                    {
                         $sheet->setCellValue('A'.$line, ($line-6));
                         $sheet->setCellValue('B'.$line, $dtmPickupDate);
                         $sheet->setCellValue('C'.$line, $truckID_FK);
                         $sheet->setCellValue('D'.$line, $truckCap);
                         $sheet->setCellValue('F'.$line, $quantityLoad);
                         if($isConsol == 1)
                         {
                              $sheet->setCellValue('K'.$line, $transportcharge);
                         }
                         else
                         {
                              $sheet->setCellValue('G'.$line, $transportcharge);
                         }
                         $sheet->setCellValue('H'.$line, $faf);
                    
                         $sheet->setCellValue('O'.$line, $remarks);
                         $sheet->setCellValue('P'.$line, $dtmNoID_PK);
                         $sheet->setCellValue('Q'.$line, $consolShow);

                         $sheet->setCellValue('N'.$line,'=SUM(G'.$line.':M'.$line.')');
                         $sheet->getStyle('A'.$line.':N'.$line)->getAlignment()->setHorizontal('center');
                         $line++;
                    }
               }   
          }
         
          
     }


     if($companyID == 43)
     {
          if(  $_POST['costcenter'] == 19 || 
          $_POST['costcenter'] == 20 || 
          $_POST['costcenter'] == 21 ||
          $_POST['costcenter'] == 28 ||
               $_POST['costcenter'] ==  24) 
          {
               $sheet->setCellValue('A'.$line, "GRAND TOTAL");
               $sheet->mergeCells('A'.$line.':O'.$line);
               $sheet->setCellValue('P'.$line,'=SUM(P7:P'.($line-1).')');
               $sheet->setCellValue('Q'.$line,'=SUM(Q7:Q'.($line-1).')');
               $sheet->setCellValue('R'.$line,'=SUM(R7:R'.($line-1).')');
               $sheet->setCellValue('S'.$line,'=SUM(S7:S'.($line-1).')');
               $sheet->setCellValue('T'.$line,'=SUM(T7:T'.($line-1).')');
               $sheet->setCellValue('U'.$line,'=SUM(U7:U'.($line-1).')');
               $sheet->setCellValue('V'.$line,'=SUM(V7:V'.($line-1).')');
               $sheet->setCellValue('W'.$line,'=SUM(W7:W'.($line-1).')');
               $sheet->getStyle('P'.$line.':W'.$line)->getAlignment()->setHorizontal('center');
          }
          else if ($_POST['costcenter'] == 38)
          {
               $sheet->setCellValue('A'.$line, "GRAND TOTAL");
               $sheet->mergeCells('A'.$line.':P'.$line);
               $sheet->setCellValue('Q'.$line,'=SUM(Q7:Q'.($line-1).')');
               $sheet->setCellValue('R'.$line,'=SUM(R7:R'.($line-1).')');
               $sheet->setCellValue('S'.$line,'=SUM(S7:S'.($line-1).')');
               $sheet->setCellValue('T'.$line,'=SUM(T7:T'.($line-1).')');
               $sheet->setCellValue('U'.$line,'=SUM(U7:U'.($line-1).')');
               $sheet->setCellValue('V'.$line,'=SUM(V7:V'.($line-1).')');
               $sheet->setCellValue('W'.$line,'=SUM(W7:W'.($line-1).')');
               $sheet->setCellValue('X'.$line,'=SUM(X7:X'.($line-1).')');
               $sheet->setCellValue('Y'.$line,'=SUM(Y7:Y'.($line-1).')');
               $sheet->getStyle('Q'.$line.':Y'.$line)->getAlignment()->setHorizontal('center');
          }
          else if ($_POST['costcenter'] == 39)
          {
               $sheet->setCellValue('A'.$line, "GRAND TOTAL");
               $sheet->mergeCells('A'.$line.':H'.$line);
               $sheet->setCellValue('I'.$line,'=SUM(I7:I'.($line-1).')');
               $sheet->setCellValue('J'.$line,'=SUM(J7:J'.($line-1).')');
               $sheet->setCellValue('K'.$line,'=SUM(K7:K'.($line-1).')');
               $sheet->setCellValue('L'.$line,'=SUM(L7:L'.($line-1).')');
               $sheet->setCellValue('M'.$line,'=SUM(M7:M'.($line-1).')');
               $sheet->setCellValue('N'.$line,'=SUM(N7:N'.($line-1).')');
               $sheet->setCellValue('O'.$line,'=SUM(O7:O'.($line-1).')');
               $sheet->setCellValue('P'.$line,'=SUM(P7:P'.($line-1).')');
               $sheet->getStyle('I'.$line.':P'.$line)->getAlignment()->setHorizontal('center');
          }
          else
          {
               normalTemplateBottom($sheet,$line);
          }
     }
      else if($companyID == 73)
     {
          if($_POST['costcenter'] == 29 || $_POST['costcenter'] == 30 || $_POST['costcenter'] == 31) //BCOM,LOCAL,TRANSFER
          {
               $sheet->setCellValue('A'.$line, "TOTAL AMOUNT");
               $sheet->mergeCells('A'.$line.':J'.$line);
               $sheet->setCellValue('K'.$line,'=SUM(K7:K'.($line-1).')');
               $sheet->setCellValue('M'.$line,'=SUM(M7:M'.($line-1).')');
               $sheet->setCellValue('O'.$line,'=SUM(O7:O'.($line-1).')');
               $sheet->setCellValue('P'.$line,'=SUM(P7:P'.($line-1).')');
               $sheet->setCellValue('Q'.$line,'=SUM(Q7:Q'.($line-1).')');
               $sheet->setCellValue('R'.$line,'=SUM(R7:R'.($line-1).')');
               $sheet->setCellValue('S'.$line,'=SUM(S7:S'.($line-1).')');
               $sheet->setCellValue('T'.$line,'=SUM(T7:T'.($line-1).')');
               $sheet->getStyle('K'.$line.':T'.$line)->getAlignment()->setHorizontal('center');
          }
          else
          {
               normalTemplateBottom($sheet,$line);
          }
     }
     else if($companyID == 62)
     {  
          if($_POST['costcenter'] == 34 || $_POST['costcenter'] == 35)//Air Export And Air Import
          {
               $sheet->setCellValue('A'.$line, "TOTAL ");
               $sheet->mergeCells('A'.$line.':G'.$line);
               $sheet->setCellValue('H'.$line,'=SUM(H7:H'.($line-1).')');
               $sheet->setCellValue('I'.$line,'=SUM(I7:I'.($line-1).')');
               $sheet->setCellValue('J'.$line,'=SUM(J7:J'.($line-1).')');
               $sheet->setCellValue('K'.$line,'=SUM(K7:K'.($line-1).')');
               $sheet->setCellValue('L'.$line,'=SUM(L7:L'.($line-1).')');
               $sheet->setCellValue('M'.$line,'=SUM(M7:M'.($line-1).')');
               $sheet->setCellValue('N'.$line,'=SUM(N7:N'.($line-1).')');
               $sheet->setCellValue('O'.$line,'=SUM(O7:O'.($line-1).')');
               $sheet->setCellValue('P'.$line,'=SUM(P7:P'.($line-1).')');
               $sheet->getStyle('H'.$line.':P'.$line)->getAlignment()->setHorizontal('center');
          
          }
          else
          {
               normalTemplateBottom($sheet,$line);
          }
     }
     else if($companyID == 79)
     {
          if($_POST['costcenter'] == 32 || $_POST['costcenter'] == 33)//Pickup And Delivery
          {
               $sheet->setCellValue('A'.$line, "TOTAL AMOUNT");
               $sheet->mergeCells('A'.$line.':J'.$line);
               $sheet->setCellValue('K'.$line,'=SUM(K7:K'.($line-1).')');
               $sheet->setCellValue('L'.$line,'=SUM(L7:L'.($line-1).')');
               $sheet->setCellValue('M'.$line,'=SUM(M7:M'.($line-1).')');
               $sheet->setCellValue('N'.$line,'=SUM(N7:N'.($line-1).')');
               $sheet->setCellValue('O'.$line,'=SUM(O7:O'.($line-1).')');
               $sheet->setCellValue('P'.$line,'=SUM(P7:P'.($line-1).')');
               $sheet->getStyle('K'.$line.':P'.$line)->getAlignment()->setHorizontal('center');
          }
          else
          {
               normalTemplateBottom($sheet,$line);
          }
     }
     else if($companyID == 70)
     {
          if($_POST['costcenter'] == 36 || $_POST['costcenter'] == 37)//Inbound and Outbound
          {
               $sheet->setCellValue('B'.$line, "TOTAL");

               $sheet->setCellValue('E'.$line,'=SUM(E7:E'.($line-1).')');
               $sheet->setCellValue('F'.$line,'=SUM(F7:F'.($line-1).')');
               $sheet->setCellValue('K'.$line,'=SUM(K7:K'.($line-1).')');
               $sheet->getStyle('A'.$line.':N'.$line)->getAlignment()->setHorizontal('center');

               $sheet->setCellValue('B'.($line+1), "SUB TOTAL");
               $sheet->setCellValue('L'.($line+1),'=SUM(E'.$line.' + F'.$line.' + K'.$line.')');
               $sheet->getStyle('A'.$line.':N'.$line)->getAlignment()->setHorizontal('center');
          }
          else
          {
               normalTemplateBottom($sheet,$line);
          }
     }
     else if($companyID == 55)
     {
          $sheet->setCellValue('AJ'.$line,'=SUM(AJ7:AJ'.($line-1).')');
          $sheet->setCellValue('AK'.$line,'=SUM(AK7:AK'.($line-1).')');
          $sheet->setCellValue('AL'.$line,'=SUM(AL7:AL'.($line-1).')');
          $sheet->getStyle('A'.$line.':AL'.$line)->getAlignment()->setHorizontal('center');
     }  
     else
     {
          normalTemplateBottom($sheet,$line);
     }  
    
}
else
{
     $sheet->setCellValue('A2', "error 1");
}


$writer = new Xlsx($spreadsheet);
$writer->save('php://output');

}
else
{
     header('Location:invoicePrint.php?message=1');
}

?>
