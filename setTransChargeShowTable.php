<?php
require 'generalFunction.php';
$conn = connDB();

$fromPage = $_POST['fromPage'];
$condition = $_POST['condition'];
$pageNo = $_POST['pageNo'];
$filter = $_POST['filter'];
$searchWord = $_POST['searchWord'];

// echo $fromPage;
// echo $condition;
// echo $pageNo;
// echo $filter;
// echo " =".$searchWord."=";

if($filter == null)
{
    $filter= 1;
}
if($pageNo == null)
{
    $filter= 1;
}
if($searchWord == null)
{
    $searchWord = "";
}

$sqlPageNo = 0;
$sqlPageNo = ($pageNo - 1) * 10;

if($filter == 1)
{
    if($fromPage == 30)
    {
        $orderBy = "companyID_FK";
    }
}


$sql = "";
$sql2 = "";

if($fromPage == 30)
{
    $sql .= " SELECT * FROM (transportrate INNER JOIN company ON transportrate.companyID_FK = company.companyID_PK) WHERE transportrate.showThis = 1 ";
    $sql2 .= " SELECT COUNT(*) as total2 FROM (transportrate INNER JOIN company ON transportrate.companyID_FK = company.companyID_PK) WHERE transportrate.showThis = 1 ";
}


if($searchWord != null && $searchWord != "")
{
    if($fromPage == 30)
    {
        $sql .= " AND companyName LIKE '%".$searchWord."%' ";
        $sql2 .= " AND companyName LIKE '%".$searchWord."%' ";
    }
}

if ($orderBy != "") 
{
    if($filter == 1)
    {
        $sql .= " ORDER BY ".$orderBy." DESC ";
        $sql2 .= " ORDER BY ".$orderBy." DESC ";
    }
    else
    {
        $sql .= " ORDER BY ".$orderBy." ASC ";
        $sql2 .= " ORDER BY ".$orderBy." ASC ";
    }
}

$sql .=" LIMIT ".$sqlPageNo.",10 ";

if($condition == 1)
{

    $initialSql = "SELECT COUNT(*) as total from (transportrate INNER JOIN company ON transportrate.companyID_FK = company.companyID_PK) WHERE transportrate.showThis = 1 ";


    $result = mysqli_query($conn,$initialSql);
    $data = mysqli_fetch_assoc($result);
    $no_of_pages = 0;
    $no_of_pages = ceil($data['total'] / 10);
}
else
{
    $result2 = mysqli_query($conn,$sql2);
    $dataCount = mysqli_fetch_assoc($result2);
    $no_of_pages = 0;
    $no_of_pages = ceil($dataCount['total2'] / 10);
    
}

$querylisting = mysqli_query($conn,$sql);

generateDeleteModal($fromPage);
generateConfirmationDeleteModal($fromPage);
?>
<script>$("#pagination"+<?php echo $fromPage;?>+" option").remove();</script>
<table class="table table-sm dtmTableNoWrap table-hovered table-striped table-responsive-xl removebottommargin">
    <thead>
        <tr>
        <?php 
            if($fromPage == 30)
            {
                ?>
                    <th class="adminTableWidthTD">Selection</th>
                    <th >Agent Name</th>
                    <th >Load Capacity</th>
                    <th >Transport Rates</th>
                    <th >Consol Rates</th>
                    <th >From</th>
                    <th >To</th>
                <?php
            }
        ?>
        </tr>
  </thead>
  <tbody>
    <?php 
        if (mysqli_num_rows($querylisting) > 0) 
        {
            while($row = mysqli_fetch_array($querylisting))
            {
    ?>
    <tr>
        <?php 

            
            if($fromPage == 30)
            {
                
                ?>
                    <td class="adminTableWidthTD">
                        <div class="adminAlignOptionInline">
                            <form action="CDEdit.php" method="POST" class="adminformEdit">
                                <input type="hidden" name="tableType" value="<?php echo $fromPage;?>">
                                <button class="btn btn-warning edtOpt" value="<?php echo $row['id'];?>" name="edit">Update</button>
                            </form>
                            <button class="btn btn-danger edtOpt" value="<?php echo $row['id'];?>" onclick="deleteAdmin(<?php echo $fromPage;?>,this.value)">Delete</button>
                        </div>
                    </td>
                    <td >
                        <?php
                        $conn = connDB();
                        $sql_select_costCenter = "SELECT * FROM company WHERE companyID_PK = ".$row['companyID_FK'];
                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                        if (mysqli_num_rows($result_select_costCenter) > 0)
                        {
                            // output data of each row
                            while($row2 = mysqli_fetch_assoc($result_select_costCenter))
                            {
                                echo $row2["companyName"];
                            }
                        }?>
                    </td>
                    <td >
                        <?php echo $row["loadTransport"];?>
                    </td>
                    <td >
                        <?php echo $row["rates"];?>
                    </td>
                    <td >
                        <?php echo $row["consoleRate"];?>
                    </td>
                    <td >
                        <?php
                            $sqlA = " SELECT zonesName FROM zones WHERE zonesID_PK = ".$row["origin"];
                            $result1A = mysqli_query($conn,$sqlA);
                            
                            if (mysqli_num_rows($result1A) > 0) 
                            {
                                while($row3 = mysqli_fetch_array($result1A))
                                { 
                                    echo $username = $row3['zonesName'];    
                                }
                            }
                         ?>
                    </td>
                    <td >
                        <?php

                            $sqlAB = " SELECT zonesName FROM zones WHERE zonesID_PK = ".$row["destination"];
                            $result1AB = mysqli_query($conn,$sqlAB);
                            
                            if (mysqli_num_rows($result1AB) > 0) 
                            {
                                while($row4 = mysqli_fetch_array($result1AB))
                                { 
                                    echo $username = $row4['zonesName'];    
                                }
                            }
                         ?>
                    </td>
                <?php
            }
                ?>
        </tr>
    <?php 
            }
        }
        else
        {
            echo  $conn->error;
            if($fromPage == 30)
            {
                ?>
                    <tr>
                        <td colspan="7" style="text-align:center;">No Records Found</td>
                    </tr>
                <?php
            }
        }
    ?>
  </tbody>
</table>
<?php
    if($condition == 1)
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$data['total']);
    }
    else
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$dataCount['total2']);
    }
?>
 