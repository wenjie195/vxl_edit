<?php
//Start the session
session_start();

//Check f the session is empty/exist or not
if(!empty($_SESSION))
{
    require 'generalFunction.php';
    $conn = connDB();

    ?>
    <!doctype html>
    <html lang="en">
    <head>
        <title>DTM Print</title>
        <?php require 'indexHeader.php';?>
        <style>
        .dsfPagination {
            margin-left: 25px;
        }
        .dsfFilterPara {
            margin-left: 350px;
        }
    </style>
    </head>
    <body>
    <?php require 'indexNavbar.php';?>
    <div class="container-fluid">
        <div class="row">
            <?php require 'indexSidebar.php';
            generateSimpleModal();
            ?>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h3>Print DTM By Date</h3>
                </div>
                <div class="row">
                    <form class="col-xl-12 row" method="POST" action="printDTM.php">
                        <div class="col-xl-1" ></div>
                        <div class="form-group col-xl-5">
                            <label for="fromDate" >From(Date)</label>
                            <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="fromDate" name="fromDate">
                        </div>
                        <div class="form-group col-xl-5">
                            <label for="toDate" >To(Date)</label>
                            <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="toDate" name="toDate">
                        </div>
                        <div class="col-xl-1" ></div>

                        <div class="col-xl-3"></div>
                        <div class="col-xl-6 text-center">
                            <button class="btn formButtonPrimary " onclick="checkDTMDATE();">Print DTM</button>
                        </div>
                        <div class="col-xl-3"></div>
                    </form>
                </div>
            </main>
        </div>
    </div>
    <?php require 'indexFooter.php';?>
    <script>
        $(document).ready(function()
        {
               let from,to;
               from = initializeDate(document.getElementById('fromDate'),from);
               to = initializeDate(document.getElementById('toDate'),to);
        });

        function initializeDate(thisId,setVariable)
        {
            var setVariable = new Pikaday({
                field: thisId,
                format: 'YYYYMMDD',
                onSelect: function() {
                    // console.log(this.getMoment().format('Do MMMM YYYY'));
                }
            });
            return setVariable;
        }
    </script>
    </body>
    </html>
    <?php
}
else
{
    // Go back to index page
    // NOTE : MUST PROMPT ERROR
    header('Location:index.php');
}
?>