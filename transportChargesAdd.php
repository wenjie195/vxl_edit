<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        if(isset($_POST['add']))
        {   
            echo $add = $_POST['add'];// --> dtmID_FK
            require 'generalFunction.php';

?>
<!doctype html>
<html lang="en">
    <head>
        <title>Add Transport Charges</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>
                              Add Transport Charges
                        </h3>
                    </div>
                    <?php   
                        generateConfirmationModal();
                        generateSimpleModal();
                    ?>
                    <?php
                         $conn = connDB(); 
                         $costCenterDisplay = "SELECT * FROM dtmlist WHERE dtmID_PK = ".$add;
                         $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                         if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                         {    
                              while($row = mysqli_fetch_array($costCenterDisplayQuery))
                              {
                                   ?>
                                   <table class="table dtmTableNoWrap table-sm table-hovered table-bordered table-striped table-responsive-xl  dtmTableNoWrap"style="text-align:center;">
                                        <thead>
                                             <tr>
                                                  <th>Planner</th>
                                                  <th>DTM NO</th>
                                                  <th>VXL D/O No</th>
                                                  <th>Pickup Date</th>
                                                  <th>Request By</th>
                                                  <th>Pickup Time</th>
                                                  <th>From</th>
                                                  <th>To</th>
                                             </tr>
                                        </thead>
                                        <tbody>
                                             <tr>
                                                  <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT userNickName FROM user WHERE userID_PK = ".$row['dtmPlannerID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['userNickName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php echo $row['dtmID_PK'];?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                        $vxlDO = "";
                                                        if($row['dtmAdhoc'])
                                                        {
                                                            $vxlDO = $row['dtmAdhoc'];
                                                        }
                                                            echo $vxlDO;
                                                       ?>
                                                  </td>
                                                  <!-- <td>
                                                       <?php 
                                                       // $shipmentDate = date("d M Y",strtotime($row['dtmBookingDate']));
                                                       // echo $shipmentDate;
                                                       ?>
                                                  </td> -->
                                                  <td>
                                                       <?php 
                                                            $pickupDate = date("d M Y",strtotime($row['dtmPickupDate']));
                                                            echo $pickupDate;
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php echo $row['dtmRequestBy'];?>
                                                  </td>
                                                  <!-- <td>
                                                       <?php 
                                                       // $shipmentTime = date("G:i",strtotime($row['dtmBookingTime']));
                                                       // echo $shipmentTime;
                                                       ?>
                                                  </td> -->
                                                  <td>
                                                       <?php 
                                                            $pickupTime = date("G:i",strtotime($row['dtmPickupTime']));
                                                            echo $pickupTime;
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$row['dtmOriginPointID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['pointzonePlaceName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$row['dtmDestinationPointID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['pointzonePlaceName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                             </tr>
                                        </tbody>
                                   </table>
                                   <table class="table table-sm table-hovered table-bordered table-striped table-responsive-xl  dtmTableNoWrap"style="text-align:center;">
                                        <thead>
                                             <tr>
                                                  <th>Agent</th>
                                                  <th>Cost Centre</th>
                                                  <th>Truck No</th>
                                                  <th>CAP</th>
                                                  <th>Consol</th>
                                                  <th>Driver Name</th>
                                                  <th>Remark</th>
                                                  <th>From Zones</th>
                                                  <th>To Zones</th>
                                             </tr>
                                        </thead>
                                        <tbody>
                                             <tr>

                                             <td>
                                                       <?php
                                                       $companyID_FK =  $row['companyID_FK']; 
                                                       $costCenterDisplay = "SELECT companyName FROM company WHERE companyID_PK = ".$row['companyID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['companyName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td><?php 
                                                  $isConsol = $row['isConsol'];
                                                  $ee = "SELECT costCenterName FROM costcenter WHERE costCenterID_PK = ".$row['costCenterID_FK'];
                                                  $e = mysqli_query($conn,$ee);
                                                  if (mysqli_num_rows($e) > 0) 
                                                  {
                                                       while($urow1 = mysqli_fetch_array($e))
                                                       {
                                                            echo $adhoc  = $urow1['costCenterName'];
                                                       }
                                                  }
                                                  
                                                  ?></td>
                                                  <td>
                                                       <?php
                                                       $truckID_FK =  $row['truckID_FK'];
                                                       $costCenterDisplay = "SELECT truckPlateNo FROM trucks WHERE truckID_PK = ".$row['truckID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['truckPlateNo'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                            echo $loadCap = $row['loadCap'];
                                                       ?>
                                                  </td>
                                                  <td>
                                                  <?php
                                                  $isConsol = $row['isConsol'];
                                                  if($isConsol == 1)
                                                  {
                                                       echo "CONSOL";
                                                  }
                                                  else
                                                  {
                                                       echo "-";
                                                  }
                                                  ?>
                                                  </td>
                                                  <td class="text-left">
                                                       <?php 
                                                       $costCenterDisplay = "SELECT driverName FROM driver WHERE driverID_PK = ".$row['driverID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo "1.".$urow1['driverName'];
                                                            }
                                                       }

                                                       if($row['driver2ID_FK'])
                                                       {
                                                                 $costCenterDisplay = "SELECT driverName FROM driver WHERE driverID_PK = ".$row['driver2ID_FK'];
                                                                 $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                                 if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                                 {
                                                                      while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                                      {
                                                                           echo "<br>2.".$urow1['driverName'];
                                                                      }
                                                                 }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td style="text-align:left;"><?php 
                                                  if( $row['lockupf'] || $row['lockupdockTime'] || $row['lockupcompleteLoadingTime'] || $row['lockupdepartureTimeFromPost'] ||
                                                  $row['lockuparrivalTimeAtPost'] || $row['lockupdestinationDockTime'] || $row['lockupcompleteUnloadTime'] || $row['lockupdepartTimeFromPost'])
                                                  {
                                                  echo "->Lockup/Detention<br>->".strtoupper($row['dtmRemarks']);
                                                  }
                                                  else
                                                  {
                                                  echo "->".strtoupper($row['dtmRemarks']);
                                                  }?></td>
                                                 
                                                  <td>
                                                       <?php 

                                                       $costCenterDisplay = "SELECT zonesName FROM zones WHERE zonesID_PK = ".$row['dtmOriginZoneID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo  $urow1['zonesName'];
                                                                 $originPlace = $row['dtmOriginZoneID_FK'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                  <?php 

                                                       $costCenterDisplay = "SELECT zonesName FROM zones WHERE zonesID_PK = ".$row['dtmDestinationZoneID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['zonesName'];
                                                                 $destinationPlace = $row['dtmDestinationZoneID_FK'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                             </tr>
                                        </tbody>
                                   </table>
                                   <?php
                              }
                         }
                    ?>
                    <div class="row adminAddMarginTop">
                        <div class="col-xl-12 row">
                         <div class="col-xl-1"></div>
                         <div class="form-group col-xl-5">
                              <label for="field_1" >Remarks For Transport Charges</label>
                              <input  type="text" class="form-control adminAddSetPadding" id="field_1">
                         </div>
                         <div class="form-group col-xl-5">
                              <label for="field_2" >Operating Hours</label>
                              <input  type="number" class="form-control adminAddSetPadding" id="field_2" min="1" step="1">
                         </div>
                         <div class="col-xl-1"></div>
                         <div class="col-xl-1"></div>
                         <div class="form-group col-xl-5">
                              <?php 
                              if($isConsol == 1)
                              {
                                   ?><label for="field_3" >Consol Charges</label><?php 
                                   $costCenterDisplay = "SELECT consoleRate ";
                              }
                              else
                              {
                                   ?><label for="field_3" >Transport Charges</label><?php 
                                   $costCenterDisplay = "SELECT rates ";
                              }

                              $costCenterDisplay .= "FROM transportrate WHERE companyID_FK = ".$companyID_FK." 
                              AND loadTransport = '".$loadCap."'
                              AND origin = '".$originPlace."'
                              AND destination = '".$destinationPlace."' ";

                              $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                              if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                              {
                                   while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                   {
                                        if($isConsol == 1)
                                        {
                                             $transportrate = $urow1['consoleRate'];
                                        }
                                        else
                                        {
                                             $transportrate = $urow1['rates'];
                                        }
                                         
                                   }
                              }
                              ?>
                              <input  type="number" class="form-control adminAddSetPadding" id="field_3" min="0" value="<?php echo $transportrate;?>">
                         </div>
                         <div class="form-group col-xl-5">
                              <label for="field_4" >FAF (%)</label>
                              <input  type="number" class="form-control adminAddSetPadding" id="field_4" >
                         </div>
                         <div class="col-xl-1"></div>
                         <div class="col-xl-3"></div>
                         <div class="col-xl-6 adminAddUserButton">
                         <input type="hidden" id="truckID" value="<?php echo $truckID_FK;?>">
                         <input type="hidden" id="companyID" value="<?php echo $companyID_FK;?>">
                              <button class="btn formButtonPrimary indexSubmitButton" onclick="addTransportCharge(<?php echo $add;?>,1);">Add Transport Charge</button>
                         </div>
                         <div class="col-xl-3"></div>
                             
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
    </body>
</html>
<?php
        }
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>