<?php
    include 'generalFunction.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>VXL System</title>
        <?php require 'indexHeader.php';?>
    </head>
    <body>
    <?php require 'indexNavbar.php';?>
    <div class="container-fluid">
        <div class="row">
            <div class="col"></div>
            <div class="col-xl-6 col-lg-6 col-md-8 col-sm-10 col-10">
                <?php
                    generateSimpleModal();
                    checkLogin();
                ?>
                <div class="indexHeaderText">
                    <h3 class="indexHeader">Welcome To VXL System</h3>
                </div>
                <form class="indexFormPosition" method="POST">
                    <div class="form-group">
                        <input type="email" class="form-control" name="field_1" placeholder="Email address" >
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="field_2" placeholder="Password" >
                    </div>
                    <div class="indexSubmitButtonDiv">
                        <button class="btn formButtonPrimary indexSubmitButton">Log In</button>
                    </div>
                </form>
            </div>
            <div class="col"></div>
        </div>
    </div>    
    <?php require 'indexFooter.php';?>
    </body>
</html>