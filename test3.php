<?php
date_default_timezone_set("Asia/Kuala_Lumpur");
$todayDate = date("Ymd_his");
$filename = 'filename="Invoice_'.$todayDate.'.xlsx"';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; '.$filename);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;

define('NUMBER_OF_COLUMNS', 37); 
require 'generalFunction.php';
require 'phpexcel/vendor/autoload.php';
ob_end_clean();

function getUser($uid)
{
     $conn = connDB();
     $sqlA = " SELECT userName FROM user WHERE  userID_PK = ".$uid;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['userName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getPlace($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['pointzonePlaceName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getCompany($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT companyName FROM company WHERE companyID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['companyName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getDriver($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT driverName FROM driver WHERE driverID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['driverName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getTruckPlateNo($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT truckPlateNo FROM trucks WHERE truckID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['truckPlateNo'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getCostCenter($ccId)
{
     $conn = connDB();
     $sqlA = " SELECT costCenterName FROM costcenter WHERE costCenterID_PK = ".$ccId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['costCenterName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}

$conn = connDB();
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$sheet->setCellValue('A1', 'Invoice No');
$sheet->getColumnDimension('A')->setWidth(15);
$sheet->setCellValue('B1', 'Planner');
$sheet->getColumnDimension('B')->setWidth(25);
$sheet->setCellValue('C1', 'Booking Date');
$sheet->getColumnDimension('C')->setWidth(20);
$sheet->setCellValue('E1', 'Pickup Date');
$sheet->getColumnDimension('D')->setWidth(20);
$sheet->setCellValue('G1', 'Request By');
$sheet->getColumnDimension('E')->setWidth(25);
$sheet->setCellValue('D1', 'Booking Time');
$sheet->getColumnDimension('F')->setWidth(20);
$sheet->setCellValue('F1', 'Pickup Time');
$sheet->getColumnDimension('G')->setWidth(20);
$sheet->setCellValue('H1', 'From');
$sheet->getColumnDimension('H')->setWidth(25);
$sheet->setCellValue('I1', 'To');
$sheet->getColumnDimension('I')->setWidth(25);
$sheet->setCellValue('J1', 'Remark');
$sheet->getColumnDimension('J')->setWidth(25);
$sheet->setCellValue('M1', 'Truck No');
$sheet->getColumnDimension('K')->setWidth(25);
$sheet->setCellValue('N1', 'CAP');
$sheet->getColumnDimension('L')->setWidth(25);
$sheet->setCellValue('O1', 'Consol');
$sheet->getColumnDimension('M')->setWidth(25);
$sheet->setCellValue('P1', 'Driver Name');
$sheet->getColumnDimension('N')->setWidth(25);
$sheet->setCellValue('Q1', 'Remark 2');
$sheet->getColumnDimension('O')->setWidth(25);
$sheet->setCellValue('K1', 'Agent');
$sheet->getColumnDimension('P')->setWidth(25);
$sheet->setCellValue('L1', 'Cost Center');
$sheet->getColumnDimension('Q')->setWidth(25);
$sheet->setCellValue('R1', 'Operating Hour');
$sheet->getColumnDimension('R')->setWidth(20);
$sheet->setCellValue('S1', 'Transport Charge');
$sheet->getColumnDimension('S')->setWidth(20);
$sheet->setCellValue('T1', 'FAF %');
$sheet->getColumnDimension('T')->setWidth(15);
$sheet->setCellValue('U1', 'Total');
$sheet->getColumnDimension('U')->setWidth(15);
$sheet->setCellValue('V1', 'D/O No');
$sheet->getColumnDimension('V')->setWidth(15);
$sheet->setCellValue('W1', 'DTM No');
$sheet->getColumnDimension('W')->setWidth(15);

$sheet->getStyle('A1:W1')->getAlignment()->setHorizontal('center');


$sqlo = " SELECT * FROM invoice WHERE isSelected = 0 OR isSelected IS NULL ORDER BY invoiceNo DESC";
     
$result = mysqli_query($conn,$sqlo);

if (mysqli_num_rows($result) > 0) 
{
     $line = 2;
     while($row = mysqli_fetch_array($result))
     { 
          
          $sql = " SELECT * FROM transportcharge WHERE id = ".$row['transportID_FK'];
          $result1 = mysqli_query($conn,$sql);
          
          if (mysqli_num_rows($result1) > 0) 
          {
               while($row2 = mysqli_fetch_array($result1))
               { 
                    $sqlA = " SELECT * FROM dtmlist WHERE dtmID_PK = ".$row2['dtmID_FK'];
                    $result1A = mysqli_query($conn,$sqlA);
                    
                    if (mysqli_num_rows($result1A) > 0) 
                    {
                         while($row3 = mysqli_fetch_array($result1A))
                         { 
                              $dtmIDPK = $row3['dtmID_PK'];
                              $planner = getUser($row3['dtmPlannerID_FK']);
                              $dtmRequestBy = $row3['dtmRequestBy'];
                              $dtmBookingTime = $row3['dtmBookingTime'];
                              $dtmPickupTime = $row3['dtmPickupTime'];

                              $dtmBookingDate = date("d M Y",strtotime($row3['dtmBookingDate']));
                              $dtmPickupDate = date("d M Y",strtotime($row3['dtmPickupDate']));

                              $dtmOriginPointID_FK = getPlace($row3['dtmOriginPointID_FK']);
                              $dtmDestinationPointID_FK = getPlace($row3['dtmDestinationPointID_FK']);
                              $companyID_FK = getCompany($row3['companyID_FK']);
                              $truckID_FK = getTruckPlateNo($row3['truckID_FK']);
                              $loadCap = $row3['loadCap'];

                              $drivers = "";
                              $driverID_FK = getDriver($row3['driverID_FK']);
                              $drivers.= "(1) ".$driverID_FK;

                              if(isset($row3['driver2ID_FK']))
                              {$driver2ID_FK = getDriver($row3['driverID_FK']);
                              $drivers .= "\n(2) ".$driver2ID_FK;}

                              if( $row3['lockupf'] || $row3['lockupdockTime'] || $row3['lockupcompleteLoadingTime'] || $row3['lockupdepartureTimeFromPost'] ||
                              $row3['lockuparrivalTimeAtPost'] || $row3['lockupdestinationDockTime'] || $row3['lockupcompleteUnloadTime'] || $row3['lockupdepartTimeFromPost'])
                              { 
                                   $remarks1 = "LOCKUP,".strtoupper($row3['dtmRemarks']);
                              }
                              else
                              { 
                                   $remarks1 = $row3['dtmRemarks'];
                              }

                              $isConsol ="";

                              $costcenter = getCostCenter($row3['costCenterID_FK']);
                              $isTempConsol = $row3['isConsol'];
                              if($isTempConsol == 1)
                              {
                                   $isConsol = "CONSOL";
                              }

                              $vxlDO = "";
                              if($row3['dtmAdhoc'])
                              {
                                   $vxlDO = $row3['dtmAdhoc'];
                              }
                              
                              // if($row3['dtmAdhoc'] == "consol")
                              // {
                              //      $consol = $row3['dtmAdhoc'];
                              // }
                              // else
                              // {
                              //      $consol = "";
                              // }
                              // if($costcenterTemp == "CONSOL")
                              // {
                              //      echo "CONSOL";
                              // }
                              // else
                              // {
                              //      echo "";
                              // }
                         }
                    }
                    else
                    {
                         
                    }

                    $remarks = strtoupper($row2['remarks']);
                    $operatingHour = $row2['operatingHour'];
                    $transportcharge = $row2['transportcharge'];
                    // $faf = $row2['faf'];
                    $faf = ($row2['faf'] / 100) * $row2['transportcharge'];
                    $totalCharges = $transportcharge + $faf;
               }
          }
          else
          {
               
          }

          $sheet->setCellValue('A'.$line, $row['invoiceNo']);
          $sheet->setCellValue('B'.$line, $planner);
          $sheet->setCellValue('C'.$line, $dtmBookingDate);
          $sheet->setCellValue('E'.$line, $dtmPickupDate);
          $sheet->setCellValue('G'.$line, $dtmRequestBy);
          $sheet->setCellValue('D'.$line, $dtmBookingTime);
          $sheet->setCellValue('F'.$line, $dtmPickupTime);
          $sheet->setCellValue('H'.$line, $dtmOriginPointID_FK);
          $sheet->setCellValue('I'.$line, $dtmDestinationPointID_FK);
          $sheet->setCellValue('J'.$line, $remarks1);
          $sheet->setCellValue('M'.$line, $truckID_FK);
          $sheet->setCellValue('N'.$line, $loadCap);
          $sheet->setCellValue('O'.$line, $isConsol);
          $sheet->setCellValue('P'.$line, $drivers);
          $sheet->getStyle('P'.$line)->getAlignment()->setWrapText(true);
          $sheet->setCellValue('Q'.$line, $remarks);
          $sheet->setCellValue('K'.$line, $companyID_FK);
          $sheet->setCellValue('L'.$line, $costcenter);
          $sheet->setCellValue('R'.$line, $operatingHour." Hour");
          $sheet->setCellValue('S'.$line, "RM ".sprintf('%0.2f',$transportcharge));
          $sheet->setCellValue('T'.$line, "RM ".sprintf('%0.2f',$faf));
          $sheet->setCellValue('U'.$line, "RM ".sprintf('%0.2f',$totalCharges));
          $sheet->setCellValue('V'.$line, $vxlDO);
          $sheet->setCellValue('W'.$line, $dtmIDPK);
          $sheet->getStyle('S'.$line)->getNumberFormat()->setFormatCode('0.00'); 
          $sheet->getStyle('T'.$line)->getNumberFormat()->setFormatCode('0.00'); 
          $sheet->getStyle('U'.$line)->getNumberFormat()->setFormatCode('0.00'); 
          $sheet->getStyle('A'.$line.':V'.$line)->getAlignment()->setHorizontal('center');
          $line++;
     }
}
else
{
     
}




$writer = new Xlsx($spreadsheet);
$writer->save('php://output');

?>