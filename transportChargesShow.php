<?php

require 'generalFunction.php';

if(isset($_POST['fromPage']))
{    
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-12 adminAlignThings">
                <?php if($_POST['fromPage'] == 20){?>
                <button class="btn btn-success" name="add">
                <a href="transportChargeView.php">
                        View Transport Charges
                </a>
                </button>
                <?php }?>
                <?php if($_POST['fromPage'] == 21){?>
                <button class="btn btn-warning" name="add" style="margin-right: 2px;
    margin-left: -15px;
    width: 176px;">
                <a href="invoiceView.php">
                        View Invoice
                </a>
                </button>
                <button class="btn btn-success" name="viewIn" style="margin-left: 20px;" onclick="getAllSelectedAsOneInvoice();">Add Invoice No</button>
                <input type="number" placeholder="Invoice No" min="1" class="form-control adminAddSetPadding" id="invoiceNO" required style="margin-left: 20px;width: 121px;">
                <?php }?>
            <p class="paginationClass dsfPagination" style="margin-left: 15px;">
                Page : 
                <select onchange="checkCondition(this.value,null,null,0,<?php echo $_POST['fromPage'];?>);" id="pagination<?php echo $_POST['fromPage']; ?>"></select> 
                of 
            </p>
            <p class="paginationClass paginationClassTotal " id="totalpages<?php echo $_POST['fromPage']; ?>"></p>
            <p class="filterPara dsfFilterPara" style="margin-left: 96px;">Filter By: </p>
            <div class="adminAlignRight">  
                <select class="filterClass" id="filter<?php echo $_POST['fromPage']; ?>" onchange="checkCondition(null,this.value,null,0,<?php echo $_POST['fromPage'];?>);">
                    
                <?php if($_POST['fromPage'] == 20){?>
                    <option selected disabled>Filter By</option>
                    <option value="1">Pickup Date</option>
                    <option value="2">Date Created(Newest)</option>
                    <option value="3">Agent</option>
                    <option value="4">Truck No</option>
                    <option value="5">Cost Center</option>
                    <option value="6">DTM No</option>
                    <option value="7">VXL D/O No</option>
                <?php }?>

                <?php if($_POST['fromPage'] == 21){?>
                    <option selected disabled>Filter By</option>
                    <option value="1">Pickup Date</option>
                    <option value="2">Date Created(Newest)</option>
                    <option value="3">Agent</option>
                    <option value="4">Truck No</option>
                    <option value="5">Cost Center</option>
                    <option value="6">DTM No</option>
                    <option value="7">VXL D/O No</option>
                <?php }?>
                  
                </select>
              
                    <input type="text" id="search<?php echo $_POST['fromPage']; ?>" class="searchClass" placeholder="Search Keyword">
                    <button class="searchClass searchClassButton btn btn-primary " onclick="checkCondition(null,null,this.value,0,<?php echo $_POST['fromPage'];?>);">Search</button>
            
            </div>
        </div>
        <div class="col-xl-12 adminAlignThings">
           
        </div>
    </div>
    </div>
<div style="overflow-x:auto;" id="getTable<?php echo $_POST['fromPage'];?>"></div>
<?php
}
?>