<?php
require 'generalFunction.php';
$conn = connDB();

$fromPage = $_POST['fromPage'];
$condition = $_POST['condition'];
$pageNo = $_POST['pageNo'];
$filter = $_POST['filter'];
$searchWord = $_POST['searchWord'];

// echo $fromPage;
// echo $condition;
// echo $pageNo;
// echo $filter;
// echo " =".$searchWord."=";

if($filter == null)
{
    $filter= 1;
}
if($pageNo == null)
{
    $filter= 1;
}
if($searchWord == null)
{
    $searchWord = "";
}

$sqlPageNo = 0;
$sqlPageNo = ($pageNo - 1) * 10;


if($filter == 1)
{
     $orderBy = "invoice.dateCreated";
}
if($filter == 2)
{
     $orderBy = "invoice.invoiceNo";
}
if($filter == 3)
{
     $orderBy = "invoice.isSelected";
}
if($filter == 4)
{
     $orderBy = "invoice.isSelected";
}
if($filter == 5)
{
     $orderBy = "costcenter.costCenterName";
}
if($filter == 6)
{
     $orderBy = "trucks.truckPlateNo";
}
if($filter == 7)
{
     $orderBy = "company.companyName";
}
if($filter == 8)
{
     $orderBy = "dtmlist.dtmID_PK";
}
if($filter == 9)
{
     $orderBy = "dtmlist.dtmAdhoc";
}

$sql = " SELECT *,invoice.id AS invoiceID_PK,dtmlist.companyID_FK AS companyID_Foreign,dtmlist.truckID_FK AS truckID_Foreign FROM (((((invoice 
          INNER JOIN transportcharge ON invoice.transportID_FK = transportcharge.id)
          INNER JOIN dtmlist ON transportcharge.dtmID_FK = dtmlist.dtmID_PK)
          INNER JOIN company ON dtmlist.companyID_FK = company.companyID_PK)
          INNER JOIN costcenter ON dtmlist.costCenterID_FK = costcenter.costCenterID_PK)
          INNER JOIN trucks ON dtmlist.truckID_FK = trucks.truckID_PK) ";
          
$sql2 = " SELECT COUNT(*) as total2 FROM (((((invoice 
          INNER JOIN transportcharge ON invoice.transportID_FK = transportcharge.id)
          INNER JOIN dtmlist ON transportcharge.dtmID_FK = dtmlist.dtmID_PK)
          INNER JOIN company ON dtmlist.companyID_FK = company.companyID_PK)
          INNER JOIN costcenter ON dtmlist.costCenterID_FK = costcenter.costCenterID_PK)
          INNER JOIN trucks ON dtmlist.truckID_FK = trucks.truckID_PK) ";

if($searchWord != null && $searchWord != "")
{
     if($filter == 1)
     {

     }
     else if($filter == 2)
     {
          $sql .= " WHERE invoice.invoiceNo = '".$searchWord."'  ";
          $sql2 .= " WHERE invoice.invoiceNo = '".$searchWord."'  ";
     }
     else if($filter == 3 || $filter == 4)
     {
          $sql .= " WHERE invoice.isSelected LIKE '%".$searchWord."%'  ";
          $sql2 .= " WHERE invoice.isSelected LIKE '%".$searchWord."%'  ";
     }
     else if($filter == 5)
     {
          $sql .= " WHERE costcenter.costCenterName LIKE '%".$searchWord."%'  ";
          $sql2 .= " WHERE costcenter.costCenterName LIKE '%".$searchWord."%'  ";
     }
     else if($filter == 6)
     {
          $sql .= " WHERE trucks.truckPlateNo LIKE '%".$searchWord."%'  ";
          $sql2 .= " WHERE trucks.truckPlateNo LIKE '%".$searchWord."%'  ";
     }
     else if($filter == 7)
     {
          $sql .= " WHERE company.companyName LIKE '%".$searchWord."%'  ";
          $sql2 .= " WHERE company.companyName LIKE '%".$searchWord."%'  ";
     }
     else if($filter == 8)
     {
          $sql .= " WHERE dtmlist.dtmID_PK = '".$searchWord."'  ";
          $sql2 .= " WHERE dtmlist.dtmID_PK = '".$searchWord."'  ";
     }
     else if($filter == 9)
     {
          $sql .= " WHERE dtmlist.dtmAdhoc = '".$searchWord."'  ";
          $sql2 .= " WHERE dtmlist.dtmAdhoc = '".$searchWord."'  ";
     }
}

if ($orderBy != "") 
{
    if($filter == 1 || $filter == 4)
    {
        $sql .= " ORDER BY ".$orderBy." DESC ";
        $sql2 .= " ORDER BY ".$orderBy." DESC ";
    }
    else
    {
        $sql .= " ORDER BY ".$orderBy." ASC ";
        $sql2 .= " ORDER BY ".$orderBy." ASC ";
    }
}

$sql .=" LIMIT ".$sqlPageNo.",10 ";

if($condition == 1)
{
    if($fromPage == 22)
    {
        $initialSql = " SELECT COUNT(*) as total from invoice ";
    }

    $result = mysqli_query($conn,$initialSql);
    $data = mysqli_fetch_assoc($result);
    $no_of_pages = 0;
    $no_of_pages = ceil($data['total'] / 10);
}
else
{
    $result2 = mysqli_query($conn,$sql2);
    $dataCount = mysqli_fetch_assoc($result2);
    $no_of_pages = 0;
    $no_of_pages = ceil($dataCount['total2'] / 10);
    
}

$querylisting = mysqli_query($conn,$sql);

generateDeleteModal($fromPage);
generateConfirmationDeleteModal($fromPage);

// echo $sql;
?>
<script>$("#pagination"+<?php echo $fromPage;?>+" option").remove();</script>
<table class="table table-sm table-hovered table-striped table-responsive-xl dtmTableNoWrap removebottommargin">
    <thead>
        <tr>
               <th class="adminTableWidthTD" colspan="2">Selection</th>
               <th>Invoice No</th>
               <th>VXL D/O No</th>
               <th>DTM No</th>
               <th>Planner</th>
               <th>Booking Date</th>
               <th>Pickup Date</th>
               <th>Request By</th>
               <th>Booking Time</th>
               <th>Pickup Time</th>
               <th>From</th>
               <th>To</th>
               <th>Agent</th>
               <th>Cost Center</th>
               <th>Truck No</th>
               <th>CAP</th>
               <th>Consol</th>
               <th>Driver Name</th>
               <th>Remark</th>
               <th>Remark 2</th>
               <th>Operating Hour</th>
               <th>Transport/Consol Charge</th>
               <th>FAF %</th>
               <th>Total</th>
        </tr>
  </thead>
  <tbody>
    <?php 
//     echo mysqli_num_rows($querylisting) > 0;
// echo $conn->error;
        if (mysqli_num_rows($querylisting) > 0) 
        {
            while($row = mysqli_fetch_array($querylisting))
            {
                 
    ?>
    <tr>
          <td class="adminTableWidthTD">
          <div class="adminAlignOptionInline">
               <form method ="POST">
                    <?php if($row['isSelected'] == 1)
                    {
                         ?> <input type="checkbox" value="<?php echo $row['invoiceID_PK'];?>"  onchange="handleChange(this);" name="invoiceID" checked><?php
                    }
                    else
                    {
                         ?> <input type="checkbox" value="<?php echo $row['invoiceID_PK'];?>"  onchange="handleChange(this);" name="invoiceID"><?php
                    }
                    ?>
                    
               </form>
          </div>
          </td>
          <td>
               <?php 
               if($row['isSelected'] == 0)
               {
               ?>
               <div class="adminAlignOptionInline">
                    <form action="invoiceEdit.php" method="POST" class="adminformEdit">
                         <button class="btn btn-warning edtOpt" value="<?php echo $row['invoiceID_PK'];?>" name="edit">Update</button>
                    </form>
               </div>
               <?php
               }
               ?>
          </td>
          <td>
               <?php echo $row['invoiceNo'];?>
          </td>
          <td>
               <?php 
               $vxlDO = "";
               if($row['dtmAdhoc'])
               {
                    $vxlDO = $row['dtmAdhoc'];
               }
                    echo $vxlDO;
               ?>
          </td>
          <td>
               <?php echo $row['dtmID_PK'];?>
          </td>
          <td>
               <?php
               $a = "SELECT userNickName FROM user WHERE userID_PK = ".$row['dtmPlannerID_FK'];
               $aa = mysqli_query($conn,$a);
               if (mysqli_num_rows($aa) > 0) 
               {
                    while($urow1 = mysqli_fetch_array($aa))
                    {
                         echo $urow1['userNickName'];
                    }
               }
                    
               ?>
          </td>
          <td>
               <?php 
                    $shipmentDate = date("d M Y",strtotime($row['dtmBookingDate']));
                    echo $shipmentDate;
               ?>
          </td>
          <td>
               <?php 
                    $pickupDate = date("d M Y",strtotime($row['dtmPickupDate']));
                    echo $pickupDate;
               ?>
          </td>
          <td>
               <?php echo $row['dtmRequestBy'];?>
          </td>
          <td>
               <?php 
                    $shipmentTime = date("G:i",strtotime($row['dtmBookingTime']));
                    echo $shipmentTime;
               ?>
          </td>
          <td>
               <?php 
                    $pickupTime = date("G:i",strtotime($row['dtmPickupTime']));
                    echo $pickupTime;
               ?>
          </td>
          <td>
               <?php 
               $bb = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$row['dtmOriginPointID_FK'];
               $b = mysqli_query($conn,$bb);
               if (mysqli_num_rows($b) > 0) 
               {
                    while($urow1 = mysqli_fetch_array($b))
                    {
                         echo $urow1['pointzonePlaceName'];
                    }
               }
               ?>
          </td>
          <td><?php 
               $bb = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$row['dtmDestinationPointID_FK'];
               $b = mysqli_query($conn,$bb);
               if (mysqli_num_rows($b) > 0) 
               {
                    while($urow1 = mysqli_fetch_array($b))
                    {
                         echo $urow1['pointzonePlaceName'];
                    }
               }
               ?>
          </td>
          <td>
               <?php 
                    echo $row['companyName'];
               ?>
          </td>
          <td>
               <?php 
                    echo $row['costCenterName'];
               ?>
          </td>
          <td>
               <?php 
                    echo $row['truckPlateNo'];
               ?>
          </td>
          <td>
          <?php 
               echo $loadCap = $row['loadCap'];
               ?>
          </td>
          <td>
          <?php 
               $isConsol = $row['isConsol'];
               if($isConsol == 1)
               {
                    echo "CONSOL";
               }
               else
               {
                    echo "-";
               }
               ?>
          </td>
          <td>
          <?php 
               $g = "SELECT driverName FROM driver WHERE driverID_PK = ".$row['driverID_FK'];
               $gg = mysqli_query($conn,$g);
               if (mysqli_num_rows($gg) > 0) 
               {
                    while($urow1 = mysqli_fetch_array($gg))
                    {
                         echo "1.".$urow1['driverName'];
                    }
               }

               if($row['driver2ID_FK'])
               {
                         $ee = "SELECT driverName FROM driver WHERE driverID_PK = ".$row['driver2ID_FK'];
                         $e = mysqli_query($conn,$ee);
                         if (mysqli_num_rows($e) > 0) 
                         {
                              while($urow1 = mysqli_fetch_array($e))
                              {
                                   echo "<br>2.".$urow1['driverName'];
                              }
                         }
               }
               ?>
          </td>
          <td style="text-align:left;">
          <?php 
               if( $row['lockupf'] || $row['lockupdockTime'] || $row['lockupcompleteLoadingTime'] || $row['lockupdepartureTimeFromPost'] ||
               $row['lockuparrivalTimeAtPost'] || $row['lockupdestinationDockTime'] || $row['lockupcompleteUnloadTime'] || $row['lockupdepartTimeFromPost'])
               {
               echo "->Next Day<br>->".strtoupper($row['dtmRemarks']);
               }
               else
               {
               echo "->".strtoupper($row['dtmRemarks']);
               }?></td>
               <td><?php echo strtoupper($row['remarks']);?>
          </td>
          <td class="text-center"><?php echo $row['operatingHour'];?></td>
          <td class="text-center"><?php echo "RM ".sprintf('%0.2f',$row['transportcharge']);?></td>
          <td class="text-center"><?php
               $fafPercentageAfter = ($row['faf'] / 100) * $row['transportcharge'];
               echo "(".sprintf('%0.2f',$row['faf'])."%)<br>RM". sprintf('%0.2f',$fafPercentageAfter);
               // echo sprintf('%0.2f',$fafPercentageAfter);
               ?>
          </td>
          <td class="text-center"><?php 
          echo "RM ".sprintf('%0.2f',$row['transportcharge'] + $fafPercentageAfter);
     ?>
          </td>
        </tr>
    <?php 
          }
        }
        else
        {
            ?>
            <tr>
                <td colspan="22" style="text-align:center;">No Records Found</td>
            </tr>
            <?php
        }
    ?>
  </tbody>
</table>
<?php
    if($condition == 1)
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$data['total']);
    }
    else
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$dataCount['total2']);
    }
?>
 