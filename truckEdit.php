<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        if(isset($_POST['edit']))
        {   
            $edit = $_POST['edit'];
            $tableType = $_POST['tableType'];

            require 'generalFunction.php';
            $conn = connDB();
?>
<!doctype html>
<html lang="en">
    <head>
        <title>HR Edit Truck</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>
                            <?php 
                            if($tableType == 7)
                            {
                                echo "HR Edit Trucks";
                            }
                            ?>
                        </h3>
                    </div>
                    <?php   
                        generateConfirmationModal();
                        generateSimpleModal();
                    ?>
                    <div class="row adminAddMarginTop">
                        <div class="col-xl-12 row">
                        <?php 
                            if($tableType == 7)
                            {
                                $sql = "SELECT * FROM trucks WHERE truckID_PK = ".$edit;
                                $query = mysqli_query($conn,$sql);
                                if (mysqli_num_rows($query) > 0) 
                                {
                                    while($urow = mysqli_fetch_array($query))
                                    {
                                        $cbDate = date("Ymd",strtotime($urow['truckCustomExpired']));
                                ?>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                        <label for="field_1" >Plate No</label>
                                        <input  type="text" class="form-control adminAddSetPadding" placeholder="Plate No" id="field_1" value="<?php echo $urow['truckPlateNo'];?>">
                                </div>
                                <div class="form-group col-xl-5">
                                        <label for="field_2" >Capacity</label>
                                        <select class="form-control adminAddSetPadding" id="field_2">
                                            <?php displayCapacity($urow["truckCapacity"]);?>
                                        </select>
                                </div>
                                <div class="col-xl-1"></div>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                        <label for="field_3" >Made</label>
                                        <input  type="text" class="form-control adminAddSetPadding" placeholder="Hino" id="field_3" value="<?php echo $urow['truckMade'];?>">
                                </div>
                                <div class="form-group col-xl-5">
                                        <label for="field_4" >Model</label>
                                        <input  type="text" class="form-control adminAddSetPadding" placeholder="XXXXXX" id="field_4" value="<?php echo $urow['truckModel'];?>">
                               
                                </div>
                                <div class="col-xl-1"></div>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                        <label for="field_5" >Custom Bond</label>
                                        <input  type="text" class="form-control adminAddSetPadding" placeholder="XXXXXXXXXXXX" id="field_5" value="<?php echo $urow['truckCustomBond'];?>">
                                
                                </div>
                                <div class="form-group col-xl-5">
                                        <label for="field_6" >Expired Custom Bond Date</label>
                                        <input  type="text" class="form-control adminAddSetPadding" placeholder="YYYYMMDD" id="field_6" value="<?php echo $cbDate;?>">
                                </div>
                                <div class="col-xl-1"></div>
                                <div class="col-xl-3"></div>
                                <div class="col-xl-6 adminAddUserButton">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addHRData(<?php echo $tableType;?>,2);" value="<?php echo $edit;?>" id="idpk">Edit Truck Record</button>
                                </div>
                                <div class="col-xl-3"></div>
                                <?php
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
        <script>
        $(document).ready(function()
        {
            var picker = new Pikaday({
                    field: document.getElementById('field_6'),
                    format: 'YYYYMMDD',
                    onSelect: function() {
                        console.log(this.getMoment().format('Do MMMM YYYY'));
                    }
                });
        });
        </script>
    </body>
</html>
<?php
        }
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>