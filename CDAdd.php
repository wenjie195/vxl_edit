<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        if(isset($_POST['add']))
        {   
            $add = $_POST['add'];
            require 'generalFunction.php';

?>
<!doctype html>
<html lang="en">
    <head>
        <title>Admin Add Field</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>
                            <?php 
                            if($add == 1)
                            {
                                echo "Admin Add Places";
                            }
                            if($add == 2)
                            {
                                echo "Admin Add Zones";
                            }  
                            if($add == 3)
                            {
                                echo "Admin Add Cost Centers";
                            }
                            if($add == 4)
                            {
                                echo "Admin Add Agent";
                            }
                            if($add == 5)
                            {
                                echo "Admin Add User";
                            }
                            if($add == 30)
                            {
                                echo "Transport Charges Add Rates";
                            }
                            if($add == 31)
                            {
                                echo "Driver Service Fee Place Rates";
                            }
                            if($add == 41)
                            {
                                echo "Driver Additional Service Fee Place Rates";
                            }
                            ?>
                        </h3>
                    </div>
                    <?php   
                        generateConfirmationModal();
                        generateSimpleModal();
                    ?>
                    <div class="row adminAddMarginTop">
                        <div class="col-xl-12 row">
                        <?php 
                            if($add == 1)
                            {
                                ?>
                                <div class="col"></div>
                                <div class="form-group col-xl-10">
                                    <label for="field_1">Place Name</label>
                                    <input type="text" class="form-control adminAddFormControl" id="field_1">
                                </div>
                                <div class="col"></div>
                                <div class="indexSubmitButtonDiv col-xl-12">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addAdminData(<?php echo $add;?>,1);">Add Place</button>
                                </div>
                                <?php
                            }
                            if($add == 2)
                            {
                                ?>
                                <div class="col"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_1">Zone Name</label>
                                    <input type="text" class="form-control adminAddFormControl" id="field_1" >
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="field_2">Zone State</label>
                                    <input type="text" class="form-control adminAddFormControl" id="field_2" >
                                </div>
                                <div class="col"></div>
                                <div class="indexSubmitButtonDiv col-xl-12">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addAdminData(<?php echo $add;?>,1);">Add Zone</button>
                                </div>
                                <?php
                            }  
                            if($add == 3)
                            {
                                ?>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_1">Cost Center Name</label>
                                    <input type="text" class="form-control adminAddSetPadding" id="field_1" >
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="field_2">Select Agent</label>
                                    <select class="form-control adminAddSetPadding" id="field_2">
                                        <option disabled selected hidden>-- Pick one agent --</option>
                                        <?php
                                        $conn = connDB();
                                        $sql_select_costCenter = "SELECT * FROM company WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                echo '<option value="'.$row["companyID_PK"].'">'.$row["companyName"].'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xl-1"></div>
                                <div class="indexSubmitButtonDiv col-xl-12">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addAdminData(<?php echo $add;?>,1);">Add Cost Center</button>
                                </div>
                                <?php
                            }
                            if($add == 4)
                            {
                                ?>
                                <div class="col"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_1">Agent Name</label>
                                    <input type="text" class="form-control adminAddFormControl" id="field_1" >
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="field_2">Agent Short Form</label>
                                    <input type="text" class="form-control adminAddFormControl" id="field_2" >
                                </div>
                                <div class="col"></div>
                                <div class="indexSubmitButtonDiv col-xl-12">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addAdminData(<?php echo $add;?>,1);">Add Agent</button>
                                </div>
                                <?php
                            }
                            if($add == 5)
                            {
                                ?>
                                <div class="col-xl-1"></div>
                                <div class="col-xl-5 form-group">
                                    <label for="field_1">Name</label>
                                    <input type="text" class="form-control adminAddFormControl" id="field_1" >
                                </div>
                                <div class="col-xl-5 form-group">
                                    <label for="field_2" >Nickname</label>
                                    <input type="text" class="form-control adminAddFormControl" id="field_2" >
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="col-xl-5">
                                    <div class="form-group">
                                        <label for="field_3" >Position</label>
                                        <select class="form-control adminAddSetPadding" id="field_3">
                                            <option disabled selected hidden>-- Pick one position --</option>
                                            <option value="1">Admin</option>
                                            <option value="2">Staff Operational</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-5">
                                    <div class="form-group">
                                        <label for="field_4" >IC No</label>
                                        <input type="text" class="form-control adminAddSetPadding"  id="field_4" >
                                    </div>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="col-xl-5">
                                    <div class="form-group">
                                        <label for="field_5" >Telephone No</label>
                                        <input type="text" class="form-control adminAddSetPadding"  id="field_5">
                                    </div>
                                </div>
                                <div class="col-xl-5">
                                    <div class="form-group">
                                        <label for="field_6" >Email</label>
                                        <input type="email" class="form-control adminAddSetPadding" id="field_6" >
                                    </div>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="col-xl-5">
                                    <div class="form-group">
                                        <label for="field_7" >Address</label>
                                        <input  type="text" class="form-control adminAddSetPadding"  id="field_7" >
                                    </div>
                                </div>
                                <div class="col-xl-5">
                                    <div class="form-group">
                                        <label for="field_8" >State</label>
                                        <select class="form-control adminAddSetPadding" id="field_8">
                                            <option disabled selected hidden>-- Pick one state --</option>
                                            <option value="Kuala Lumpur">WP Kuala Lumpur</option>
                                            <option value="Labuan">WP Labuan</option>
                                            <option value="Putrajaya">WP Putrajaya</option>
                                            <option value="Perlis">Perlis</option>
                                            <option value="Kedah">Kedah</option>
                                            <option value="Terengganu">Terengganu</option>
                                            <option value="Kelantan">Kelantan</option>
                                            <option value="Pulau Pinang">Pulau Pinang</option>
                                            <option value="Perak">Perak</option>
                                            <option value="Pahang">Pahang</option>
                                            <option value="Selangor">Selangor</option>
                                            <option value="Negeri Sembilan">Negeri Sembilan</option>
                                            <option value="Melaka">Melaka</option>
                                            <option value="Johor">Johor</option>
                                            <option value="Sabah">Sabah</option>
                                            <option value="Sarawak">Sarawak</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-3"></div>
                                <div class="col-xl-6 adminAddUserButton">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addAdminData(<?php echo $add;?>,1);">Create New User</button>
                                </div>
                                <div class="col-xl-3"></div>
                                <?php
                            }
                            if($add == 30)
                            {
                                ?>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_1">Select Agent</label>
                                    <select class="form-control adminAddSetPadding" id="field_1">
                                        <option disabled selected hidden>-- Pick one company --</option>
                                        <?php
                                        $conn = connDB();
                                        $sql_select_costCenter = "SELECT * FROM company WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                echo '<option value="'.$row["companyID_PK"].'">'.$row["companyName"].'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for='field_2' >Load Capacity</label>
                                    <select class='form-control adminAddSetPadding' id='field_2'>
                                        <option disabled selected hidden>-- Pick one capacity --</option>
                                        <option value='20 Ft / 10 Ton'>20 Ft / 10 Ton</option>
                                        <option value='1 Tonner'>1 Tonner</option>
                                        <option value='3 Ton'> 3 Ton</option>
                                        <option value='40 Ft / Q6'> 40 Ft / Q6</option>
                                        <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
                                        <option value='45 Ft / Q7'>45 Ft / Q7</option>
                                    </select>
                                </div>
                                <div class="col-xl-1"></div>
                                
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_3">Transport Rates</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_3" min="0">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="field_4">Consol Rates</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_4" min="0">
                                </div>
                                <div class="col-xl-1"></div>
                                 <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_5" >Origin Zone</label>
                                    <select class="form-control adminAddSetPadding" id="field_5">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php

                                        $sql_select_costCenter = "SELECT * FROM zones WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                echo '<option value="'.$row["zonesID_PK"].'">'.$row["zonesName"].','.$row["zonesState"].'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-xl-5" >
                                    <label for="field_6" >Destination Zone</label>
                                    <select class="form-control adminAddSetPadding" id="field_6" >
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php

                                        $sql_select_costCenter = "SELECT * FROM zones WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                echo '<option value="'.$row["zonesID_PK"].'">'.$row["zonesName"].','.$row["zonesState"].'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="indexSubmitButtonDiv col-xl-12">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addTransportRates(<?php echo $add;?>,1);">Add Transport Rates</button>
                                </div>
                                <?php
                            }
                            if($add == 31)
                            {
                                $conn = connDB();
                                ?>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_1" >Origin Zone</label>
                                    <select class="form-control adminAddSetPadding" id="field_1">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php

                                        $sql_select_costCenter = "SELECT * FROM zones WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                echo '<option value="'.$row["zonesID_PK"].'">'.$row["zonesName"].','.$row["zonesState"].'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-xl-5" >
                                    <label for="field_2" >Destination Zone</label>
                                    <select class="form-control adminAddSetPadding" id="field_2" >
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php

                                        $sql_select_costCenter = "SELECT * FROM zones WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                echo '<option value="'.$row["zonesID_PK"].'">'.$row["zonesName"].','.$row["zonesState"].'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_3">Service Fee Rates</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_3" min="0">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for='field_4' >Load Capacity</label>
                                    <select class='form-control adminAddSetPadding' id='field_4'>
                                        <option disabled selected hidden>-- Pick one capacity --</option>
                                        <option value='20 Ft / 10 Ton'>20 Ft / 10 Ton</option>
                                        <option value='1 Tonner'>1 Tonner</option>
                                        <option value='3 Ton'> 3 Ton</option>
                                        <option value='40 Ft / Q6'> 40 Ft / Q6</option>
                                        <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
                                        <option value='45 Ft / Q7'>45 Ft / Q7</option>
                                    </select>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for='field_5' >No Of Drivers</label>
                                    <select class='form-control adminAddSetPadding' id='field_5'>
                                        <option disabled selected hidden>-- Pick one --</option>
                                        <option value='1'>1 Driver</option>
                                        <option value='2'>2 Driver</option>
                                    </select>
                                </div>
                                <div class="form-group col-xl-6"></div>

                                <div class="indexSubmitButtonDiv col-xl-12">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addAdminData(<?php echo $add;?>,1);">Add Service Fee</button>
                                </div>
                                <?php
                            }
                            if($add == 41)
                            {
                                ?>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                <?php
                                $conn = connDB();

                                $sql_select_costCenter = "SELECT loadTrans FROM additionalservicefee";
                                $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                if (mysqli_num_rows($result_select_costCenter) > 0)
                                {
                                    echo" 
                                    <label for='field_1' >Load Capacity</label>
                                    <select class='form-control adminAddSetPadding' id='field_1'>
                                        <option disabled selected hidden>-- Pick one capacity --</option>
                                        <option value='20 Ft / 10 Ton'>20 Ft / 10 Ton</option>
                                        <option value='1 Tonner'>1 Tonner</option>
                                        <option value='3 Ton'> 3 Ton</option>
                                        <option value='40 Ft / Q6'> 40 Ft / Q6</option>
                                        <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
                                        <option value='45 Ft / Q7'>45 Ft / Q7</option>
                                    </select>
                                    <script>
                                    var selectobject = document.getElementById('field_1');
                                
                                    ";
                                    while($row = mysqli_fetch_assoc($result_select_costCenter))
                                    {
                                        echo "    
                                        for (var i=0; i < selectobject.length; i++)
                                        {
                                            if (selectobject.options[i].value == '' )
                                            {
                                               
                                            }
                                        ";
                                        if($row['loadTrans'] == '20 Ft / 10 Ton')
                                        {
                                            echo "else if (selectobject.options[i].value == '20 Ft / 10 Ton' )
                                                    {selectobject.remove(i);}";
                                        }
                                        else if($row['loadTrans'] == '1 Tonner')
                                        {
                                            echo "else if (selectobject.options[i].value == '1 Tonner' )
                                                    {selectobject.remove(i);}";
                                        }
                                        else if($row['loadTrans'] == '3 Ton')
                                        {
                                            echo "else if (selectobject.options[i].value == '3 Ton' )
                                                    {selectobject.remove(i);}";
                                        }
                                        else if($row['loadTrans'] == '40 Ft / Q6')
                                        {
                                            echo "else if (selectobject.options[i].value == '40 Ft / Q6' )
                                                    {selectobject.remove(i);}";
                                        }
                                        else if($row['loadTrans'] == '40 Ft / Q6 Air-Ride')
                                        {
                                            echo "else if (selectobject.options[i].value == '40 Ft / Q6 Air-Ride' )
                                                    {selectobject.remove(i);}";
                                        }
                                        else if($row['loadTrans'] == '45 Ft / Q7')
                                        {
                                            echo "else if (selectobject.options[i].value == '45 Ft / Q7' )
                                                    {selectobject.remove(i);}";
                                        }
                                        echo" }";   
                                    }
                                    echo"</script>";
                                }
                                else
                                {
                                    echo" 
                                    <label for='field_1' >Load Capacity</label>
                                    <select class='form-control adminAddSetPadding' id='field_1'>
                                        <option disabled selected hidden>-- Pick one capacity --</option>
                                        <option value='20 Ft / 10 Ton'>20 Ft / 10 Ton</option>
                                        <option value='1 Tonner'>1 Tonner</option>
                                        <option value='3 Ton'> 3 Ton</option>
                                        <option value='40 Ft / Q6'> 40 Ft / Q6</option>
                                        <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
                                        <option value='45 Ft / Q7'>45 Ft / Q7</option>
                                    </select>";
                                }
                                ?>
                                </div>
                                <div class="form-group col-xl-5" >
                                    <label for="field_2">Overnight Rates</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_2" min="0">
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_3">Night Deliveries Rates</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_3" min="0">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="field_4">Sunday Rates</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_4" min="0">
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_5">Public Holiday Rates</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_5" min="0">
                                </div>

                                <div class="form-group col-xl-5">
                                    <label for="field_6">Cancellation Charges</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_6" min="0">
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="field_7">Lockup (Day)</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_7" min="0">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="field_8">Lockup (Night)</label>
                                    <input type="number" class="form-control adminAddSetPadding" id="field_8" min="0">
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="indexSubmitButtonDiv col-xl-12">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addAdminData(<?php echo $add;?>,1);">Add Additional Service Fee</button>
                                </div>
                                <?php
                            }
                            
                            ?>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
    </body>
</html>
<?php
        }
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>