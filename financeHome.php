<?php
//Start the session
session_start();

//Check f the session is empty/exist or not
if(!empty($_SESSION))
{
    require 'generalFunction.php';

    ?>
    <!doctype html>
    <html lang="en">
    <head>
        <title>HR Home</title>
        <?php require 'indexHeader.php';?>
    </head>
    <body>
    <?php require 'indexNavbar.php';?>
    <div class="container-fluid">
        <div class="row">
            <?php require 'indexSidebar.php';?>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h3>Finance Dashboard</h3>
                </div>
                <div class="row">
                    <div class="col adminAlignCenter">
                        <a href="transportChargesHome.php" class="adminAlignCenterGrid">
                            <img src="./img/transportCharges.png" width="135px" height="121px">
                            <p class="adminLinkRef">Transport Charges</p>
                        </a>
                    </div>
                    <div class="col adminAlignCenter">
                        <a href="invoiceView.php" class="adminAlignCenterGrid">
                            <img src="./img/transportCharges.png" width="135px" height="121px">
                            <p class="adminLinkRef">View Invoice</p>
                        </a>
                    </div>
                    <div class="col adminAlignCenter">
                        <a href="feeHome.php" class="adminAlignCenterGrid">
                            <img src="./img/transportCharges.png" width="135px" height="121px">
                            <p class="adminLinkRef">Driver Service Fee</p>
                        </a>
                    </div>
                    <div class="col adminAlignCenter">
                        <a href="salesReportByCustomerHome.php" class="adminAlignCenterGrid">
                            <img src="./img/charges.png" width="135px" height="121px" style="margin-top:10px;">
                            <p class="adminLinkRef">Sales Report By Customer</p>
                        </a>
                    </div>
                    <div class="col adminAlignCenter">
                        <a href="salesRevenueBytripHome.php" class="adminAlignCenterGrid">
                            <img src="./img/invoice.png" width="135px" height="121px" style="margin-top:10px;">
                            <p class="adminLinkRef">Sales Revenue By Trip</p>
                        </a>
                    </div>
                </div>
            </main>
        </div>
    </div>
    <?php require 'indexFooter.php';?>
    </body>
    </html>
    <?php
}
else
{
    // Go back to index page
    // NOTE : MUST PROMPT ERROR
    header('Location:index.php');
}
?>