<?php

require 'generalFunction.php';

if(isset($_POST['fromPage']))
{    
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-12 adminAlignThings">
            <!-- <form action="dtmAdd.php" method="POST">
                <button class="btn btn-success" value="<?php //echo $_POST['fromPage'];?>" name="add">
                    Add New Driver Service Fee
                </button>
            </form> -->
            <p class="paginationClass dsfPagination">
                Page : 
                <select onchange="checkCondition(this.value,null,null,0,<?php echo $_POST['fromPage'];?>);" id="pagination<?php echo $_POST['fromPage']; ?>"></select> 
                of 
            </p>
            <p class="paginationClass paginationClassTotal " id="totalpages<?php echo $_POST['fromPage']; ?>"></p>
            <p class="filterPara dsfFilterPara">Filter By: </p>
            <div class="adminAlignRight">  
                <select class="filterClass" id="filter<?php echo $_POST['fromPage']; ?>" onchange="checkCondition(null,this.value,null,0,<?php echo $_POST['fromPage'];?>);">
                    <option selected disabled>Filter By</option>
                    <option value="1">Date Created</option>
                </select>
                <input type="text" id="search<?php echo $_POST['fromPage']; ?>" class="searchClass" placeholder="Search Keyword">
                <button class="searchClass searchClassButton btn btn-primary " onclick="checkCondition(null,null,this.value,0,<?php echo $_POST['fromPage'];?>);">Search</button>
            </div>
        </div>
    </div>
    </div>
<div style="overflow-x:auto;" id="getTable<?php echo $_POST['fromPage'];?>"></div>
<?php
}
?>