<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        if(isset($_POST['edit']))
        {   
            $edit = $_POST['edit'];

            require 'generalFunction.php';
            $conn = connDB();
?>
<!doctype html>
<html lang="en">
    <head>
        <title>Update Invoice</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>
                            Edit Transport Charges and Invoice
                        </h3>
                    </div>
                    <?php   
                        generateConfirmationModal();
                        generateSimpleModal();
                    ?>
                    <div class="row adminAddMarginTop">
                        <div class="col-xl-12 row">
                        <?php 
                           
                                $sql = " SELECT *,invoice.id AS invoiceID_PK,dtmlist.companyID_FK AS companyID_Foreign,dtmlist.truckID_FK AS truckID_Foreign FROM (((((invoice 
                                INNER JOIN transportcharge ON invoice.transportID_FK = transportcharge.id)
                                INNER JOIN dtmlist ON transportcharge.dtmID_FK = dtmlist.dtmID_PK)
                                INNER JOIN company ON transportcharge.companyID_FK = company.companyID_PK)
                                INNER JOIN costcenter ON dtmlist.costCenterID_FK = costcenter.costCenterID_PK)
                                INNER JOIN trucks ON transportcharge.truckID_FK = trucks.truckID_PK)
                                WHERE invoice.id = ".$edit;
                                $query = mysqli_query($conn,$sql);
                                if (mysqli_num_rows($query) > 0) 
                                {
                                    while($urow = mysqli_fetch_array($query))
                                    {
                                        $isConsol = $urow['isConsol'];
                                ?>
                                  <table class="table dtmTableNoWrap table-sm table-hovered table-bordered table-striped table-responsive-xl  dtmTableNoWrap"style="text-align:center;">
                                        <thead>
                                             <tr>
                                                  <th>Planner</th>
                                                  <th>DTM NO</th>
                                                  <th>VXL D/O No</th>
                                                  <th>Pickup Date</th>
                                                  <th>Pickup Time</th>
                                                  <th>Request By</th>
                                                  <th>From</th>
                                                  <th>To</th>
                                             </tr>
                                        </thead>
                                        <tbody>
                                             <tr>
                                                  <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT userNickName FROM user WHERE userID_PK = ".$urow['dtmPlannerID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['userNickName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php echo $urow['dtmID_PK'];?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                       $vxlDO = "";
                                                       if($urow['dtmAdhoc'])
                                                       {
                                                       $vxlDO = $urow['dtmAdhoc'];
                                                       }
                                                       echo $vxlDO;
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                            $pickupDate = date("d M Y",strtotime($urow['dtmPickupDate']));
                                                            echo $pickupDate;
                                                       ?>
                                                  </td>
                                                  
                                                  <td>
                                                       <?php 
                                                            $pickupTime = date("G:i",strtotime($urow['dtmPickupTime']));
                                                            echo $pickupTime;
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php echo $urow['dtmRequestBy'];?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$urow['dtmOriginPointID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['pointzonePlaceName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$urow['dtmDestinationPointID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['pointzonePlaceName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                             </tr>
                                        </tbody>
                                   </table>
                                   <table class="table table-sm table-hovered table-bordered table-striped table-responsive-xl  dtmTableNoWrap"style="text-align:center;">
                                        <thead>
                                             <tr>
                                                  <th>Agent</th>
                                                  <th>Cost Centre</th>
                                                  <th>Truck No</th>
                                                  <th>CAP</th>
                                                  <th>Consol</th>
                                                  <th>Driver Name</th>
                                                  <th>Remark</th>
                                                  
                                             </tr>
                                        </thead>
                                        <tbody>
                                             <tr>
                                             <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT companyName FROM company WHERE companyID_PK = ".$urow['companyID_Foreign'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['companyName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td><?php 
                                                            
                                                            $ee = "SELECT costCenterName FROM costcenter WHERE costCenterID_PK = ".$urow['costCenterID_FK'];
                                                            $e = mysqli_query($conn,$ee);
                                                            if (mysqli_num_rows($e) > 0) 
                                                            {
                                                                 while($urow1 = mysqli_fetch_array($e))
                                                                 {
                                                                      echo $adhoc  = $urow1['costCenterName'];
                                                                 }
                                                            }
                                                           ?></td>
                                                  <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT truckPlateNo FROM trucks WHERE truckID_PK = ".$urow['truckID_Foreign'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['truckPlateNo'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  
                                                  <td>
                                                       <?php 
                                                       echo $loadCap = $urow['loadCap'];
                                                       ?>
                                                  </td>
                                                  <td>
                                                  <?php
                                                  $isConsol = $urow['isConsol'];
                                                  if($isConsol == 1)
                                                  {
                                                       echo "CONSOL";
                                                  }
                                                  else
                                                  {
                                                       echo "-";
                                                  }
                                                  ?>
                                                  </td>
                                                  <td class="text-left">
                                                       <?php 
                                                       $costCenterDisplay = "SELECT driverName FROM driver WHERE driverID_PK = ".$urow['driverID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo "1.".$urow1['driverName'];
                                                            }
                                                       }

                                                       if($urow['driver2ID_FK'])
                                                       {
                                                                 $costCenterDisplay = "SELECT driverName FROM driver WHERE driverID_PK = ".$urow['driver2ID_FK'];
                                                                 $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                                 if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                                 {
                                                                      while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                                      {
                                                                           echo "<br>2.".$urow1['driverName'];
                                                                      }
                                                                 }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td style="text-align:left;"><?php 
                                                  if( $urow['lockupf'] || $urow['lockupdockTime'] || $urow['lockupcompleteLoadingTime'] || $urow['lockupdepartureTimeFromPost'] ||
                                                  $urow['lockuparrivalTimeAtPost'] || $urow['lockupdestinationDockTime'] || $urow['lockupcompleteUnloadTime'] || $urow['lockupdepartTimeFromPost'])
                                                  {
                                                  echo "->Lockup/Detention<br>->".strtoupper($urow['dtmRemarks']);
                                                  }
                                                  else
                                                  {
                                                  echo "->".strtoupper($urow['dtmRemarks']);
                                                  }?></td>
                                                 
                                             </tr>
                                        </tbody>
                                   </table>

                                   <div class="col-xl-1"></div>
                                   <div class="form-group col-xl-5">
                                        <label for="field_1" >Remarks For Transport Charges</label>
                                        <input  type="text" class="form-control adminAddSetPadding" id="field_1" value="<?php echo $urow['remarks'];?>">
                                   </div>
                                   <div class="form-group col-xl-5">
                                        <label for="field_2" >Operating Hours</label>
                                        <input  type="number" class="form-control adminAddSetPadding" id="field_2" min="1" step="1" value="<?php echo $urow['operatingHour'];?>">
                                   </div>
                                   <div class="col-xl-1"></div>
                                   <div class="col-xl-1"></div>
                                   <div class="form-group col-xl-5">
                                   <?php 
                                        if($isConsol == 1)
                                        {
                                             ?><label for="field_3" >Consol Charges</label><?php 
                                        }
                                        else
                                        {
                                             ?><label for="field_3" >Transport Charges</label><?php 
                                        }
                                   ?>
                                        <input  type="number" class="form-control adminAddSetPadding" id="field_3" min="0"  step="0.01" value="<?php echo sprintf('%0.2f',$urow['transportcharge']);?>" >
                                   </div>
                                   <div class="form-group col-xl-5">
                                        <label for="field_4" >FAF (%)</label>
                                        <input  type="number" class="form-control adminAddSetPadding" id="field_4" step="0.01" value="<?php echo sprintf('%0.2f',$urow['faf']);?>">
                                   </div>
                                   <div class="col-xl-1"></div>

                                   <div class="col-xl-1"></div>
                                   <div class="form-group col-xl-5">
                                        <label for="field_5" >Invoice No</label>
                                        <input  type="number" class="form-control adminAddSetPadding" id="field_5" min="1" step="1" value="<?php echo $urow['invoiceNo'];?>">
                                   </div>
                                   <div class="col-xl-6"></div>

                                   <div class="col-xl-3"></div>
                                   <div class="col-xl-6 adminAddUserButton">
                                        <button class="btn formButtonPrimary indexSubmitButton"  name="invoiceID_PK" id="invoiceID_PK" onclick="updateInvoicing(this.value);" value="<?php echo $urow['invoiceID_PK'];?>">Edit Invoice</button>
                                   </div>
                                   <div class="col-xl-3"></div>
                                <?php
                                    }
                                }
                            
                            ?>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
        <script>

        </script>
    </body>
</html>
<?php
        }
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>