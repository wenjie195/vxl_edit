<?php
//Start the session
session_start();

//Check f the session is empty/exist or not
if(!empty($_SESSION))
{
	if ($_SERVER["REQUEST_METHOD"] == "POST") 
	{
        require 'generalFunction.php';
        $conn = connDB();

        $fromPage = $_POST['fromPage'];
        if(isset($_POST['addOrEdit']))
        {
            $addOrEdit = $_POST['addOrEdit'];
        }

        if($fromPage == 9 && $addOrEdit != 3)
        {
            $dsfDONumber = $_POST['dsfDONumber'];
            $dtmID_FK = $_POST['dtmID_FK'];
            $night = $_POST['night'];
            $public = $_POST['public'];
            $load = $_POST['load'];
            $dc = $_POST['dc'];

            $total_charges = 0;

            $sqlgetInfo = " SELECT * FROM (((((((dtmlist
                            INNER JOIN company ON dtmlist.companyID_FK = company.companyID_PK)
                            INNER JOIN driver ON dtmlist.driverID_FK = driver.driverID_PK)
                            INNER JOIN pointzone ON dtmlist.dtmDestinationPointID_FK = pointzone.pointzoneID_PK)
                            INNER JOIN trucks ON dtmlist.truckID_FK = trucks.truckID_PK)
                            INNER JOIN user ON dtmlist.dtmPlannerID_FK = user.userID_PK)
                            INNER JOIN zones ON dtmlist.dtmDestinationZoneID_FK = zones.zonesID_PK)
                            INNER JOIN costcenter ON dtmlist.costCenterID_FK = costcenter.costCenterID_PK) 
                            WHERE dtmID_PK = ".$dtmID_FK;

                            $result = mysqli_query($conn,$sqlgetInfo);
                            if (mysqli_num_rows($result) > 0) 
                            {
                                while($row = mysqli_fetch_array($result))
                                {

                                    $costCenterDisplay = "SELECT * FROM zones WHERE zonesID_PK = ".$row['dtmOriginZoneID_FK'];;
                                    $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                    if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                    {
                                        while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                        {
                                            $origin = $urow1['zonesName']."<br>";
                                        }
                                    }

                                    $dayOfDelivery = date("D",strtotime($row['dtmPickupDate']));

                                    if($row['driver2ID_FK'] && $row['driver2ID_FK'] !=0 && $row['driver2ID_FK']!="")
                                    {
                                        $secondDriver = $row['driver2ID_FK'];
                                    }

                                    if($row['truckCapacity'] == "3 Ton")
                                    {
                                        //Trucking
                                        if($row['zonesName'] == "Penang")
                                        {
                                            $total_charges += 10;
                                        }
                                        else if($row['zonesName'] == "Prai" || $row['zonesName'] == "Batu Kawan")
                                        {
                                            $total_charges += 17;
                                        }
                                        else if($row['zonesName'] == "Kulim" || $row['zonesName'] == "Sungai Petani")
                                        {
                                            $total_charges += 22;
                                        }
                                        else if($row['zonesName'] == "Ipoh")
                                        {
                                            $total_charges += 70;
                                        }

                                        //Detention Charges
                                        if ($dc != 0)
                                        {
                                            $total_charges += ($dc * 5);
                                        }
                                        //Night Charges
                                        if ($night != "no")
                                        {
                                            $total_charges += 10;
                                        }
                                        //Public Charges
                                        if ($public == "yes" && $dayOfDelivery = "Sun")
                                        {
                                            $total_charges += 45;
                                        }
                                        //rest day Charges
                                        if ($public == "no" && $dayOfDelivery = "Sun")
                                        {
                                            $total_charges += 30;
                                        }

                                    }
                                    else if($row['truckCapacity'] == "20 Ft / 10 Ton")
                                    {
                                        
                                        if($origin && $origin == "Penang")
                                        {
                                            //Trucking
                                            if($row['zonesName'] == "Penang")
                                            {
                                                $total_charges += 15;
                                            }
                                            else if($row['zonesName'] == "Prai" || $row['zonesName'] == "Batu Kawan")
                                            {
                                                $total_charges += 25;
                                            }
                                            else if($row['zonesName'] == "Kulim" || $row['zonesName'] == "Sungai Petani")
                                            {
                                                $total_charges += 28;
                                            }
                                            else if($row['zonesName'] == "Ipoh")
                                            {
                                                $total_charges += 90;
                                            }
                                            else if($row['zonesName'] == "KLIA")
                                            {
                                                if($row['driver2ID_FK'] && $row['driver2ID_FK'] !=0 && $row['driver2ID_FK']!="")
                                                {
                                                    $total_charges += 100;
                                                }
                                                else
                                                {
                                                    $total_charges += 120;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if($row['zonesName'] == "Klang" || $row['zonesName'] == "Kua;a Lumpur" || $row['zonesName'] == "Sepang" || $row['zonesName'] == "Shah Alam")
                                            {
                                                $total_charges += 50;
                                            }
                                            else if($row['zonesName'] == "Bukit Beruntung")
                                            {
                                                $total_charges += 60;
                                            }
                                            else if($row['zonesName'] == "Nilai")
                                            {
                                                $total_charges += 30;
                                            }
                                            else if($row['zonesName'] == "Senawang")
                                            {
                                                $total_charges += 45;
                                            }
                                            else if($row['zonesName'] == "Seremban")
                                            {
                                                $total_charges += 50;
                                            }
                                            else if($row['zonesName'] == "Cyberjaya" || $row['zonesName'] == "Bangi")
                                            {
                                                $total_charges += 45;
                                            }
                                        }

                                        

                                        //Detention Charges
                                        if ($dc != 0)
                                        {
                                            $total_charges += ($dc * 6);
                                        }
                                        //Night Charges
                                        if ($night != "no")
                                        {
                                            $total_charges += 10;
                                        }
                                        //Public Charges
                                        if ($public == "yes" && $dayOfDelivery = "Sun")
                                        {
                                            $total_charges += 45;
                                        }
                                        //rest day Charges
                                        if ($public == "no" && $dayOfDelivery = "Sun")
                                        {
                                            $total_charges += 30;
                                        }
                                    }
                                    else if($row['truckCapacity'] == "40 Ft / Q6 Air-Ride" || $row['truckCapacity'] == "40 Ft / Q6")
                                    {
                                        if($origin && $origin == "Penang")
                                        {
                                            //Trucking
                                            if($row['zonesName'] == "Penang")
                                            {
                                                $total_charges += 28;
                                            }
                                            else if($row['zonesName'] == "Prai" || $row['zonesName'] == "Batu Kawan")
                                            {
                                                $total_charges += 37;
                                            }
                                            else if($row['zonesName'] == "Kulim" || $row['zonesName'] == "Sungai Petani")
                                            {
                                                $total_charges += 42;
                                            }
                                            else if($row['zonesName'] == "Ipoh")
                                            {
                                                $total_charges += 120;
                                            }
                                            else if($row['zonesName'] == "KLIA")
                                            {
                                                if($row['driver2ID_FK'] && $row['driver2ID_FK'] !=0 && $row['driver2ID_FK']!="")
                                                {
                                                    $total_charges += 125;
                                                }
                                                else
                                                {
                                                    $total_charges += 150;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if($row['zonesName'] == "Klang" || $row['zonesName'] == "Kua;a Lumpur" || $row['zonesName'] == "Sepang" || $row['zonesName'] == "Shah Alam")
                                            {
                                                $total_charges += 70;
                                            }
                                            else if($row['zonesName'] == "Bukit Beruntung")
                                            {
                                                $total_charges += 86;
                                            }
                                            else if($row['zonesName'] == "Nilai")
                                            {
                                                $total_charges += 50;
                                            }
                                            else if($row['zonesName'] == "Senawang")
                                            {
                                                $total_charges += 55;
                                            }
                                            else if($row['zonesName'] == "Seremban")
                                            {
                                                $total_charges += 55;
                                            }
                                            else if($row['zonesName'] == "Cyberjaya" || $row['zonesName'] == "Bangi")
                                            {
                                                $total_charges += 65;
                                            }
                                            else if($row['zonesName'] == "Melaka")
                                            {
                                                $total_charges += 60;
                                            }
                                        }

                                        //Detention Charges
                                        if ($dc != 0)
                                        {
                                            $total_charges += ($dc * 9);
                                        }
                                        //Night Charges
                                        if ($night != "no")
                                        {
                                            $total_charges += 10;
                                        }
                                        //Public Charges
                                        if ($public == "yes" && $dayOfDelivery = "Sun")
                                        {
                                            $total_charges += 45;
                                        }
                                        //rest day Charges
                                        if ($public == "no" && $dayOfDelivery = "Sun")
                                        {
                                            $total_charges += 30;
                                        }
                                    }
                                    else if($row['truckCapacity'] == "45 Ft / Q7")
                                    {
                                        if($load == "Q6")
                                        {
                                            if($origin && $origin == "Penang")
                                            {
                                                //Trucking
                                                if($row['zonesName'] == "Penang")
                                                {
                                                    $total_charges += 35;
                                                }
                                                else if($row['zonesName'] == "Prai" || $row['zonesName'] == "Batu Kawan")
                                                {
                                                    $total_charges += 45;
                                                }
                                                else if($row['zonesName'] == "Kulim" || $row['zonesName'] == "Sungai Petani")
                                                {
                                                    $total_charges += 50;
                                                }                               
                                            }
    
                                            //Detention Charges
                                            if ($dc != 0)
                                            {
                                                $total_charges += ($dc * 9);
                                            }
                                            //Night Charges
                                            if ($night != "no")
                                            {
                                                $total_charges += 10;
                                            }
                                            //Public Charges
                                            if ($public == "yes" && $dayOfDelivery = "Sun")
                                            {
                                                $total_charges += 45;
                                            }
                                            //rest day Charges
                                            if ($public == "no" && $dayOfDelivery = "Sun")
                                            {
                                                $total_charges += 30;
                                            }
                                        }
                                        else
                                        {
                                            if($origin && $origin == "Penang")
                                            {
                                                //Trucking
                                                if($row['zonesName'] == "Penang")
                                                {
                                                    $total_charges += 56;
                                                }
                                                else if($row['zonesName'] == "Prai" || $row['zonesName'] == "Batu Kawan")
                                                {
                                                    $total_charges += 74;
                                                }
                                                else if($row['zonesName'] == "Kulim" || $row['zonesName'] == "Sungai Petani")
                                                {
                                                    $total_charges += 84;
                                                }
                                                else if($row['zonesName'] == "KLIA")
                                                {
                                                    if($row['driver2ID_FK'] && $row['driver2ID_FK'] !=0 && $row['driver2ID_FK']!="")
                                                    {
                                                        $total_charges += 400;
                                                    }
                                                    else
                                                    {
                                                        $total_charges += 200;
                                                    }
                                                }
                                            }

                                            //Detention Charges
                                            if ($dc != 0)
                                            {
                                                $total_charges += ($dc * 9);
                                            }
                                            //Night Charges
                                            if ($night != "no")
                                            {
                                                $total_charges += 10;
                                            }
                                            //Public Charges
                                            if ($public == "yes" && $dayOfDelivery = "Sun")
                                            {
                                                $total_charges += 45;
                                            }
                                            //rest day Charges
                                            if ($public == "no" && $dayOfDelivery = "Sun")
                                            {
                                                $total_charges += 30;
                                            }
                                        }
                                    }
                                }
                            }

            
            if($addOrEdit == 1)
            {
                $sql = "INSERT INTO driverservicefee (
                    dsfDONumber,dtmID_FK,
                    dsfDateCreated
                    )
                VALUES (
                '".$dsfDONumber."','".$dtmID_FK."',now())";
    
                if (mysqli_query($conn, $sql)) 
                {
                    echo "Service Fee is recorded";
                } 
                else 
                {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }

                if($secondDriver)
                {
                    $sql = "INSERT INTO driverservicefee (
                        dsfDONumber,dtmID_FK,
                        dsfDateCreated
                        )
                    VALUES (
                    '".$dsfDONumber."','".$dtmID_FK."',now())";
        
                    if (mysqli_query($conn, $sql)) 
                    {
                        echo "Service Fee is recorded";
                    } 
                    else 
                    {
                        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                    }
                }

                $sql2 = "UPDATE dtmlist SET 
                dtmIsPaidDriverServiceFee = 1
                WHERE dtmID_PK = ".$dtmID_FK."";
    
                if (mysqli_query($conn, $sql2)) 
                {
                    echo "Service Fee is Updated";
                } 
                else 
                {
                    echo "Error: " . $sql2 . "<br>" . mysqli_error($conn);
                }
            }
        }
        if($fromPage == 9 && $addOrEdit == 3)
        {
            // $idpk = $_POST['idpk'];
            // $sql = "UPDATE user SET showThis = 0 WHERE userID_PK = ".$idpk;

            // if (mysqli_query($conn, $sql)) {
            //     echo "User deleted successfully";
            // } else {
            //     echo "Error deleting record: " . mysqli_error($conn);
            // }
        }
    }
    else
    {

    }
}