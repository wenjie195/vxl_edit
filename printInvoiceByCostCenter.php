<?php
date_default_timezone_set("Asia/Kuala_Lumpur");
$todayDate = date("Ymd_his");

require_once 'generalFunction.php';
require_once 'templateFunctions.php';
$conn = connDB();

$companyName = getCompanyName($_POST['company'],$conn);
$costCenterName = getCostCenterName($_POST['costcenter'],$conn);
$fromDate = $_POST['fromDate'];
$toDate = $_POST['toDate'];

$filename = 'filename="Invoice_'.$companyName.'_'.$costCenterName.'__'.$fromDate.'_'.$toDate.'.xlsx"';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; '.$filename);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;


require_once 'phpexcel/vendor/autoload.php';


$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();


$invoiceNo = $_POST['invoiceNo'];
$companyID = $_POST['company'];
$costcenterID = $_POST['costcenter'];

// echo "From Date :".$_POST['fromDate']."<br>";
// echo "To Date :".$_POST['toDate']."<br>";
// echo "Invoice No :".$_POST['invoiceNo']."<br>";
// echo "Company Name :".getCompanyName($_POST['company'],$conn).":".$_POST['company']."<br>";
// echo "Cost Center Name :".getCostCenterName($_POST['costcenter'],$conn).":".$_POST['costcenter']."<br>";

$fromDateFormatted = getDatePHP($fromDate);
$toDateFormatted = getDatePHPWithTime($toDate,0);


$costCenterDisplay = " SELECT * FROM ((invoice 
INNER JOIN transportcharge ON invoice.transportID_FK = transportcharge.id)
INNER JOIN dtmlist ON transportcharge.dtmID_FK = dtmlist.dtmID_PK)
WHERE (invoice.invoiceNo = $invoiceNo) 
AND (invoice.isSelected = 0 OR invoice.isSelected IS NULL)
AND (dtmlist.dtmPickupDate BETWEEN '$fromDateFormatted' AND '$toDateFormatted')
AND (dtmlist.companyID_FK = '$companyID')
AND (dtmlist.costCenterID_FK = '$costcenterID')
ORDER BY dtmlist.dtmPickupDate ASC  , dtmlist.costCenterID_FK ASC , dtmlist.dtmPickupTime ASC
";
$costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
$counterNo = mysqli_num_rows($costCenterDisplayQuery);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////HEADER FOR TEMPLATE/////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if($counterNo > 0)
{
     if($companyID == 43) // SCHENKER 
     {
          if($costcenterID == 19   //   AEG
          || $costcenterID == 20   //   AIG  
          || $costcenterID == 21)  //   AED
          {
               $sheet->setCellValue('A1', 'Attn');
               $sheet->setCellValue('A2', 'Mode');
               $sheet->setCellValue('A3', 'Dates');
               $sheet->setCellValue('A4', 'Invoice No');
               $sheet->setCellValue('B4', $invoiceNo);
          
               $sheet->setCellValue('B2', $companyName );
               $sheet->setCellValue('C2', $costCenterName );
               $sheet->setCellValue('B3', $fromDate);
               $sheet->setCellValue('C3',' To ');
               $sheet->setCellValue('D3', $toDate);
               $sheet->getStyle('B3:D3')->getAlignment()->setHorizontal('center');
               $sheet->getStyle('B2')->getAlignment()->setHorizontal('center');
               $sheet->getStyle('B4')->getAlignment()->setHorizontal('center');

               $sheet->setCellValue('A6', 'No');
               $sheet->setCellValue('B6', 'Pickup Date');
               $sheet->setCellValue('C6', 'Truck No');
               $sheet->setCellValue('D6', 'Truck Capacity');
               $sheet->setCellValue('E6', 'D/O No');
               $sheet->setCellValue('F6', 'Job No');
               $sheet->setCellValue('G6', 'Pickup Location');
               $sheet->setCellValue('H6', 'Pickup From');
               $sheet->setCellValue('I6', 'Deliver To');
               $sheet->setCellValue('J6', 'Quantity');
               $sheet->setCellValue('K6', 'Time In (GBE)');
               $sheet->setCellValue('L6', 'Time Out (GBE)');
               $sheet->setCellValue('M6', 'Time In (PLAN)');
               $sheet->setCellValue('N6', 'Time Out (Plan)');
               $sheet->setCellValue('O6', 'TPT');
               $sheet->setCellValue('P6', 'Truck Charges');
               $sheet->setCellValue('Q6', 'FAF (9%)');
               $sheet->setCellValue('R6', 'Detention');
               $sheet->setCellValue('S6', 'D`Point');
               $sheet->setCellValue('T6', 'Lockup');
               $sheet->setCellValue('U6', 'Sunday/PH Pickup');
               $sheet->setCellValue('V6', 'Others');
               $sheet->setCellValue('W6', 'Total');
               $sheet->setCellValue('X6', 'Transport Remark');
               $sheet->setCellValue('Y6', 'DTM Remark');
               $sheet->setCellValue('Z6', 'DTM No');
               $sheet->setCellValue('AA6', 'Consol Status');

               putBorderStyleRow($sheet,'A6:AA6',2,'99AFFF');
               putWidth('A','AB',15,$sheet);
               $sheet->getColumnDimension('J')->setWidth(30);
               $sheet->getStyle('A6:AA6')->getAlignment()->setHorizontal('center');
               $sheet->getStyle('A1:D4')->getAlignment()->setHorizontal('center');
          }
          else 
          if($costcenterID == 24   //   IPM  
          || $costcenterID == 28)  //   TMG  
          {
               $sheet->setCellValue('A1', 'Attn');
               $sheet->setCellValue('A2', 'Mode');
               $sheet->setCellValue('A3', 'Dates');
               $sheet->setCellValue('A4', 'Invoice No');
               $sheet->setCellValue('B4', $invoiceNo);
          
               $sheet->setCellValue('B2', $companyName );
               $sheet->setCellValue('C2', $costCenterName);
               $sheet->setCellValue('B3', $fromDate);
               $sheet->setCellValue('C3',' To ');
               $sheet->setCellValue('D3', $toDate);
               $sheet->getStyle('B3:D3')->getAlignment()->setHorizontal('center');
               $sheet->getStyle('B2')->getAlignment()->setHorizontal('center');
               $sheet->getStyle('B4')->getAlignment()->setHorizontal('center');

               $sheet->setCellValue('A6', 'No');
               $sheet->setCellValue('B6', 'Pickup Date');
               $sheet->setCellValue('C6', 'Pickup Time');
               $sheet->setCellValue('D6', 'Delivery Time');
               $sheet->setCellValue('E6', 'TPT');
               $sheet->setCellValue('F6', 'Truck No');
               $sheet->setCellValue('G6', 'Truck Cap');
               $sheet->setCellValue('H6', 'D/O No');
               $sheet->setCellValue('I6', 'Job No');
               $sheet->setCellValue('J6', 'Booking Ref No');
               $sheet->setCellValue('K6', 'Pickup Location');
               $sheet->setCellValue('L6', 'C/Form Mode');
               $sheet->setCellValue('M6', 'Pickup From');
               $sheet->setCellValue('N6', 'Deliver To');
               $sheet->setCellValue('O6', 'Quantity');
               $sheet->setCellValue('P6', 'Truck Charges');
               $sheet->setCellValue('Q6', 'FAF (9%)');
               $sheet->setCellValue('R6', 'D`Point');
               $sheet->setCellValue('S6', 'Lockup');
               $sheet->setCellValue('T6', 'Sunday/PH Pickup');
               $sheet->setCellValue('U6', 'Others');
               $sheet->setCellValue('V6', 'GST');
               $sheet->setCellValue('W6', 'Total');
               $sheet->setCellValue('X6', 'Transport Remark');
               $sheet->setCellValue('Y6', 'DTM Remark');
               $sheet->setCellValue('Z6', 'DTM No');
               $sheet->setCellValue('AA6', 'Consol Status');

               putBorderStyleRow($sheet,'A6:AA6',2,'99AFFF');
               putWidth('A','AB',15,$sheet);
               $sheet->getColumnDimension('O')->setWidth(30);
               $sheet->getStyle('A6:AA6')->getAlignment()->setHorizontal('center');
               $sheet->getStyle('A1:D4')->getAlignment()->setHorizontal('center');
          }
          else 
          if($costcenterID == 38)  //   AIMP  
          {
               $sheet->setCellValue('A1', 'Attn');
               $sheet->setCellValue('A2', 'Mode');
               $sheet->setCellValue('A3', 'Dates');
               $sheet->setCellValue('A4', 'Invoice No');
               $sheet->setCellValue('B4', $invoiceNo);
          
               $sheet->setCellValue('B2', $companyName );
               $sheet->setCellValue('C2', $costCenterName );
               $sheet->setCellValue('B3', $fromDate);
               $sheet->setCellValue('C3',' To ');
               $sheet->setCellValue('D3', $toDate);
               $sheet->getStyle('B3:D3')->getAlignment()->setHorizontal('center');
               $sheet->getStyle('B2')->getAlignment()->setHorizontal('center');
               $sheet->getStyle('B4')->getAlignment()->setHorizontal('center');

               $sheet->setCellValue('A6', 'No');
               $sheet->setCellValue('B6', 'Pickup Date');
               $sheet->setCellValue('C6', 'Truck No');
               $sheet->setCellValue('D6', 'Truck Cap');
               $sheet->setCellValue('E6', 'D/O No');
               $sheet->setCellValue('F6', 'HAWB');
               $sheet->setCellValue('G6', 'Weight');
               $sheet->setCellValue('H6', 'Pickup Location');
               $sheet->setCellValue('I6', 'Pickup From');
               $sheet->setCellValue('J6', 'Deliver To');
               $sheet->setCellValue('K6', 'Quantity');
               $sheet->setCellValue('L6', 'Time In (GBE)');
               $sheet->setCellValue('M6', 'Time Out (GBE)');
               $sheet->setCellValue('N6', 'Time In (PLAN)');
               $sheet->setCellValue('O6', 'Time Out (Plan)');
               $sheet->setCellValue('P6', 'TPT');
               $sheet->setCellValue('Q6', 'Truck Charges');
               $sheet->setCellValue('R6', 'FAF (9%)');
               $sheet->setCellValue('S6', 'Detention');
               $sheet->setCellValue('T6', 'GPS Charge');
               $sheet->setCellValue('U6', 'D`Point');
               $sheet->setCellValue('V6', 'Lockup');
               $sheet->setCellValue('W6', 'Sunday/PH Pickup');
               $sheet->setCellValue('X6', 'Others');
               $sheet->setCellValue('Y6', 'Total');
               $sheet->setCellValue('Z6', 'Transport Remark');
               $sheet->setCellValue('AA6', 'DTM Remark');
               $sheet->setCellValue('AB6', 'DTM No');
               $sheet->setCellValue('AC6', 'Consol Status');

               putBorderStyleRow($sheet,'A6:AC6',2,'99AFFF');
               putWidth('A','AD',15,$sheet);
               $sheet->getColumnDimension('K')->setWidth(30);
               $sheet->getStyle('A6:AC6')->getAlignment()->setHorizontal('center');
               $sheet->getStyle('A1:D4')->getAlignment()->setHorizontal('center');
          }
          else 
          if($costcenterID == 39)  //   AEXP  
          {
               $sheet->setCellValue('A1', 'Attn');
               $sheet->setCellValue('A2', 'Mode');
               $sheet->setCellValue('A3', 'Dates');
               $sheet->setCellValue('A4', 'Invoice No');
               $sheet->setCellValue('B4', $invoiceNo);
          
               $sheet->setCellValue('B2', $companyName );
               $sheet->setCellValue('C2', $costCenterName );
               $sheet->setCellValue('B3', $fromDate);
               $sheet->setCellValue('C3',' To ');
               $sheet->setCellValue('D3', $toDate);
               $sheet->getStyle('B3:D3')->getAlignment()->setHorizontal('center');
               $sheet->getStyle('B2')->getAlignment()->setHorizontal('center');
               $sheet->getStyle('B4')->getAlignment()->setHorizontal('center');

               $sheet->setCellValue('A6', 'No');
               $sheet->setCellValue('B6', 'Pickup Date');
               $sheet->setCellValue('C6', 'Truck No');
               $sheet->setCellValue('D6', 'Truck Cap');
               $sheet->setCellValue('E6', 'D/O No');
               $sheet->setCellValue('F6', 'Pickup From');
               $sheet->setCellValue('G6', 'Deliver To');
               $sheet->setCellValue('H6', 'Quantity');
               $sheet->setCellValue('I6', 'Truck Charges');
               $sheet->setCellValue('J6', 'FAF (9%)');
               $sheet->setCellValue('K6', 'Detention');
               $sheet->setCellValue('L6', 'GPS Charge');
               $sheet->setCellValue('M6', 'D`Point');
               $sheet->setCellValue('N6', 'Sunday/PH Pickup');
               $sheet->setCellValue('O6', 'Others');
               $sheet->setCellValue('P6', 'Total');
               $sheet->setCellValue('Q6', 'Transport Remark');
               $sheet->setCellValue('R6', 'DTM Remark');
               $sheet->setCellValue('S6', 'DTM No');
               $sheet->setCellValue('T6', 'Consol Status');

               putBorderStyleRow($sheet,'A6:T6',2,'99AFFF');
               putWidth('A','W',15,$sheet);
               $sheet->getColumnDimension('H')->setWidth(30);
               $sheet->getStyle('A6:T6')->getAlignment()->setHorizontal('center');
          }
          else 
          {
               normalTemplateTop($sheet,$fromDate,$toDate,$invoiceNo,$companyName,$costCenterName);
          }
     }
     else
     if($companyID == 73) // PEN EXPRESS 
     {
          if($costcenterID == 29   //   BROADCOMM
          || $costcenterID == 30   //   LOCAL  
          || $costcenterID == 31)  //   TRANSFER
          {
               $sheet->setCellValue('A1', 'Date');
               $sheet->setCellValue('A2', 'Project');
               $sheet->setCellValue('B2', $companyName );
               $sheet->setCellValue('C2', $costCenterName );
               $sheet->setCellValue('D2', 'Invoice No');
               $sheet->setCellValue('E2', $invoiceNo);

               $sheet->setCellValue('A5', 'No');
               $sheet->mergeCells('A5:A6');
               $sheet->setCellValue('B5', 'Pickup Date');
               $sheet->mergeCells('B5:B6');
               $sheet->setCellValue('C5', 'Truck No');
               $sheet->mergeCells('C5:C6');
               $sheet->setCellValue('D5', 'Truck Cap');
               $sheet->mergeCells('D5:D6');
               $sheet->setCellValue('E5', 'VXL D/O No');
               $sheet->mergeCells('E5:E6');
               $sheet->setCellValue('F5', 'Destination');
               $sheet->mergeCells('F5:H5');
               $sheet->setCellValue('F6', 'Pickup From');
               $sheet->setCellValue('G6', 'Deliver To');
               $sheet->setCellValue('H6', 'Agent');

               $sheet->setCellValue('I5', 'Destination');
               $sheet->mergeCells('I5:K5');
               $sheet->setCellValue('I6', 'Truck In');
               $sheet->setCellValue('J6', 'Truck Out');
               $sheet->setCellValue('K6', 'Total');

               $sheet->setCellValue('L5', 'Transport Cost');
               $sheet->mergeCells('L5:L6');
               $sheet->setCellValue('M5', 'Detention');
               $sheet->mergeCells('M5:N5');
               $sheet->setCellValue('M6', 'Hrs');
               $sheet->setCellValue('N6', 'Charges');

               $sheet->setCellValue('O5', 'Drop Point');
               $sheet->mergeCells('O5:P5');

               $sheet->setCellValue('O6', '#of Drop Point');
               $sheet->setCellValue('P6', 'Charges ');
               $sheet->setCellValue('Q6', 'Delivery After');
               $sheet->setCellValue('R6', 'Sunday/PH Pickup ');
               $sheet->setCellValue('S6', 'Others ');
               $sheet->setCellValue('T6', 'GST 0% ');
               $sheet->setCellValue('U6', 'Total');
               $sheet->setCellValue('V6', 'Transport Remark');
               $sheet->setCellValue('W6', 'DTM Remark');
               $sheet->setCellValue('X6', 'DTM No');
               $sheet->setCellValue('Y6', 'Consol Status');

               // putBorderStyleRow($sheet,'A5:P6',2,'99AFFF');
               putWidth('A','Z',15,$sheet);
               $sheet->getColumnDimension('H')->setWidth(30);
               $sheet->getStyle('A5:Y5')->getAlignment()->setHorizontal('center');
               $sheet->getStyle('A6:Y6')->getAlignment()->setHorizontal('center');
               $sheet->getStyle('A1:D4')->getAlignment()->setHorizontal('center');
          }
          else 
          {
               normalTemplateTop($sheet,$fromDate,$toDate,$invoiceNo,$companyName,$costCenterName);
          }
     }
     else
     if($companyID == 62) // EXPEDITORS
     {
          if($costcenterID == 34    //   AIR EXPORT  
          || $costcenterID == 35)   //   AIR IMPORT  
          {
               $sheet->setCellValue('A1', 'Department');
               $sheet->setCellValue('B1', $companyName );
               $sheet->setCellValue('C1', $costCenterName );
               $sheet->setCellValue('A2', 'Truckers');
               $sheet->setCellValue('B2', 'VXL Alliance SDN BHD' );
               
               $sheet->setCellValue('D1', 'Month');
               $sheet->setCellValue('E1', 'Serial No');
               $sheet->setCellValue('D1', 'Date');

               $sheet->setCellValue('A6', 'Item No');
               $sheet->setCellValue('B6', 'Shipment Date');
               $sheet->setCellValue('C6', 'Vendor Tax Invoice');
               $sheet->setCellValue('D6', 'Expeditors D/O /Pickup Order');
               $sheet->setCellValue('E6', 'Expeditors House Bills');
               $sheet->setCellValue('F6', 'Truck No');
               $sheet->setCellValue('G6', 'Tonner');
               $sheet->setCellValue('H6', 'Truck Charges ');
               $sheet->setCellValue('I6', 'Additional P/Up or DLV');
               $sheet->setCellValue('J6', 'Overtime/Detention ');
               $sheet->setCellValue('K6', 'Lockup Charges');
               $sheet->setCellValue('L6', 'Other Charges');
               $sheet->setCellValue('M6', 'FAF ');
               $sheet->setCellValue('N6', 'Total Cost');
               $sheet->setCellValue('O6', 'GST 0%');
               $sheet->setCellValue('P6', 'Grand Total Cost ');
               $sheet->setCellValue('Q6', 'Transport Remark');
               $sheet->setCellValue('R6', 'DTM Remark');
               $sheet->setCellValue('S6', 'DTM No');
               $sheet->setCellValue('T6', 'Consol Status');

               putBorderStyleRow($sheet,'A6:T6',2,'99AFFF');
               putWidth('A','U',15,$sheet);
               $sheet->getColumnDimension('B')->setWidth(20);
               $sheet->getColumnDimension('C')->setWidth(20);
               $sheet->getColumnDimension('D')->setWidth(25);
               $sheet->getColumnDimension('E')->setWidth(20);
               $sheet->getColumnDimension('I')->setWidth(25);
               $sheet->getColumnDimension('J')->setWidth(20);
               $sheet->getColumnDimension('P')->setWidth(20);
               $sheet->getStyle('A6:T6')->getAlignment()->setHorizontal('center');
          }
          else 
          {
               normalTemplateTop($sheet,$fromDate,$toDate,$invoiceNo,$companyName,$costCenterName);
          }
     }
     else
     if($companyID == 79) // UPS
     {
          if($costcenterID == 32    //   PICKUP  
          || $costcenterID == 33)   //   DELIVERY 
          {
               $sheet->setCellValue('A1', 'Week');
               $sheet->setCellValue('B1', $fromDate);
               $sheet->setCellValue('C1',' To ');
               $sheet->setCellValue('D1', $toDate);

               $sheet->setCellValue('A2', 'Invoice No');
               $sheet->setCellValue('B2', $invoiceNo);
               $sheet->setCellValue('A3', 'Mode');
               $sheet->setCellValue('B3', $companyName );
               $sheet->setCellValue('C3', $costCenterName );

               $sheet->setCellValue('A6', 'Item No');
               $sheet->setCellValue('B6', 'Pickup Date');
               $sheet->setCellValue('C6', 'Truck No');
               $sheet->setCellValue('D6', 'Truck Capacity');
               $sheet->setCellValue('E6', 'D/O No');
               $sheet->setCellValue('F6', 'Pickup From');
               $sheet->setCellValue('G6', 'Pickup Location');
               $sheet->setCellValue('H6', 'Deliver To');
               $sheet->setCellValue('I6', 'Deliver Location');
               $sheet->setCellValue('J6', 'Quantity');
               $sheet->setCellValue('K6', 'Truck Charges');
               $sheet->setCellValue('L6', 'FAF (9%)');
               $sheet->setCellValue('M6', 'Pickup/D`Point');
               $sheet->setCellValue('N6', 'Sunday/PH Pickup');
               $sheet->setCellValue('O6', 'Others');
               $sheet->setCellValue('P6', 'Total');
               $sheet->setCellValue('Q6', 'Transport Remark');
               $sheet->setCellValue('R6', 'DTM Remark');
               $sheet->setCellValue('S6', 'DTM No');
               $sheet->setCellValue('T6', 'Consol Status');

               putBorderStyleRow($sheet,'A6:T6',2,'99AFFF');
               putWidth('A','U',15,$sheet);
               $sheet->getColumnDimension('J')->setWidth(30);
               $sheet->getStyle('A6:T6')->getAlignment()->setHorizontal('center');
               $sheet->getStyle('A1:D4')->getAlignment()->setHorizontal('center');
          }
          else 
          {
               normalTemplateTop($sheet,$fromDate,$toDate,$invoiceNo,$companyName,$costCenterName);
          }
     }
     else
     if($companyID == 70) // KOBE TECHNOLOGY
     {
          if($costcenterID == 36    //   INBOUND  
          || $costcenterID == 37)   //   OUTBOUND 
          {
               $sheet->setCellValue('A1', $companyName );
               $sheet->setCellValue('B1', $costCenterName );
               $sheet->setCellValue('A2', 'VXL Alliance SDN BHD' );
               $sheet->setCellValue('D1', $fromDate);
               $sheet->setCellValue('E1', 'To' );
               $sheet->setCellValue('F1', $toDate);

               $sheet->setCellValue('A6', 'Shipment Date');
               $sheet->setCellValue('B6', 'KPT INV/PL');
               $sheet->setCellValue('C6', 'Quantity');
               $sheet->setCellValue('D6', 'Transport Size');
               $sheet->setCellValue('E6', 'Transport Cost');
               $sheet->setCellValue('F6', 'P`kup/Delivery Point');
               $sheet->setCellValue('G6', 'D/O No');
               $sheet->setCellValue('H6', 'From');
               $sheet->setCellValue('I6', 'To');
               $sheet->setCellValue('J6', 'Qty pits');
               $sheet->setCellValue('K6', 'Additional Charges');
               $sheet->setCellValue('L6', 'Cost');
               $sheet->setCellValue('M6', 'Remarks');
               $sheet->setCellValue('N6', 'Commodity');
               $sheet->setCellValue('O6', 'Transport Remark');
               $sheet->setCellValue('P6', 'DTM Remark');
               $sheet->setCellValue('Q6', 'DTM No');
               $sheet->setCellValue('R6', 'Consol Status');

               putBorderStyleRow($sheet,'A6:R6',2,'99AFFF');
               putWidth('A','S',15,$sheet);
               $sheet->getColumnDimension('C')->setWidth(30);
               $sheet->getStyle('A6:R6')->getAlignment()->setHorizontal('center');
               $sheet->getStyle('D1:F1')->getAlignment()->setHorizontal('center');
          }
          else 
          {
               normalTemplateTop($sheet,$fromDate,$toDate,$invoiceNo,$companyName,$costCenterName);
          }
     }
     else
     if($companyID == 55      // DHL DGF
     || $companyID == 63)     // EZYHAUL
     {
          $sheet->setCellValue('A1', 'DATES');
          $sheet->setCellValue('B1', $fromDate);
          $sheet->setCellValue('C1', '---' );
          $sheet->setCellValue('D1', $toDate);

          $sheet->setCellValue('A2', 'VENDOR');
          $sheet->setCellValue('B2', 'VXL ALLIANCE');
          $sheet->setCellValue('E2', 'AGENT');
          $sheet->setCellValue('F2', $companyName);

          $sheet->setCellValue('E3', 'COST CENTER');
          $sheet->setCellValue('F3', $costCenterName);
          
          $sheet->setCellValue('A3', 'INVOICE NO');
          $sheet->setCellValue('B3', $invoiceNo);
          $sheet->setCellValue('A4', 'INVOICE DATE');

          $sheet->setCellValue('A6', 'Item No');
          $sheet->setCellValue('B6', 'Pickup Date');
          $sheet->setCellValue('C6', 'Pickup Time');
          $sheet->setCellValue('D6', 'DGF Reference');
          $sheet->setCellValue('E6', 'Urgent Memo');
          $sheet->setCellValue('F6', 'Requestor');
          $sheet->setCellValue('G6', 'Pickup Point');
          $sheet->setCellValue('H6', 'Pickup Zone');
          $sheet->setCellValue('I6', 'Delivery Point');
          $sheet->setCellValue('J6', 'Delivery Zone');
          $sheet->setCellValue('K6', 'Date Truck Reached');
          $sheet->setCellValue('L6', 'Time Truck Reached');
          $sheet->setCellValue('M6', 'Date Started to Load Cargo');
          $sheet->setCellValue('N6', 'Time Started to Load Cargo');
          $sheet->setCellValue('O6', 'Date Finished Loading Cargo');
          $sheet->setCellValue('P6', 'Time Finished Loading Cargo');
          $sheet->setCellValue('Q6', 'Date Received Custom Form');
          $sheet->setCellValue('R6', 'Time Received Custom Form');
          $sheet->setCellValue('S6', 'Date Truck Depart');
          $sheet->setCellValue('T6', 'Time Truck Depart');
          $sheet->setCellValue('U6', 'Date Truck Reached');
          $sheet->setCellValue('V6', 'Time Truck Reached');
          $sheet->setCellValue('W6', 'Date Received Custom Form');
          $sheet->setCellValue('X6', 'Time Received Custom Form');
          $sheet->setCellValue('Y6', 'Date Truck Reached');
          $sheet->setCellValue('Z6', 'Time Truck Reached');
          $sheet->setCellValue('AA6', 'Date Started to Unload Cargo');
          $sheet->setCellValue('AB6', 'Time Started to Unload Cargo');
          $sheet->setCellValue('AC6', 'Date Finished Unloading Cargo');
          $sheet->setCellValue('AD6', 'Time Finished Unloading Cargo');
          $sheet->setCellValue('AE6', 'Carton');
          $sheet->setCellValue('AF6', 'Pallets');
          $sheet->setCellValue('AG6', 'Cages/Crates');
          $sheet->setCellValue('AH6', 'Truck No');
          $sheet->setCellValue('AI6', 'Truck Size');
          $sheet->setCellValue('AJ6', 'Truck Type');
          $sheet->setCellValue('AK6', 'Transportation Cost');
          $sheet->setCellValue('AL6', 'Fuel Adjustment Factor');
          $sheet->setCellValue('AM6', 'GST');
          $sheet->setCellValue('AN6', 'Sub Total Amount (w`out GST)'); 
          $sheet->setCellValue('AO6', 'Net Total Amount');
          $sheet->setCellValue('AP6', 'Transport Remark');
          $sheet->setCellValue('AQ6', 'DTM Remark');
          $sheet->setCellValue('AR6', 'DTM No');
          $sheet->setCellValue('AS6', 'Consol Status');

          putBorderStyleRow($sheet,'A6:AS6',2,'99AFFF');
          putWidth('A','AT',15,$sheet);
          $sheet->getStyle('A6:AS6')->getAlignment()->setHorizontal('center');
          $sheet->getStyle('A1:F4')->getAlignment()->setHorizontal('center');
     }
     else 
     {
          normalTemplateTop($sheet,$fromDate,$toDate,$invoiceNo,$companyName,$costCenterName);
     }
}
else 
{
     $sheet->setCellValue('A1','No data matches');
     $sheet->setCellValue('A2','Please Try Again');
     $sheet->setCellValue('A3','Using Correct Info');
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////END HEADER FOR TEMPLATE/////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$line = 7;

if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
{
     while($row = mysqli_fetch_array($costCenterDisplayQuery))
     {
          // Transport Charges Table
          $transportchargeRemarks = strtoupper($row['remarks']);
          $transportchargeOperatingHour = $row['operatingHour'];
          $transportchargeTransportCharge = $row['transportcharge'];
          $transportchargeFaf = ($row['faf'] / 100) * $row['transportcharge'];
          $transportchargeTotalCharges = $transportchargeTransportCharge + $transportchargeFaf;

          // DTM Table
          $dtmNoID_PK = $row['dtmID_PK'];
          $dtmDGF = $row['dtmDGF'];
          // $companyName = getCompanyName($_POST['company'],$conn);
          // $costCenterName = getCostCenterName($_POST['costcenter'],$conn);

               // Booking & Pickup Time
          $dtmBookingTime = getTimeFormats($row['dtmBookingTime']);
          $dtmPickupTime = getTimeFormats($row['dtmPickupTime']);
          $dtmPickupTime1 = getTimeFormats2($row['dtmPickupTime']);
          $dtmBookingDate = getDateFormats($row['dtmBookingDate']);
          $dtmPickupDate = getDateFormats($row['dtmPickupDate']);

               // Origin & Destination Time
          $dtmOriginPointID_FK = getPlaceName($row['dtmOriginPointID_FK'],$conn);
          $dtmDestinationPointID_FK = getPlaceName($row['dtmDestinationPointID_FK'],$conn);
          $dtmOriginZone = getZoneName($row['dtmOriginZoneID_FK'],$conn);
          $dtmDestinationZone = getZoneName($row['dtmDestinationZoneID_FK'],$conn);
           
               // Trucks & Drivers & Actual Loads
          $truckArray = getTruckDetails($row['truckID_FK'],$conn);
          $truckPlateNo = $truckArray[0];
          $truckCap = $truckArray[1];
          $loadCap = $row['loadCap'];
          $dtmUrgentMemo = "No";

          $driversName = "(1) ".getDriverName($row['driverID_FK'],$conn);
          if(isset($row['driver2ID_FK']))
          {
               $driversName .= "\n(2) ".getDriverName($row['driver2ID_FK'],$conn);
          }

          if($row['dtmUrgentMemo'] == 1)
          {
               $dtmUrgentMemo = "Yes";
          }
          
               // Trucks Loading Times
          $startIn = getTimeFormats($row['dtmOriginF']);
               $timedockTime = getTimeFormats($row['dtmOriginDockTime']);
               $timecompleteLoadingTime = getTimeFormats($row['dtmOriginCompleteLoadingTime']);
          $startOut = getTimeFormats($row['dtmOriginDepartureTimeFromPost']);

          $endIn = getTimeFormats($row['dtmDestinationArrivalTimeAtPost']);
               $timedestinationDockTime = getTimeFormats($row['dtmDestinationDockTime']);
               $timecompleteUnloadTime = getTimeFormats($row['dtmDestinationCompleteUnloadTime']);
          $endOut = getTimeFormats($row['dtmDestinationDepartTimeFromPost']);

          $startIn1 = getTimeFormats2($row['dtmOriginF']);
               $timedockTime1 = getTimeFormats2($row['dtmOriginDockTime']);
               $timecompleteLoadingTime1 = getTimeFormats2($row['dtmOriginCompleteLoadingTime']);
          $startOut1 = getTimeFormats2($row['dtmOriginDepartureTimeFromPost']);

          $endIn1 = getTimeFormats2($row['dtmDestinationArrivalTimeAtPost']);
               $timedestinationDockTime1 = getTimeFormats2($row['dtmDestinationDockTime']);
               $timecompleteUnloadTime1 = getTimeFormats2($row['dtmDestinationCompleteUnloadTime']);
          $endOut1 = getTimeFormats2($row['dtmDestinationDepartTimeFromPost']);

          $tpt = compareValue($row['dtmDestinationDepartTimeFromPost'],$row['dtmOriginF'],$row['lockupdepartTimeFromPost'],$row['lockupf'],$row['dtmPickupDate']);
          
          $lockupf = checkLockupDates($row['lockupf']);
          $lockupdockTime = checkLockupDates($row['lockupdockTime']);
          $lockupcompleteLoadingTime = checkLockupDates($row['lockupcompleteLoadingTime']);
          $lockupdepartureTimeFromPost = checkLockupDates($row['lockupdepartureTimeFromPost']);

          $lockuparrivalTimeAtPost = checkLockupDates($row['lockuparrivalTimeAtPost']);
          $lockupdestinationDockTime = checkLockupDates($row['lockupdestinationDockTime']);
          $lockupcompleteUnloadTime = checkLockupDates($row['lockupcompleteUnloadTime']);
          $lockupdepartTimeFromPost = checkLockupDates($row['lockupdepartTimeFromPost']);

               // Quantities
          $quantityLoad = "";

          $cartonQuantity = 0;
          $palletsQuantity = 0;
          $cagesQuantity = 0;

          if($row['dtmIsQuantity'] == 1)
          {
               $quantityLoad .= "Carton = ".$row['dtmQuantityCarton'].",";
               $cartonQuantity = $row['dtmQuantityCarton'];
          }
          if($row['dtmIsPallets']== 1)
          {
               $quantityLoad .= "Pallets = ".$row['dtmQuantityPalllets'].",";
               $palletsQuantity = $row['dtmQuantityPalllets'];
          }
          if($row['dtmIsCages']== 1)
          {
               $quantityLoad .= "Cages/Crates = ".$row['dtmQuantityCages'];
               $cagesQuantity = $row['dtmQuantityCages'];
          }

               //DTM additional info
          $dtmTripOnTime = $row['dtmTripOnTime'];
          $dtmDelayReasons = "";
          if($dtmTripOnTime == 0 && isset($row['dtmDelayReasons']))
          {
               $dtmDelayReasons = $row['dtmDelayReasons'];
          }
          $planner = getPlannerName($row['dtmPlannerID_FK'],$conn);
          $dtmRequestBy = $row['dtmRequestBy'];
          $dtmRemarks = getDTM_Remark("",$row); 

          $consolShow = "";
          $isConsol = $row['isConsol'];

          if($isConsol == 1)
          {
               $consolShow="CONSOL"; 
          }

          $vxlDO = "";
          if($row['dtmAdhoc'])
          {
               $vxlDO = $row['dtmAdhoc'];
          }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////CONTENT FOR TEMPLATE/////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


          if($companyID == 43) // SCHENKER 
          {
               if($costcenterID == 19   //   AEG
               || $costcenterID == 20   //   AIG  
               || $costcenterID == 21)  //   AED
               {
                    $sheet->setCellValue('A'.$line, ($line-6));
                    $sheet->setCellValue('B'.$line, $dtmPickupDate);
                    $sheet->setCellValue('C'.$line, $truckPlateNo);
                    $sheet->setCellValue('D'.$line, $loadCap);
                    $sheet->setCellValue('E'.$line, $vxlDO);
     
                    $sheet->setCellValue('G'.$line, $dtmOriginZone);
                    $sheet->setCellValue('H'.$line, $dtmOriginPointID_FK);
                    $sheet->setCellValue('I'.$line, $dtmDestinationPointID_FK);
                    $sheet->setCellValue('J'.$line, $quantityLoad);

                    $sheet->setCellValue('K'.$line, $startIn);
                    $sheet->setCellValue('L'.$line, $startOut);
                    $sheet->setCellValue('M'.$line, $endIn);
                    $sheet->setCellValue('N'.$line, $endOut);
                    $sheet->setCellValue('O'.$line, $tpt);
                    if($isConsol == 1)
                    {
                         $sheet->setCellValue('S'.$line, $transportchargeTransportCharge);
                    }
                    else
                    {
                         $sheet->setCellValue('P'.$line, $transportchargeTransportCharge);
                    }
                    $sheet->setCellValue('Q'.$line, $transportchargeFaf);
                    $sheet->setCellValue('W'.$line,'=SUM(P'.$line.':V'.$line.')');
                    $sheet->setCellValue('X'.$line, $transportchargeRemarks);
                    $sheet->setCellValue('Y'.$line, $dtmRemarks);
                    $sheet->setCellValue('Z'.$line, $dtmNoID_PK);
                    $sheet->setCellValue('AA'.$line, $consolShow);
                    putDecimalPoints('P','X',$line,$sheet);
                    $sheet->getStyle('A'.$line.':AA'.$line)->getAlignment()->setHorizontal('center');
                    $line++;
               }
               else 
               if($costcenterID == 24   //   IPM  
               || $costcenterID == 28)  //   TMG  
               {
                    $sheet->setCellValue('A'.$line, ($line-6));
                    $sheet->setCellValue('B'.$line, $dtmPickupDate);
                    $sheet->setCellValue('C'.$line, $startIn);
                    $sheet->setCellValue('D'.$line, $endOut);
                    $sheet->setCellValue('E'.$line, $tpt);
                    $sheet->setCellValue('F'.$line, $truckPlateNo);
                    $sheet->setCellValue('G'.$line, $loadCap);
                    $sheet->setCellValue('H'.$line, $vxlDO);

                    $sheet->setCellValue('K'.$line, $dtmOriginZone);

                    $sheet->setCellValue('M'.$line, $dtmOriginPointID_FK);
                    $sheet->setCellValue('N'.$line, $dtmDestinationPointID_FK);
                    $sheet->setCellValue('O'.$line, $quantityLoad);
                    if($isConsol == 1)
                    {
                         $sheet->setCellValue('R'.$line, $transportchargeTransportCharge);
                    }
                    else
                    {
                         $sheet->setCellValue('P'.$line, $transportchargeTransportCharge);
                    }
                    $sheet->setCellValue('Q'.$line, $transportchargeFaf);
                    $sheet->setCellValue('W'.$line,'=SUM(P'.$line.':V'.$line.')');
                    $sheet->setCellValue('X'.$line, $transportchargeRemarks);
                    $sheet->setCellValue('Y'.$line, $dtmRemarks);
                    $sheet->setCellValue('Z'.$line, $dtmNoID_PK);
                    $sheet->setCellValue('AA'.$line, $consolShow);
                    putDecimalPoints('P','X',$line,$sheet);
                    $sheet->getStyle('A'.$line.':AA'.$line)->getAlignment()->setHorizontal('center');
                    $line++;
               }
               else 
               if($costcenterID == 38)  //   AIMP  
               {
                    $sheet->setCellValue('A'.$line, ($line-6));
                    $sheet->setCellValue('B'.$line, $dtmPickupDate);
                    $sheet->setCellValue('C'.$line, $truckPlateNo);
                    $sheet->setCellValue('D'.$line, $loadCap);
                    $sheet->setCellValue('E'.$line, $vxlDO);

                    $sheet->setCellValue('H'.$line, $dtmOriginZone);
                    $sheet->setCellValue('I'.$line, $dtmOriginPointID_FK);
                    $sheet->setCellValue('J'.$line, $dtmDestinationPointID_FK);
                    $sheet->setCellValue('K'.$line, $quantityLoad);

                    $sheet->setCellValue('L'.$line, $startIn);
                    $sheet->setCellValue('M'.$line, $startOut);
                    $sheet->setCellValue('N'.$line, $endIn);
                    $sheet->setCellValue('O'.$line, $endOut);
                    $sheet->setCellValue('P'.$line, $tpt);
                    if($isConsol == 1)
                    {
                         $sheet->setCellValue('U'.$line, $transportchargeTransportCharge);
                    }
                    else
                    {
                         $sheet->setCellValue('Q'.$line, $transportchargeTransportCharge);
                    }
                    $sheet->setCellValue('R'.$line, $transportchargeFaf);
                    putDecimalPoints('Q','Z',$line,$sheet);
                    $sheet->setCellValue('Z'.$line, $transportchargeRemarks);
                    $sheet->setCellValue('AA'.$line, $dtmRemarks);
                    $sheet->setCellValue('AB'.$line, $dtmNoID_PK);
                    $sheet->setCellValue('AC'.$line, $consolShow);

                    $sheet->setCellValue('Y'.$line,'=SUM(Q'.$line.':X'.$line.')');
                    $sheet->getStyle('A'.$line.':AC'.$line)->getAlignment()->setHorizontal('center');
                    $line++;
               }
               else 
               if($costcenterID == 39)  //   AEXP  
               {
                    $sheet->setCellValue('A'.$line, ($line-6));
                    $sheet->setCellValue('B'.$line, $dtmPickupDate);
                    $sheet->setCellValue('C'.$line, $truckPlateNo);
                    $sheet->setCellValue('D'.$line, $loadCap);
                    $sheet->setCellValue('E'.$line, $vxlDO);

                    $sheet->setCellValue('F'.$line, $dtmOriginPointID_FK);
                    $sheet->setCellValue('G'.$line, $dtmDestinationPointID_FK);
                    $sheet->setCellValue('H'.$line, $quantityLoad);
                    if($isConsol == 1)
                    {
                         $sheet->setCellValue('M'.$line, $transportchargeTransportCharge);
                    }
                    else
                    {
                         $sheet->setCellValue('I'.$line, $transportchargeTransportCharge);
                    }
                    $sheet->setCellValue('J'.$line, $transportchargeFaf);

                    $sheet->setCellValue('Q'.$line, $transportchargeRemarks);
                    $sheet->setCellValue('R'.$line, $dtmRemarks);
                    $sheet->setCellValue('S'.$line, $dtmNoID_PK);
                    $sheet->setCellValue('T'.$line, $consolShow);
                    putDecimalPoints('I','Q',$line,$sheet);
                    $sheet->setCellValue('P'.$line,'=SUM(I'.$line.':O'.$line.')');
                    $sheet->getStyle('A'.$line.':T'.$line)->getAlignment()->setHorizontal('center');
                    $line++;
               }
               else 
               {
                    $sheet->setCellValue('A'.$line, ($line-6));
                    $sheet->setCellValue('B'.$line, $dtmPickupDate);
                    $sheet->setCellValue('C'.$line, $truckPlateNo);
                    $sheet->setCellValue('D'.$line, $loadCap);
                    $sheet->setCellValue('E'.$line, $vxlDO);
                    $sheet->setCellValue('F'.$line, $quantityLoad);
                    if($isConsol == 1)
                    {
                         $sheet->setCellValue('K'.$line, $transportchargeTransportCharge);
                    }
                    else
                    {
                         $sheet->setCellValue('G'.$line, $transportchargeTransportCharge);
                    }
                    $sheet->setCellValue('H'.$line, $transportchargeFaf);
          
                    $sheet->setCellValue('O'.$line, $transportchargeRemarks);
                    $sheet->setCellValue('P'.$line, $dtmRemarks);
                    $sheet->setCellValue('Q'.$line, $dtmNoID_PK);
                    $sheet->setCellValue('R'.$line, $consolShow);
                    putDecimalPoints('G','O',$line,$sheet);
                    $sheet->setCellValue('N'.$line,'=SUM(G'.$line.':M'.$line.')');
                    $sheet->getStyle('A'.$line.':R'.$line)->getAlignment()->setHorizontal('center');
                    $line++;
               }
          }
          else
          if($companyID == 73) // PEN EXPRESS 
          {
               if($costcenterID == 29   //   BROADCOMM
               || $costcenterID == 30   //   LOCAL  
               || $costcenterID == 31)  //   TRANSFER
               {
                    $sheet->setCellValue('A'.$line, ($line-6));
                    $sheet->setCellValue('B'.$line, $dtmPickupDate);
                    $sheet->setCellValue('C'.$line, $truckPlateNo);
                    $sheet->setCellValue('D'.$line, $loadCap);
                    $sheet->setCellValue('E'.$line, $vxlDO);
                    $sheet->setCellValue('F'.$line, $dtmOriginPointID_FK);
                    $sheet->setCellValue('G'.$line, $dtmDestinationPointID_FK);

                    $sheet->setCellValue('I'.$line, $startIn);
                    $sheet->setCellValue('J'.$line, $startOut);
                    $sheet->setCellValue('K'.$line, $tpt);
                    if($isConsol == 1)
                    {
                         $sheet->setCellValue('P'.$line, $transportchargeTransportCharge);
                         $sheet->setCellValue('O'.$line, 1);
                    }
                    else
                    {
                         $sheet->setCellValue('L'.$line, $transportchargeTransportCharge);
                    }
                    
                    $sheet->setCellValue('V'.$line, $transportchargeRemarks);
                    $sheet->setCellValue('W'.$line, $dtmRemarks);
                    $sheet->setCellValue('X'.$line, $dtmNoID_PK);
                    $sheet->setCellValue('Y'.$line, $consolShow);

                    $sheet->setCellValue('U'.$line,'=SUM(L'.$line.'+N'.$line.'+P'.$line.'+Q'.$line.'+R'.$line.'+S'.$line.'+T'.$line.')');
                    $sheet->getStyle('A'.$line.':Z'.$line)->getAlignment()->setHorizontal('center');
                    putDecimalPoints('L','M',$line,$sheet);
                    putDecimalPoints('N','O',$line,$sheet);
                    putDecimalPoints('P','Q',$line,$sheet);
                    putDecimalPoints('Q','V',$line,$sheet);
                    $line++;
               }
               else 
               {
                    $sheet->setCellValue('A'.$line, ($line-6));
                    $sheet->setCellValue('B'.$line, $dtmPickupDate);
                    $sheet->setCellValue('C'.$line, $truckPlateNo);
                    $sheet->setCellValue('D'.$line, $loadCap);
                    $sheet->setCellValue('E'.$line, $vxlDO);
                    $sheet->setCellValue('F'.$line, $quantityLoad);
                    if($isConsol == 1)
                    {
                         $sheet->setCellValue('K'.$line, $transportchargeTransportCharge);
                    }
                    else
                    {
                         $sheet->setCellValue('G'.$line, $transportchargeTransportCharge);
                    }
                    $sheet->setCellValue('H'.$line, $transportchargeFaf);
          
                    $sheet->setCellValue('O'.$line, $transportchargeRemarks);
                    $sheet->setCellValue('P'.$line, $dtmRemarks);
                    $sheet->setCellValue('Q'.$line, $dtmNoID_PK);
                    $sheet->setCellValue('R'.$line, $consolShow);
                    putDecimalPoints('G','O',$line,$sheet);
                    $sheet->setCellValue('N'.$line,'=SUM(G'.$line.':M'.$line.')');
                    $sheet->getStyle('A'.$line.':R'.$line)->getAlignment()->setHorizontal('center');
                    $line++;
               }
          }
          else
          if($companyID == 62) // EXPEDITORS
          {
               if($costcenterID == 34    //   AIR EXPORT  
               || $costcenterID == 35)   //   AIR IMPORT  
               {
                    $sheet->setCellValue('A'.$line, ($line-6));
                    $sheet->setCellValue('B'.$line, $dtmPickupDate);
                    $sheet->setCellValue('D'.$line, $vxlDO);
                    $sheet->setCellValue('F'.$line, $truckPlateNo);
                    $sheet->setCellValue('G'.$line, $loadCap);
                    if($isConsol == 1)
                    {
                         $sheet->setCellValue('I'.$line, $transportchargeTransportCharge);
                    }
                    else
                    {
                         $sheet->setCellValue('H'.$line, $transportchargeTransportCharge);
                    }

                    $sheet->setCellValue('M'.$line, $transportchargeFaf);

                    $sheet->setCellValue('Q'.$line, $transportchargeRemarks);
                    $sheet->setCellValue('R'.$line, $dtmRemarks);
                    $sheet->setCellValue('S'.$line, $dtmNoID_PK);
                    $sheet->setCellValue('T'.$line, $consolShow);

                    $sheet->setCellValue('P'.$line,'=SUM(H'.$line.':O'.$line.')');
                    $sheet->getStyle('A'.$line.':T'.$line)->getAlignment()->setHorizontal('center');
                    putDecimalPoints('H','Q',$line,$sheet);
                    $line++;
               }
               else 
               {
                    $sheet->setCellValue('A'.$line, ($line-6));
                    $sheet->setCellValue('B'.$line, $dtmPickupDate);
                    $sheet->setCellValue('C'.$line, $truckPlateNo);
                    $sheet->setCellValue('D'.$line, $loadCap);
                    $sheet->setCellValue('E'.$line, $vxlDO);
                    $sheet->setCellValue('F'.$line, $quantityLoad);
                    if($isConsol == 1)
                    {
                         $sheet->setCellValue('K'.$line, $transportchargeTransportCharge);
                    }
                    else
                    {
                         $sheet->setCellValue('G'.$line, $transportchargeTransportCharge);
                    }
                    $sheet->setCellValue('H'.$line, $transportchargeFaf);
          
                    $sheet->setCellValue('O'.$line, $transportchargeRemarks);
                    $sheet->setCellValue('P'.$line, $dtmRemarks);
                    $sheet->setCellValue('Q'.$line, $dtmNoID_PK);
                    $sheet->setCellValue('R'.$line, $consolShow);
                    putDecimalPoints('G','O',$line,$sheet);
                    $sheet->setCellValue('N'.$line,'=SUM(G'.$line.':M'.$line.')');
                    $sheet->getStyle('A'.$line.':R'.$line)->getAlignment()->setHorizontal('center');
                    $line++;
               }
          }
          else
          if($companyID == 79) // UPS
          {
               if($costcenterID == 32    //   PICKUP  
               || $costcenterID == 33)   //   DELIVERY 
               {
                    $sheet->setCellValue('A'.$line, ($line-6));
                    $sheet->setCellValue('B'.$line, $dtmPickupDate);
                    $sheet->setCellValue('C'.$line, $truckPlateNo);
                    $sheet->setCellValue('D'.$line, $loadCap);
                    $sheet->setCellValue('E'.$line, $vxlDO);
                    $sheet->setCellValue('F'.$line, $dtmOriginPointID_FK);
                    $sheet->setCellValue('G'.$line, $dtmOriginZone);
                    $sheet->setCellValue('H'.$line, $dtmDestinationPointID_FK);
                    $sheet->setCellValue('I'.$line, $dtmDestinationZone);
                    $sheet->setCellValue('J'.$line, $quantityLoad);
                    if($isConsol == 1)
                    {
                         $sheet->setCellValue('M'.$line, $transportchargeTransportCharge);
                    }
                    else
                    {
                         $sheet->setCellValue('K'.$line, $transportchargeTransportCharge);
                    }
                    $sheet->setCellValue('L'.$line, $transportchargeFaf);

                    $sheet->setCellValue('Q'.$line, $transportchargeRemarks);
                    $sheet->setCellValue('R'.$line, $dtmRemarks);
                    $sheet->setCellValue('S'.$line, $dtmNoID_PK);
                    $sheet->setCellValue('T'.$line, $consolShow);

                    $sheet->setCellValue('P'.$line,'=SUM(K'.$line.':O'.$line.')');
                    $sheet->getStyle('A'.$line.':T'.$line)->getAlignment()->setHorizontal('center');
                    putDecimalPoints('K','Q',$line,$sheet);
                    $line++;
               }
               else 
               {
                    $sheet->setCellValue('A'.$line, ($line-6));
                    $sheet->setCellValue('B'.$line, $dtmPickupDate);
                    $sheet->setCellValue('C'.$line, $truckPlateNo);
                    $sheet->setCellValue('D'.$line, $loadCap);
                    $sheet->setCellValue('E'.$line, $vxlDO);
                    $sheet->setCellValue('F'.$line, $quantityLoad);
                    if($isConsol == 1)
                    {
                         $sheet->setCellValue('K'.$line, $transportchargeTransportCharge);
                    }
                    else
                    {
                         $sheet->setCellValue('G'.$line, $transportchargeTransportCharge);
                    }
                    $sheet->setCellValue('H'.$line, $transportchargeFaf);
          
                    $sheet->setCellValue('O'.$line, $transportchargeRemarks);
                    $sheet->setCellValue('P'.$line, $dtmRemarks);
                    $sheet->setCellValue('Q'.$line, $dtmNoID_PK);
                    $sheet->setCellValue('R'.$line, $consolShow);
                    putDecimalPoints('G','O',$line,$sheet);
                    $sheet->setCellValue('N'.$line,'=SUM(G'.$line.':M'.$line.')');
                    $sheet->getStyle('A'.$line.':R'.$line)->getAlignment()->setHorizontal('center');
                    $line++;
               }
          }
          else
          if($companyID == 70) // KOBE TECHNOLOGY
          {
               if($costcenterID == 36    //   INBOUND  
               || $costcenterID == 37)   //   OUTBOUND 
               {
                    $sheet->setCellValue('A'.$line, $dtmPickupDate);
                    $sheet->setCellValue('C'.$line, $quantityLoad);
                    $sheet->setCellValue('D'.$line, $loadCap);
                    if($isConsol == 1)
                    {
                         $sheet->setCellValue('F'.$line, $transportchargeTransportCharge);
                    }
                    else
                    {
                         $sheet->setCellValue('E'.$line, $transportchargeTransportCharge);
                    }
                    $sheet->setCellValue('G'.$line, $vxlDO);
                    $sheet->setCellValue('H'.$line, $dtmOriginPointID_FK);
                    $sheet->setCellValue('I'.$line, $dtmDestinationPointID_FK);
                    $sheet->setCellValue('L'.$line,'=SUM(E'.$line.'+F'.$line.'+K'.$line.')');

                    $sheet->setCellValue('O'.$line, $transportchargeRemarks);
                    $sheet->setCellValue('P'.$line, $dtmRemarks);
                    $sheet->setCellValue('Q'.$line, $dtmNoID_PK);
                    $sheet->setCellValue('R'.$line, $consolShow);

                    $sheet->getStyle('A'.$line.':R'.$line)->getAlignment()->setHorizontal('center');
                    putDecimalPoints('E','G',$line,$sheet);
                    putDecimalPoints('K','M',$line,$sheet);
                    $line++;
               }
               else 
               {
                    $sheet->setCellValue('A'.$line, ($line-6));
                    $sheet->setCellValue('B'.$line, $dtmPickupDate);
                    $sheet->setCellValue('C'.$line, $truckPlateNo);
                    $sheet->setCellValue('D'.$line, $loadCap);
                    $sheet->setCellValue('E'.$line, $vxlDO);
                    $sheet->setCellValue('F'.$line, $quantityLoad);
                    if($isConsol == 1)
                    {
                         $sheet->setCellValue('K'.$line, $transportchargeTransportCharge);
                    }
                    else
                    {
                         $sheet->setCellValue('G'.$line, $transportchargeTransportCharge);
                    }
                    $sheet->setCellValue('H'.$line, $transportchargeFaf);
          
                    $sheet->setCellValue('O'.$line, $transportchargeRemarks);
                    $sheet->setCellValue('P'.$line, $dtmRemarks);
                    $sheet->setCellValue('Q'.$line, $dtmNoID_PK);
                    $sheet->setCellValue('R'.$line, $consolShow);
                    putDecimalPoints('G','O',$line,$sheet);
                    $sheet->setCellValue('N'.$line,'=SUM(G'.$line.':M'.$line.')');
                    $sheet->getStyle('A'.$line.':R'.$line)->getAlignment()->setHorizontal('center');
                    $line++;
               }
          }
          else
          if($companyID == 55      // DHL DGF
          || $companyID == 63)     // EzyHaul
          {
               $sheet->setCellValue('A'.$line, ($line-6));
               $sheet->setCellValue('B'.$line, $dtmPickupDate);
               $sheet->setCellValue('C'.$line, $dtmPickupTime1);
               $sheet->setCellValue('D'.$line, $dtmDGF);
               $sheet->setCellValue('E'.$line, $dtmUrgentMemo);
               $sheet->setCellValue('F'.$line, $dtmRequestBy);
               $sheet->setCellValue('G'.$line, $dtmOriginPointID_FK);
               $sheet->setCellValue('H'.$line, $dtmOriginZone);
               $sheet->setCellValue('I'.$line, $dtmDestinationPointID_FK);
               $sheet->setCellValue('J'.$line, $dtmDestinationZone);

               $sheet->setCellValue('K'.$line, $lockupf);
               $sheet->setCellValue('L'.$line, $startIn1);
               $sheet->setCellValue('M'.$line, $lockupdockTime);
               $sheet->setCellValue('N'.$line, $timedockTime1);
               $sheet->setCellValue('O'.$line, $lockupcompleteLoadingTime);
               $sheet->setCellValue('P'.$line, $timecompleteLoadingTime1);
               $sheet->setCellValue('Q'.$line, "");
               $sheet->setCellValue('R'.$line, "");
               $sheet->setCellValue('S'.$line, $lockupdepartureTimeFromPost);
               $sheet->setCellValue('T'.$line, $startOut1);
               $sheet->setCellValue('U'.$line, $lockuparrivalTimeAtPost);
               $sheet->setCellValue('V'.$line, $endIn1);
               $sheet->setCellValue('W'.$line, "");
               $sheet->setCellValue('X'.$line, "");
               $sheet->setCellValue('Y'.$line, $lockupdestinationDockTime);
               $sheet->setCellValue('Z'.$line, $timedestinationDockTime1);
               $sheet->setCellValue('AA'.$line, $lockupcompleteUnloadTime);
               $sheet->setCellValue('AB'.$line, $timecompleteUnloadTime1);
               $sheet->setCellValue('AC'.$line, $lockupdepartTimeFromPost);
               $sheet->setCellValue('AD'.$line, $endOut1);

               $sheet->setCellValue('AE'.$line, $cartonQuantity);
               $sheet->setCellValue('AF'.$line, $palletsQuantity);
               $sheet->setCellValue('AG'.$line, $cagesQuantity);

               $sheet->setCellValue('AH'.$line, $truckPlateNo);
               $sheet->setCellValue('AI'.$line, $loadCap);
               $sheet->setCellValue('AK'.$line, $transportchargeTransportCharge);
               $sheet->setCellValue('AL'.$line, $transportchargeFaf);
               $sheet->setCellValue('AM'.$line, 0);

               $sheet->setCellValue('AP'.$line, $transportchargeRemarks);
               $sheet->setCellValue('AQ'.$line, $dtmRemarks);
               $sheet->setCellValue('AR'.$line, $dtmNoID_PK);
               $sheet->setCellValue('AS'.$line, $consolShow);

               putWidth('K','AE',15,$sheet);
               putDecimalPoints('AK','AP',$line,$sheet);
               putWidth('AK','AT',20,$sheet);
               // $sheet->getColumnDimension('AK')->setWidth(20);
               // $sheet->getColumnDimension('AL')->setWidth(20);
               // $sheet->getColumnDimension('AM')->setWidth(20);
               // $sheet->getColumnDimension('AN')->setWidth(20);
               // $sheet->getColumnDimension('AO')->setWidth(20);
               // $sheet->getColumnDimension('AP')->setWidth(20);
               // $sheet->getColumnDimension('AP')->setWidth(20);
               // $sheet->getColumnDimension('AP')->setWidth(20);
               // $sheet->getColumnDimension('AP')->setWidth(20);
               $sheet->setCellValue('AN'.$line,'=SUM(AK'.$line.':AL'.$line.')');
               $sheet->setCellValue('AO'.$line,'=SUM(AK'.$line.':AM'.$line.')');
               $sheet->getStyle('A'.$line.':AS'.$line)->getAlignment()->setHorizontal('center');
               $line++;
          }
          else 
          {
               $sheet->setCellValue('A'.$line, ($line-6));
               $sheet->setCellValue('B'.$line, $dtmPickupDate);
               $sheet->setCellValue('C'.$line, $truckPlateNo);
               $sheet->setCellValue('D'.$line, $loadCap);
               $sheet->setCellValue('E'.$line, $vxlDO);
               $sheet->setCellValue('F'.$line, $quantityLoad);
               if($isConsol == 1)
               {
                    $sheet->setCellValue('K'.$line, $transportchargeTransportCharge);
               }
               else
               {
                    $sheet->setCellValue('G'.$line, $transportchargeTransportCharge);
               }
               $sheet->setCellValue('H'.$line, $transportchargeFaf);
     
               $sheet->setCellValue('O'.$line, $transportchargeRemarks);
               $sheet->setCellValue('P'.$line, $dtmRemarks);
               $sheet->setCellValue('Q'.$line, $dtmNoID_PK);
               $sheet->setCellValue('R'.$line, $consolShow);
               putDecimalPoints('G','O',$line,$sheet);
               $sheet->setCellValue('N'.$line,'=SUM(G'.$line.':M'.$line.')');
               $sheet->getStyle('A'.$line.':R'.$line)->getAlignment()->setHorizontal('center');
               $line++;
          }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////END CONTENT FOR TEMPLATE/////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




     }
}
else 
{
    $sheet->setCellValue('A1','No data matches');
    $sheet->setCellValue('A2','Please Try Again');
    $sheet->setCellValue('A3','Using Correct Info');
}
if($counterNo > 0)
{
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////FOOTER FOR TEMPLATE/////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


     if($companyID == 43) // SCHENKER 
     {
          if($costcenterID == 19   //   AEG
          || $costcenterID == 20   //   AIG  
          || $costcenterID == 21   //   AED
          || $costcenterID == 24   //   IPM  
          || $costcenterID == 28)  //   TMG
          {
               $sheet->setCellValue('A'.$line, "GRAND TOTAL");
               $sheet->mergeCells('A'.$line.':O'.$line);
               $sheet->setCellValue('P'.$line,'=SUM(P7:P'.($line-1).')');
               $sheet->setCellValue('Q'.$line,'=SUM(Q7:Q'.($line-1).')');
               $sheet->setCellValue('R'.$line,'=SUM(R7:R'.($line-1).')');
               $sheet->setCellValue('S'.$line,'=SUM(S7:S'.($line-1).')');
               $sheet->setCellValue('T'.$line,'=SUM(T7:T'.($line-1).')');
               $sheet->setCellValue('U'.$line,'=SUM(U7:U'.($line-1).')');
               $sheet->setCellValue('V'.$line,'=SUM(V7:V'.($line-1).')');
               $sheet->setCellValue('W'.$line,'=SUM(W7:W'.($line-1).')');
               putDecimalPoints('P','X',$line,$sheet);
               putBorderStyleRow($sheet,'A'.$line.':O'.$line,2,'F0E68C');
               $sheet->getStyle('P'.$line.':W'.$line)->getAlignment()->setHorizontal('center');
          }
          else 
          if($costcenterID == 38)  //   AIMP  
          {
               $sheet->setCellValue('A'.$line, "GRAND TOTAL");
               $sheet->mergeCells('A'.$line.':P'.$line);
               putBorderStyleRow($sheet,'A'.$line.':P'.$line,2,'F0E68C');
               $sheet->setCellValue('Q'.$line,'=SUM(Q7:Q'.($line-1).')');
               $sheet->setCellValue('R'.$line,'=SUM(R7:R'.($line-1).')');
               $sheet->setCellValue('S'.$line,'=SUM(S7:S'.($line-1).')');
               $sheet->setCellValue('T'.$line,'=SUM(T7:T'.($line-1).')');
               $sheet->setCellValue('U'.$line,'=SUM(U7:U'.($line-1).')');
               $sheet->setCellValue('V'.$line,'=SUM(V7:V'.($line-1).')');
               $sheet->setCellValue('W'.$line,'=SUM(W7:W'.($line-1).')');
               $sheet->setCellValue('X'.$line,'=SUM(X7:X'.($line-1).')');
               $sheet->setCellValue('Y'.$line,'=SUM(Y7:Y'.($line-1).')');
               putDecimalPoints('Q','Z',$line,$sheet);
               $sheet->getStyle('Q'.$line.':Y'.$line)->getAlignment()->setHorizontal('center');
          }
          else 
          if($costcenterID == 39)  //   AEXP  
          {
               $sheet->setCellValue('A'.$line, "GRAND TOTAL");
               $sheet->mergeCells('A'.$line.':H'.$line);
               $sheet->setCellValue('I'.$line,'=SUM(I7:I'.($line-1).')');
               $sheet->setCellValue('J'.$line,'=SUM(J7:J'.($line-1).')');
               $sheet->setCellValue('K'.$line,'=SUM(K7:K'.($line-1).')');
               $sheet->setCellValue('L'.$line,'=SUM(L7:L'.($line-1).')');
               $sheet->setCellValue('M'.$line,'=SUM(M7:M'.($line-1).')');
               $sheet->setCellValue('N'.$line,'=SUM(N7:N'.($line-1).')');
               $sheet->setCellValue('O'.$line,'=SUM(O7:O'.($line-1).')');
               $sheet->setCellValue('P'.$line,'=SUM(P7:P'.($line-1).')');
               putDecimalPoints('I','Q',$line,$sheet);
               $sheet->getStyle('I'.$line.':P'.$line)->getAlignment()->setHorizontal('center');
          }
          else 
          {
               normalTemplateBottom($sheet,$line);
          }
     }
     else
     if($companyID == 73) // PEN EXPRESS 
     {
          if($costcenterID == 29   //   BROADCOMM
          || $costcenterID == 30   //   LOCAL  
          || $costcenterID == 31)  //   TRANSFER
          {
               $sheet->setCellValue('A'.$line, "TOTAL AMOUNT");
               $sheet->mergeCells('A'.$line.':K'.$line);
               $sheet->setCellValue('L'.$line,'=SUM(L7:L'.($line-1).')');
               $sheet->setCellValue('N'.$line,'=SUM(N7:N'.($line-1).')');
               $sheet->setCellValue('P'.$line,'=SUM(P7:P'.($line-1).')');
               $sheet->setCellValue('Q'.$line,'=SUM(Q7:Q'.($line-1).')');
               $sheet->setCellValue('R'.$line,'=SUM(R7:R'.($line-1).')');
               $sheet->setCellValue('S'.$line,'=SUM(S7:S'.($line-1).')');
               $sheet->setCellValue('T'.$line,'=SUM(T7:T'.($line-1).')');
               $sheet->setCellValue('U'.$line,'=SUM(U7:U'.($line-1).')');
               putDecimalPoints('L','M',$line,$sheet);
               putDecimalPoints('N','O',$line,$sheet);
               putDecimalPoints('P','Q',$line,$sheet);
               putDecimalPoints('Q','V',$line,$sheet);
               $sheet->getStyle('L'.$line.':U'.$line)->getAlignment()->setHorizontal('center');
          }
          else 
          {
               normalTemplateBottom($sheet,$line);
          }
     }
     else
     if($companyID == 62) // EXPEDITORS
     {
          if($costcenterID == 34    //   AIR EXPORT  
          || $costcenterID == 35)   //   AIR IMPORT  
          {
               $sheet->setCellValue('A'.$line, "TOTAL ");
               $sheet->mergeCells('A'.$line.':G'.$line);
               $sheet->setCellValue('H'.$line,'=SUM(H7:H'.($line-1).')');
               $sheet->setCellValue('I'.$line,'=SUM(I7:I'.($line-1).')');
               $sheet->setCellValue('J'.$line,'=SUM(J7:J'.($line-1).')');
               $sheet->setCellValue('K'.$line,'=SUM(K7:K'.($line-1).')');
               $sheet->setCellValue('L'.$line,'=SUM(L7:L'.($line-1).')');
               $sheet->setCellValue('M'.$line,'=SUM(M7:M'.($line-1).')');
               $sheet->setCellValue('N'.$line,'=SUM(N7:N'.($line-1).')');
               $sheet->setCellValue('O'.$line,'=SUM(O7:O'.($line-1).')');
               $sheet->setCellValue('P'.$line,'=SUM(P7:P'.($line-1).')');
               putDecimalPoints('H','Q',$line,$sheet);
               $sheet->getStyle('H'.$line.':P'.$line)->getAlignment()->setHorizontal('center');
          }
          else 
          {
               normalTemplateBottom($sheet,$line);
          }
     }
     else
     if($companyID == 79) // UPS
     {
          if($costcenterID == 32    //   PICKUP  
          || $costcenterID == 33)   //   DELIVERY 
          {
               $sheet->setCellValue('A'.$line, "TOTAL AMOUNT");
               $sheet->mergeCells('A'.$line.':J'.$line);
               $sheet->setCellValue('K'.$line,'=SUM(K7:K'.($line-1).')');
               $sheet->setCellValue('L'.$line,'=SUM(L7:L'.($line-1).')');
               $sheet->setCellValue('M'.$line,'=SUM(M7:M'.($line-1).')');
               $sheet->setCellValue('N'.$line,'=SUM(N7:N'.($line-1).')');
               $sheet->setCellValue('O'.$line,'=SUM(O7:O'.($line-1).')');
               $sheet->setCellValue('P'.$line,'=SUM(P7:P'.($line-1).')');
               putDecimalPoints('K','Q',$line,$sheet);
               $sheet->getStyle('K'.$line.':P'.$line)->getAlignment()->setHorizontal('center');
          }
          else 
          {
               normalTemplateBottom($sheet,$line);
          }
     }
     else
     if($companyID == 70) // KOBE TECHNOLOGY
     {
          if($costcenterID == 36    //   INBOUND  
          || $costcenterID == 37)   //   OUTBOUND 
          {
               $sheet->setCellValue('B'.$line, "TOTAL");

               $sheet->setCellValue('E'.$line,'=SUM(E7:E'.($line-1).')');
               $sheet->setCellValue('F'.$line,'=SUM(F7:F'.($line-1).')');
               $sheet->setCellValue('K'.$line,'=SUM(K7:K'.($line-1).')');
               $sheet->getStyle('A'.$line.':N'.$line)->getAlignment()->setHorizontal('center');

               $sheet->setCellValue('B'.($line+1), "SUB TOTAL");
               $sheet->setCellValue('L'.($line+1),'=SUM(E'.$line.' + F'.$line.' + K'.$line.')');
               $sheet->getStyle('A'.($line+1).':N'.($line+1))->getAlignment()->setHorizontal('center');
               $sheet->getStyle('A'.$line.':N'.$line)->getAlignment()->setHorizontal('center');
               putBorderStyleRow($sheet,'A'.$line.':N'.$line,2,'FFFFFF');
               putBorderStyleRow($sheet,'A'.($line+1).':N'.($line+1),2,'FFFFFF');
               putDecimalPoints('E','G',$line,$sheet);
               putDecimalPoints('K','M',$line,$sheet);
          }
          else 
          {
               normalTemplateBottom($sheet,$line);
          }
     }
     else
     if($companyID == 55      // DHL DGF
     || $companyID == 63)     // EzyHaul
     {
          $sheet->setCellValue('A'.$line,'TOTALS');
          $sheet->getStyle('A'.$line)->getAlignment()->setHorizontal('left');
          putBorderStyleRow($sheet,'A'.$line.':AL'.$line,2,'F0E68C');
          $sheet->mergeCells('A'.$line.':AI'.$line);
          $sheet->setCellValue('AM'.$line,'=SUM(AM7:AM'.($line-1).')');
          $sheet->setCellValue('AN'.$line,'=SUM(AN7:AN'.($line-1).')');
          $sheet->setCellValue('AO'.$line,'=SUM(AO7:AO'.($line-1).')');
          putDecimalPoints('AM','AP',$line,$sheet);
          $sheet->getStyle('AM'.$line.':AO'.$line)->getAlignment()->setHorizontal('center');
     }
     else 
     {
          normalTemplateBottom($sheet,$line);
     }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////END FOOTER FOR TEMPLATE/////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
else 
{
     $sheet->setCellValue('A1','No data matches');
     $sheet->setCellValue('A2','Please Try Again');
     $sheet->setCellValue('A3','Using Correct Info');
}




$writer = new Xlsx($spreadsheet);
$writer->save('php://output');
?>