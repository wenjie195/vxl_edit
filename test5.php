<?php

require 'generalFunction.php';
session_start();
if(isset($_POST['fromPage']))
{    
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-12 adminAlignThings">
            <?php if($_SESSION['userLevel'] == 2){?>
            <form action="dtmAdd.php" method="POST">
                <button class="btn btn-success" value="<?php echo $_POST['fromPage'];?>" name="add">
                    Add DTM
                </button>
            </form>
            <?php }?>
            <form action="dtmPrint.php" method="POST" class="ml-2">
                <button class="btn btn-success">
                    Print DTM
                </button>
            </form>
            <p class="paginationClass">
                Page : 
                <select onchange="checkCondition(this.value,null,null,0,<?php echo $_POST['fromPage'];?>);" id="pagination<?php echo $_POST['fromPage']; ?>"></select> 
                of 
            </p>
            <p class="paginationClass paginationClassTotal " id="totalpages<?php echo $_POST['fromPage']; ?>"></p>
            <p class="filterPara dtmFilterPara" style="margin-left: 86px;">Filter By: </p>
            <div class="adminAlignRight">  
                <select class="filterClass" id="filter<?php echo $_POST['fromPage']; ?>" onchange="checkCondition(null,this.value,null,0,<?php echo $_POST['fromPage'];?>);">
                    <option selected disabled>Filter By</option>
                    <option value="1">Unfinished</option>
                    <option value="2">DTM No(Newest)</option>
                    <option value="3">Agent</option>
                    <option value="4">Truck No</option>
                    <option value="5">Pickup Date(Newest)</option>
                    <option value="6">Booking Date(Newest)</option>
                    <option value="7">T`king Coded</option>
                    <option value="8">Finished</option>
                    <option value="9">VXL D/O Number</option>
                    <option value="100">Show All</option>
                </select>
                <input type="text" id="search<?php echo $_POST['fromPage']; ?>" class="searchClass" placeholder="Search Keyword">
                <button class="searchClass searchClassButton btn btn-primary " onclick="checkCondition(null,null,this.value,0,<?php echo $_POST['fromPage'];?>);">Search</button>
            </div>
        </div>
    </div>
    </div>
<div style="overflow-x:auto;" id="getTable<?php echo $_POST['fromPage'];?>"></div>
<?php
}
?>